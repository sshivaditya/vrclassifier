﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs>
struct Action_1_t61B82ED6C34AFA284483A0FB8F1E0D07BA0C266E;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs>
struct Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs>
struct Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs>
struct Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs>
struct Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261;
// System.Action`1<UnityEngine.XR.Interaction.Toolkit.LocomotionSystem>
struct Action_1_t84833279C5597E1E0FE303CF3065416470244C93;
// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.AxisEventData>
struct Action_2_t2E1567A92AA3BBF15C7916601A0CA9FF174E10A8;
// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData>
struct Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3;
// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData>
struct Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB;
// System.Action`2<UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>>
struct Action_2_tF7F5D29D757EA874F339B7A237256E126BFEECB0;
// System.Comparison`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct Comparison_1_t816BE556819540BA6AFE8FBB13F995AEED590F30;
// System.Collections.Generic.Dictionary`2<UnityEngine.Collider,UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractable,System.ValueTuple`2<UnityEngine.MeshFilter,UnityEngine.Renderer>[]>
struct Dictionary_2_t879F533E50F5919795E89088ADF39BA7A56A4256;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractable,System.Single>
struct Dictionary_2_t86132C0F8B31101EE4D1A23B71484DB7D3AD6F35;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractor,UnityEngine.GameObject>
struct Dictionary_2_tA69FF83EA17DA0267E4A9ADE6AFF4334D537FBD8;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable,UnityEngine.Pose>
struct Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor,UnityEngine.Pose>
struct Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData>
struct Dictionary_2_tB97027AE691647A2073D7680E86BE6AB876F1CBF;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Type>
struct Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.InputFeatureUsage`1<System.Boolean>>
struct Dictionary_2_t62EEA22F57F8BEB4675F7726F2A7105ABCD711D5;
// System.Collections.Generic.HashSet`1<UnityEngine.Collider>
struct HashSet_1_tE4535201ED12CB585DB82A688DBE933550FDFDCA;
// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractable>
struct HashSet_1_tF6BC2B8B0620E4DF0BEEE501FCEC44042347B107;
// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F;
// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable>
struct HashSet_1_tDFB5CED1A4E6F4596865F0D1796182482075030E;
// System.Collections.Generic.List`1<System.Type[]>
struct List_1_t6AF577A6E10586BF9BFEFA6182485EDDD8935C0E;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractable>
struct List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractor>
struct List_1_t8F5833796A8407AFD49E7C1864E6E76653D3F2C4;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractor>
struct List_1_tCAFBFD76706B54DB653FF3DD6594D969F208A84E;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable>
struct List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor>
struct List_1_tCCED540317FA295CA1BAEBCCD4362DE7EFFB64FD;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.InputActionAsset>
struct List_1_tA032B571296F8928BB10DB71CA24B00DB5DBF315;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F;
// System.Collections.Generic.List`1<UnityEngine.MeshFilter>
struct List_1_tF4FF55D8DD6EFED1BBCBF60B3D5905B0C1CA6C8E;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t16A48BE6E71AEE33E12B53A47FDF4F51B5D9AE8D;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseController>
struct List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable>
struct List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor>
struct List_1_tEF7E6AA6FF2A58B22A1943486BE24649DC3CC2AC;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem>
struct List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRInteractionManager>
struct List_1_t19B2C24A261798D687174CD32FE67952C7E70DD7;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData>
struct List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor>
struct List_1_tBA68AA99B3AE6EFB57B37FF744D7E22CFE9F81BE;
// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch>
struct List_1_tF908382B3F9A3360582C830544139A8160622A6C;
// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager/RegistrationList`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable>
struct RegistrationList_1_t6F5496053891E363EC0159ED3ADE4170E39DEA22;
// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager/RegistrationList`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractor>
struct RegistrationList_1_t4D52D9B0E80F106AA327DDFA7DCDBCA8F4051098;
// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>[]
struct InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D;
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.InputSystem.InputControl[]
struct InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo[]
struct ButtonInfoU5BU5D_tCBEFC9095F25F60B577E75A16241D9BA65E464E4;
// UnityEngine.XR.Interaction.Toolkit.ActivateEvent
struct ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.InputSystem.Controls.AxisControl
struct AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876;
// UnityEngine.InputSystem.Controls.ButtonControl
struct ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E;
// UnityEngine.XR.Interaction.Toolkit.DeactivateEvent
struct DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// UnityEngine.XR.Interaction.Toolkit.HoverEnterEvent
struct HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6;
// UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs
struct HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9;
// UnityEngine.XR.Interaction.Toolkit.HoverExitEvent
struct HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341;
// UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs
struct HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.XR.Interaction.Toolkit.UI.IUIInteractor
struct IUIInteractor_t7746EA01BE60ABAE9161AC5F71D6793C6E6A1CE3;
// UnityEngine.XR.Interaction.Toolkit.IXRInteractable
struct IXRInteractable_t55C14481D43774E6E55842D0E9FACD38350FC2E8;
// UnityEngine.XR.Interaction.Toolkit.IXRInteractor
struct IXRInteractor_t5C1C6907DC8A4085FDEF3DCADEBACEB00B1EA27A;
// UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable
struct IXRSelectInteractable_t6DEF4FD20BE3330B8877118E4ED4EF88320AC930;
// UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor
struct IXRSelectInteractor_t48BDDD33F735BFB31B95990F96FA44A8B2C00E1A;
// UnityEngine.InputSystem.InputAction
struct InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B;
// UnityEngine.InputSystem.InputActionReference
struct InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A;
// UnityEngine.InputSystem.InputDevice
struct InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154;
// UnityEngine.InputSystem.Controls.IntegerControl
struct IntegerControl_t7A27284DD4A3D30C25AA43717A80A6CCB30E26A8;
// UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs
struct InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F;
// UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs
struct InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D;
// UnityEngine.XR.Interaction.Toolkit.InteractionLayerSettings
struct InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600;
// UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs
struct InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE;
// UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs
struct InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Light
struct Light_tA2F349FE839781469A0344CF6039B51512394275;
// UnityEngine.XR.Interaction.Toolkit.LocomotionProvider
struct LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448;
// UnityEngine.XR.Interaction.Toolkit.LocomotionSystem
struct LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// PausePlane
struct PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.InputSystem.Controls.QuaternionControl
struct QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.XR.Interaction.Toolkit.SelectEnterEvent
struct SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918;
// UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs
struct SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE;
// UnityEngine.XR.Interaction.Toolkit.SelectExitEvent
struct SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8;
// UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs
struct SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E;
// System.String
struct String_t;
// UnityEngine.XR.Interaction.Toolkit.TeleportationProvider
struct TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B;
// UnityEngine.XR.Interaction.Toolkit.TeleportingEvent
struct TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC;
// UnityEngine.XR.Interaction.Toolkit.TeleportingEventArgs
struct TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor
struct TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.InputSystem.Controls.Vector2Control
struct Vector2Control_t342BA848221108F8818F05BF3CB484934F582935;
// UnityEngine.InputSystem.Controls.Vector3Control
struct Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable
struct XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF;
// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent
struct XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD;
// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager
struct XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4;
// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent
struct XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98;
// Unity.XR.CoreUtils.XROrigin
struct XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D;
// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController
struct XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49;
// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMD
struct XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540;
// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/ScriptEventDelegate
struct ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF;
// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitComparer
struct RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16;
// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment
struct RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D;
// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitComparer
struct RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tD0E606FEBD3AF2B94DEE1193A20FCA9C0948302E 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tC73534E9AD663B095CF9C60F9C749B6802A17739 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_t69A12ACC68B26CBF93242393D9998A3A6AF09ABF 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_t64969A942955B3EDA5706F8A4B9775DA4D443F6C 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tDC2EA6706419814CC03038FF021EE1F939403A8A 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032 
{
public:

public:
};


// System.Object


// UnityEngine.XR.Interaction.Toolkit.AR.ARAnnotationInteractable
struct ARAnnotationInteractable_t038AE6C4FE0CEAB32D7D6E7EE69E43308C9182F8  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARBaseGestureInteractable
struct ARBaseGestureInteractable_t23E292D8BF7761CD567A2A14BD5B73F17626C64E  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARGestureInteractor
struct ARGestureInteractor_t8162B91DB362EA8E9F16CCB718A04E2D46F5CAAA  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARPlacementInteractable
struct ARPlacementInteractable_t9FC365819011A6B5B7D29994DA736C3FE7038E9F  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARRotationInteractable
struct ARRotationInteractable_t9E79563F8CDEED454F21219F8DFCBE4DB2B6F604  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARScaleInteractable
struct ARScaleInteractable_t6DE1B0299957EEE70877933668B1DD9140DC17B0  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARSelectionInteractable
struct ARSelectionInteractable_tE9BB609D0CC7804B0A87CAD70982C4EDE6329432  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.AR.ARTranslationInteractable
struct ARTranslationInteractable_tB56B128EF9128113068C52C6AD07BFCFB6F4366E  : public RuntimeObject
{
public:

public:
};


// UnityEngine.EventSystems.AbstractEventData
struct AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.BaseInteractionEventArgs
struct BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE  : public RuntimeObject
{
public:
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractor UnityEngine.XR.Interaction.Toolkit.BaseInteractionEventArgs::<interactorObject>k__BackingField
	RuntimeObject* ___U3CinteractorObjectU3Ek__BackingField_0;
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractable UnityEngine.XR.Interaction.Toolkit.BaseInteractionEventArgs::<interactableObject>k__BackingField
	RuntimeObject* ___U3CinteractableObjectU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinteractorObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE, ___U3CinteractorObjectU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CinteractorObjectU3Ek__BackingField_0() const { return ___U3CinteractorObjectU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CinteractorObjectU3Ek__BackingField_0() { return &___U3CinteractorObjectU3Ek__BackingField_0; }
	inline void set_U3CinteractorObjectU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CinteractorObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractorObjectU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinteractableObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE, ___U3CinteractableObjectU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinteractableObjectU3Ek__BackingField_1() const { return ___U3CinteractableObjectU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinteractableObjectU3Ek__BackingField_1() { return &___U3CinteractableObjectU3Ek__BackingField_1; }
	inline void set_U3CinteractableObjectU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinteractableObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractableObjectU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.BaseRegistrationEventArgs
struct BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461  : public RuntimeObject
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.BaseRegistrationEventArgs::<manager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CmanagerU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmanagerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461, ___U3CmanagerU3Ek__BackingField_0)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CmanagerU3Ek__BackingField_0() const { return ___U3CmanagerU3Ek__BackingField_0; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CmanagerU3Ek__BackingField_0() { return &___U3CmanagerU3Ek__BackingField_0; }
	inline void set_U3CmanagerU3Ek__BackingField_0(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CmanagerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmanagerU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.CardinalUtility
struct CardinalUtility_tE2F96394871E99473C454D859132C25103C1EEBF  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.GizmoHelpers
struct GizmoHelpers_t7514C63B55789C91EAF8C3C1BEE367DDF8F3FC43  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.InputActionPropertyExtensions
struct InputActionPropertyExtensions_tCDD85C0FA0E326449655C08EDEE5FB8E33A1E80B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.InputHelpers
struct InputHelpers_t14ED0E9CACB9EAE012078A8FCDE3162F9701B4DD  : public RuntimeObject
{
public:

public:
};

struct InputHelpers_t14ED0E9CACB9EAE012078A8FCDE3162F9701B4DD_StaticFields
{
public:
	// UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo[] UnityEngine.XR.Interaction.Toolkit.InputHelpers::s_ButtonData
	ButtonInfoU5BU5D_tCBEFC9095F25F60B577E75A16241D9BA65E464E4* ___s_ButtonData_0;

public:
	inline static int32_t get_offset_of_s_ButtonData_0() { return static_cast<int32_t>(offsetof(InputHelpers_t14ED0E9CACB9EAE012078A8FCDE3162F9701B4DD_StaticFields, ___s_ButtonData_0)); }
	inline ButtonInfoU5BU5D_tCBEFC9095F25F60B577E75A16241D9BA65E464E4* get_s_ButtonData_0() const { return ___s_ButtonData_0; }
	inline ButtonInfoU5BU5D_tCBEFC9095F25F60B577E75A16241D9BA65E464E4** get_address_of_s_ButtonData_0() { return &___s_ButtonData_0; }
	inline void set_s_ButtonData_0(ButtonInfoU5BU5D_tCBEFC9095F25F60B577E75A16241D9BA65E464E4* value)
	{
		___s_ButtonData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ButtonData_0), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.ProjectPath
struct ProjectPath_tCCC636F5B2F49C2AEAB89E62E77192AA8BAE798E  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.ReflectionUtils
struct ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1_StaticFields
{
public:
	// System.Reflection.Assembly[] UnityEngine.XR.Interaction.Toolkit.Utilities.ReflectionUtils::s_Assemblies
	AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0* ___s_Assemblies_0;
	// System.Collections.Generic.List`1<System.Type[]> UnityEngine.XR.Interaction.Toolkit.Utilities.ReflectionUtils::s_TypesPerAssembly
	List_1_t6AF577A6E10586BF9BFEFA6182485EDDD8935C0E * ___s_TypesPerAssembly_1;

public:
	inline static int32_t get_offset_of_s_Assemblies_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1_StaticFields, ___s_Assemblies_0)); }
	inline AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0* get_s_Assemblies_0() const { return ___s_Assemblies_0; }
	inline AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0** get_address_of_s_Assemblies_0() { return &___s_Assemblies_0; }
	inline void set_s_Assemblies_0(AssemblyU5BU5D_t42061B4CA43487DD8ECD8C3AACCEF783FA081FF0* value)
	{
		___s_Assemblies_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Assemblies_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_TypesPerAssembly_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1_StaticFields, ___s_TypesPerAssembly_1)); }
	inline List_1_t6AF577A6E10586BF9BFEFA6182485EDDD8935C0E * get_s_TypesPerAssembly_1() const { return ___s_TypesPerAssembly_1; }
	inline List_1_t6AF577A6E10586BF9BFEFA6182485EDDD8935C0E ** get_address_of_s_TypesPerAssembly_1() { return &___s_TypesPerAssembly_1; }
	inline void set_s_TypesPerAssembly_1(List_1_t6AF577A6E10586BF9BFEFA6182485EDDD8935C0E * value)
	{
		___s_TypesPerAssembly_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_TypesPerAssembly_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.SimulatedInputLayoutLoader
struct SimulatedInputLayoutLoader_t8046A145569C26D24927A4892FBD47A085154B72  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.SortingHelpers
struct SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5  : public RuntimeObject
{
public:

public:
};

struct SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractable,System.Single> UnityEngine.XR.Interaction.Toolkit.SortingHelpers::s_InteractableDistanceSqrMap
	Dictionary_2_t86132C0F8B31101EE4D1A23B71484DB7D3AD6F35 * ___s_InteractableDistanceSqrMap_0;
	// System.Comparison`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.SortingHelpers::s_InteractableDistanceComparison
	Comparison_1_t816BE556819540BA6AFE8FBB13F995AEED590F30 * ___s_InteractableDistanceComparison_1;

public:
	inline static int32_t get_offset_of_s_InteractableDistanceSqrMap_0() { return static_cast<int32_t>(offsetof(SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5_StaticFields, ___s_InteractableDistanceSqrMap_0)); }
	inline Dictionary_2_t86132C0F8B31101EE4D1A23B71484DB7D3AD6F35 * get_s_InteractableDistanceSqrMap_0() const { return ___s_InteractableDistanceSqrMap_0; }
	inline Dictionary_2_t86132C0F8B31101EE4D1A23B71484DB7D3AD6F35 ** get_address_of_s_InteractableDistanceSqrMap_0() { return &___s_InteractableDistanceSqrMap_0; }
	inline void set_s_InteractableDistanceSqrMap_0(Dictionary_2_t86132C0F8B31101EE4D1A23B71484DB7D3AD6F35 * value)
	{
		___s_InteractableDistanceSqrMap_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InteractableDistanceSqrMap_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_InteractableDistanceComparison_1() { return static_cast<int32_t>(offsetof(SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5_StaticFields, ___s_InteractableDistanceComparison_1)); }
	inline Comparison_1_t816BE556819540BA6AFE8FBB13F995AEED590F30 * get_s_InteractableDistanceComparison_1() const { return ___s_InteractableDistanceComparison_1; }
	inline Comparison_1_t816BE556819540BA6AFE8FBB13F995AEED590F30 ** get_address_of_s_InteractableDistanceComparison_1() { return &___s_InteractableDistanceComparison_1; }
	inline void set_s_InteractableDistanceComparison_1(Comparison_1_t816BE556819540BA6AFE8FBB13F995AEED590F30 * value)
	{
		___s_InteractableDistanceComparison_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InteractableDistanceComparison_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor
struct TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::contactAdded
	Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * ___contactAdded_0;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::contactRemoved
	Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * ___contactRemoved_1;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::<interactionManager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CinteractionManagerU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Collider,UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::m_EnteredColliders
	Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * ___m_EnteredColliders_3;
	// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::m_UnorderedInteractables
	HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * ___m_UnorderedInteractables_4;
	// System.Collections.Generic.HashSet`1<UnityEngine.Collider> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::m_EnteredUnassociatedColliders
	HashSet_1_tE4535201ED12CB585DB82A688DBE933550FDFDCA * ___m_EnteredUnassociatedColliders_5;

public:
	inline static int32_t get_offset_of_contactAdded_0() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___contactAdded_0)); }
	inline Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * get_contactAdded_0() const { return ___contactAdded_0; }
	inline Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 ** get_address_of_contactAdded_0() { return &___contactAdded_0; }
	inline void set_contactAdded_0(Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * value)
	{
		___contactAdded_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contactAdded_0), (void*)value);
	}

	inline static int32_t get_offset_of_contactRemoved_1() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___contactRemoved_1)); }
	inline Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * get_contactRemoved_1() const { return ___contactRemoved_1; }
	inline Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 ** get_address_of_contactRemoved_1() { return &___contactRemoved_1; }
	inline void set_contactRemoved_1(Action_1_t1D98516589D89AD8E61335AEB4B5A3B0E579EC27 * value)
	{
		___contactRemoved_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contactRemoved_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinteractionManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___U3CinteractionManagerU3Ek__BackingField_2)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CinteractionManagerU3Ek__BackingField_2() const { return ___U3CinteractionManagerU3Ek__BackingField_2; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CinteractionManagerU3Ek__BackingField_2() { return &___U3CinteractionManagerU3Ek__BackingField_2; }
	inline void set_U3CinteractionManagerU3Ek__BackingField_2(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CinteractionManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractionManagerU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_EnteredColliders_3() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___m_EnteredColliders_3)); }
	inline Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * get_m_EnteredColliders_3() const { return ___m_EnteredColliders_3; }
	inline Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 ** get_address_of_m_EnteredColliders_3() { return &___m_EnteredColliders_3; }
	inline void set_m_EnteredColliders_3(Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * value)
	{
		___m_EnteredColliders_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EnteredColliders_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_UnorderedInteractables_4() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___m_UnorderedInteractables_4)); }
	inline HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * get_m_UnorderedInteractables_4() const { return ___m_UnorderedInteractables_4; }
	inline HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F ** get_address_of_m_UnorderedInteractables_4() { return &___m_UnorderedInteractables_4; }
	inline void set_m_UnorderedInteractables_4(HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * value)
	{
		___m_UnorderedInteractables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UnorderedInteractables_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_EnteredUnassociatedColliders_5() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5, ___m_EnteredUnassociatedColliders_5)); }
	inline HashSet_1_tE4535201ED12CB585DB82A688DBE933550FDFDCA * get_m_EnteredUnassociatedColliders_5() const { return ___m_EnteredUnassociatedColliders_5; }
	inline HashSet_1_tE4535201ED12CB585DB82A688DBE933550FDFDCA ** get_address_of_m_EnteredUnassociatedColliders_5() { return &___m_EnteredUnassociatedColliders_5; }
	inline void set_m_EnteredUnassociatedColliders_5(HashSet_1_tE4535201ED12CB585DB82A688DBE933550FDFDCA * value)
	{
		___m_EnteredUnassociatedColliders_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EnteredUnassociatedColliders_5), (void*)value);
	}
};

struct TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor::s_ScratchColliders
	List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * ___s_ScratchColliders_6;

public:
	inline static int32_t get_offset_of_s_ScratchColliders_6() { return static_cast<int32_t>(offsetof(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5_StaticFields, ___s_ScratchColliders_6)); }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * get_s_ScratchColliders_6() const { return ___s_ScratchColliders_6; }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B ** get_address_of_s_ScratchColliders_6() { return &___s_ScratchColliders_6; }
	inline void set_s_ScratchColliders_6(List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * value)
	{
		___s_ScratchColliders_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ScratchColliders_6), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.TypeExtensions
struct TypeExtensions_t849319246D688D34249D2F540FFFAFCB2CF24066  : public RuntimeObject
{
public:

public:
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.XR.Interaction.Toolkit.XRHelpURLConstants
struct XRHelpURLConstants_t8BCF4AC68F4BADEE60DDB985355B5F5113B10BDB  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.XRInteractionUpdateOrder
struct XRInteractionUpdateOrder_t35C39CFF0A38DF75D4AF3B11C1AED199237194A8  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitComparer
struct RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment
struct RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment::m_Count
	int32_t ___m_Count_0;
	// UnityEngine.RaycastHit[] UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment::m_Hits
	RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* ___m_Hits_1;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment::m_CurrentIndex
	int32_t ___m_CurrentIndex_2;

public:
	inline static int32_t get_offset_of_m_Count_0() { return static_cast<int32_t>(offsetof(RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D, ___m_Count_0)); }
	inline int32_t get_m_Count_0() const { return ___m_Count_0; }
	inline int32_t* get_address_of_m_Count_0() { return &___m_Count_0; }
	inline void set_m_Count_0(int32_t value)
	{
		___m_Count_0 = value;
	}

	inline static int32_t get_offset_of_m_Hits_1() { return static_cast<int32_t>(offsetof(RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D, ___m_Hits_1)); }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* get_m_Hits_1() const { return ___m_Hits_1; }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09** get_address_of_m_Hits_1() { return &___m_Hits_1; }
	inline void set_m_Hits_1(RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* value)
	{
		___m_Hits_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hits_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_2() { return static_cast<int32_t>(offsetof(RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D, ___m_CurrentIndex_2)); }
	inline int32_t get_m_CurrentIndex_2() const { return ___m_CurrentIndex_2; }
	inline int32_t* get_address_of_m_CurrentIndex_2() { return &___m_CurrentIndex_2; }
	inline void set_m_CurrentIndex_2(int32_t value)
	{
		___m_CurrentIndex_2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitComparer
struct RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>
struct InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.ActivateEventArgs>
struct UnityEvent_1_tF361661C3A10A4FB7C7A08A1A891EADF60307F31  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tF361661C3A10A4FB7C7A08A1A891EADF60307F31, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.DeactivateEventArgs>
struct UnityEvent_1_tD66DFFBAD5AEE7DAC3A486BA211C734125EA6511  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tD66DFFBAD5AEE7DAC3A486BA211C734125EA6511, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs>
struct UnityEvent_1_tEB176A995B6F186AB635923B0B9103DA633C8049  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tEB176A995B6F186AB635923B0B9103DA633C8049, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs>
struct UnityEvent_1_t93850870EB53E8B7A97D9684AB82147A83D8CC74  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t93850870EB53E8B7A97D9684AB82147A83D8CC74, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs>
struct UnityEvent_1_t793F3D070F12FF57919C6FA8BDDA742A3D75773D  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t793F3D070F12FF57919C6FA8BDDA742A3D75773D, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs>
struct UnityEvent_1_tC7D1BD63B6A342C8CAF366C3242C680CC371BE61  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tC7D1BD63B6A342C8CAF366C3242C680CC371BE61, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.TeleportingEventArgs>
struct UnityEvent_1_t800896CA3F0FC97BB33D86AD5354F6F7D09C74E1  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t800896CA3F0FC97BB33D86AD5354F6F7D09C74E1, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable>
struct UnityEvent_1_t2F14C6E4FF180C5250B9D83ECAF7D1195DB46FED  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2F14C6E4FF180C5250B9D83ECAF7D1195DB46FED, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor>
struct UnityEvent_1_t492118CAA84F8D886170A2BF3ED1BABEF620340D  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t492118CAA84F8D886170A2BF3ED1BABEF620340D, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.ActivateEventArgs
struct ActivateEventArgs_t184A1D6A05ADA84E070E2A9B822703E76A55AB27  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:

public:
};


// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E  : public AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E, ___m_EventSystem_1)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeactivateEventArgs
struct DeactivateEventArgs_tC810EECD0F711B424F8483125E8B005D6F9BDB81  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.FourCC
struct FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs
struct HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs::<manager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CmanagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmanagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9, ___U3CmanagerU3Ek__BackingField_2)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CmanagerU3Ek__BackingField_2() const { return ___U3CmanagerU3Ek__BackingField_2; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CmanagerU3Ek__BackingField_2() { return &___U3CmanagerU3Ek__BackingField_2; }
	inline void set_U3CmanagerU3Ek__BackingField_2(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CmanagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmanagerU3Ek__BackingField_2), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs
struct HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs::<manager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CmanagerU3Ek__BackingField_2;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs::<isCanceled>k__BackingField
	bool ___U3CisCanceledU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmanagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293, ___U3CmanagerU3Ek__BackingField_2)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CmanagerU3Ek__BackingField_2() const { return ___U3CmanagerU3Ek__BackingField_2; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CmanagerU3Ek__BackingField_2() { return &___U3CmanagerU3Ek__BackingField_2; }
	inline void set_U3CmanagerU3Ek__BackingField_2(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CmanagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmanagerU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisCanceledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293, ___U3CisCanceledU3Ek__BackingField_3)); }
	inline bool get_U3CisCanceledU3Ek__BackingField_3() const { return ___U3CisCanceledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CisCanceledU3Ek__BackingField_3() { return &___U3CisCanceledU3Ek__BackingField_3; }
	inline void set_U3CisCanceledU3Ek__BackingField_3(bool value)
	{
		___U3CisCanceledU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.InputSystem.InputActionProperty
struct InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 
{
public:
	// System.Boolean UnityEngine.InputSystem.InputActionProperty::m_UseReference
	bool ___m_UseReference_0;
	// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionProperty::m_Action
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_Action_1;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.InputSystem.InputActionProperty::m_Reference
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Reference_2;

public:
	inline static int32_t get_offset_of_m_UseReference_0() { return static_cast<int32_t>(offsetof(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117, ___m_UseReference_0)); }
	inline bool get_m_UseReference_0() const { return ___m_UseReference_0; }
	inline bool* get_address_of_m_UseReference_0() { return &___m_UseReference_0; }
	inline void set_m_UseReference_0(bool value)
	{
		___m_UseReference_0 = value;
	}

	inline static int32_t get_offset_of_m_Action_1() { return static_cast<int32_t>(offsetof(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117, ___m_Action_1)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_Action_1() const { return ___m_Action_1; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_Action_1() { return &___m_Action_1; }
	inline void set_m_Action_1(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_Action_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Reference_2() { return static_cast<int32_t>(offsetof(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117, ___m_Reference_2)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Reference_2() const { return ___m_Reference_2; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Reference_2() { return &___m_Reference_2; }
	inline void set_m_Reference_2(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Reference_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Reference_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputActionProperty
struct InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117_marshaled_pinvoke
{
	int32_t ___m_UseReference_0;
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_Action_1;
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Reference_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputActionProperty
struct InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117_marshaled_com
{
	int32_t ___m_UseReference_0;
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_Action_1;
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Reference_2;
};

// UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA 
{
public:
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_InterfaceName
	String_t* ___m_InterfaceName_0;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_DeviceClass
	String_t* ___m_DeviceClass_1;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Manufacturer
	String_t* ___m_Manufacturer_2;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Product
	String_t* ___m_Product_3;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Serial
	String_t* ___m_Serial_4;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Version
	String_t* ___m_Version_5;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Capabilities
	String_t* ___m_Capabilities_6;

public:
	inline static int32_t get_offset_of_m_InterfaceName_0() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_InterfaceName_0)); }
	inline String_t* get_m_InterfaceName_0() const { return ___m_InterfaceName_0; }
	inline String_t** get_address_of_m_InterfaceName_0() { return &___m_InterfaceName_0; }
	inline void set_m_InterfaceName_0(String_t* value)
	{
		___m_InterfaceName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InterfaceName_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeviceClass_1() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_DeviceClass_1)); }
	inline String_t* get_m_DeviceClass_1() const { return ___m_DeviceClass_1; }
	inline String_t** get_address_of_m_DeviceClass_1() { return &___m_DeviceClass_1; }
	inline void set_m_DeviceClass_1(String_t* value)
	{
		___m_DeviceClass_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceClass_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Manufacturer_2() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_Manufacturer_2)); }
	inline String_t* get_m_Manufacturer_2() const { return ___m_Manufacturer_2; }
	inline String_t** get_address_of_m_Manufacturer_2() { return &___m_Manufacturer_2; }
	inline void set_m_Manufacturer_2(String_t* value)
	{
		___m_Manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Manufacturer_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Product_3() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_Product_3)); }
	inline String_t* get_m_Product_3() const { return ___m_Product_3; }
	inline String_t** get_address_of_m_Product_3() { return &___m_Product_3; }
	inline void set_m_Product_3(String_t* value)
	{
		___m_Product_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Product_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Serial_4() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_Serial_4)); }
	inline String_t* get_m_Serial_4() const { return ___m_Serial_4; }
	inline String_t** get_address_of_m_Serial_4() { return &___m_Serial_4; }
	inline void set_m_Serial_4(String_t* value)
	{
		___m_Serial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Serial_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_5() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_Version_5)); }
	inline String_t* get_m_Version_5() const { return ___m_Version_5; }
	inline String_t** get_address_of_m_Version_5() { return &___m_Version_5; }
	inline void set_m_Version_5(String_t* value)
	{
		___m_Version_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Version_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Capabilities_6() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA, ___m_Capabilities_6)); }
	inline String_t* get_m_Capabilities_6() const { return ___m_Capabilities_6; }
	inline String_t** get_address_of_m_Capabilities_6() { return &___m_Capabilities_6; }
	inline void set_m_Capabilities_6(String_t* value)
	{
		___m_Capabilities_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Capabilities_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA_marshaled_pinvoke
{
	char* ___m_InterfaceName_0;
	char* ___m_DeviceClass_1;
	char* ___m_Manufacturer_2;
	char* ___m_Product_3;
	char* ___m_Serial_4;
	char* ___m_Version_5;
	char* ___m_Capabilities_6;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA_marshaled_com
{
	Il2CppChar* ___m_InterfaceName_0;
	Il2CppChar* ___m_DeviceClass_1;
	Il2CppChar* ___m_Manufacturer_2;
	Il2CppChar* ___m_Product_3;
	Il2CppChar* ___m_Serial_4;
	Il2CppChar* ___m_Version_5;
	Il2CppChar* ___m_Capabilities_6;
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs
struct InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F  : public BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461
{
public:
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractable UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs::<interactableObject>k__BackingField
	RuntimeObject* ___U3CinteractableObjectU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinteractableObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F, ___U3CinteractableObjectU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinteractableObjectU3Ek__BackingField_1() const { return ___U3CinteractableObjectU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinteractableObjectU3Ek__BackingField_1() { return &___U3CinteractableObjectU3Ek__BackingField_1; }
	inline void set_U3CinteractableObjectU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinteractableObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractableObjectU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs
struct InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D  : public BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461
{
public:
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractable UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs::<interactableObject>k__BackingField
	RuntimeObject* ___U3CinteractableObjectU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinteractableObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D, ___U3CinteractableObjectU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinteractableObjectU3Ek__BackingField_1() const { return ___U3CinteractableObjectU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinteractableObjectU3Ek__BackingField_1() { return &___U3CinteractableObjectU3Ek__BackingField_1; }
	inline void set_U3CinteractableObjectU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinteractableObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractableObjectU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractionLayerMask
struct InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47 
{
public:
	// System.UInt32 UnityEngine.XR.Interaction.Toolkit.InteractionLayerMask::m_Bits
	uint32_t ___m_Bits_0;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.InteractionLayerMask::m_Mask
	int32_t ___m_Mask_1;

public:
	inline static int32_t get_offset_of_m_Bits_0() { return static_cast<int32_t>(offsetof(InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47, ___m_Bits_0)); }
	inline uint32_t get_m_Bits_0() const { return ___m_Bits_0; }
	inline uint32_t* get_address_of_m_Bits_0() { return &___m_Bits_0; }
	inline void set_m_Bits_0(uint32_t value)
	{
		___m_Bits_0 = value;
	}

	inline static int32_t get_offset_of_m_Mask_1() { return static_cast<int32_t>(offsetof(InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47, ___m_Mask_1)); }
	inline int32_t get_m_Mask_1() const { return ___m_Mask_1; }
	inline int32_t* get_address_of_m_Mask_1() { return &___m_Mask_1; }
	inline void set_m_Mask_1(int32_t value)
	{
		___m_Mask_1 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs
struct InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE  : public BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461
{
public:
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractor UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs::<interactorObject>k__BackingField
	RuntimeObject* ___U3CinteractorObjectU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinteractorObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE, ___U3CinteractorObjectU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinteractorObjectU3Ek__BackingField_1() const { return ___U3CinteractorObjectU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinteractorObjectU3Ek__BackingField_1() { return &___U3CinteractorObjectU3Ek__BackingField_1; }
	inline void set_U3CinteractorObjectU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinteractorObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractorObjectU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs
struct InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206  : public BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461
{
public:
	// UnityEngine.XR.Interaction.Toolkit.IXRInteractor UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs::<interactorObject>k__BackingField
	RuntimeObject* ___U3CinteractorObjectU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinteractorObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206, ___U3CinteractorObjectU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinteractorObjectU3Ek__BackingField_1() const { return ___U3CinteractorObjectU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinteractorObjectU3Ek__BackingField_1() { return &___U3CinteractorObjectU3Ek__BackingField_1; }
	inline void set_U3CinteractorObjectU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinteractorObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractorObjectU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.ScriptableSettingsPathAttribute
struct ScriptableSettingsPathAttribute_t9D69DCC2E340AB6B3209F95783BD118EC27DEB50  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.XR.Interaction.Toolkit.Utilities.ScriptableSettingsPathAttribute::<path>k__BackingField
	String_t* ___U3CpathU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScriptableSettingsPathAttribute_t9D69DCC2E340AB6B3209F95783BD118EC27DEB50, ___U3CpathU3Ek__BackingField_0)); }
	inline String_t* get_U3CpathU3Ek__BackingField_0() const { return ___U3CpathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpathU3Ek__BackingField_0() { return &___U3CpathU3Ek__BackingField_0; }
	inline void set_U3CpathU3Ek__BackingField_0(String_t* value)
	{
		___U3CpathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpathU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs
struct SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs::<manager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CmanagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmanagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE, ___U3CmanagerU3Ek__BackingField_2)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CmanagerU3Ek__BackingField_2() const { return ___U3CmanagerU3Ek__BackingField_2; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CmanagerU3Ek__BackingField_2() { return &___U3CmanagerU3Ek__BackingField_2; }
	inline void set_U3CmanagerU3Ek__BackingField_2(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CmanagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmanagerU3Ek__BackingField_2), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs
struct SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs::<manager>k__BackingField
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___U3CmanagerU3Ek__BackingField_2;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs::<isCanceled>k__BackingField
	bool ___U3CisCanceledU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmanagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E, ___U3CmanagerU3Ek__BackingField_2)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_U3CmanagerU3Ek__BackingField_2() const { return ___U3CmanagerU3Ek__BackingField_2; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_U3CmanagerU3Ek__BackingField_2() { return &___U3CmanagerU3Ek__BackingField_2; }
	inline void set_U3CmanagerU3Ek__BackingField_2(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___U3CmanagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmanagerU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisCanceledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E, ___U3CisCanceledU3Ek__BackingField_3)); }
	inline bool get_U3CisCanceledU3Ek__BackingField_3() const { return ___U3CisCanceledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CisCanceledU3Ek__BackingField_3() { return &___U3CisCanceledU3Ek__BackingField_3; }
	inline void set_U3CisCanceledU3Ek__BackingField_3(bool value)
	{
		___U3CisCanceledU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84 
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Type> UnityEngine.InputSystem.Utilities.TypeTable::table
	Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 * ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84, ___table_0)); }
	inline Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 * get_table_0() const { return ___table_0; }
	inline Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___table_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84_marshaled_pinvoke
{
	Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 * ___table_0;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84_marshaled_com
{
	Dictionary_2_tF31B1670D5F3FD89062596A16A6A3F982D3AA2B3 * ___table_0;
};

// System.UInt64
struct UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf
struct XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F 
{
public:
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf::x
	float ___x_0;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf::y
	float ___y_1;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf::z
	float ___z_2;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector2f
struct XrVector2f_t7B19E7F74801E61C8C4A8F0949E26FFFE01D4715 
{
public:
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector2f::x
	float ___x_0;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector2f::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(XrVector2f_t7B19E7F74801E61C8C4A8F0949E26FFFE01D4715, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(XrVector2f_t7B19E7F74801E61C8C4A8F0949E26FFFE01D4715, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector3f
struct XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4 
{
public:
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector3f::x
	float ___x_0;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector3f::y
	float ___y_1;
	// System.Single UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector3f::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct __StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA__padding[16];
	};

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData
struct InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData::<hoverTargets>k__BackingField
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData::<pointerTarget>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63, ___U3ChoverTargetsU3Ek__BackingField_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_U3ChoverTargetsU3Ek__BackingField_0() const { return ___U3ChoverTargetsU3Ek__BackingField_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_U3ChoverTargetsU3Ek__BackingField_0() { return &___U3ChoverTargetsU3Ek__BackingField_0; }
	inline void set_U3ChoverTargetsU3Ek__BackingField_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___U3ChoverTargetsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChoverTargetsU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerTargetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63, ___U3CpointerTargetU3Ek__BackingField_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerTargetU3Ek__BackingField_1() const { return ___U3CpointerTargetU3Ek__BackingField_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerTargetU3Ek__BackingField_1() { return &___U3CpointerTargetU3Ek__BackingField_1; }
	inline void set_U3CpointerTargetU3Ek__BackingField_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerTargetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerTargetU3Ek__BackingField_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData
struct InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63_marshaled_pinvoke
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData
struct InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63_marshaled_com
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
};

// UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup
struct ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E__padding[1];
	};

public:
};

struct ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::mode
	int32_t ___mode_0;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::srcBlend
	int32_t ___srcBlend_1;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::dstBlend
	int32_t ___dstBlend_2;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::zWrite
	int32_t ___zWrite_3;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::baseColor
	int32_t ___baseColor_4;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor/ShaderPropertyLookup::color
	int32_t ___color_5;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_srcBlend_1() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___srcBlend_1)); }
	inline int32_t get_srcBlend_1() const { return ___srcBlend_1; }
	inline int32_t* get_address_of_srcBlend_1() { return &___srcBlend_1; }
	inline void set_srcBlend_1(int32_t value)
	{
		___srcBlend_1 = value;
	}

	inline static int32_t get_offset_of_dstBlend_2() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___dstBlend_2)); }
	inline int32_t get_dstBlend_2() const { return ___dstBlend_2; }
	inline int32_t* get_address_of_dstBlend_2() { return &___dstBlend_2; }
	inline void set_dstBlend_2(int32_t value)
	{
		___dstBlend_2 = value;
	}

	inline static int32_t get_offset_of_zWrite_3() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___zWrite_3)); }
	inline int32_t get_zWrite_3() const { return ___zWrite_3; }
	inline int32_t* get_address_of_zWrite_3() { return &___zWrite_3; }
	inline void set_zWrite_3(int32_t value)
	{
		___zWrite_3 = value;
	}

	inline static int32_t get_offset_of_baseColor_4() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___baseColor_4)); }
	inline int32_t get_baseColor_4() const { return ___baseColor_4; }
	inline int32_t* get_address_of_baseColor_4() { return &___baseColor_4; }
	inline void set_baseColor_4(int32_t value)
	{
		___baseColor_4 = value;
	}

	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields, ___color_5)); }
	inline int32_t get_color_5() const { return ___color_5; }
	inline int32_t* get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(int32_t value)
	{
		___color_5 = value;
	}
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tAE2FF88FB3600B4B96FF4BDC31623A4700964BC1  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tAE2FF88FB3600B4B96FF4BDC31623A4700964BC1_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D
	__StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA  ___EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0;

public:
	inline static int32_t get_offset_of_EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tAE2FF88FB3600B4B96FF4BDC31623A4700964BC1_StaticFields, ___EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0)); }
	inline __StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA  get_EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0() const { return ___EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0; }
	inline __StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA * get_address_of_EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0() { return &___EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0; }
	inline void set_EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0(__StaticArrayInitTypeSizeU3D16_t588CE4B118CCFE01A554E1307ED0B824FFC37EEA  value)
	{
		___EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.ActivateEvent
struct ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F  : public UnityEvent_1_tF361661C3A10A4FB7C7A08A1A891EADF60307F31
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState
struct ButtonDeltaState_t49C4EFA8C1D3EAFA4A6E1C88169D98A0C57E80A5 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonDeltaState_t49C4EFA8C1D3EAFA4A6E1C88169D98A0C57E80A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Cardinal
struct Cardinal_tCC65359DA694D1FE249B8A590598161786FD3BC1 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Cardinal::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Cardinal_tCC65359DA694D1FE249B8A590598161786FD3BC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.ControllerButton
struct ControllerButton_t7C56CD3F9B93A26CC907FBD592F9767BA9F19379 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.ControllerButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerButton_t7C56CD3F9B93A26CC907FBD592F9767BA9F19379, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.CursorLockMode
struct CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeactivateEvent
struct DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C  : public UnityEvent_1_tD66DFFBAD5AEE7DAC3A486BA211C734125EA6511
{
public:

public:
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.XR.Interaction.Toolkit.HoverEnterEvent
struct HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6  : public UnityEvent_1_tEB176A995B6F186AB635923B0B9103DA633C8049
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.HoverExitEvent
struct HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341  : public UnityEvent_1_t93850870EB53E8B7A97D9684AB82147A83D8CC74
{
public:

public:
};


// UnityEngine.InputSystem.InputBindingComposite
struct InputBindingComposite_t33B4E44C18D396E83D688E4686AC40CA884AFDDC  : public RuntimeObject
{
public:

public:
};

struct InputBindingComposite_t33B4E44C18D396E83D688E4686AC40CA884AFDDC_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.TypeTable UnityEngine.InputSystem.InputBindingComposite::s_Composites
	TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84  ___s_Composites_0;

public:
	inline static int32_t get_offset_of_s_Composites_0() { return static_cast<int32_t>(offsetof(InputBindingComposite_t33B4E44C18D396E83D688E4686AC40CA884AFDDC_StaticFields, ___s_Composites_0)); }
	inline TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84  get_s_Composites_0() const { return ___s_Composites_0; }
	inline TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84 * get_address_of_s_Composites_0() { return &___s_Composites_0; }
	inline void set_s_Composites_0(TypeTable_t3EB4CA1FCA025CA6C1FD86D69C40017E5BC99C84  value)
	{
		___s_Composites_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Composites_0))->___table_0), (void*)NULL);
	}
};


// UnityEngine.XR.InputDeviceCharacteristics
struct InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64 
{
public:
	// System.UInt32 UnityEngine.XR.InputDeviceCharacteristics::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.LowLevel.InputStateBlock
struct InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C 
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::<format>k__BackingField
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___U3CformatU3Ek__BackingField_33;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<byteOffset>k__BackingField
	uint32_t ___U3CbyteOffsetU3Ek__BackingField_34;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<bitOffset>k__BackingField
	uint32_t ___U3CbitOffsetU3Ek__BackingField_35;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_36;

public:
	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CformatU3Ek__BackingField_33)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_U3CformatU3Ek__BackingField_33() const { return ___U3CformatU3Ek__BackingField_33; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_U3CformatU3Ek__BackingField_33() { return &___U3CformatU3Ek__BackingField_33; }
	inline void set_U3CformatU3Ek__BackingField_33(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___U3CformatU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CbyteOffsetU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CbyteOffsetU3Ek__BackingField_34)); }
	inline uint32_t get_U3CbyteOffsetU3Ek__BackingField_34() const { return ___U3CbyteOffsetU3Ek__BackingField_34; }
	inline uint32_t* get_address_of_U3CbyteOffsetU3Ek__BackingField_34() { return &___U3CbyteOffsetU3Ek__BackingField_34; }
	inline void set_U3CbyteOffsetU3Ek__BackingField_34(uint32_t value)
	{
		___U3CbyteOffsetU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CbitOffsetU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CbitOffsetU3Ek__BackingField_35)); }
	inline uint32_t get_U3CbitOffsetU3Ek__BackingField_35() const { return ___U3CbitOffsetU3Ek__BackingField_35; }
	inline uint32_t* get_address_of_U3CbitOffsetU3Ek__BackingField_35() { return &___U3CbitOffsetU3Ek__BackingField_35; }
	inline void set_U3CbitOffsetU3Ek__BackingField_35(uint32_t value)
	{
		___U3CbitOffsetU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CsizeInBitsU3Ek__BackingField_36)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_36() const { return ___U3CsizeInBitsU3Ek__BackingField_36; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_36() { return &___U3CsizeInBitsU3Ek__BackingField_36; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_36(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_36 = value;
	}
};

struct InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatBit
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatBit_2;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSBit
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatSBit_4;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatInt
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatInt_6;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUInt
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatUInt_8;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatShort
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatShort_10;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUShort
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatUShort_12;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatByte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatByte_14;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSByte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatSByte_16;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatLong
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatLong_18;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatULong
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatULong_20;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatFloat
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatFloat_22;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatDouble
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatDouble_24;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2_26;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3_27;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatQuaternion
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatQuaternion_28;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Short
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2Short_29;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Short
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3Short_30;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Byte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2Byte_31;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Byte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3Byte_32;

public:
	inline static int32_t get_offset_of_FormatBit_2() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatBit_2)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatBit_2() const { return ___FormatBit_2; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatBit_2() { return &___FormatBit_2; }
	inline void set_FormatBit_2(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatBit_2 = value;
	}

	inline static int32_t get_offset_of_FormatSBit_4() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatSBit_4)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatSBit_4() const { return ___FormatSBit_4; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatSBit_4() { return &___FormatSBit_4; }
	inline void set_FormatSBit_4(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatSBit_4 = value;
	}

	inline static int32_t get_offset_of_FormatInt_6() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatInt_6)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatInt_6() const { return ___FormatInt_6; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatInt_6() { return &___FormatInt_6; }
	inline void set_FormatInt_6(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatInt_6 = value;
	}

	inline static int32_t get_offset_of_FormatUInt_8() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatUInt_8)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatUInt_8() const { return ___FormatUInt_8; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatUInt_8() { return &___FormatUInt_8; }
	inline void set_FormatUInt_8(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatUInt_8 = value;
	}

	inline static int32_t get_offset_of_FormatShort_10() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatShort_10)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatShort_10() const { return ___FormatShort_10; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatShort_10() { return &___FormatShort_10; }
	inline void set_FormatShort_10(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatShort_10 = value;
	}

	inline static int32_t get_offset_of_FormatUShort_12() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatUShort_12)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatUShort_12() const { return ___FormatUShort_12; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatUShort_12() { return &___FormatUShort_12; }
	inline void set_FormatUShort_12(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatUShort_12 = value;
	}

	inline static int32_t get_offset_of_FormatByte_14() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatByte_14)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatByte_14() const { return ___FormatByte_14; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatByte_14() { return &___FormatByte_14; }
	inline void set_FormatByte_14(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatByte_14 = value;
	}

	inline static int32_t get_offset_of_FormatSByte_16() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatSByte_16)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatSByte_16() const { return ___FormatSByte_16; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatSByte_16() { return &___FormatSByte_16; }
	inline void set_FormatSByte_16(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatSByte_16 = value;
	}

	inline static int32_t get_offset_of_FormatLong_18() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatLong_18)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatLong_18() const { return ___FormatLong_18; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatLong_18() { return &___FormatLong_18; }
	inline void set_FormatLong_18(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatLong_18 = value;
	}

	inline static int32_t get_offset_of_FormatULong_20() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatULong_20)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatULong_20() const { return ___FormatULong_20; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatULong_20() { return &___FormatULong_20; }
	inline void set_FormatULong_20(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatULong_20 = value;
	}

	inline static int32_t get_offset_of_FormatFloat_22() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatFloat_22)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatFloat_22() const { return ___FormatFloat_22; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatFloat_22() { return &___FormatFloat_22; }
	inline void set_FormatFloat_22(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatFloat_22 = value;
	}

	inline static int32_t get_offset_of_FormatDouble_24() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatDouble_24)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatDouble_24() const { return ___FormatDouble_24; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatDouble_24() { return &___FormatDouble_24; }
	inline void set_FormatDouble_24(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatDouble_24 = value;
	}

	inline static int32_t get_offset_of_FormatVector2_26() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2_26)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2_26() const { return ___FormatVector2_26; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2_26() { return &___FormatVector2_26; }
	inline void set_FormatVector2_26(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2_26 = value;
	}

	inline static int32_t get_offset_of_FormatVector3_27() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3_27)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3_27() const { return ___FormatVector3_27; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3_27() { return &___FormatVector3_27; }
	inline void set_FormatVector3_27(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3_27 = value;
	}

	inline static int32_t get_offset_of_FormatQuaternion_28() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatQuaternion_28)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatQuaternion_28() const { return ___FormatQuaternion_28; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatQuaternion_28() { return &___FormatQuaternion_28; }
	inline void set_FormatQuaternion_28(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatQuaternion_28 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Short_29() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2Short_29)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2Short_29() const { return ___FormatVector2Short_29; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2Short_29() { return &___FormatVector2Short_29; }
	inline void set_FormatVector2Short_29(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2Short_29 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Short_30() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3Short_30)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3Short_30() const { return ___FormatVector3Short_30; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3Short_30() { return &___FormatVector3Short_30; }
	inline void set_FormatVector3Short_30(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3Short_30 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Byte_31() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2Byte_31)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2Byte_31() const { return ___FormatVector2Byte_31; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2Byte_31() { return &___FormatVector2Byte_31; }
	inline void set_FormatVector2Byte_31(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2Byte_31 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Byte_32() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3Byte_32)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3Byte_32() const { return ___FormatVector3Byte_32; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3Byte_32() { return &___FormatVector3Byte_32; }
	inline void set_FormatVector3Byte_32(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3Byte_32 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractableSelectMode
struct InteractableSelectMode_t60921C1737D9470BB32F45B4E557B29F17ACAD68 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.InteractableSelectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractableSelectMode_t60921C1737D9470BB32F45B4E557B29F17ACAD68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.MatchOrientation
struct MatchOrientation_t6A47BD089FA54BAE235D4DAE2414F83F54577D0E 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.MatchOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MatchOrientation_t6A47BD089FA54BAE235D4DAE2414F83F54577D0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.MoveDirection
struct MoveDirection_t740623362F85DF2963BE20C702F7B8EF44E91645 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoveDirection_t740623362F85DF2963BE20C702F7B8EF44E91645, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.QueryTriggerInteraction
struct QueryTriggerInteraction_t9B82FB8CCAF559F47B6B8C0ECE197515ABFA96B0 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t9B82FB8CCAF559F47B6B8C0ECE197515ABFA96B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___m_GameObject_0)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___module_1)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldPosition_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___worldNormal_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___screenPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___screenPosition_9 = value;
	}

	inline static int32_t get_offset_of_displayIndex_10() { return static_cast<int32_t>(offsetof(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE, ___displayIndex_10)); }
	inline int32_t get_displayIndex_10() const { return ___displayIndex_10; }
	inline int32_t* get_address_of_displayIndex_10() { return &___displayIndex_10; }
	inline void set_displayIndex_10(int32_t value)
	{
		___displayIndex_10 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com
{
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_GameObject_0;
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldNormal_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// UnityEngine.XR.Interaction.Toolkit.RequestResult
struct RequestResult_tAAD8D78E7F477222E3AE79D3CE637A8D243B8D31 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.RequestResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestResult_tAAD8D78E7F477222E3AE79D3CE637A8D243B8D31, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.SelectEnterEvent
struct SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918  : public UnityEvent_1_t793F3D070F12FF57919C6FA8BDDA742A3D75773D
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.SelectExitEvent
struct SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8  : public UnityEvent_1_tC7D1BD63B6A342C8CAF366C3242C680CC371BE61
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.TeleportingEvent
struct TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC  : public UnityEvent_1_t800896CA3F0FC97BB33D86AD5354F6F7D09C74E1
{
public:

public:
};


// UnityEngine.TouchPhase
struct TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.TrackingOriginModeFlags
struct TrackingOriginModeFlags_t256D586CBC67509591B0DFEC26F2D2B5C0B532B0 
{
public:
	// System.Int32 UnityEngine.XR.TrackingOriginModeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingOriginModeFlags_t256D586CBC67509591B0DFEC26F2D2B5C0B532B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.TypeCode
struct TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent
struct XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD  : public UnityEvent_1_t492118CAA84F8D886170A2BF3ED1BABEF620340D
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent
struct XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98  : public UnityEvent_1_t2F14C6E4FF180C5250B9D83ECAF7D1195DB46FED
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState
struct XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175 
{
public:
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::primary2DAxis
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trigger_1_OffsetPadding[8];
					// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::trigger
					float ___trigger_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trigger_1_OffsetPadding_forAlignmentOnly[8];
					float ___trigger_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___grip_2_OffsetPadding[12];
					// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::grip
					float ___grip_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___grip_2_OffsetPadding_forAlignmentOnly[12];
					float ___grip_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding[16];
					// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::secondary2DAxis
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding_forAlignmentOnly[16];
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___buttons_4_OffsetPadding[24];
					// System.UInt16 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::buttons
					uint16_t ___buttons_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___buttons_4_OffsetPadding_forAlignmentOnly[24];
					uint16_t ___buttons_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___batteryLevel_5_OffsetPadding[26];
					// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::batteryLevel
					float ___batteryLevel_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___batteryLevel_5_OffsetPadding_forAlignmentOnly[26];
					float ___batteryLevel_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[30];
					// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::trackingState
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[30];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[34];
					// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::isTracked
					bool ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[34];
					bool ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[35];
					// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::devicePosition
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[35];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[47];
					// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState::deviceRotation
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[47];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175__padding[63];
	};

public:
	inline static int32_t get_offset_of_primary2DAxis_0() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___primary2DAxis_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_primary2DAxis_0() const { return ___primary2DAxis_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_primary2DAxis_0() { return &___primary2DAxis_0; }
	inline void set_primary2DAxis_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___primary2DAxis_0 = value;
	}

	inline static int32_t get_offset_of_trigger_1() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___trigger_1)); }
	inline float get_trigger_1() const { return ___trigger_1; }
	inline float* get_address_of_trigger_1() { return &___trigger_1; }
	inline void set_trigger_1(float value)
	{
		___trigger_1 = value;
	}

	inline static int32_t get_offset_of_grip_2() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___grip_2)); }
	inline float get_grip_2() const { return ___grip_2; }
	inline float* get_address_of_grip_2() { return &___grip_2; }
	inline void set_grip_2(float value)
	{
		___grip_2 = value;
	}

	inline static int32_t get_offset_of_secondary2DAxis_3() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___secondary2DAxis_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_secondary2DAxis_3() const { return ___secondary2DAxis_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_secondary2DAxis_3() { return &___secondary2DAxis_3; }
	inline void set_secondary2DAxis_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___secondary2DAxis_3 = value;
	}

	inline static int32_t get_offset_of_buttons_4() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___buttons_4)); }
	inline uint16_t get_buttons_4() const { return ___buttons_4; }
	inline uint16_t* get_address_of_buttons_4() { return &___buttons_4; }
	inline void set_buttons_4(uint16_t value)
	{
		___buttons_4 = value;
	}

	inline static int32_t get_offset_of_batteryLevel_5() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___batteryLevel_5)); }
	inline float get_batteryLevel_5() const { return ___batteryLevel_5; }
	inline float* get_address_of_batteryLevel_5() { return &___batteryLevel_5; }
	inline void set_batteryLevel_5(float value)
	{
		___batteryLevel_5 = value;
	}

	inline static int32_t get_offset_of_trackingState_6() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___trackingState_6)); }
	inline int32_t get_trackingState_6() const { return ___trackingState_6; }
	inline int32_t* get_address_of_trackingState_6() { return &___trackingState_6; }
	inline void set_trackingState_6(int32_t value)
	{
		___trackingState_6 = value;
	}

	inline static int32_t get_offset_of_isTracked_7() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___isTracked_7)); }
	inline bool get_isTracked_7() const { return ___isTracked_7; }
	inline bool* get_address_of_isTracked_7() { return &___isTracked_7; }
	inline void set_isTracked_7(bool value)
	{
		___isTracked_7 = value;
	}

	inline static int32_t get_offset_of_devicePosition_8() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___devicePosition_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_devicePosition_8() const { return ___devicePosition_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_devicePosition_8() { return &___devicePosition_8; }
	inline void set_devicePosition_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___devicePosition_8 = value;
	}

	inline static int32_t get_offset_of_deviceRotation_9() { return static_cast<int32_t>(offsetof(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175, ___deviceRotation_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_deviceRotation_9() const { return ___deviceRotation_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_deviceRotation_9() { return &___deviceRotation_9; }
	inline void set_deviceRotation_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___deviceRotation_9 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState
struct XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trigger_1_OffsetPadding[8];
					float ___trigger_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trigger_1_OffsetPadding_forAlignmentOnly[8];
					float ___trigger_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___grip_2_OffsetPadding[12];
					float ___grip_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___grip_2_OffsetPadding_forAlignmentOnly[12];
					float ___grip_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding[16];
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding_forAlignmentOnly[16];
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___buttons_4_OffsetPadding[24];
					uint16_t ___buttons_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___buttons_4_OffsetPadding_forAlignmentOnly[24];
					uint16_t ___buttons_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___batteryLevel_5_OffsetPadding[26];
					float ___batteryLevel_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___batteryLevel_5_OffsetPadding_forAlignmentOnly[26];
					float ___batteryLevel_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[30];
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[30];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[34];
					int32_t ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[34];
					int32_t ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[35];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[35];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[47];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[47];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175__padding[63];
	};
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState
struct XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175_marshaled_com
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___primary2DAxis_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trigger_1_OffsetPadding[8];
					float ___trigger_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trigger_1_OffsetPadding_forAlignmentOnly[8];
					float ___trigger_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___grip_2_OffsetPadding[12];
					float ___grip_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___grip_2_OffsetPadding_forAlignmentOnly[12];
					float ___grip_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding[16];
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___secondary2DAxis_3_OffsetPadding_forAlignmentOnly[16];
					Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___secondary2DAxis_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___buttons_4_OffsetPadding[24];
					uint16_t ___buttons_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___buttons_4_OffsetPadding_forAlignmentOnly[24];
					uint16_t ___buttons_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___batteryLevel_5_OffsetPadding[26];
					float ___batteryLevel_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___batteryLevel_5_OffsetPadding_forAlignmentOnly[26];
					float ___batteryLevel_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[30];
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[30];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[34];
					int32_t ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[34];
					int32_t ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[35];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[35];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[47];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[47];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175__padding[63];
	};
};

// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState
struct XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7 
{
public:
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::leftEyePosition
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding[12];
					// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::leftEyeRotation
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding_forAlignmentOnly[12];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding[28];
					// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::rightEyePosition
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding_forAlignmentOnly[28];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding[40];
					// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::rightEyeRotation
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding_forAlignmentOnly[40];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding[56];
					// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::centerEyePosition
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding_forAlignmentOnly[56];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding[68];
					// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::centerEyeRotation
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding_forAlignmentOnly[68];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[84];
					// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::trackingState
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[84];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[88];
					// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::isTracked
					bool ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[88];
					bool ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[89];
					// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::devicePosition
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[89];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[101];
					// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState::deviceRotation
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[101];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7__padding[117];
	};

public:
	inline static int32_t get_offset_of_leftEyePosition_0() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___leftEyePosition_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftEyePosition_0() const { return ___leftEyePosition_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftEyePosition_0() { return &___leftEyePosition_0; }
	inline void set_leftEyePosition_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftEyePosition_0 = value;
	}

	inline static int32_t get_offset_of_leftEyeRotation_1() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___leftEyeRotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_leftEyeRotation_1() const { return ___leftEyeRotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_leftEyeRotation_1() { return &___leftEyeRotation_1; }
	inline void set_leftEyeRotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___leftEyeRotation_1 = value;
	}

	inline static int32_t get_offset_of_rightEyePosition_2() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___rightEyePosition_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightEyePosition_2() const { return ___rightEyePosition_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightEyePosition_2() { return &___rightEyePosition_2; }
	inline void set_rightEyePosition_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightEyePosition_2 = value;
	}

	inline static int32_t get_offset_of_rightEyeRotation_3() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___rightEyeRotation_3)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rightEyeRotation_3() const { return ___rightEyeRotation_3; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rightEyeRotation_3() { return &___rightEyeRotation_3; }
	inline void set_rightEyeRotation_3(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rightEyeRotation_3 = value;
	}

	inline static int32_t get_offset_of_centerEyePosition_4() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___centerEyePosition_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_centerEyePosition_4() const { return ___centerEyePosition_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_centerEyePosition_4() { return &___centerEyePosition_4; }
	inline void set_centerEyePosition_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___centerEyePosition_4 = value;
	}

	inline static int32_t get_offset_of_centerEyeRotation_5() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___centerEyeRotation_5)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_centerEyeRotation_5() const { return ___centerEyeRotation_5; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_centerEyeRotation_5() { return &___centerEyeRotation_5; }
	inline void set_centerEyeRotation_5(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___centerEyeRotation_5 = value;
	}

	inline static int32_t get_offset_of_trackingState_6() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___trackingState_6)); }
	inline int32_t get_trackingState_6() const { return ___trackingState_6; }
	inline int32_t* get_address_of_trackingState_6() { return &___trackingState_6; }
	inline void set_trackingState_6(int32_t value)
	{
		___trackingState_6 = value;
	}

	inline static int32_t get_offset_of_isTracked_7() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___isTracked_7)); }
	inline bool get_isTracked_7() const { return ___isTracked_7; }
	inline bool* get_address_of_isTracked_7() { return &___isTracked_7; }
	inline void set_isTracked_7(bool value)
	{
		___isTracked_7 = value;
	}

	inline static int32_t get_offset_of_devicePosition_8() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___devicePosition_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_devicePosition_8() const { return ___devicePosition_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_devicePosition_8() { return &___devicePosition_8; }
	inline void set_devicePosition_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___devicePosition_8 = value;
	}

	inline static int32_t get_offset_of_deviceRotation_9() { return static_cast<int32_t>(offsetof(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7, ___deviceRotation_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_deviceRotation_9() const { return ___deviceRotation_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_deviceRotation_9() { return &___deviceRotation_9; }
	inline void set_deviceRotation_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___deviceRotation_9 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState
struct XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding[12];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding_forAlignmentOnly[12];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding[28];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding_forAlignmentOnly[28];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding[40];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding_forAlignmentOnly[40];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding[56];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding_forAlignmentOnly[56];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding[68];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding_forAlignmentOnly[68];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[84];
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[84];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[88];
					int32_t ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[88];
					int32_t ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[89];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[89];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[101];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[101];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7__padding[117];
	};
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState
struct XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7_marshaled_com
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0;
				};
				#pragma pack(pop, tp)
				struct
				{
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftEyePosition_0_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding[12];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___leftEyeRotation_1_OffsetPadding_forAlignmentOnly[12];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___leftEyeRotation_1_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding[28];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyePosition_2_OffsetPadding_forAlignmentOnly[28];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightEyePosition_2_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding[40];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___rightEyeRotation_3_OffsetPadding_forAlignmentOnly[40];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rightEyeRotation_3_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding[56];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyePosition_4_OffsetPadding_forAlignmentOnly[56];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___centerEyePosition_4_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding[68];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___centerEyeRotation_5_OffsetPadding_forAlignmentOnly[68];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___centerEyeRotation_5_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___trackingState_6_OffsetPadding[84];
					int32_t ___trackingState_6;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___trackingState_6_OffsetPadding_forAlignmentOnly[84];
					int32_t ___trackingState_6_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___isTracked_7_OffsetPadding[88];
					int32_t ___isTracked_7;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___isTracked_7_OffsetPadding_forAlignmentOnly[88];
					int32_t ___isTracked_7_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___devicePosition_8_OffsetPadding[89];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___devicePosition_8_OffsetPadding_forAlignmentOnly[89];
					Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___devicePosition_8_forAlignmentOnly;
				};
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceRotation_9_OffsetPadding[101];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9;
				};
				#pragma pack(pop, tp)
				struct
				{
					char ___deviceRotation_9_OffsetPadding_forAlignmentOnly[101];
					Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___deviceRotation_9_forAlignmentOnly;
				};
			};
		};
		uint8_t XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7__padding[117];
	};
};

// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrPosef
struct XrPosef_tB03D03353E15F17DAE1AD0D9977F69E2A9B3CED9 
{
public:
	// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrQuaternionf UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrPosef::orientation
	XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F  ___orientation_0;
	// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrVector3f UnityEngine.XR.OpenXR.Features.ConformanceAutomation.XrPosef::position
	XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4  ___position_1;

public:
	inline static int32_t get_offset_of_orientation_0() { return static_cast<int32_t>(offsetof(XrPosef_tB03D03353E15F17DAE1AD0D9977F69E2A9B3CED9, ___orientation_0)); }
	inline XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F  get_orientation_0() const { return ___orientation_0; }
	inline XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F * get_address_of_orientation_0() { return &___orientation_0; }
	inline void set_orientation_0(XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F  value)
	{
		___orientation_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(XrPosef_tB03D03353E15F17DAE1AD0D9977F69E2A9B3CED9, ___position_1)); }
	inline XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4  get_position_1() const { return ___position_1; }
	inline XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4  value)
	{
		___position_1 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable/TeleportTrigger
struct TeleportTrigger_t5BA91931CF4212743F56E048DDAF656BA4F6D4E5 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable/TeleportTrigger::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TeleportTrigger_t5BA91931CF4212743F56E048DDAF656BA4F6D4E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase/GravityApplicationMode
struct GravityApplicationMode_tD5AF61AA4BF84CE90354D159B0AA2A1FE368B3F4 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase/GravityApplicationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GravityApplicationMode_tD5AF61AA4BF84CE90354D159B0AA2A1FE368B3F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider/InputAxes
struct InputAxes_tFE5F92BD5F5CC2643A056F4DB0B100BBE6FD9088 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider/InputAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputAxes_tFE5F92BD5F5CC2643A056F4DB0B100BBE6FD9088, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider/InputAxes
struct InputAxes_t7BCA8B7350420526D7BDD69EFDDFF4F1AAC49679 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider/InputAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputAxes_t7BCA8B7350420526D7BDD69EFDDFF4F1AAC49679, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider/InputAxes
struct InputAxes_tCC63ED974CDE78E4FEB19C8098822FD391B4ED53 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider/InputAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputAxes_tCC63ED974CDE78E4FEB19C8098822FD391B4ED53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputControl/ControlFlags
struct ControlFlags_tC210B095E24C31AC8E07C6E09B02CAA80C2563B6 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputControl/ControlFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlFlags_tC210B095E24C31AC8E07C6E09B02CAA80C2563B6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputDevice/DeviceFlags
struct DeviceFlags_tEA83E21B2294D1CFE7395FE7C9455CFC71BBD1F4 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputDevice/DeviceFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceFlags_tEA83E21B2294D1CFE7395FE7C9455CFC71BBD1F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InputHelpers/Button
struct Button_tADDD1BFDCE9AADC94CC4B6D4678484CB0FB5833C 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.InputHelpers/Button::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Button_tADDD1BFDCE9AADC94CC4B6D4678484CB0FB5833C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonReadType
struct ButtonReadType_t681DDCB14F5D5D7849EBFF6D3A3D64315D698BAE 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonReadType_t681DDCB14F5D5D7849EBFF6D3A3D64315D698BAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/ScriptEvent
struct ScriptEvent_tA81311951D5C2E7C1929F4B9D4BA3FF14C3C824F 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/ScriptEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScriptEvent_tA81311951D5C2E7C1929F4B9D4BA3FF14C3C824F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrReferenceSpaceType
struct XrReferenceSpaceType_t6B2E244095E7092E0951F8017620159C39FE76E8 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrReferenceSpaceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrReferenceSpaceType_t6B2E244095E7092E0951F8017620159C39FE76E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrResult
struct XrResult_tF77146A73E185AD8F8E819BD2594ABB23963D377 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrResult_tF77146A73E185AD8F8E819BD2594ABB23963D377, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrSessionState
struct XrSessionState_tC70FDAC8A29DBA216D5F0AD647D517BFC65DAB07 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrSessionState_tC70FDAC8A29DBA216D5F0AD647D517BFC65DAB07, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrSpaceLocationFlags
struct XrSpaceLocationFlags_t38EFB6B836CCDA335449EDE431C293C3EA7594C4 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrSpaceLocationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrSpaceLocationFlags_t38EFB6B836CCDA335449EDE431C293C3EA7594C4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrViewConfigurationType
struct XrViewConfigurationType_t04F6747D133D0D0CCF9BC3B648E0C18FA75532A7 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrViewConfigurationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrViewConfigurationType_t04F6747D133D0D0CCF9BC3B648E0C18FA75532A7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrViewStateFlags
struct XrViewStateFlags_t33EB51930377C244468A9D7E75BEB092CC226F72 
{
public:
	// System.Int32 UnityEngine.XR.OpenXR.Features.Mock.MockDriver/XrViewStateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XrViewStateFlags_t33EB51930377C244468A9D7E75BEB092CC226F72, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.PointerEventData/InputButton
struct InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tA5409FE587ADC841D2BF80835D04074A89C59A9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/Directions
struct Directions_t706FFDE7EB6AC4B89416A24CEA9BBD645934B1F8 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/Directions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Directions_t706FFDE7EB6AC4B89416A24CEA9BBD645934B1F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/State
struct State_t19A336E7F61A10B1B18A391FD305B0BB89FF7506 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t19A336E7F61A10B1B18A391FD305B0BB89FF7506, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/SweepBehavior
struct SweepBehavior_tDAD4BA105BD12352FE6CD1A76FAC0240D2AF970D 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/SweepBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SweepBehavior_tDAD4BA105BD12352FE6CD1A76FAC0240D2AF970D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData
struct RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41 
{
public:
	// UnityEngine.UI.Graphic UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData::<graphic>k__BackingField
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___U3CgraphicU3Ek__BackingField_0;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData::<worldHitPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldHitPositionU3Ek__BackingField_1;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData::<screenPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscreenPositionU3Ek__BackingField_2;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData::<distance>k__BackingField
	float ___U3CdistanceU3Ek__BackingField_3;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData::<displayIndex>k__BackingField
	int32_t ___U3CdisplayIndexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CgraphicU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41, ___U3CgraphicU3Ek__BackingField_0)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_U3CgraphicU3Ek__BackingField_0() const { return ___U3CgraphicU3Ek__BackingField_0; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_U3CgraphicU3Ek__BackingField_0() { return &___U3CgraphicU3Ek__BackingField_0; }
	inline void set_U3CgraphicU3Ek__BackingField_0(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___U3CgraphicU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CgraphicU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CworldHitPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41, ___U3CworldHitPositionU3Ek__BackingField_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldHitPositionU3Ek__BackingField_1() const { return ___U3CworldHitPositionU3Ek__BackingField_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldHitPositionU3Ek__BackingField_1() { return &___U3CworldHitPositionU3Ek__BackingField_1; }
	inline void set_U3CworldHitPositionU3Ek__BackingField_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldHitPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CscreenPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41, ___U3CscreenPositionU3Ek__BackingField_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CscreenPositionU3Ek__BackingField_2() const { return ___U3CscreenPositionU3Ek__BackingField_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CscreenPositionU3Ek__BackingField_2() { return &___U3CscreenPositionU3Ek__BackingField_2; }
	inline void set_U3CscreenPositionU3Ek__BackingField_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CscreenPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41, ___U3CdistanceU3Ek__BackingField_3)); }
	inline float get_U3CdistanceU3Ek__BackingField_3() const { return ___U3CdistanceU3Ek__BackingField_3; }
	inline float* get_address_of_U3CdistanceU3Ek__BackingField_3() { return &___U3CdistanceU3Ek__BackingField_3; }
	inline void set_U3CdistanceU3Ek__BackingField_3(float value)
	{
		___U3CdistanceU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdisplayIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41, ___U3CdisplayIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CdisplayIndexU3Ek__BackingField_4() const { return ___U3CdisplayIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CdisplayIndexU3Ek__BackingField_4() { return &___U3CdisplayIndexU3Ek__BackingField_4; }
	inline void set_U3CdisplayIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CdisplayIndexU3Ek__BackingField_4 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData
struct RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41_marshaled_pinvoke
{
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___U3CgraphicU3Ek__BackingField_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldHitPositionU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscreenPositionU3Ek__BackingField_2;
	float ___U3CdistanceU3Ek__BackingField_3;
	int32_t ___U3CdisplayIndexU3Ek__BackingField_4;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData
struct RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41_marshaled_com
{
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___U3CgraphicU3Ek__BackingField_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldHitPositionU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscreenPositionU3Ek__BackingField_2;
	float ___U3CdistanceU3Ek__BackingField_3;
	int32_t ___U3CdisplayIndexU3Ek__BackingField_4;
};

// Whiteboard/ButtonOption
struct ButtonOption_tCF6CB86364E608A8E6F00C72916A695CA5442E27 
{
public:
	// System.Int32 Whiteboard/ButtonOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonOption_tCF6CB86364E608A8E6F00C72916A695CA5442E27, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Axis2DTargets
struct Axis2DTargets_tDA1ABED2BAB31548E373BA5BA1FA1CE335A16174 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Axis2DTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis2DTargets_tDA1ABED2BAB31548E373BA5BA1FA1CE335A16174, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Space
struct Space_t145148EFA4ABDCF429FA18A63E4C01D03E2CB75C 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t145148EFA4ABDCF429FA18A63E4C01D03E2CB75C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/TransformationMode
struct TransformationMode_tFAD711C19386573876EBDE687EA3E407CB1C191A 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/TransformationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransformationMode_tFAD711C19386573876EBDE687EA3E407CB1C191A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRInteractionUpdateOrder/UpdatePhase
struct UpdatePhase_tDDD5125832C7AE3CDF170690377B689E62D6DD07 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.XRInteractionUpdateOrder/UpdatePhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdatePhase_tDDD5125832C7AE3CDF170690377B689E62D6DD07, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.XR.CoreUtils.XROrigin/TrackingOriginMode
struct TrackingOriginMode_t530277AD3A09FB8A64A2C9B5B8A77CF54199A7DD 
{
public:
	// System.Int32 Unity.XR.CoreUtils.XROrigin/TrackingOriginMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingOriginMode_t530277AD3A09FB8A64A2C9B5B8A77CF54199A7DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputBindingComposite`1<UnityEngine.Quaternion>
struct InputBindingComposite_1_tAD804AA4F39B67288551C933EBAF3E713CA9E7A9  : public InputBindingComposite_t33B4E44C18D396E83D688E4686AC40CA884AFDDC
{
public:

public:
};


// UnityEngine.InputSystem.InputBindingComposite`1<UnityEngine.Vector3>
struct InputBindingComposite_1_t3A5382AF521E7CFF44595C551F17FA4DC78717F4  : public InputBindingComposite_t33B4E44C18D396E83D688E4686AC40CA884AFDDC
{
public:

public:
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954  : public BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___m_PointerPress_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerClickU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerClickU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerClickU3Ek__BackingField_7() const { return ___U3CpointerClickU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerClickU3Ek__BackingField_7() { return &___U3CpointerClickU3Ek__BackingField_7; }
	inline void set_U3CpointerClickU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerClickU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerClickU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerCurrentRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerCurrentRaycastU3Ek__BackingField_8() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_8() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_8(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerPressRaycastU3Ek__BackingField_9)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpointerPressRaycastU3Ek__BackingField_9() const { return ___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_9() { return &___U3CpointerPressRaycastU3Ek__BackingField_9; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_9(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___hovered_10)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_hovered_10() const { return ___hovered_10; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_hovered_10() { return &___hovered_10; }
	inline void set_hovered_10(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___hovered_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CeligibleForClickU3Ek__BackingField_11)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_11() const { return ___U3CeligibleForClickU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_11() { return &___U3CeligibleForClickU3Ek__BackingField_11; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_11(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpointerIdU3Ek__BackingField_12)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_12() const { return ___U3CpointerIdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_12() { return &___U3CpointerIdU3Ek__BackingField_12; }
	inline void set_U3CpointerIdU3Ek__BackingField_12(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpositionU3Ek__BackingField_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpositionU3Ek__BackingField_13() const { return ___U3CpositionU3Ek__BackingField_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpositionU3Ek__BackingField_13() { return &___U3CpositionU3Ek__BackingField_13; }
	inline void set_U3CpositionU3Ek__BackingField_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpositionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdeltaU3Ek__BackingField_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaU3Ek__BackingField_14() const { return ___U3CdeltaU3Ek__BackingField_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaU3Ek__BackingField_14() { return &___U3CdeltaU3Ek__BackingField_14; }
	inline void set_U3CdeltaU3Ek__BackingField_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CpressPositionU3Ek__BackingField_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressPositionU3Ek__BackingField_15() const { return ___U3CpressPositionU3Ek__BackingField_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressPositionU3Ek__BackingField_15() { return &___U3CpressPositionU3Ek__BackingField_15; }
	inline void set_U3CpressPositionU3Ek__BackingField_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldPositionU3Ek__BackingField_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldPositionU3Ek__BackingField_16() const { return ___U3CworldPositionU3Ek__BackingField_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldPositionU3Ek__BackingField_16() { return &___U3CworldPositionU3Ek__BackingField_16; }
	inline void set_U3CworldPositionU3Ek__BackingField_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldPositionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CworldNormalU3Ek__BackingField_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CworldNormalU3Ek__BackingField_17() const { return ___U3CworldNormalU3Ek__BackingField_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CworldNormalU3Ek__BackingField_17() { return &___U3CworldNormalU3Ek__BackingField_17; }
	inline void set_U3CworldNormalU3Ek__BackingField_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CworldNormalU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickTimeU3Ek__BackingField_18)); }
	inline float get_U3CclickTimeU3Ek__BackingField_18() const { return ___U3CclickTimeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_18() { return &___U3CclickTimeU3Ek__BackingField_18; }
	inline void set_U3CclickTimeU3Ek__BackingField_18(float value)
	{
		___U3CclickTimeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CclickCountU3Ek__BackingField_19)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_19() const { return ___U3CclickCountU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_19() { return &___U3CclickCountU3Ek__BackingField_19; }
	inline void set_U3CclickCountU3Ek__BackingField_19(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CscrollDeltaU3Ek__BackingField_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CscrollDeltaU3Ek__BackingField_20() const { return ___U3CscrollDeltaU3Ek__BackingField_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CscrollDeltaU3Ek__BackingField_20() { return &___U3CscrollDeltaU3Ek__BackingField_20; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CuseDragThresholdU3Ek__BackingField_21)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_21() const { return ___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_21() { return &___U3CuseDragThresholdU3Ek__BackingField_21; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_21(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CdraggingU3Ek__BackingField_22)); }
	inline bool get_U3CdraggingU3Ek__BackingField_22() const { return ___U3CdraggingU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_22() { return &___U3CdraggingU3Ek__BackingField_22; }
	inline void set_U3CdraggingU3Ek__BackingField_22(bool value)
	{
		___U3CdraggingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954, ___U3CbuttonU3Ek__BackingField_23)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_23() const { return ___U3CbuttonU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_23() { return &___U3CbuttonU3Ek__BackingField_23; }
	inline void set_U3CbuttonU3Ek__BackingField_23(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_23 = value;
	}
};


// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction
struct SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864  : public RuntimeObject
{
public:
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/Directions UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::directions
	int32_t ___directions_0;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/SweepBehavior UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::sweepBehavior
	int32_t ___sweepBehavior_1;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::pressPoint
	float ___pressPoint_2;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction/State UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::m_State
	int32_t ___m_State_4;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::m_WasValidDirection
	bool ___m_WasValidDirection_5;

public:
	inline static int32_t get_offset_of_directions_0() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864, ___directions_0)); }
	inline int32_t get_directions_0() const { return ___directions_0; }
	inline int32_t* get_address_of_directions_0() { return &___directions_0; }
	inline void set_directions_0(int32_t value)
	{
		___directions_0 = value;
	}

	inline static int32_t get_offset_of_sweepBehavior_1() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864, ___sweepBehavior_1)); }
	inline int32_t get_sweepBehavior_1() const { return ___sweepBehavior_1; }
	inline int32_t* get_address_of_sweepBehavior_1() { return &___sweepBehavior_1; }
	inline void set_sweepBehavior_1(int32_t value)
	{
		___sweepBehavior_1 = value;
	}

	inline static int32_t get_offset_of_pressPoint_2() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864, ___pressPoint_2)); }
	inline float get_pressPoint_2() const { return ___pressPoint_2; }
	inline float* get_address_of_pressPoint_2() { return &___pressPoint_2; }
	inline void set_pressPoint_2(float value)
	{
		___pressPoint_2 = value;
	}

	inline static int32_t get_offset_of_m_State_4() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864, ___m_State_4)); }
	inline int32_t get_m_State_4() const { return ___m_State_4; }
	inline int32_t* get_address_of_m_State_4() { return &___m_State_4; }
	inline void set_m_State_4(int32_t value)
	{
		___m_State_4 = value;
	}

	inline static int32_t get_offset_of_m_WasValidDirection_5() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864, ___m_WasValidDirection_5)); }
	inline bool get_m_WasValidDirection_5() const { return ___m_WasValidDirection_5; }
	inline bool* get_address_of_m_WasValidDirection_5() { return &___m_WasValidDirection_5; }
	inline void set_m_WasValidDirection_5(bool value)
	{
		___m_WasValidDirection_5 = value;
	}
};

struct SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864_StaticFields
{
public:
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Interactions.SectorInteraction::<defaultPressPoint>k__BackingField
	float ___U3CdefaultPressPointU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CdefaultPressPointU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864_StaticFields, ___U3CdefaultPressPointU3Ek__BackingField_3)); }
	inline float get_U3CdefaultPressPointU3Ek__BackingField_3() const { return ___U3CdefaultPressPointU3Ek__BackingField_3; }
	inline float* get_address_of_U3CdefaultPressPointU3Ek__BackingField_3() { return &___U3CdefaultPressPointU3Ek__BackingField_3; }
	inline void set_U3CdefaultPressPointU3Ek__BackingField_3(float value)
	{
		___U3CdefaultPressPointU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.TeleportRequest
struct TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2 
{
public:
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.TeleportRequest::destinationPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___destinationPosition_0;
	// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.TeleportRequest::destinationRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___destinationRotation_1;
	// System.Single UnityEngine.XR.Interaction.Toolkit.TeleportRequest::requestTime
	float ___requestTime_2;
	// UnityEngine.XR.Interaction.Toolkit.MatchOrientation UnityEngine.XR.Interaction.Toolkit.TeleportRequest::matchOrientation
	int32_t ___matchOrientation_3;

public:
	inline static int32_t get_offset_of_destinationPosition_0() { return static_cast<int32_t>(offsetof(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2, ___destinationPosition_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_destinationPosition_0() const { return ___destinationPosition_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_destinationPosition_0() { return &___destinationPosition_0; }
	inline void set_destinationPosition_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___destinationPosition_0 = value;
	}

	inline static int32_t get_offset_of_destinationRotation_1() { return static_cast<int32_t>(offsetof(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2, ___destinationRotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_destinationRotation_1() const { return ___destinationRotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_destinationRotation_1() { return &___destinationRotation_1; }
	inline void set_destinationRotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___destinationRotation_1 = value;
	}

	inline static int32_t get_offset_of_requestTime_2() { return static_cast<int32_t>(offsetof(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2, ___requestTime_2)); }
	inline float get_requestTime_2() const { return ___requestTime_2; }
	inline float* get_address_of_requestTime_2() { return &___requestTime_2; }
	inline void set_requestTime_2(float value)
	{
		___requestTime_2 = value;
	}

	inline static int32_t get_offset_of_matchOrientation_3() { return static_cast<int32_t>(offsetof(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2, ___matchOrientation_3)); }
	inline int32_t get_matchOrientation_3() const { return ___matchOrientation_3; }
	inline int32_t* get_address_of_matchOrientation_3() { return &___matchOrientation_3; }
	inline void set_matchOrientation_3(int32_t value)
	{
		___matchOrientation_3 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo
struct ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED 
{
public:
	// System.String UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo::name
	String_t* ___name_0;
	// UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonReadType UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo
struct ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED_marshaled_pinvoke
{
	char* ___name_0;
	int32_t ___type_1;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.InputHelpers/ButtonInfo
struct ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED_marshaled_com
{
	Il2CppChar* ___name_0;
	int32_t ___type_1;
};

// UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel/ImplementationData
struct ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel/ImplementationData::<consecutiveMoveCount>k__BackingField
	int32_t ___U3CconsecutiveMoveCountU3Ek__BackingField_0;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel/ImplementationData::<lastMoveDirection>k__BackingField
	int32_t ___U3ClastMoveDirectionU3Ek__BackingField_1;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel/ImplementationData::<lastMoveTime>k__BackingField
	float ___U3ClastMoveTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CconsecutiveMoveCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B, ___U3CconsecutiveMoveCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CconsecutiveMoveCountU3Ek__BackingField_0() const { return ___U3CconsecutiveMoveCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CconsecutiveMoveCountU3Ek__BackingField_0() { return &___U3CconsecutiveMoveCountU3Ek__BackingField_0; }
	inline void set_U3CconsecutiveMoveCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CconsecutiveMoveCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3ClastMoveDirectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B, ___U3ClastMoveDirectionU3Ek__BackingField_1)); }
	inline int32_t get_U3ClastMoveDirectionU3Ek__BackingField_1() const { return ___U3ClastMoveDirectionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3ClastMoveDirectionU3Ek__BackingField_1() { return &___U3ClastMoveDirectionU3Ek__BackingField_1; }
	inline void set_U3ClastMoveDirectionU3Ek__BackingField_1(int32_t value)
	{
		___U3ClastMoveDirectionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3ClastMoveTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B, ___U3ClastMoveTimeU3Ek__BackingField_2)); }
	inline float get_U3ClastMoveTimeU3Ek__BackingField_2() const { return ___U3ClastMoveTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3ClastMoveTimeU3Ek__BackingField_2() { return &___U3ClastMoveTimeU3Ek__BackingField_2; }
	inline void set_U3ClastMoveTimeU3Ek__BackingField_2(float value)
	{
		___U3ClastMoveTimeU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData
struct ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98 
{
public:
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<isDragging>k__BackingField
	bool ___U3CisDraggingU3Ek__BackingField_0;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<pressedTime>k__BackingField
	float ___U3CpressedTimeU3Ek__BackingField_1;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<pressedPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_2;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<pressedRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpressedRaycastU3Ek__BackingField_3;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<pressedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<pressedGameObjectRaw>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData::<draggedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CisDraggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CisDraggingU3Ek__BackingField_0)); }
	inline bool get_U3CisDraggingU3Ek__BackingField_0() const { return ___U3CisDraggingU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisDraggingU3Ek__BackingField_0() { return &___U3CisDraggingU3Ek__BackingField_0; }
	inline void set_U3CisDraggingU3Ek__BackingField_0(bool value)
	{
		___U3CisDraggingU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CpressedTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CpressedTimeU3Ek__BackingField_1)); }
	inline float get_U3CpressedTimeU3Ek__BackingField_1() const { return ___U3CpressedTimeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CpressedTimeU3Ek__BackingField_1() { return &___U3CpressedTimeU3Ek__BackingField_1; }
	inline void set_U3CpressedTimeU3Ek__BackingField_1(float value)
	{
		___U3CpressedTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CpressedPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CpressedPositionU3Ek__BackingField_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressedPositionU3Ek__BackingField_2() const { return ___U3CpressedPositionU3Ek__BackingField_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressedPositionU3Ek__BackingField_2() { return &___U3CpressedPositionU3Ek__BackingField_2; }
	inline void set_U3CpressedPositionU3Ek__BackingField_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressedPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CpressedRaycastU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CpressedRaycastU3Ek__BackingField_3)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpressedRaycastU3Ek__BackingField_3() const { return ___U3CpressedRaycastU3Ek__BackingField_3; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpressedRaycastU3Ek__BackingField_3() { return &___U3CpressedRaycastU3Ek__BackingField_3; }
	inline void set_U3CpressedRaycastU3Ek__BackingField_3(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpressedRaycastU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CpressedGameObjectU3Ek__BackingField_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectU3Ek__BackingField_4() const { return ___U3CpressedGameObjectU3Ek__BackingField_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectU3Ek__BackingField_4() { return &___U3CpressedGameObjectU3Ek__BackingField_4; }
	inline void set_U3CpressedGameObjectU3Ek__BackingField_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CpressedGameObjectRawU3Ek__BackingField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectRawU3Ek__BackingField_5() const { return ___U3CpressedGameObjectRawU3Ek__BackingField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectRawU3Ek__BackingField_5() { return &___U3CpressedGameObjectRawU3Ek__BackingField_5; }
	inline void set_U3CpressedGameObjectRawU3Ek__BackingField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectRawU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98, ___U3CdraggedGameObjectU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CdraggedGameObjectU3Ek__BackingField_6() const { return ___U3CdraggedGameObjectU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CdraggedGameObjectU3Ek__BackingField_6() { return &___U3CdraggedGameObjectU3Ek__BackingField_6; }
	inline void set_U3CdraggedGameObjectU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CdraggedGameObjectU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData
struct ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98_marshaled_pinvoke
{
	int32_t ___U3CisDraggingU3Ek__BackingField_0;
	float ___U3CpressedTimeU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_2;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke ___U3CpressedRaycastU3Ek__BackingField_3;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_4;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_5;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_6;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData
struct ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98_marshaled_com
{
	int32_t ___U3CisDraggingU3Ek__BackingField_0;
	float ___U3CpressedTimeU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_2;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com ___U3CpressedRaycastU3Ek__BackingField_3;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_4;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_5;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_6;
};

// UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData
struct ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<hoverTargets>k__BackingField
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pointerTarget>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<isDragging>k__BackingField
	bool ___U3CisDraggingU3Ek__BackingField_2;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pressedTime>k__BackingField
	float ___U3CpressedTimeU3Ek__BackingField_3;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pressedPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_4;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pressedRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpressedRaycastU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pressedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<pressedGameObjectRaw>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_7;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData::<draggedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3ChoverTargetsU3Ek__BackingField_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_U3ChoverTargetsU3Ek__BackingField_0() const { return ___U3ChoverTargetsU3Ek__BackingField_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_U3ChoverTargetsU3Ek__BackingField_0() { return &___U3ChoverTargetsU3Ek__BackingField_0; }
	inline void set_U3ChoverTargetsU3Ek__BackingField_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___U3ChoverTargetsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChoverTargetsU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerTargetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpointerTargetU3Ek__BackingField_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerTargetU3Ek__BackingField_1() const { return ___U3CpointerTargetU3Ek__BackingField_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerTargetU3Ek__BackingField_1() { return &___U3CpointerTargetU3Ek__BackingField_1; }
	inline void set_U3CpointerTargetU3Ek__BackingField_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerTargetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerTargetU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisDraggingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CisDraggingU3Ek__BackingField_2)); }
	inline bool get_U3CisDraggingU3Ek__BackingField_2() const { return ___U3CisDraggingU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CisDraggingU3Ek__BackingField_2() { return &___U3CisDraggingU3Ek__BackingField_2; }
	inline void set_U3CisDraggingU3Ek__BackingField_2(bool value)
	{
		___U3CisDraggingU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CpressedTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpressedTimeU3Ek__BackingField_3)); }
	inline float get_U3CpressedTimeU3Ek__BackingField_3() const { return ___U3CpressedTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CpressedTimeU3Ek__BackingField_3() { return &___U3CpressedTimeU3Ek__BackingField_3; }
	inline void set_U3CpressedTimeU3Ek__BackingField_3(float value)
	{
		___U3CpressedTimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CpressedPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpressedPositionU3Ek__BackingField_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressedPositionU3Ek__BackingField_4() const { return ___U3CpressedPositionU3Ek__BackingField_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressedPositionU3Ek__BackingField_4() { return &___U3CpressedPositionU3Ek__BackingField_4; }
	inline void set_U3CpressedPositionU3Ek__BackingField_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressedPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpressedRaycastU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpressedRaycastU3Ek__BackingField_5)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpressedRaycastU3Ek__BackingField_5() const { return ___U3CpressedRaycastU3Ek__BackingField_5; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpressedRaycastU3Ek__BackingField_5() { return &___U3CpressedRaycastU3Ek__BackingField_5; }
	inline void set_U3CpressedRaycastU3Ek__BackingField_5(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpressedRaycastU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_5))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_5))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpressedGameObjectU3Ek__BackingField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectU3Ek__BackingField_6() const { return ___U3CpressedGameObjectU3Ek__BackingField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectU3Ek__BackingField_6() { return &___U3CpressedGameObjectU3Ek__BackingField_6; }
	inline void set_U3CpressedGameObjectU3Ek__BackingField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CpressedGameObjectRawU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectRawU3Ek__BackingField_7() const { return ___U3CpressedGameObjectRawU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectRawU3Ek__BackingField_7() { return &___U3CpressedGameObjectRawU3Ek__BackingField_7; }
	inline void set_U3CpressedGameObjectRawU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectRawU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectRawU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886, ___U3CdraggedGameObjectU3Ek__BackingField_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CdraggedGameObjectU3Ek__BackingField_8() const { return ___U3CdraggedGameObjectU3Ek__BackingField_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CdraggedGameObjectU3Ek__BackingField_8() { return &___U3CdraggedGameObjectU3Ek__BackingField_8; }
	inline void set_U3CdraggedGameObjectU3Ek__BackingField_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CdraggedGameObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdraggedGameObjectU3Ek__BackingField_8), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData
struct ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886_marshaled_pinvoke
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	int32_t ___U3CisDraggingU3Ek__BackingField_2;
	float ___U3CpressedTimeU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_4;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke ___U3CpressedRaycastU3Ek__BackingField_5;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_6;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_7;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_8;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData
struct ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886_marshaled_com
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	int32_t ___U3CisDraggingU3Ek__BackingField_2;
	float ___U3CpressedTimeU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_4;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com ___U3CpressedRaycastU3Ek__BackingField_5;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_6;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_7;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_8;
};

// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData
struct ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<hoverTargets>k__BackingField
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pointerTarget>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<isDragging>k__BackingField
	bool ___U3CisDraggingU3Ek__BackingField_2;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pressedTime>k__BackingField
	float ___U3CpressedTimeU3Ek__BackingField_3;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<position>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_4;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pressedPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_5;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pressedRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CpressedRaycastU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pressedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_7;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<pressedGameObjectRaw>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_8;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData::<draggedGameObject>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3ChoverTargetsU3Ek__BackingField_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_U3ChoverTargetsU3Ek__BackingField_0() const { return ___U3ChoverTargetsU3Ek__BackingField_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_U3ChoverTargetsU3Ek__BackingField_0() { return &___U3ChoverTargetsU3Ek__BackingField_0; }
	inline void set_U3ChoverTargetsU3Ek__BackingField_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___U3ChoverTargetsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChoverTargetsU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerTargetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpointerTargetU3Ek__BackingField_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpointerTargetU3Ek__BackingField_1() const { return ___U3CpointerTargetU3Ek__BackingField_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpointerTargetU3Ek__BackingField_1() { return &___U3CpointerTargetU3Ek__BackingField_1; }
	inline void set_U3CpointerTargetU3Ek__BackingField_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpointerTargetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerTargetU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisDraggingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CisDraggingU3Ek__BackingField_2)); }
	inline bool get_U3CisDraggingU3Ek__BackingField_2() const { return ___U3CisDraggingU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CisDraggingU3Ek__BackingField_2() { return &___U3CisDraggingU3Ek__BackingField_2; }
	inline void set_U3CisDraggingU3Ek__BackingField_2(bool value)
	{
		___U3CisDraggingU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CpressedTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpressedTimeU3Ek__BackingField_3)); }
	inline float get_U3CpressedTimeU3Ek__BackingField_3() const { return ___U3CpressedTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CpressedTimeU3Ek__BackingField_3() { return &___U3CpressedTimeU3Ek__BackingField_3; }
	inline void set_U3CpressedTimeU3Ek__BackingField_3(float value)
	{
		___U3CpressedTimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpositionU3Ek__BackingField_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpositionU3Ek__BackingField_4() const { return ___U3CpositionU3Ek__BackingField_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpositionU3Ek__BackingField_4() { return &___U3CpositionU3Ek__BackingField_4; }
	inline void set_U3CpositionU3Ek__BackingField_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpressedPositionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpressedPositionU3Ek__BackingField_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CpressedPositionU3Ek__BackingField_5() const { return ___U3CpressedPositionU3Ek__BackingField_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CpressedPositionU3Ek__BackingField_5() { return &___U3CpressedPositionU3Ek__BackingField_5; }
	inline void set_U3CpressedPositionU3Ek__BackingField_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CpressedPositionU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CpressedRaycastU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpressedRaycastU3Ek__BackingField_6)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CpressedRaycastU3Ek__BackingField_6() const { return ___U3CpressedRaycastU3Ek__BackingField_6; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CpressedRaycastU3Ek__BackingField_6() { return &___U3CpressedRaycastU3Ek__BackingField_6; }
	inline void set_U3CpressedRaycastU3Ek__BackingField_6(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CpressedRaycastU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_6))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpressedRaycastU3Ek__BackingField_6))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpressedGameObjectU3Ek__BackingField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectU3Ek__BackingField_7() const { return ___U3CpressedGameObjectU3Ek__BackingField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectU3Ek__BackingField_7() { return &___U3CpressedGameObjectU3Ek__BackingField_7; }
	inline void set_U3CpressedGameObjectU3Ek__BackingField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CpressedGameObjectRawU3Ek__BackingField_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CpressedGameObjectRawU3Ek__BackingField_8() const { return ___U3CpressedGameObjectRawU3Ek__BackingField_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CpressedGameObjectRawU3Ek__BackingField_8() { return &___U3CpressedGameObjectRawU3Ek__BackingField_8; }
	inline void set_U3CpressedGameObjectRawU3Ek__BackingField_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CpressedGameObjectRawU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpressedGameObjectRawU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6, ___U3CdraggedGameObjectU3Ek__BackingField_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CdraggedGameObjectU3Ek__BackingField_9() const { return ___U3CdraggedGameObjectU3Ek__BackingField_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CdraggedGameObjectU3Ek__BackingField_9() { return &___U3CdraggedGameObjectU3Ek__BackingField_9; }
	inline void set_U3CdraggedGameObjectU3Ek__BackingField_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CdraggedGameObjectU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdraggedGameObjectU3Ek__BackingField_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData
struct ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6_marshaled_pinvoke
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	int32_t ___U3CisDraggingU3Ek__BackingField_2;
	float ___U3CpressedTimeU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_4;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_5;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke ___U3CpressedRaycastU3Ek__BackingField_6;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_7;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_8;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_9;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData
struct ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6_marshaled_com
{
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___U3ChoverTargetsU3Ek__BackingField_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpointerTargetU3Ek__BackingField_1;
	int32_t ___U3CisDraggingU3Ek__BackingField_2;
	float ___U3CpressedTimeU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpositionU3Ek__BackingField_4;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CpressedPositionU3Ek__BackingField_5;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com ___U3CpressedRaycastU3Ek__BackingField_6;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectU3Ek__BackingField_7;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CpressedGameObjectRawU3Ek__BackingField_8;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CdraggedGameObjectU3Ek__BackingField_9;
};

// UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.FallbackComposite`1<UnityEngine.Quaternion>
struct FallbackComposite_1_t582074E1F6238B69BA417654C1966AF9B014903C  : public InputBindingComposite_1_tAD804AA4F39B67288551C933EBAF3E713CA9E7A9
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.FallbackComposite`1<UnityEngine.Vector3>
struct FallbackComposite_1_tB2DC43DF37A87D3BF7654039F6F6B8B51D85DD3B  : public InputBindingComposite_1_t3A5382AF521E7CFF44595C551F17FA4DC78717F4
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.InputSystem.InputControl
struct InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.LowLevel.InputStateBlock UnityEngine.InputSystem.InputControl::m_StateBlock
	InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  ___m_StateBlock_0;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Name
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Name_1;
	// System.String UnityEngine.InputSystem.InputControl::m_Path
	String_t* ___m_Path_2;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayName
	String_t* ___m_DisplayName_3;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayNameFromLayout
	String_t* ___m_DisplayNameFromLayout_4;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayName
	String_t* ___m_ShortDisplayName_5;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayNameFromLayout
	String_t* ___m_ShortDisplayNameFromLayout_6;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Layout
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Layout_7;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Variants
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Variants_8;
	// UnityEngine.InputSystem.InputDevice UnityEngine.InputSystem.InputControl::m_Device
	InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * ___m_Device_9;
	// UnityEngine.InputSystem.InputControl UnityEngine.InputSystem.InputControl::m_Parent
	InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * ___m_Parent_10;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageCount
	int32_t ___m_UsageCount_11;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageStartIndex
	int32_t ___m_UsageStartIndex_12;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasCount
	int32_t ___m_AliasCount_13;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasStartIndex
	int32_t ___m_AliasStartIndex_14;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildCount
	int32_t ___m_ChildCount_15;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildStartIndex
	int32_t ___m_ChildStartIndex_16;
	// UnityEngine.InputSystem.InputControl/ControlFlags UnityEngine.InputSystem.InputControl::m_ControlFlags
	int32_t ___m_ControlFlags_17;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_DefaultState
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_DefaultState_18;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MinValue
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_MinValue_19;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MaxValue
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_MaxValue_20;

public:
	inline static int32_t get_offset_of_m_StateBlock_0() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_StateBlock_0)); }
	inline InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  get_m_StateBlock_0() const { return ___m_StateBlock_0; }
	inline InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C * get_address_of_m_StateBlock_0() { return &___m_StateBlock_0; }
	inline void set_m_StateBlock_0(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  value)
	{
		___m_StateBlock_0 = value;
	}

	inline static int32_t get_offset_of_m_Name_1() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Name_1)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Name_1() const { return ___m_Name_1; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Name_1() { return &___m_Name_1; }
	inline void set_m_Name_1(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Path_2)); }
	inline String_t* get_m_Path_2() const { return ___m_Path_2; }
	inline String_t** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(String_t* value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayName_3() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DisplayName_3)); }
	inline String_t* get_m_DisplayName_3() const { return ___m_DisplayName_3; }
	inline String_t** get_address_of_m_DisplayName_3() { return &___m_DisplayName_3; }
	inline void set_m_DisplayName_3(String_t* value)
	{
		___m_DisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayName_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayNameFromLayout_4() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DisplayNameFromLayout_4)); }
	inline String_t* get_m_DisplayNameFromLayout_4() const { return ___m_DisplayNameFromLayout_4; }
	inline String_t** get_address_of_m_DisplayNameFromLayout_4() { return &___m_DisplayNameFromLayout_4; }
	inline void set_m_DisplayNameFromLayout_4(String_t* value)
	{
		___m_DisplayNameFromLayout_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayNameFromLayout_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayName_5() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ShortDisplayName_5)); }
	inline String_t* get_m_ShortDisplayName_5() const { return ___m_ShortDisplayName_5; }
	inline String_t** get_address_of_m_ShortDisplayName_5() { return &___m_ShortDisplayName_5; }
	inline void set_m_ShortDisplayName_5(String_t* value)
	{
		___m_ShortDisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayNameFromLayout_6() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ShortDisplayNameFromLayout_6)); }
	inline String_t* get_m_ShortDisplayNameFromLayout_6() const { return ___m_ShortDisplayNameFromLayout_6; }
	inline String_t** get_address_of_m_ShortDisplayNameFromLayout_6() { return &___m_ShortDisplayNameFromLayout_6; }
	inline void set_m_ShortDisplayNameFromLayout_6(String_t* value)
	{
		___m_ShortDisplayNameFromLayout_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayNameFromLayout_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Layout_7() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Layout_7)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Layout_7() const { return ___m_Layout_7; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Layout_7() { return &___m_Layout_7; }
	inline void set_m_Layout_7(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Layout_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Variants_8() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Variants_8)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Variants_8() const { return ___m_Variants_8; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Variants_8() { return &___m_Variants_8; }
	inline void set_m_Variants_8(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Variants_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Device_9() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Device_9)); }
	inline InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * get_m_Device_9() const { return ___m_Device_9; }
	inline InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 ** get_address_of_m_Device_9() { return &___m_Device_9; }
	inline void set_m_Device_9(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * value)
	{
		___m_Device_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Device_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Parent_10() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Parent_10)); }
	inline InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * get_m_Parent_10() const { return ___m_Parent_10; }
	inline InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 ** get_address_of_m_Parent_10() { return &___m_Parent_10; }
	inline void set_m_Parent_10(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * value)
	{
		___m_Parent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Parent_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsageCount_11() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_UsageCount_11)); }
	inline int32_t get_m_UsageCount_11() const { return ___m_UsageCount_11; }
	inline int32_t* get_address_of_m_UsageCount_11() { return &___m_UsageCount_11; }
	inline void set_m_UsageCount_11(int32_t value)
	{
		___m_UsageCount_11 = value;
	}

	inline static int32_t get_offset_of_m_UsageStartIndex_12() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_UsageStartIndex_12)); }
	inline int32_t get_m_UsageStartIndex_12() const { return ___m_UsageStartIndex_12; }
	inline int32_t* get_address_of_m_UsageStartIndex_12() { return &___m_UsageStartIndex_12; }
	inline void set_m_UsageStartIndex_12(int32_t value)
	{
		___m_UsageStartIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_AliasCount_13() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_AliasCount_13)); }
	inline int32_t get_m_AliasCount_13() const { return ___m_AliasCount_13; }
	inline int32_t* get_address_of_m_AliasCount_13() { return &___m_AliasCount_13; }
	inline void set_m_AliasCount_13(int32_t value)
	{
		___m_AliasCount_13 = value;
	}

	inline static int32_t get_offset_of_m_AliasStartIndex_14() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_AliasStartIndex_14)); }
	inline int32_t get_m_AliasStartIndex_14() const { return ___m_AliasStartIndex_14; }
	inline int32_t* get_address_of_m_AliasStartIndex_14() { return &___m_AliasStartIndex_14; }
	inline void set_m_AliasStartIndex_14(int32_t value)
	{
		___m_AliasStartIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildCount_15() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ChildCount_15)); }
	inline int32_t get_m_ChildCount_15() const { return ___m_ChildCount_15; }
	inline int32_t* get_address_of_m_ChildCount_15() { return &___m_ChildCount_15; }
	inline void set_m_ChildCount_15(int32_t value)
	{
		___m_ChildCount_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildStartIndex_16() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ChildStartIndex_16)); }
	inline int32_t get_m_ChildStartIndex_16() const { return ___m_ChildStartIndex_16; }
	inline int32_t* get_address_of_m_ChildStartIndex_16() { return &___m_ChildStartIndex_16; }
	inline void set_m_ChildStartIndex_16(int32_t value)
	{
		___m_ChildStartIndex_16 = value;
	}

	inline static int32_t get_offset_of_m_ControlFlags_17() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ControlFlags_17)); }
	inline int32_t get_m_ControlFlags_17() const { return ___m_ControlFlags_17; }
	inline int32_t* get_address_of_m_ControlFlags_17() { return &___m_ControlFlags_17; }
	inline void set_m_ControlFlags_17(int32_t value)
	{
		___m_ControlFlags_17 = value;
	}

	inline static int32_t get_offset_of_m_DefaultState_18() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DefaultState_18)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_DefaultState_18() const { return ___m_DefaultState_18; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_DefaultState_18() { return &___m_DefaultState_18; }
	inline void set_m_DefaultState_18(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_DefaultState_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_MinValue_19)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_MaxValue_20)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_MaxValue_20 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel
struct JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93 
{
public:
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::<move>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CmoveU3Ek__BackingField_0;
	// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::<submitButtonDelta>k__BackingField
	int32_t ___U3CsubmitButtonDeltaU3Ek__BackingField_1;
	// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::<cancelButtonDelta>k__BackingField
	int32_t ___U3CcancelButtonDeltaU3Ek__BackingField_2;
	// UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel/ImplementationData UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::<implementationData>k__BackingField
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B  ___U3CimplementationDataU3Ek__BackingField_3;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::m_SubmitButtonDown
	bool ___m_SubmitButtonDown_4;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel::m_CancelButtonDown
	bool ___m_CancelButtonDown_5;

public:
	inline static int32_t get_offset_of_U3CmoveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___U3CmoveU3Ek__BackingField_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CmoveU3Ek__BackingField_0() const { return ___U3CmoveU3Ek__BackingField_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CmoveU3Ek__BackingField_0() { return &___U3CmoveU3Ek__BackingField_0; }
	inline void set_U3CmoveU3Ek__BackingField_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CmoveU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CsubmitButtonDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___U3CsubmitButtonDeltaU3Ek__BackingField_1)); }
	inline int32_t get_U3CsubmitButtonDeltaU3Ek__BackingField_1() const { return ___U3CsubmitButtonDeltaU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CsubmitButtonDeltaU3Ek__BackingField_1() { return &___U3CsubmitButtonDeltaU3Ek__BackingField_1; }
	inline void set_U3CsubmitButtonDeltaU3Ek__BackingField_1(int32_t value)
	{
		___U3CsubmitButtonDeltaU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CcancelButtonDeltaU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___U3CcancelButtonDeltaU3Ek__BackingField_2)); }
	inline int32_t get_U3CcancelButtonDeltaU3Ek__BackingField_2() const { return ___U3CcancelButtonDeltaU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcancelButtonDeltaU3Ek__BackingField_2() { return &___U3CcancelButtonDeltaU3Ek__BackingField_2; }
	inline void set_U3CcancelButtonDeltaU3Ek__BackingField_2(int32_t value)
	{
		___U3CcancelButtonDeltaU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CimplementationDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___U3CimplementationDataU3Ek__BackingField_3)); }
	inline ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B  get_U3CimplementationDataU3Ek__BackingField_3() const { return ___U3CimplementationDataU3Ek__BackingField_3; }
	inline ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B * get_address_of_U3CimplementationDataU3Ek__BackingField_3() { return &___U3CimplementationDataU3Ek__BackingField_3; }
	inline void set_U3CimplementationDataU3Ek__BackingField_3(ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B  value)
	{
		___U3CimplementationDataU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_SubmitButtonDown_4() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___m_SubmitButtonDown_4)); }
	inline bool get_m_SubmitButtonDown_4() const { return ___m_SubmitButtonDown_4; }
	inline bool* get_address_of_m_SubmitButtonDown_4() { return &___m_SubmitButtonDown_4; }
	inline void set_m_SubmitButtonDown_4(bool value)
	{
		___m_SubmitButtonDown_4 = value;
	}

	inline static int32_t get_offset_of_m_CancelButtonDown_5() { return static_cast<int32_t>(offsetof(JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93, ___m_CancelButtonDown_5)); }
	inline bool get_m_CancelButtonDown_5() const { return ___m_CancelButtonDown_5; }
	inline bool* get_address_of_m_CancelButtonDown_5() { return &___m_CancelButtonDown_5; }
	inline void set_m_CancelButtonDown_5(bool value)
	{
		___m_CancelButtonDown_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel
struct JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93_marshaled_pinvoke
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CmoveU3Ek__BackingField_0;
	int32_t ___U3CsubmitButtonDeltaU3Ek__BackingField_1;
	int32_t ___U3CcancelButtonDeltaU3Ek__BackingField_2;
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B  ___U3CimplementationDataU3Ek__BackingField_3;
	int32_t ___m_SubmitButtonDown_4;
	int32_t ___m_CancelButtonDown_5;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.JoystickModel
struct JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93_marshaled_com
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CmoveU3Ek__BackingField_0;
	int32_t ___U3CsubmitButtonDeltaU3Ek__BackingField_1;
	int32_t ___U3CcancelButtonDeltaU3Ek__BackingField_2;
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B  ___U3CimplementationDataU3Ek__BackingField_3;
	int32_t ___m_SubmitButtonDown_4;
	int32_t ___m_CancelButtonDown_5;
};

// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel
struct MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818 
{
public:
	// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel::<lastFrameDelta>k__BackingField
	int32_t ___U3ClastFrameDeltaU3Ek__BackingField_0;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel::m_IsDown
	bool ___m_IsDown_1;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel/ImplementationData UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel::m_ImplementationData
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98  ___m_ImplementationData_2;

public:
	inline static int32_t get_offset_of_U3ClastFrameDeltaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818, ___U3ClastFrameDeltaU3Ek__BackingField_0)); }
	inline int32_t get_U3ClastFrameDeltaU3Ek__BackingField_0() const { return ___U3ClastFrameDeltaU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClastFrameDeltaU3Ek__BackingField_0() { return &___U3ClastFrameDeltaU3Ek__BackingField_0; }
	inline void set_U3ClastFrameDeltaU3Ek__BackingField_0(int32_t value)
	{
		___U3ClastFrameDeltaU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_IsDown_1() { return static_cast<int32_t>(offsetof(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818, ___m_IsDown_1)); }
	inline bool get_m_IsDown_1() const { return ___m_IsDown_1; }
	inline bool* get_address_of_m_IsDown_1() { return &___m_IsDown_1; }
	inline void set_m_IsDown_1(bool value)
	{
		___m_IsDown_1 = value;
	}

	inline static int32_t get_offset_of_m_ImplementationData_2() { return static_cast<int32_t>(offsetof(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818, ___m_ImplementationData_2)); }
	inline ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98  get_m_ImplementationData_2() const { return ___m_ImplementationData_2; }
	inline ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98 * get_address_of_m_ImplementationData_2() { return &___m_ImplementationData_2; }
	inline void set_m_ImplementationData_2(ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98  value)
	{
		___m_ImplementationData_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel
struct MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_pinvoke
{
	int32_t ___U3ClastFrameDeltaU3Ek__BackingField_0;
	int32_t ___m_IsDown_1;
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98_marshaled_pinvoke ___m_ImplementationData_2;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel
struct MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_com
{
	int32_t ___U3ClastFrameDeltaU3Ek__BackingField_0;
	int32_t ___m_IsDown_1;
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98_marshaled_com ___m_ImplementationData_2;
};

// UnityEngine.XR.OpenXR.Features.OpenXRFeature
struct OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::m_enabled
	bool ___m_enabled_4;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::<failedInitialization>k__BackingField
	bool ___U3CfailedInitializationU3Ek__BackingField_5;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::nameUi
	String_t* ___nameUi_7;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::version
	String_t* ___version_8;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::featureIdInternal
	String_t* ___featureIdInternal_9;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::openxrExtensionStrings
	String_t* ___openxrExtensionStrings_10;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::company
	String_t* ___company_11;
	// System.Int32 UnityEngine.XR.OpenXR.Features.OpenXRFeature::priority
	int32_t ___priority_12;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::required
	bool ___required_13;

public:
	inline static int32_t get_offset_of_m_enabled_4() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___m_enabled_4)); }
	inline bool get_m_enabled_4() const { return ___m_enabled_4; }
	inline bool* get_address_of_m_enabled_4() { return &___m_enabled_4; }
	inline void set_m_enabled_4(bool value)
	{
		___m_enabled_4 = value;
	}

	inline static int32_t get_offset_of_U3CfailedInitializationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___U3CfailedInitializationU3Ek__BackingField_5)); }
	inline bool get_U3CfailedInitializationU3Ek__BackingField_5() const { return ___U3CfailedInitializationU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CfailedInitializationU3Ek__BackingField_5() { return &___U3CfailedInitializationU3Ek__BackingField_5; }
	inline void set_U3CfailedInitializationU3Ek__BackingField_5(bool value)
	{
		___U3CfailedInitializationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_nameUi_7() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___nameUi_7)); }
	inline String_t* get_nameUi_7() const { return ___nameUi_7; }
	inline String_t** get_address_of_nameUi_7() { return &___nameUi_7; }
	inline void set_nameUi_7(String_t* value)
	{
		___nameUi_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameUi_7), (void*)value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___version_8), (void*)value);
	}

	inline static int32_t get_offset_of_featureIdInternal_9() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___featureIdInternal_9)); }
	inline String_t* get_featureIdInternal_9() const { return ___featureIdInternal_9; }
	inline String_t** get_address_of_featureIdInternal_9() { return &___featureIdInternal_9; }
	inline void set_featureIdInternal_9(String_t* value)
	{
		___featureIdInternal_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___featureIdInternal_9), (void*)value);
	}

	inline static int32_t get_offset_of_openxrExtensionStrings_10() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___openxrExtensionStrings_10)); }
	inline String_t* get_openxrExtensionStrings_10() const { return ___openxrExtensionStrings_10; }
	inline String_t** get_address_of_openxrExtensionStrings_10() { return &___openxrExtensionStrings_10; }
	inline void set_openxrExtensionStrings_10(String_t* value)
	{
		___openxrExtensionStrings_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___openxrExtensionStrings_10), (void*)value);
	}

	inline static int32_t get_offset_of_company_11() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___company_11)); }
	inline String_t* get_company_11() const { return ___company_11; }
	inline String_t** get_address_of_company_11() { return &___company_11; }
	inline void set_company_11(String_t* value)
	{
		___company_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___company_11), (void*)value);
	}

	inline static int32_t get_offset_of_priority_12() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___priority_12)); }
	inline int32_t get_priority_12() const { return ___priority_12; }
	inline int32_t* get_address_of_priority_12() { return &___priority_12; }
	inline void set_priority_12(int32_t value)
	{
		___priority_12 = value;
	}

	inline static int32_t get_offset_of_required_13() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA, ___required_13)); }
	inline bool get_required_13() const { return ___required_13; }
	inline bool* get_address_of_required_13() { return &___required_13; }
	inline void set_required_13(bool value)
	{
		___required_13 = value;
	}
};

struct OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA_StaticFields
{
public:
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::<requiredFeatureFailed>k__BackingField
	bool ___U3CrequiredFeatureFailedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CrequiredFeatureFailedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA_StaticFields, ___U3CrequiredFeatureFailedU3Ek__BackingField_6)); }
	inline bool get_U3CrequiredFeatureFailedU3Ek__BackingField_6() const { return ___U3CrequiredFeatureFailedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CrequiredFeatureFailedU3Ek__BackingField_6() { return &___U3CrequiredFeatureFailedU3Ek__BackingField_6; }
	inline void set_U3CrequiredFeatureFailedU3Ek__BackingField_6(bool value)
	{
		___U3CrequiredFeatureFailedU3Ek__BackingField_6 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase
struct ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};

struct ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields
{
public:
	// System.Char[] UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase::k_PathTrimChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___k_PathTrimChars_9;
	// System.Char[] UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase::k_InvalidCharacters
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___k_InvalidCharacters_10;
	// System.String[] UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase::k_InvalidStrings
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___k_InvalidStrings_11;

public:
	inline static int32_t get_offset_of_k_PathTrimChars_9() { return static_cast<int32_t>(offsetof(ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields, ___k_PathTrimChars_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_k_PathTrimChars_9() const { return ___k_PathTrimChars_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_k_PathTrimChars_9() { return &___k_PathTrimChars_9; }
	inline void set_k_PathTrimChars_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___k_PathTrimChars_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_PathTrimChars_9), (void*)value);
	}

	inline static int32_t get_offset_of_k_InvalidCharacters_10() { return static_cast<int32_t>(offsetof(ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields, ___k_InvalidCharacters_10)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_k_InvalidCharacters_10() const { return ___k_InvalidCharacters_10; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_k_InvalidCharacters_10() { return &___k_InvalidCharacters_10; }
	inline void set_k_InvalidCharacters_10(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___k_InvalidCharacters_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_InvalidCharacters_10), (void*)value);
	}

	inline static int32_t get_offset_of_k_InvalidStrings_11() { return static_cast<int32_t>(offsetof(ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields, ___k_InvalidStrings_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_k_InvalidStrings_11() const { return ___k_InvalidStrings_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_k_InvalidStrings_11() { return &___k_InvalidStrings_11; }
	inline void set_k_InvalidStrings_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___k_InvalidStrings_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_InvalidStrings_11), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.TeleportingEventArgs
struct TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D  : public BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE
{
public:
	// UnityEngine.XR.Interaction.Toolkit.TeleportRequest UnityEngine.XR.Interaction.Toolkit.TeleportingEventArgs::<teleportRequest>k__BackingField
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  ___U3CteleportRequestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CteleportRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D, ___U3CteleportRequestU3Ek__BackingField_2)); }
	inline TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  get_U3CteleportRequestU3Ek__BackingField_2() const { return ___U3CteleportRequestU3Ek__BackingField_2; }
	inline TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2 * get_address_of_U3CteleportRequestU3Ek__BackingField_2() { return &___U3CteleportRequestU3Ek__BackingField_2; }
	inline void set_U3CteleportRequestU3Ek__BackingField_2(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  value)
	{
		___U3CteleportRequestU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.TouchModel
struct TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::<selectDelta>k__BackingField
	int32_t ___U3CselectDeltaU3Ek__BackingField_1;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::<changedThisFrame>k__BackingField
	bool ___U3CchangedThisFrameU3Ek__BackingField_2;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::<deltaPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	// UnityEngine.TouchPhase UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::m_SelectPhase
	int32_t ___m_SelectPhase_4;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::m_Position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_5;
	// UnityEngine.XR.Interaction.Toolkit.UI.TouchModel/ImplementationData UnityEngine.XR.Interaction.Toolkit.UI.TouchModel::m_ImplementationData
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886  ___m_ImplementationData_6;

public:
	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___U3CpointerIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_0() const { return ___U3CpointerIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_0() { return &___U3CpointerIdU3Ek__BackingField_0; }
	inline void set_U3CpointerIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CselectDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___U3CselectDeltaU3Ek__BackingField_1)); }
	inline int32_t get_U3CselectDeltaU3Ek__BackingField_1() const { return ___U3CselectDeltaU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CselectDeltaU3Ek__BackingField_1() { return &___U3CselectDeltaU3Ek__BackingField_1; }
	inline void set_U3CselectDeltaU3Ek__BackingField_1(int32_t value)
	{
		___U3CselectDeltaU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CchangedThisFrameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___U3CchangedThisFrameU3Ek__BackingField_2)); }
	inline bool get_U3CchangedThisFrameU3Ek__BackingField_2() const { return ___U3CchangedThisFrameU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CchangedThisFrameU3Ek__BackingField_2() { return &___U3CchangedThisFrameU3Ek__BackingField_2; }
	inline void set_U3CchangedThisFrameU3Ek__BackingField_2(bool value)
	{
		___U3CchangedThisFrameU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___U3CdeltaPositionU3Ek__BackingField_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaPositionU3Ek__BackingField_3() const { return ___U3CdeltaPositionU3Ek__BackingField_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaPositionU3Ek__BackingField_3() { return &___U3CdeltaPositionU3Ek__BackingField_3; }
	inline void set_U3CdeltaPositionU3Ek__BackingField_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_SelectPhase_4() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___m_SelectPhase_4)); }
	inline int32_t get_m_SelectPhase_4() const { return ___m_SelectPhase_4; }
	inline int32_t* get_address_of_m_SelectPhase_4() { return &___m_SelectPhase_4; }
	inline void set_m_SelectPhase_4(int32_t value)
	{
		___m_SelectPhase_4 = value;
	}

	inline static int32_t get_offset_of_m_Position_5() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___m_Position_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Position_5() const { return ___m_Position_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Position_5() { return &___m_Position_5; }
	inline void set_m_Position_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Position_5 = value;
	}

	inline static int32_t get_offset_of_m_ImplementationData_6() { return static_cast<int32_t>(offsetof(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984, ___m_ImplementationData_6)); }
	inline ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886  get_m_ImplementationData_6() const { return ___m_ImplementationData_6; }
	inline ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886 * get_address_of_m_ImplementationData_6() { return &___m_ImplementationData_6; }
	inline void set_m_ImplementationData_6(ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886  value)
	{
		___m_ImplementationData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_6))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_6))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_6))->___U3CpressedRaycastU3Ek__BackingField_5))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_6))->___U3CpressedRaycastU3Ek__BackingField_5))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_6))->___U3CpressedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_6))->___U3CpressedGameObjectRawU3Ek__BackingField_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_6))->___U3CdraggedGameObjectU3Ek__BackingField_8), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TouchModel
struct TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984_marshaled_pinvoke
{
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	int32_t ___U3CselectDeltaU3Ek__BackingField_1;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_2;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	int32_t ___m_SelectPhase_4;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_5;
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886_marshaled_pinvoke ___m_ImplementationData_6;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TouchModel
struct TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984_marshaled_com
{
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	int32_t ___U3CselectDeltaU3Ek__BackingField_1;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_2;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	int32_t ___m_SelectPhase_4;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_5;
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886_marshaled_com ___m_ImplementationData_6;
};

// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData
struct TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63  : public PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData::<rayPoints>k__BackingField
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___U3CrayPointsU3Ek__BackingField_24;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData::<rayHitIndex>k__BackingField
	int32_t ___U3CrayHitIndexU3Ek__BackingField_25;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData::<layerMask>k__BackingField
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___U3ClayerMaskU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_U3CrayPointsU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63, ___U3CrayPointsU3Ek__BackingField_24)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_U3CrayPointsU3Ek__BackingField_24() const { return ___U3CrayPointsU3Ek__BackingField_24; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_U3CrayPointsU3Ek__BackingField_24() { return &___U3CrayPointsU3Ek__BackingField_24; }
	inline void set_U3CrayPointsU3Ek__BackingField_24(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___U3CrayPointsU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrayPointsU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrayHitIndexU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63, ___U3CrayHitIndexU3Ek__BackingField_25)); }
	inline int32_t get_U3CrayHitIndexU3Ek__BackingField_25() const { return ___U3CrayHitIndexU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CrayHitIndexU3Ek__BackingField_25() { return &___U3CrayHitIndexU3Ek__BackingField_25; }
	inline void set_U3CrayHitIndexU3Ek__BackingField_25(int32_t value)
	{
		___U3CrayHitIndexU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3ClayerMaskU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63, ___U3ClayerMaskU3Ek__BackingField_26)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_U3ClayerMaskU3Ek__BackingField_26() const { return ___U3ClayerMaskU3Ek__BackingField_26; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_U3ClayerMaskU3Ek__BackingField_26() { return &___U3ClayerMaskU3Ek__BackingField_26; }
	inline void set_U3ClayerMaskU3Ek__BackingField_26(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___U3ClayerMaskU3Ek__BackingField_26 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel
struct TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494 
{
public:
	// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel/ImplementationData UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_ImplementationData
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6  ___m_ImplementationData_1;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_2;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_SelectDown
	bool ___m_SelectDown_3;
	// UnityEngine.XR.Interaction.Toolkit.UI.ButtonDeltaState UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::<selectDelta>k__BackingField
	int32_t ___U3CselectDeltaU3Ek__BackingField_4;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::<changedThisFrame>k__BackingField
	bool ___U3CchangedThisFrameU3Ek__BackingField_5;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_Position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Position_6;
	// UnityEngine.Quaternion UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_Orientation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___m_Orientation_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_RaycastPoints
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_RaycastPoints_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::<currentRaycast>k__BackingField
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  ___U3CcurrentRaycastU3Ek__BackingField_9;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::<currentRaycastEndpointIndex>k__BackingField
	int32_t ___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_RaycastLayerMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_RaycastLayerMask_11;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_ScrollDelta
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_12;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel::m_MaxRaycastDistance
	float ___m_MaxRaycastDistance_13;

public:
	inline static int32_t get_offset_of_m_ImplementationData_1() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_ImplementationData_1)); }
	inline ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6  get_m_ImplementationData_1() const { return ___m_ImplementationData_1; }
	inline ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6 * get_address_of_m_ImplementationData_1() { return &___m_ImplementationData_1; }
	inline void set_m_ImplementationData_1(ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6  value)
	{
		___m_ImplementationData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_1))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_1))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_1))->___U3CpressedRaycastU3Ek__BackingField_6))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ImplementationData_1))->___U3CpressedRaycastU3Ek__BackingField_6))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_1))->___U3CpressedGameObjectU3Ek__BackingField_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_1))->___U3CpressedGameObjectRawU3Ek__BackingField_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ImplementationData_1))->___U3CdraggedGameObjectU3Ek__BackingField_9), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___U3CpointerIdU3Ek__BackingField_2)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_2() const { return ___U3CpointerIdU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_2() { return &___U3CpointerIdU3Ek__BackingField_2; }
	inline void set_U3CpointerIdU3Ek__BackingField_2(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectDown_3() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_SelectDown_3)); }
	inline bool get_m_SelectDown_3() const { return ___m_SelectDown_3; }
	inline bool* get_address_of_m_SelectDown_3() { return &___m_SelectDown_3; }
	inline void set_m_SelectDown_3(bool value)
	{
		___m_SelectDown_3 = value;
	}

	inline static int32_t get_offset_of_U3CselectDeltaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___U3CselectDeltaU3Ek__BackingField_4)); }
	inline int32_t get_U3CselectDeltaU3Ek__BackingField_4() const { return ___U3CselectDeltaU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CselectDeltaU3Ek__BackingField_4() { return &___U3CselectDeltaU3Ek__BackingField_4; }
	inline void set_U3CselectDeltaU3Ek__BackingField_4(int32_t value)
	{
		___U3CselectDeltaU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CchangedThisFrameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___U3CchangedThisFrameU3Ek__BackingField_5)); }
	inline bool get_U3CchangedThisFrameU3Ek__BackingField_5() const { return ___U3CchangedThisFrameU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CchangedThisFrameU3Ek__BackingField_5() { return &___U3CchangedThisFrameU3Ek__BackingField_5; }
	inline void set_U3CchangedThisFrameU3Ek__BackingField_5(bool value)
	{
		___U3CchangedThisFrameU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_m_Position_6() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_Position_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Position_6() const { return ___m_Position_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Position_6() { return &___m_Position_6; }
	inline void set_m_Position_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Position_6 = value;
	}

	inline static int32_t get_offset_of_m_Orientation_7() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_Orientation_7)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_m_Orientation_7() const { return ___m_Orientation_7; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_m_Orientation_7() { return &___m_Orientation_7; }
	inline void set_m_Orientation_7(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___m_Orientation_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPoints_8() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_RaycastPoints_8)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_RaycastPoints_8() const { return ___m_RaycastPoints_8; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_RaycastPoints_8() { return &___m_RaycastPoints_8; }
	inline void set_m_RaycastPoints_8(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_RaycastPoints_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastPoints_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrentRaycastU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___U3CcurrentRaycastU3Ek__BackingField_9)); }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  get_U3CcurrentRaycastU3Ek__BackingField_9() const { return ___U3CcurrentRaycastU3Ek__BackingField_9; }
	inline RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE * get_address_of_U3CcurrentRaycastU3Ek__BackingField_9() { return &___U3CcurrentRaycastU3Ek__BackingField_9; }
	inline void set_U3CcurrentRaycastU3Ek__BackingField_9(RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE  value)
	{
		___U3CcurrentRaycastU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CcurrentRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CcurrentRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10)); }
	inline int32_t get_U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10() const { return ___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10() { return &___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10; }
	inline void set_U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10(int32_t value)
	{
		___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastLayerMask_11() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_RaycastLayerMask_11)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_RaycastLayerMask_11() const { return ___m_RaycastLayerMask_11; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_RaycastLayerMask_11() { return &___m_RaycastLayerMask_11; }
	inline void set_m_RaycastLayerMask_11(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_RaycastLayerMask_11 = value;
	}

	inline static int32_t get_offset_of_m_ScrollDelta_12() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_ScrollDelta_12)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_ScrollDelta_12() const { return ___m_ScrollDelta_12; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_ScrollDelta_12() { return &___m_ScrollDelta_12; }
	inline void set_m_ScrollDelta_12(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_ScrollDelta_12 = value;
	}

	inline static int32_t get_offset_of_m_MaxRaycastDistance_13() { return static_cast<int32_t>(offsetof(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494, ___m_MaxRaycastDistance_13)); }
	inline float get_m_MaxRaycastDistance_13() const { return ___m_MaxRaycastDistance_13; }
	inline float* get_address_of_m_MaxRaycastDistance_13() { return &___m_MaxRaycastDistance_13; }
	inline void set_m_MaxRaycastDistance_13(float value)
	{
		___m_MaxRaycastDistance_13 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel
struct TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494_marshaled_pinvoke
{
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6_marshaled_pinvoke ___m_ImplementationData_1;
	int32_t ___U3CpointerIdU3Ek__BackingField_2;
	int32_t ___m_SelectDown_3;
	int32_t ___U3CselectDeltaU3Ek__BackingField_4;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_5;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Position_6;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___m_Orientation_7;
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_RaycastPoints_8;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_pinvoke ___U3CcurrentRaycastU3Ek__BackingField_9;
	int32_t ___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10;
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_RaycastLayerMask_11;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_12;
	float ___m_MaxRaycastDistance_13;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel
struct TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494_marshaled_com
{
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6_marshaled_com ___m_ImplementationData_1;
	int32_t ___U3CpointerIdU3Ek__BackingField_2;
	int32_t ___m_SelectDown_3;
	int32_t ___U3CselectDeltaU3Ek__BackingField_4;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_5;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Position_6;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___m_Orientation_7;
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_RaycastPoints_8;
	RaycastResult_t9EFDE24B29650BD6DC8A49D954A3769E17146BCE_marshaled_com ___U3CcurrentRaycastU3Ek__BackingField_9;
	int32_t ___U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10;
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_RaycastLayerMask_11;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_12;
	float ___m_MaxRaycastDistance_13;
};

// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/ScriptEventDelegate
struct ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase`1<UnityEngine.XR.Interaction.Toolkit.InteractionLayerSettings>
struct ScriptableSettingsBase_1_tEBF32352180DD61FDD6EA7D4906BD5AA1F6741BA  : public ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2
{
public:

public:
};

struct ScriptableSettingsBase_1_tEBF32352180DD61FDD6EA7D4906BD5AA1F6741BA_StaticFields
{
public:
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase`1::k_HasCustomPath
	bool ___k_HasCustomPath_12;
	// T UnityEngine.XR.Interaction.Toolkit.Utilities.Internal.ScriptableSettingsBase`1::s_Instance
	InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600 * ___s_Instance_13;

public:
	inline static int32_t get_offset_of_k_HasCustomPath_12() { return static_cast<int32_t>(offsetof(ScriptableSettingsBase_1_tEBF32352180DD61FDD6EA7D4906BD5AA1F6741BA_StaticFields, ___k_HasCustomPath_12)); }
	inline bool get_k_HasCustomPath_12() const { return ___k_HasCustomPath_12; }
	inline bool* get_address_of_k_HasCustomPath_12() { return &___k_HasCustomPath_12; }
	inline void set_k_HasCustomPath_12(bool value)
	{
		___k_HasCustomPath_12 = value;
	}

	inline static int32_t get_offset_of_s_Instance_13() { return static_cast<int32_t>(offsetof(ScriptableSettingsBase_1_tEBF32352180DD61FDD6EA7D4906BD5AA1F6741BA_StaticFields, ___s_Instance_13)); }
	inline InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600 * get_s_Instance_13() const { return ___s_Instance_13; }
	inline InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600 ** get_address_of_s_Instance_13() { return &___s_Instance_13; }
	inline void set_s_Instance_13(InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600 * value)
	{
		___s_Instance_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Instance_13), (void*)value);
	}
};


// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature
struct ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9  : public OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA
{
public:

public:
};

struct ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9_StaticFields
{
public:
	// System.UInt64 UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature::xrInstance
	uint64_t ___xrInstance_16;
	// System.UInt64 UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature::xrSession
	uint64_t ___xrSession_17;

public:
	inline static int32_t get_offset_of_xrInstance_16() { return static_cast<int32_t>(offsetof(ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9_StaticFields, ___xrInstance_16)); }
	inline uint64_t get_xrInstance_16() const { return ___xrInstance_16; }
	inline uint64_t* get_address_of_xrInstance_16() { return &___xrInstance_16; }
	inline void set_xrInstance_16(uint64_t value)
	{
		___xrInstance_16 = value;
	}

	inline static int32_t get_offset_of_xrSession_17() { return static_cast<int32_t>(offsetof(ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9_StaticFields, ___xrSession_17)); }
	inline uint64_t get_xrSession_17() const { return ___xrSession_17; }
	inline uint64_t* get_address_of_xrSession_17() { return &___xrSession_17; }
	inline void set_xrSession_17(uint64_t value)
	{
		___xrSession_17 = value;
	}
};


// UnityEngine.InputSystem.InputDevice
struct InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154  : public InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713
{
public:
	// UnityEngine.InputSystem.InputDevice/DeviceFlags UnityEngine.InputSystem.InputDevice::m_DeviceFlags
	int32_t ___m_DeviceFlags_24;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_DeviceId
	int32_t ___m_DeviceId_25;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_ParticipantId
	int32_t ___m_ParticipantId_26;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_DeviceIndex
	int32_t ___m_DeviceIndex_27;
	// UnityEngine.InputSystem.Layouts.InputDeviceDescription UnityEngine.InputSystem.InputDevice::m_Description
	InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA  ___m_Description_28;
	// System.Double UnityEngine.InputSystem.InputDevice::m_LastUpdateTimeInternal
	double ___m_LastUpdateTimeInternal_29;
	// System.UInt32 UnityEngine.InputSystem.InputDevice::m_CurrentUpdateStepCount
	uint32_t ___m_CurrentUpdateStepCount_30;
	// UnityEngine.InputSystem.Utilities.InternedString[] UnityEngine.InputSystem.InputDevice::m_AliasesForEachControl
	InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* ___m_AliasesForEachControl_31;
	// UnityEngine.InputSystem.Utilities.InternedString[] UnityEngine.InputSystem.InputDevice::m_UsagesForEachControl
	InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* ___m_UsagesForEachControl_32;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputDevice::m_UsageToControl
	InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* ___m_UsageToControl_33;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputDevice::m_ChildrenForEachControl
	InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* ___m_ChildrenForEachControl_34;
	// System.UInt32[] UnityEngine.InputSystem.InputDevice::m_StateOffsetToControlMap
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___m_StateOffsetToControlMap_35;

public:
	inline static int32_t get_offset_of_m_DeviceFlags_24() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_DeviceFlags_24)); }
	inline int32_t get_m_DeviceFlags_24() const { return ___m_DeviceFlags_24; }
	inline int32_t* get_address_of_m_DeviceFlags_24() { return &___m_DeviceFlags_24; }
	inline void set_m_DeviceFlags_24(int32_t value)
	{
		___m_DeviceFlags_24 = value;
	}

	inline static int32_t get_offset_of_m_DeviceId_25() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_DeviceId_25)); }
	inline int32_t get_m_DeviceId_25() const { return ___m_DeviceId_25; }
	inline int32_t* get_address_of_m_DeviceId_25() { return &___m_DeviceId_25; }
	inline void set_m_DeviceId_25(int32_t value)
	{
		___m_DeviceId_25 = value;
	}

	inline static int32_t get_offset_of_m_ParticipantId_26() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_ParticipantId_26)); }
	inline int32_t get_m_ParticipantId_26() const { return ___m_ParticipantId_26; }
	inline int32_t* get_address_of_m_ParticipantId_26() { return &___m_ParticipantId_26; }
	inline void set_m_ParticipantId_26(int32_t value)
	{
		___m_ParticipantId_26 = value;
	}

	inline static int32_t get_offset_of_m_DeviceIndex_27() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_DeviceIndex_27)); }
	inline int32_t get_m_DeviceIndex_27() const { return ___m_DeviceIndex_27; }
	inline int32_t* get_address_of_m_DeviceIndex_27() { return &___m_DeviceIndex_27; }
	inline void set_m_DeviceIndex_27(int32_t value)
	{
		___m_DeviceIndex_27 = value;
	}

	inline static int32_t get_offset_of_m_Description_28() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_Description_28)); }
	inline InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA  get_m_Description_28() const { return ___m_Description_28; }
	inline InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA * get_address_of_m_Description_28() { return &___m_Description_28; }
	inline void set_m_Description_28(InputDeviceDescription_tA0075FB9BC3E3E130F6A12873960FCCB6D5DEEBA  value)
	{
		___m_Description_28 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_InterfaceName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_DeviceClass_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Manufacturer_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Product_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Serial_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Version_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Capabilities_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_LastUpdateTimeInternal_29() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_LastUpdateTimeInternal_29)); }
	inline double get_m_LastUpdateTimeInternal_29() const { return ___m_LastUpdateTimeInternal_29; }
	inline double* get_address_of_m_LastUpdateTimeInternal_29() { return &___m_LastUpdateTimeInternal_29; }
	inline void set_m_LastUpdateTimeInternal_29(double value)
	{
		___m_LastUpdateTimeInternal_29 = value;
	}

	inline static int32_t get_offset_of_m_CurrentUpdateStepCount_30() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_CurrentUpdateStepCount_30)); }
	inline uint32_t get_m_CurrentUpdateStepCount_30() const { return ___m_CurrentUpdateStepCount_30; }
	inline uint32_t* get_address_of_m_CurrentUpdateStepCount_30() { return &___m_CurrentUpdateStepCount_30; }
	inline void set_m_CurrentUpdateStepCount_30(uint32_t value)
	{
		___m_CurrentUpdateStepCount_30 = value;
	}

	inline static int32_t get_offset_of_m_AliasesForEachControl_31() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_AliasesForEachControl_31)); }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* get_m_AliasesForEachControl_31() const { return ___m_AliasesForEachControl_31; }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11** get_address_of_m_AliasesForEachControl_31() { return &___m_AliasesForEachControl_31; }
	inline void set_m_AliasesForEachControl_31(InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* value)
	{
		___m_AliasesForEachControl_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AliasesForEachControl_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsagesForEachControl_32() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_UsagesForEachControl_32)); }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* get_m_UsagesForEachControl_32() const { return ___m_UsagesForEachControl_32; }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11** get_address_of_m_UsagesForEachControl_32() { return &___m_UsagesForEachControl_32; }
	inline void set_m_UsagesForEachControl_32(InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* value)
	{
		___m_UsagesForEachControl_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UsagesForEachControl_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsageToControl_33() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_UsageToControl_33)); }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* get_m_UsageToControl_33() const { return ___m_UsageToControl_33; }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F** get_address_of_m_UsageToControl_33() { return &___m_UsageToControl_33; }
	inline void set_m_UsageToControl_33(InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* value)
	{
		___m_UsageToControl_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UsageToControl_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChildrenForEachControl_34() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_ChildrenForEachControl_34)); }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* get_m_ChildrenForEachControl_34() const { return ___m_ChildrenForEachControl_34; }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F** get_address_of_m_ChildrenForEachControl_34() { return &___m_ChildrenForEachControl_34; }
	inline void set_m_ChildrenForEachControl_34(InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* value)
	{
		___m_ChildrenForEachControl_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChildrenForEachControl_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_StateOffsetToControlMap_35() { return static_cast<int32_t>(offsetof(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154, ___m_StateOffsetToControlMap_35)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_m_StateOffsetToControlMap_35() const { return ___m_StateOffsetToControlMap_35; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_m_StateOffsetToControlMap_35() { return &___m_StateOffsetToControlMap_35; }
	inline void set_m_StateOffsetToControlMap_35(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___m_StateOffsetToControlMap_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StateOffsetToControlMap_35), (void*)value);
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockDriver
struct MockDriver_t2FED9AE481D4DFF95EB440EAA8BF0AF218DED00A  : public OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA
{
public:

public:
};

struct MockDriver_t2FED9AE481D4DFF95EB440EAA8BF0AF218DED00A_StaticFields
{
public:
	// UnityEngine.XR.OpenXR.Features.Mock.MockDriver/ScriptEventDelegate UnityEngine.XR.OpenXR.Features.Mock.MockDriver::onScriptEvent
	ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF * ___onScriptEvent_16;

public:
	inline static int32_t get_offset_of_onScriptEvent_16() { return static_cast<int32_t>(offsetof(MockDriver_t2FED9AE481D4DFF95EB440EAA8BF0AF218DED00A_StaticFields, ___onScriptEvent_16)); }
	inline ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF * get_onScriptEvent_16() const { return ___onScriptEvent_16; }
	inline ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF ** get_address_of_onScriptEvent_16() { return &___onScriptEvent_16; }
	inline void set_onScriptEvent_16(ScriptEventDelegate_tD479F8F0EA414C9AE94C5EBAE00960A1134667EF * value)
	{
		___onScriptEvent_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onScriptEvent_16), (void*)value);
	}
};


// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime
struct MockRuntime_tECF1827BDFD6C6AD4099A7015BC07B8B639304F3  : public OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA
{
public:
	// System.Boolean UnityEngine.XR.OpenXR.Features.Mock.MockRuntime::ignoreValidationErrors
	bool ___ignoreValidationErrors_16;

public:
	inline static int32_t get_offset_of_ignoreValidationErrors_16() { return static_cast<int32_t>(offsetof(MockRuntime_tECF1827BDFD6C6AD4099A7015BC07B8B639304F3, ___ignoreValidationErrors_16)); }
	inline bool get_ignoreValidationErrors_16() const { return ___ignoreValidationErrors_16; }
	inline bool* get_address_of_ignoreValidationErrors_16() { return &___ignoreValidationErrors_16; }
	inline void set_ignoreValidationErrors_16(bool value)
	{
		___ignoreValidationErrors_16 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.MouseModel
struct MouseModel_t5636263593FF0F39C4269B519076318964801C67 
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::<changedThisFrame>k__BackingField
	bool ___U3CchangedThisFrameU3Ek__BackingField_1;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_Position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_2;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::<deltaPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_ScrollDelta
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_4;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_LeftButton
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  ___m_LeftButton_5;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_RightButton
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  ___m_RightButton_6;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseButtonModel UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_MiddleButton
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  ___m_MiddleButton_7;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseModel/InternalData UnityEngine.XR.Interaction.Toolkit.UI.MouseModel::m_InternalData
	InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63  ___m_InternalData_8;

public:
	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___U3CpointerIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_0() const { return ___U3CpointerIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_0() { return &___U3CpointerIdU3Ek__BackingField_0; }
	inline void set_U3CpointerIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CchangedThisFrameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___U3CchangedThisFrameU3Ek__BackingField_1)); }
	inline bool get_U3CchangedThisFrameU3Ek__BackingField_1() const { return ___U3CchangedThisFrameU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CchangedThisFrameU3Ek__BackingField_1() { return &___U3CchangedThisFrameU3Ek__BackingField_1; }
	inline void set_U3CchangedThisFrameU3Ek__BackingField_1(bool value)
	{
		___U3CchangedThisFrameU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_Position_2() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_Position_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Position_2() const { return ___m_Position_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Position_2() { return &___m_Position_2; }
	inline void set_m_Position_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Position_2 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___U3CdeltaPositionU3Ek__BackingField_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CdeltaPositionU3Ek__BackingField_3() const { return ___U3CdeltaPositionU3Ek__BackingField_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CdeltaPositionU3Ek__BackingField_3() { return &___U3CdeltaPositionU3Ek__BackingField_3; }
	inline void set_U3CdeltaPositionU3Ek__BackingField_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CdeltaPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_ScrollDelta_4() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_ScrollDelta_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_ScrollDelta_4() const { return ___m_ScrollDelta_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_ScrollDelta_4() { return &___m_ScrollDelta_4; }
	inline void set_m_ScrollDelta_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_ScrollDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_LeftButton_5() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_LeftButton_5)); }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  get_m_LeftButton_5() const { return ___m_LeftButton_5; }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818 * get_address_of_m_LeftButton_5() { return &___m_LeftButton_5; }
	inline void set_m_LeftButton_5(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  value)
	{
		___m_LeftButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_LeftButton_5))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RightButton_6() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_RightButton_6)); }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  get_m_RightButton_6() const { return ___m_RightButton_6; }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818 * get_address_of_m_RightButton_6() { return &___m_RightButton_6; }
	inline void set_m_RightButton_6(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  value)
	{
		___m_RightButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_RightButton_6))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_MiddleButton_7() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_MiddleButton_7)); }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  get_m_MiddleButton_7() const { return ___m_MiddleButton_7; }
	inline MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818 * get_address_of_m_MiddleButton_7() { return &___m_MiddleButton_7; }
	inline void set_m_MiddleButton_7(MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818  value)
	{
		___m_MiddleButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_InternalData_8() { return static_cast<int32_t>(offsetof(MouseModel_t5636263593FF0F39C4269B519076318964801C67, ___m_InternalData_8)); }
	inline InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63  get_m_InternalData_8() const { return ___m_InternalData_8; }
	inline InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63 * get_address_of_m_InternalData_8() { return &___m_InternalData_8; }
	inline void set_m_InternalData_8(InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63  value)
	{
		___m_InternalData_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_InternalData_8))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_InternalData_8))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseModel
struct MouseModel_t5636263593FF0F39C4269B519076318964801C67_marshaled_pinvoke
{
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_2;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_4;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_pinvoke ___m_LeftButton_5;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_pinvoke ___m_RightButton_6;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_pinvoke ___m_MiddleButton_7;
	InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63_marshaled_pinvoke ___m_InternalData_8;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.MouseModel
struct MouseModel_t5636263593FF0F39C4269B519076318964801C67_marshaled_com
{
	int32_t ___U3CpointerIdU3Ek__BackingField_0;
	int32_t ___U3CchangedThisFrameU3Ek__BackingField_1;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_2;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CdeltaPositionU3Ek__BackingField_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ScrollDelta_4;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_com ___m_LeftButton_5;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_com ___m_RightButton_6;
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818_marshaled_com ___m_MiddleButton_7;
	InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63_marshaled_com ___m_InternalData_8;
};

// UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature
struct OculusQuestFeature_tCBC4ACD65C13EBA62B260C6704F476FAF1E9B5E4  : public OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.QuaternionFallbackComposite
struct QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF  : public FallbackComposite_1_t582074E1F6238B69BA417654C1966AF9B014903C
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.QuaternionFallbackComposite::first
	int32_t ___first_1;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.QuaternionFallbackComposite::second
	int32_t ___second_2;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.QuaternionFallbackComposite::third
	int32_t ___third_3;

public:
	inline static int32_t get_offset_of_first_1() { return static_cast<int32_t>(offsetof(QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF, ___first_1)); }
	inline int32_t get_first_1() const { return ___first_1; }
	inline int32_t* get_address_of_first_1() { return &___first_1; }
	inline void set_first_1(int32_t value)
	{
		___first_1 = value;
	}

	inline static int32_t get_offset_of_second_2() { return static_cast<int32_t>(offsetof(QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF, ___second_2)); }
	inline int32_t get_second_2() const { return ___second_2; }
	inline int32_t* get_address_of_second_2() { return &___second_2; }
	inline void set_second_2(int32_t value)
	{
		___second_2 = value;
	}

	inline static int32_t get_offset_of_third_3() { return static_cast<int32_t>(offsetof(QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF, ___third_3)); }
	inline int32_t get_third_3() const { return ___third_3; }
	inline int32_t* get_address_of_third_3() { return &___third_3; }
	inline void set_third_3(int32_t value)
	{
		___third_3 = value;
	}
};


// UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature
struct RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32  : public OpenXRFeature_tBDE19E44A01E5E54925EAF67574691F11E88CDAA
{
public:
	// System.UInt32 UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::cacheSize
	uint32_t ___cacheSize_17;
	// System.UInt32 UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::perThreadCacheSize
	uint32_t ___perThreadCacheSize_18;

public:
	inline static int32_t get_offset_of_cacheSize_17() { return static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32, ___cacheSize_17)); }
	inline uint32_t get_cacheSize_17() const { return ___cacheSize_17; }
	inline uint32_t* get_address_of_cacheSize_17() { return &___cacheSize_17; }
	inline void set_cacheSize_17(uint32_t value)
	{
		___cacheSize_17 = value;
	}

	inline static int32_t get_offset_of_perThreadCacheSize_18() { return static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32, ___perThreadCacheSize_18)); }
	inline uint32_t get_perThreadCacheSize_18() const { return ___perThreadCacheSize_18; }
	inline uint32_t* get_address_of_perThreadCacheSize_18() { return &___perThreadCacheSize_18; }
	inline void set_perThreadCacheSize_18(uint32_t value)
	{
		___perThreadCacheSize_18 = value;
	}
};

struct RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32_StaticFields
{
public:
	// System.Guid UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::kEditorToPlayerRequestDebuggerOutput
	Guid_t  ___kEditorToPlayerRequestDebuggerOutput_15;
	// System.Guid UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::kPlayerToEditorSendDebuggerOutput
	Guid_t  ___kPlayerToEditorSendDebuggerOutput_16;

public:
	inline static int32_t get_offset_of_kEditorToPlayerRequestDebuggerOutput_15() { return static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32_StaticFields, ___kEditorToPlayerRequestDebuggerOutput_15)); }
	inline Guid_t  get_kEditorToPlayerRequestDebuggerOutput_15() const { return ___kEditorToPlayerRequestDebuggerOutput_15; }
	inline Guid_t * get_address_of_kEditorToPlayerRequestDebuggerOutput_15() { return &___kEditorToPlayerRequestDebuggerOutput_15; }
	inline void set_kEditorToPlayerRequestDebuggerOutput_15(Guid_t  value)
	{
		___kEditorToPlayerRequestDebuggerOutput_15 = value;
	}

	inline static int32_t get_offset_of_kPlayerToEditorSendDebuggerOutput_16() { return static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32_StaticFields, ___kPlayerToEditorSendDebuggerOutput_16)); }
	inline Guid_t  get_kPlayerToEditorSendDebuggerOutput_16() const { return ___kPlayerToEditorSendDebuggerOutput_16; }
	inline Guid_t * get_address_of_kPlayerToEditorSendDebuggerOutput_16() { return &___kPlayerToEditorSendDebuggerOutput_16; }
	inline void set_kPlayerToEditorSendDebuggerOutput_16(Guid_t  value)
	{
		___kPlayerToEditorSendDebuggerOutput_16 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.Vector3FallbackComposite
struct Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB  : public FallbackComposite_1_tB2DC43DF37A87D3BF7654039F6F6B8B51D85DD3B
{
public:
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.Vector3FallbackComposite::first
	int32_t ___first_1;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.Vector3FallbackComposite::second
	int32_t ___second_2;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.Inputs.Composites.Vector3FallbackComposite::third
	int32_t ___third_3;

public:
	inline static int32_t get_offset_of_first_1() { return static_cast<int32_t>(offsetof(Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB, ___first_1)); }
	inline int32_t get_first_1() const { return ___first_1; }
	inline int32_t* get_address_of_first_1() { return &___first_1; }
	inline void set_first_1(int32_t value)
	{
		___first_1 = value;
	}

	inline static int32_t get_offset_of_second_2() { return static_cast<int32_t>(offsetof(Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB, ___second_2)); }
	inline int32_t get_second_2() const { return ___second_2; }
	inline int32_t* get_address_of_second_2() { return &___second_2; }
	inline void set_second_2(int32_t value)
	{
		___second_2 = value;
	}

	inline static int32_t get_offset_of_third_3() { return static_cast<int32_t>(offsetof(Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB, ___third_3)); }
	inline int32_t get_third_3() const { return ___third_3; }
	inline int32_t* get_address_of_third_3() { return &___third_3; }
	inline void set_third_3(int32_t value)
	{
		___third_3 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor
struct RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555 
{
public:
	// UnityEngine.XR.Interaction.Toolkit.UI.IUIInteractor UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor::interactor
	RuntimeObject* ___interactor_0;
	// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceModel UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor::model
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494  ___model_1;

public:
	inline static int32_t get_offset_of_interactor_0() { return static_cast<int32_t>(offsetof(RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555, ___interactor_0)); }
	inline RuntimeObject* get_interactor_0() const { return ___interactor_0; }
	inline RuntimeObject** get_address_of_interactor_0() { return &___interactor_0; }
	inline void set_interactor_0(RuntimeObject* value)
	{
		___interactor_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactor_0), (void*)value);
	}

	inline static int32_t get_offset_of_model_1() { return static_cast<int32_t>(offsetof(RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555, ___model_1)); }
	inline TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494  get_model_1() const { return ___model_1; }
	inline TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494 * get_address_of_model_1() { return &___model_1; }
	inline void set_model_1(TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494  value)
	{
		___model_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___m_ImplementationData_1))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___m_ImplementationData_1))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___model_1))->___m_ImplementationData_1))->___U3CpressedRaycastU3Ek__BackingField_6))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___model_1))->___m_ImplementationData_1))->___U3CpressedRaycastU3Ek__BackingField_6))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___m_ImplementationData_1))->___U3CpressedGameObjectU3Ek__BackingField_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___m_ImplementationData_1))->___U3CpressedGameObjectRawU3Ek__BackingField_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___m_ImplementationData_1))->___U3CdraggedGameObjectU3Ek__BackingField_9), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___model_1))->___m_RaycastPoints_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___U3CcurrentRaycastU3Ek__BackingField_9))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_1))->___U3CcurrentRaycastU3Ek__BackingField_9))->___module_1), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor
struct RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555_marshaled_pinvoke
{
	RuntimeObject* ___interactor_0;
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494_marshaled_pinvoke ___model_1;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor
struct RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555_marshaled_com
{
	RuntimeObject* ___interactor_0;
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494_marshaled_com ___model_1;
};

// UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch
struct RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8 
{
public:
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch::isValid
	bool ___isValid_0;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch::touchId
	int32_t ___touchId_1;
	// UnityEngine.XR.Interaction.Toolkit.UI.TouchModel UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch::model
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984  ___model_2;

public:
	inline static int32_t get_offset_of_isValid_0() { return static_cast<int32_t>(offsetof(RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8, ___isValid_0)); }
	inline bool get_isValid_0() const { return ___isValid_0; }
	inline bool* get_address_of_isValid_0() { return &___isValid_0; }
	inline void set_isValid_0(bool value)
	{
		___isValid_0 = value;
	}

	inline static int32_t get_offset_of_touchId_1() { return static_cast<int32_t>(offsetof(RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8, ___touchId_1)); }
	inline int32_t get_touchId_1() const { return ___touchId_1; }
	inline int32_t* get_address_of_touchId_1() { return &___touchId_1; }
	inline void set_touchId_1(int32_t value)
	{
		___touchId_1 = value;
	}

	inline static int32_t get_offset_of_model_2() { return static_cast<int32_t>(offsetof(RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8, ___model_2)); }
	inline TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984  get_model_2() const { return ___model_2; }
	inline TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984 * get_address_of_model_2() { return &___model_2; }
	inline void set_model_2(TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984  value)
	{
		___model_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_2))->___m_ImplementationData_6))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_2))->___m_ImplementationData_6))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___model_2))->___m_ImplementationData_6))->___U3CpressedRaycastU3Ek__BackingField_5))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___model_2))->___m_ImplementationData_6))->___U3CpressedRaycastU3Ek__BackingField_5))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_2))->___m_ImplementationData_6))->___U3CpressedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_2))->___m_ImplementationData_6))->___U3CpressedGameObjectRawU3Ek__BackingField_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___model_2))->___m_ImplementationData_6))->___U3CdraggedGameObjectU3Ek__BackingField_8), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch
struct RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8_marshaled_pinvoke
{
	int32_t ___isValid_0;
	int32_t ___touchId_1;
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984_marshaled_pinvoke ___model_2;
};
// Native definition for COM marshalling of UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch
struct RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8_marshaled_com
{
	int32_t ___isValid_0;
	int32_t ___touchId_1;
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984_marshaled_com ___model_2;
};

// UnityEngine.XR.Interaction.Toolkit.Utilities.ScriptableSettings`1<UnityEngine.XR.Interaction.Toolkit.InteractionLayerSettings>
struct ScriptableSettings_1_tDF3258339DCB126011FADD9A3DAC28C73FA20AE9  : public ScriptableSettingsBase_1_tEBF32352180DD61FDD6EA7D4906BD5AA1F6741BA
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver
struct CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.Interaction.Toolkit.LocomotionProvider UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver::m_LocomotionProvider
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * ___m_LocomotionProvider_4;
	// System.Single UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver::m_MinHeight
	float ___m_MinHeight_5;
	// System.Single UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver::m_MaxHeight
	float ___m_MaxHeight_6;
	// Unity.XR.CoreUtils.XROrigin UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver::m_XROrigin
	XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * ___m_XROrigin_7;
	// UnityEngine.CharacterController UnityEngine.XR.Interaction.Toolkit.CharacterControllerDriver::m_CharacterController
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ___m_CharacterController_8;

public:
	inline static int32_t get_offset_of_m_LocomotionProvider_4() { return static_cast<int32_t>(offsetof(CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2, ___m_LocomotionProvider_4)); }
	inline LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * get_m_LocomotionProvider_4() const { return ___m_LocomotionProvider_4; }
	inline LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 ** get_address_of_m_LocomotionProvider_4() { return &___m_LocomotionProvider_4; }
	inline void set_m_LocomotionProvider_4(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * value)
	{
		___m_LocomotionProvider_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocomotionProvider_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_MinHeight_5() { return static_cast<int32_t>(offsetof(CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2, ___m_MinHeight_5)); }
	inline float get_m_MinHeight_5() const { return ___m_MinHeight_5; }
	inline float* get_address_of_m_MinHeight_5() { return &___m_MinHeight_5; }
	inline void set_m_MinHeight_5(float value)
	{
		___m_MinHeight_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxHeight_6() { return static_cast<int32_t>(offsetof(CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2, ___m_MaxHeight_6)); }
	inline float get_m_MaxHeight_6() const { return ___m_MaxHeight_6; }
	inline float* get_address_of_m_MaxHeight_6() { return &___m_MaxHeight_6; }
	inline void set_m_MaxHeight_6(float value)
	{
		___m_MaxHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_XROrigin_7() { return static_cast<int32_t>(offsetof(CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2, ___m_XROrigin_7)); }
	inline XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * get_m_XROrigin_7() const { return ___m_XROrigin_7; }
	inline XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D ** get_address_of_m_XROrigin_7() { return &___m_XROrigin_7; }
	inline void set_m_XROrigin_7(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * value)
	{
		___m_XROrigin_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_XROrigin_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_CharacterController_8() { return static_cast<int32_t>(offsetof(CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2, ___m_CharacterController_8)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get_m_CharacterController_8() const { return ___m_CharacterController_8; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of_m_CharacterController_8() { return &___m_CharacterController_8; }
	inline void set_m_CharacterController_8(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		___m_CharacterController_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CharacterController_8), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.InputActionManager
struct InputActionManager_t200B2EBBC1D2A94D34E66818449CC1A3BA12963B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.InputActionAsset> UnityEngine.XR.Interaction.Toolkit.Inputs.InputActionManager::m_ActionAssets
	List_1_tA032B571296F8928BB10DB71CA24B00DB5DBF315 * ___m_ActionAssets_4;

public:
	inline static int32_t get_offset_of_m_ActionAssets_4() { return static_cast<int32_t>(offsetof(InputActionManager_t200B2EBBC1D2A94D34E66818449CC1A3BA12963B, ___m_ActionAssets_4)); }
	inline List_1_tA032B571296F8928BB10DB71CA24B00DB5DBF315 * get_m_ActionAssets_4() const { return ___m_ActionAssets_4; }
	inline List_1_tA032B571296F8928BB10DB71CA24B00DB5DBF315 ** get_address_of_m_ActionAssets_4() { return &___m_ActionAssets_4; }
	inline void set_m_ActionAssets_4(List_1_tA032B571296F8928BB10DB71CA24B00DB5DBF315 * value)
	{
		___m_ActionAssets_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionAssets_4), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.LocomotionProvider
struct LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.LocomotionSystem> UnityEngine.XR.Interaction.Toolkit.LocomotionProvider::beginLocomotion
	Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * ___beginLocomotion_4;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.LocomotionSystem> UnityEngine.XR.Interaction.Toolkit.LocomotionProvider::endLocomotion
	Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * ___endLocomotion_5;
	// UnityEngine.XR.Interaction.Toolkit.LocomotionSystem UnityEngine.XR.Interaction.Toolkit.LocomotionProvider::m_System
	LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D * ___m_System_6;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.LocomotionSystem> UnityEngine.XR.Interaction.Toolkit.LocomotionProvider::startLocomotion
	Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * ___startLocomotion_7;

public:
	inline static int32_t get_offset_of_beginLocomotion_4() { return static_cast<int32_t>(offsetof(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448, ___beginLocomotion_4)); }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * get_beginLocomotion_4() const { return ___beginLocomotion_4; }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 ** get_address_of_beginLocomotion_4() { return &___beginLocomotion_4; }
	inline void set_beginLocomotion_4(Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * value)
	{
		___beginLocomotion_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___beginLocomotion_4), (void*)value);
	}

	inline static int32_t get_offset_of_endLocomotion_5() { return static_cast<int32_t>(offsetof(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448, ___endLocomotion_5)); }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * get_endLocomotion_5() const { return ___endLocomotion_5; }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 ** get_address_of_endLocomotion_5() { return &___endLocomotion_5; }
	inline void set_endLocomotion_5(Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * value)
	{
		___endLocomotion_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endLocomotion_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_System_6() { return static_cast<int32_t>(offsetof(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448, ___m_System_6)); }
	inline LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D * get_m_System_6() const { return ___m_System_6; }
	inline LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D ** get_address_of_m_System_6() { return &___m_System_6; }
	inline void set_m_System_6(LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D * value)
	{
		___m_System_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_System_6), (void*)value);
	}

	inline static int32_t get_offset_of_startLocomotion_7() { return static_cast<int32_t>(offsetof(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448, ___startLocomotion_7)); }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * get_startLocomotion_7() const { return ___startLocomotion_7; }
	inline Action_1_t84833279C5597E1E0FE303CF3065416470244C93 ** get_address_of_startLocomotion_7() { return &___startLocomotion_7; }
	inline void set_startLocomotion_7(Action_1_t84833279C5597E1E0FE303CF3065416470244C93 * value)
	{
		___startLocomotion_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startLocomotion_7), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.LocomotionSystem
struct LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.Interaction.Toolkit.LocomotionProvider UnityEngine.XR.Interaction.Toolkit.LocomotionSystem::m_CurrentExclusiveProvider
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * ___m_CurrentExclusiveProvider_4;
	// System.Single UnityEngine.XR.Interaction.Toolkit.LocomotionSystem::m_TimeMadeExclusive
	float ___m_TimeMadeExclusive_5;
	// System.Single UnityEngine.XR.Interaction.Toolkit.LocomotionSystem::m_Timeout
	float ___m_Timeout_6;
	// Unity.XR.CoreUtils.XROrigin UnityEngine.XR.Interaction.Toolkit.LocomotionSystem::m_XROrigin
	XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * ___m_XROrigin_7;

public:
	inline static int32_t get_offset_of_m_CurrentExclusiveProvider_4() { return static_cast<int32_t>(offsetof(LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D, ___m_CurrentExclusiveProvider_4)); }
	inline LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * get_m_CurrentExclusiveProvider_4() const { return ___m_CurrentExclusiveProvider_4; }
	inline LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 ** get_address_of_m_CurrentExclusiveProvider_4() { return &___m_CurrentExclusiveProvider_4; }
	inline void set_m_CurrentExclusiveProvider_4(LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448 * value)
	{
		___m_CurrentExclusiveProvider_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentExclusiveProvider_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_TimeMadeExclusive_5() { return static_cast<int32_t>(offsetof(LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D, ___m_TimeMadeExclusive_5)); }
	inline float get_m_TimeMadeExclusive_5() const { return ___m_TimeMadeExclusive_5; }
	inline float* get_address_of_m_TimeMadeExclusive_5() { return &___m_TimeMadeExclusive_5; }
	inline void set_m_TimeMadeExclusive_5(float value)
	{
		___m_TimeMadeExclusive_5 = value;
	}

	inline static int32_t get_offset_of_m_Timeout_6() { return static_cast<int32_t>(offsetof(LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D, ___m_Timeout_6)); }
	inline float get_m_Timeout_6() const { return ___m_Timeout_6; }
	inline float* get_address_of_m_Timeout_6() { return &___m_Timeout_6; }
	inline void set_m_Timeout_6(float value)
	{
		___m_Timeout_6 = value;
	}

	inline static int32_t get_offset_of_m_XROrigin_7() { return static_cast<int32_t>(offsetof(LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D, ___m_XROrigin_7)); }
	inline XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * get_m_XROrigin_7() const { return ___m_XROrigin_7; }
	inline XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D ** get_address_of_m_XROrigin_7() { return &___m_XROrigin_7; }
	inline void set_m_XROrigin_7(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D * value)
	{
		___m_XROrigin_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_XROrigin_7), (void*)value);
	}
};


// PausePlane
struct PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Texture2D PausePlane::texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture_4;
	// UnityEngine.Vector2 PausePlane::textureSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___textureSize_5;

public:
	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D, ___texture_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_texture_4() const { return ___texture_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_4), (void*)value);
	}

	inline static int32_t get_offset_of_textureSize_5() { return static_cast<int32_t>(offsetof(PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D, ___textureSize_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_textureSize_5() const { return ___textureSize_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_textureSize_5() { return &___textureSize_5; }
	inline void set_textureSize_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___textureSize_5 = value;
	}
};


// UnityEngine.InputSystem.TrackedDevice
struct TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5  : public InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154
{
public:
	// UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.InputSystem.TrackedDevice::<trackingState>k__BackingField
	IntegerControl_t7A27284DD4A3D30C25AA43717A80A6CCB30E26A8 * ___U3CtrackingStateU3Ek__BackingField_39;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.InputSystem.TrackedDevice::<isTracked>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CisTrackedU3Ek__BackingField_40;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.TrackedDevice::<devicePosition>k__BackingField
	Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * ___U3CdevicePositionU3Ek__BackingField_41;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.TrackedDevice::<deviceRotation>k__BackingField
	QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * ___U3CdeviceRotationU3Ek__BackingField_42;

public:
	inline static int32_t get_offset_of_U3CtrackingStateU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5, ___U3CtrackingStateU3Ek__BackingField_39)); }
	inline IntegerControl_t7A27284DD4A3D30C25AA43717A80A6CCB30E26A8 * get_U3CtrackingStateU3Ek__BackingField_39() const { return ___U3CtrackingStateU3Ek__BackingField_39; }
	inline IntegerControl_t7A27284DD4A3D30C25AA43717A80A6CCB30E26A8 ** get_address_of_U3CtrackingStateU3Ek__BackingField_39() { return &___U3CtrackingStateU3Ek__BackingField_39; }
	inline void set_U3CtrackingStateU3Ek__BackingField_39(IntegerControl_t7A27284DD4A3D30C25AA43717A80A6CCB30E26A8 * value)
	{
		___U3CtrackingStateU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingStateU3Ek__BackingField_39), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTrackedU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5, ___U3CisTrackedU3Ek__BackingField_40)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CisTrackedU3Ek__BackingField_40() const { return ___U3CisTrackedU3Ek__BackingField_40; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CisTrackedU3Ek__BackingField_40() { return &___U3CisTrackedU3Ek__BackingField_40; }
	inline void set_U3CisTrackedU3Ek__BackingField_40(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CisTrackedU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CisTrackedU3Ek__BackingField_40), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdevicePositionU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5, ___U3CdevicePositionU3Ek__BackingField_41)); }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * get_U3CdevicePositionU3Ek__BackingField_41() const { return ___U3CdevicePositionU3Ek__BackingField_41; }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 ** get_address_of_U3CdevicePositionU3Ek__BackingField_41() { return &___U3CdevicePositionU3Ek__BackingField_41; }
	inline void set_U3CdevicePositionU3Ek__BackingField_41(Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * value)
	{
		___U3CdevicePositionU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdevicePositionU3Ek__BackingField_41), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceRotationU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5, ___U3CdeviceRotationU3Ek__BackingField_42)); }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * get_U3CdeviceRotationU3Ek__BackingField_42() const { return ___U3CdeviceRotationU3Ek__BackingField_42; }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF ** get_address_of_U3CdeviceRotationU3Ek__BackingField_42() { return &___U3CdeviceRotationU3Ek__BackingField_42; }
	inline void set_U3CdeviceRotationU3Ek__BackingField_42(QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * value)
	{
		___U3CdeviceRotationU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceRotationU3Ek__BackingField_42), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Whiteboard
struct Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Texture2D Whiteboard::texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture_4;
	// UnityEngine.Vector2 Whiteboard::textureSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___textureSize_5;
	// UnityEngine.Light Whiteboard::lSource
	Light_tA2F349FE839781469A0344CF6039B51512394275 * ___lSource_6;
	// UnityEngine.Color Whiteboard::targetColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___targetColor_7;
	// UnityEngine.Transform Whiteboard::_play
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____play_8;
	// UnityEngine.Transform Whiteboard::_pauPlane
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____pauPlane_9;
	// UnityEngine.Video.VideoPlayer Whiteboard::videoPlayer
	VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * ___videoPlayer_10;
	// UnityEngine.Texture2D Whiteboard::videoFrame
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___videoFrame_11;
	// UnityEngine.GameObject Whiteboard::objs
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___objs_12;
	// UnityEngine.XR.InputDeviceCharacteristics Whiteboard::deviceCharacteristic
	uint32_t ___deviceCharacteristic_14;
	// Whiteboard/ButtonOption Whiteboard::button
	int32_t ___button_15;
	// UnityEngine.Events.UnityEvent Whiteboard::OnPress
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnPress_16;
	// UnityEngine.Events.UnityEvent Whiteboard::OnRelease
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnRelease_17;
	// System.Boolean Whiteboard::<IsPressed>k__BackingField
	bool ___U3CIsPressedU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Whiteboard::inputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___inputDevices_19;
	// System.Boolean Whiteboard::inputValue
	bool ___inputValue_20;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Whiteboard::inputFeature
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___inputFeature_21;

public:
	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___texture_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_texture_4() const { return ___texture_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_4), (void*)value);
	}

	inline static int32_t get_offset_of_textureSize_5() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___textureSize_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_textureSize_5() const { return ___textureSize_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_textureSize_5() { return &___textureSize_5; }
	inline void set_textureSize_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___textureSize_5 = value;
	}

	inline static int32_t get_offset_of_lSource_6() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___lSource_6)); }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 * get_lSource_6() const { return ___lSource_6; }
	inline Light_tA2F349FE839781469A0344CF6039B51512394275 ** get_address_of_lSource_6() { return &___lSource_6; }
	inline void set_lSource_6(Light_tA2F349FE839781469A0344CF6039B51512394275 * value)
	{
		___lSource_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lSource_6), (void*)value);
	}

	inline static int32_t get_offset_of_targetColor_7() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___targetColor_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_targetColor_7() const { return ___targetColor_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_targetColor_7() { return &___targetColor_7; }
	inline void set_targetColor_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___targetColor_7 = value;
	}

	inline static int32_t get_offset_of__play_8() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ____play_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__play_8() const { return ____play_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__play_8() { return &____play_8; }
	inline void set__play_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____play_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____play_8), (void*)value);
	}

	inline static int32_t get_offset_of__pauPlane_9() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ____pauPlane_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__pauPlane_9() const { return ____pauPlane_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__pauPlane_9() { return &____pauPlane_9; }
	inline void set__pauPlane_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____pauPlane_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pauPlane_9), (void*)value);
	}

	inline static int32_t get_offset_of_videoPlayer_10() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___videoPlayer_10)); }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * get_videoPlayer_10() const { return ___videoPlayer_10; }
	inline VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 ** get_address_of_videoPlayer_10() { return &___videoPlayer_10; }
	inline void set_videoPlayer_10(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86 * value)
	{
		___videoPlayer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoPlayer_10), (void*)value);
	}

	inline static int32_t get_offset_of_videoFrame_11() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___videoFrame_11)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_videoFrame_11() const { return ___videoFrame_11; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_videoFrame_11() { return &___videoFrame_11; }
	inline void set_videoFrame_11(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___videoFrame_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___videoFrame_11), (void*)value);
	}

	inline static int32_t get_offset_of_objs_12() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___objs_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_objs_12() const { return ___objs_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_objs_12() { return &___objs_12; }
	inline void set_objs_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___objs_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objs_12), (void*)value);
	}

	inline static int32_t get_offset_of_deviceCharacteristic_14() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___deviceCharacteristic_14)); }
	inline uint32_t get_deviceCharacteristic_14() const { return ___deviceCharacteristic_14; }
	inline uint32_t* get_address_of_deviceCharacteristic_14() { return &___deviceCharacteristic_14; }
	inline void set_deviceCharacteristic_14(uint32_t value)
	{
		___deviceCharacteristic_14 = value;
	}

	inline static int32_t get_offset_of_button_15() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___button_15)); }
	inline int32_t get_button_15() const { return ___button_15; }
	inline int32_t* get_address_of_button_15() { return &___button_15; }
	inline void set_button_15(int32_t value)
	{
		___button_15 = value;
	}

	inline static int32_t get_offset_of_OnPress_16() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___OnPress_16)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnPress_16() const { return ___OnPress_16; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnPress_16() { return &___OnPress_16; }
	inline void set_OnPress_16(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnPress_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPress_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnRelease_17() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___OnRelease_17)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnRelease_17() const { return ___OnRelease_17; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnRelease_17() { return &___OnRelease_17; }
	inline void set_OnRelease_17(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnRelease_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRelease_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsPressedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___U3CIsPressedU3Ek__BackingField_18)); }
	inline bool get_U3CIsPressedU3Ek__BackingField_18() const { return ___U3CIsPressedU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsPressedU3Ek__BackingField_18() { return &___U3CIsPressedU3Ek__BackingField_18; }
	inline void set_U3CIsPressedU3Ek__BackingField_18(bool value)
	{
		___U3CIsPressedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_inputDevices_19() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___inputDevices_19)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_inputDevices_19() const { return ___inputDevices_19; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_inputDevices_19() { return &___inputDevices_19; }
	inline void set_inputDevices_19(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___inputDevices_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputDevices_19), (void*)value);
	}

	inline static int32_t get_offset_of_inputValue_20() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___inputValue_20)); }
	inline bool get_inputValue_20() const { return ___inputValue_20; }
	inline bool* get_address_of_inputValue_20() { return &___inputValue_20; }
	inline void set_inputValue_20(bool value)
	{
		___inputValue_20 = value;
	}

	inline static int32_t get_offset_of_inputFeature_21() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28, ___inputFeature_21)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_inputFeature_21() const { return ___inputFeature_21; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_inputFeature_21() { return &___inputFeature_21; }
	inline void set_inputFeature_21(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___inputFeature_21 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___inputFeature_21))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}
};

struct Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.InputFeatureUsage`1<System.Boolean>> Whiteboard::availableButtons
	Dictionary_2_t62EEA22F57F8BEB4675F7726F2A7105ABCD711D5 * ___availableButtons_13;

public:
	inline static int32_t get_offset_of_availableButtons_13() { return static_cast<int32_t>(offsetof(Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28_StaticFields, ___availableButtons_13)); }
	inline Dictionary_2_t62EEA22F57F8BEB4675F7726F2A7105ABCD711D5 * get_availableButtons_13() const { return ___availableButtons_13; }
	inline Dictionary_2_t62EEA22F57F8BEB4675F7726F2A7105ABCD711D5 ** get_address_of_availableButtons_13() { return &___availableButtons_13; }
	inline void set_availableButtons_13(Dictionary_2_t62EEA22F57F8BEB4675F7726F2A7105ABCD711D5 * value)
	{
		___availableButtons_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___availableButtons_13), (void*)value);
	}
};


// WhiteboardMarker
struct WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform WhiteboardMarker::_tip
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____tip_4;
	// System.Int32 WhiteboardMarker::_penSize
	int32_t ____penSize_5;
	// UnityEngine.Renderer WhiteboardMarker::_renderer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ____renderer_6;
	// UnityEngine.Color[] WhiteboardMarker::_colors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____colors_7;
	// System.Single WhiteboardMarker::_tipHeight
	float ____tipHeight_8;
	// UnityEngine.RaycastHit WhiteboardMarker::_touch
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  ____touch_9;
	// PausePlane WhiteboardMarker::_whiteboard
	PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D * ____whiteboard_10;
	// UnityEngine.Vector2 WhiteboardMarker::_touchPos
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____touchPos_11;
	// UnityEngine.Vector2 WhiteboardMarker::_lastTouchPos
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____lastTouchPos_12;
	// System.Boolean WhiteboardMarker::_touchedLastFrame
	bool ____touchedLastFrame_13;
	// UnityEngine.Quaternion WhiteboardMarker::_lastTouchRot
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____lastTouchRot_14;

public:
	inline static int32_t get_offset_of__tip_4() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____tip_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__tip_4() const { return ____tip_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__tip_4() { return &____tip_4; }
	inline void set__tip_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____tip_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tip_4), (void*)value);
	}

	inline static int32_t get_offset_of__penSize_5() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____penSize_5)); }
	inline int32_t get__penSize_5() const { return ____penSize_5; }
	inline int32_t* get_address_of__penSize_5() { return &____penSize_5; }
	inline void set__penSize_5(int32_t value)
	{
		____penSize_5 = value;
	}

	inline static int32_t get_offset_of__renderer_6() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____renderer_6)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get__renderer_6() const { return ____renderer_6; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of__renderer_6() { return &____renderer_6; }
	inline void set__renderer_6(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		____renderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____renderer_6), (void*)value);
	}

	inline static int32_t get_offset_of__colors_7() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____colors_7)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__colors_7() const { return ____colors_7; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__colors_7() { return &____colors_7; }
	inline void set__colors_7(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____colors_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____colors_7), (void*)value);
	}

	inline static int32_t get_offset_of__tipHeight_8() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____tipHeight_8)); }
	inline float get__tipHeight_8() const { return ____tipHeight_8; }
	inline float* get_address_of__tipHeight_8() { return &____tipHeight_8; }
	inline void set__tipHeight_8(float value)
	{
		____tipHeight_8 = value;
	}

	inline static int32_t get_offset_of__touch_9() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____touch_9)); }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  get__touch_9() const { return ____touch_9; }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * get_address_of__touch_9() { return &____touch_9; }
	inline void set__touch_9(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		____touch_9 = value;
	}

	inline static int32_t get_offset_of__whiteboard_10() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____whiteboard_10)); }
	inline PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D * get__whiteboard_10() const { return ____whiteboard_10; }
	inline PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D ** get_address_of__whiteboard_10() { return &____whiteboard_10; }
	inline void set__whiteboard_10(PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D * value)
	{
		____whiteboard_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____whiteboard_10), (void*)value);
	}

	inline static int32_t get_offset_of__touchPos_11() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____touchPos_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__touchPos_11() const { return ____touchPos_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__touchPos_11() { return &____touchPos_11; }
	inline void set__touchPos_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____touchPos_11 = value;
	}

	inline static int32_t get_offset_of__lastTouchPos_12() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____lastTouchPos_12)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__lastTouchPos_12() const { return ____lastTouchPos_12; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__lastTouchPos_12() { return &____lastTouchPos_12; }
	inline void set__lastTouchPos_12(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____lastTouchPos_12 = value;
	}

	inline static int32_t get_offset_of__touchedLastFrame_13() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____touchedLastFrame_13)); }
	inline bool get__touchedLastFrame_13() const { return ____touchedLastFrame_13; }
	inline bool* get_address_of__touchedLastFrame_13() { return &____touchedLastFrame_13; }
	inline void set__touchedLastFrame_13(bool value)
	{
		____touchedLastFrame_13 = value;
	}

	inline static int32_t get_offset_of__lastTouchRot_14() { return static_cast<int32_t>(offsetof(WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864, ____lastTouchRot_14)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__lastTouchRot_14() const { return ____lastTouchRot_14; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__lastTouchRot_14() { return &____lastTouchRot_14; }
	inline void set__lastTouchRot_14(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____lastTouchRot_14 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable
struct XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::registered
	Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * ___registered_4;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::unregistered
	Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * ___unregistered_5;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_InteractionManager
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___m_InteractionManager_6;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_Colliders
	List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * ___m_Colliders_7;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_InteractionLayerMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_InteractionLayerMask_8;
	// UnityEngine.XR.Interaction.Toolkit.InteractionLayerMask UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_InteractionLayers
	InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  ___m_InteractionLayers_9;
	// UnityEngine.XR.Interaction.Toolkit.InteractableSelectMode UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_SelectMode
	int32_t ___m_SelectMode_10;
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_CustomReticle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CustomReticle_11;
	// UnityEngine.XR.Interaction.Toolkit.HoverEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_FirstHoverEntered
	HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * ___m_FirstHoverEntered_12;
	// UnityEngine.XR.Interaction.Toolkit.HoverExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_LastHoverExited
	HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * ___m_LastHoverExited_13;
	// UnityEngine.XR.Interaction.Toolkit.HoverEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_HoverEntered
	HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * ___m_HoverEntered_14;
	// UnityEngine.XR.Interaction.Toolkit.HoverExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_HoverExited
	HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * ___m_HoverExited_15;
	// UnityEngine.XR.Interaction.Toolkit.SelectEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_FirstSelectEntered
	SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * ___m_FirstSelectEntered_16;
	// UnityEngine.XR.Interaction.Toolkit.SelectExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_LastSelectExited
	SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * ___m_LastSelectExited_17;
	// UnityEngine.XR.Interaction.Toolkit.SelectEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_SelectEntered
	SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * ___m_SelectEntered_18;
	// UnityEngine.XR.Interaction.Toolkit.SelectExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_SelectExited
	SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * ___m_SelectExited_19;
	// UnityEngine.XR.Interaction.Toolkit.ActivateEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_Activated
	ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F * ___m_Activated_20;
	// UnityEngine.XR.Interaction.Toolkit.DeactivateEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_Deactivated
	DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C * ___m_Deactivated_21;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractor> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::<interactorsHovering>k__BackingField
	List_1_t8F5833796A8407AFD49E7C1864E6E76653D3F2C4 * ___U3CinteractorsHoveringU3Ek__BackingField_22;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::<interactorsSelecting>k__BackingField
	List_1_tCCED540317FA295CA1BAEBCCD4362DE7EFFB64FD * ___U3CinteractorsSelectingU3Ek__BackingField_23;
	// UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::<firstInteractorSelecting>k__BackingField
	RuntimeObject* ___U3CfirstInteractorSelectingU3Ek__BackingField_24;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor,UnityEngine.Pose> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_AttachPoseOnSelect
	Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * ___m_AttachPoseOnSelect_25;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractor,UnityEngine.Pose> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_LocalAttachPoseOnSelect
	Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * ___m_LocalAttachPoseOnSelect_26;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractor,UnityEngine.GameObject> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_ReticleCache
	Dictionary_2_tA69FF83EA17DA0267E4A9ADE6AFF4334D537FBD8 * ___m_ReticleCache_27;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_RegisteredInteractionManager
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___m_RegisteredInteractionManager_28;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnFirstHoverEntered
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnFirstHoverEntered_30;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnLastHoverExited
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnLastHoverExited_31;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnHoverEntered
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnHoverEntered_32;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnHoverExited
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnHoverExited_33;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnSelectEntered
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnSelectEntered_34;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnSelectExited
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnSelectExited_35;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnSelectCanceled
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnSelectCanceled_36;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnActivate
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnActivate_37;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractableEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::m_OnDeactivate
	XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * ___m_OnDeactivate_38;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::<hoveringInteractors>k__BackingField
	List_1_tEF7E6AA6FF2A58B22A1943486BE24649DC3CC2AC * ___U3ChoveringInteractorsU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_registered_4() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___registered_4)); }
	inline Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * get_registered_4() const { return ___registered_4; }
	inline Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD ** get_address_of_registered_4() { return &___registered_4; }
	inline void set_registered_4(Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * value)
	{
		___registered_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___registered_4), (void*)value);
	}

	inline static int32_t get_offset_of_unregistered_5() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___unregistered_5)); }
	inline Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * get_unregistered_5() const { return ___unregistered_5; }
	inline Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA ** get_address_of_unregistered_5() { return &___unregistered_5; }
	inline void set_unregistered_5(Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * value)
	{
		___unregistered_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unregistered_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractionManager_6() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_InteractionManager_6)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_m_InteractionManager_6() const { return ___m_InteractionManager_6; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_m_InteractionManager_6() { return &___m_InteractionManager_6; }
	inline void set_m_InteractionManager_6(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___m_InteractionManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractionManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colliders_7() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_Colliders_7)); }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * get_m_Colliders_7() const { return ___m_Colliders_7; }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B ** get_address_of_m_Colliders_7() { return &___m_Colliders_7; }
	inline void set_m_Colliders_7(List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * value)
	{
		___m_Colliders_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colliders_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractionLayerMask_8() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_InteractionLayerMask_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_InteractionLayerMask_8() const { return ___m_InteractionLayerMask_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_InteractionLayerMask_8() { return &___m_InteractionLayerMask_8; }
	inline void set_m_InteractionLayerMask_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_InteractionLayerMask_8 = value;
	}

	inline static int32_t get_offset_of_m_InteractionLayers_9() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_InteractionLayers_9)); }
	inline InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  get_m_InteractionLayers_9() const { return ___m_InteractionLayers_9; }
	inline InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47 * get_address_of_m_InteractionLayers_9() { return &___m_InteractionLayers_9; }
	inline void set_m_InteractionLayers_9(InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  value)
	{
		___m_InteractionLayers_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectMode_10() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_SelectMode_10)); }
	inline int32_t get_m_SelectMode_10() const { return ___m_SelectMode_10; }
	inline int32_t* get_address_of_m_SelectMode_10() { return &___m_SelectMode_10; }
	inline void set_m_SelectMode_10(int32_t value)
	{
		___m_SelectMode_10 = value;
	}

	inline static int32_t get_offset_of_m_CustomReticle_11() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_CustomReticle_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CustomReticle_11() const { return ___m_CustomReticle_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CustomReticle_11() { return &___m_CustomReticle_11; }
	inline void set_m_CustomReticle_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CustomReticle_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomReticle_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstHoverEntered_12() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_FirstHoverEntered_12)); }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * get_m_FirstHoverEntered_12() const { return ___m_FirstHoverEntered_12; }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 ** get_address_of_m_FirstHoverEntered_12() { return &___m_FirstHoverEntered_12; }
	inline void set_m_FirstHoverEntered_12(HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * value)
	{
		___m_FirstHoverEntered_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstHoverEntered_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastHoverExited_13() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_LastHoverExited_13)); }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * get_m_LastHoverExited_13() const { return ___m_LastHoverExited_13; }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 ** get_address_of_m_LastHoverExited_13() { return &___m_LastHoverExited_13; }
	inline void set_m_LastHoverExited_13(HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * value)
	{
		___m_LastHoverExited_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LastHoverExited_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverEntered_14() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_HoverEntered_14)); }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * get_m_HoverEntered_14() const { return ___m_HoverEntered_14; }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 ** get_address_of_m_HoverEntered_14() { return &___m_HoverEntered_14; }
	inline void set_m_HoverEntered_14(HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * value)
	{
		___m_HoverEntered_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverEntered_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverExited_15() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_HoverExited_15)); }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * get_m_HoverExited_15() const { return ___m_HoverExited_15; }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 ** get_address_of_m_HoverExited_15() { return &___m_HoverExited_15; }
	inline void set_m_HoverExited_15(HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * value)
	{
		___m_HoverExited_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverExited_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelectEntered_16() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_FirstSelectEntered_16)); }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * get_m_FirstSelectEntered_16() const { return ___m_FirstSelectEntered_16; }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 ** get_address_of_m_FirstSelectEntered_16() { return &___m_FirstSelectEntered_16; }
	inline void set_m_FirstSelectEntered_16(SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * value)
	{
		___m_FirstSelectEntered_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelectEntered_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastSelectExited_17() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_LastSelectExited_17)); }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * get_m_LastSelectExited_17() const { return ___m_LastSelectExited_17; }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 ** get_address_of_m_LastSelectExited_17() { return &___m_LastSelectExited_17; }
	inline void set_m_LastSelectExited_17(SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * value)
	{
		___m_LastSelectExited_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LastSelectExited_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectEntered_18() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_SelectEntered_18)); }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * get_m_SelectEntered_18() const { return ___m_SelectEntered_18; }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 ** get_address_of_m_SelectEntered_18() { return &___m_SelectEntered_18; }
	inline void set_m_SelectEntered_18(SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * value)
	{
		___m_SelectEntered_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectEntered_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectExited_19() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_SelectExited_19)); }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * get_m_SelectExited_19() const { return ___m_SelectExited_19; }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 ** get_address_of_m_SelectExited_19() { return &___m_SelectExited_19; }
	inline void set_m_SelectExited_19(SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * value)
	{
		___m_SelectExited_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectExited_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Activated_20() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_Activated_20)); }
	inline ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F * get_m_Activated_20() const { return ___m_Activated_20; }
	inline ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F ** get_address_of_m_Activated_20() { return &___m_Activated_20; }
	inline void set_m_Activated_20(ActivateEvent_tA788B1E186D477BA625450B006EA76D0370B0C7F * value)
	{
		___m_Activated_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Activated_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Deactivated_21() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_Deactivated_21)); }
	inline DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C * get_m_Deactivated_21() const { return ___m_Deactivated_21; }
	inline DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C ** get_address_of_m_Deactivated_21() { return &___m_Deactivated_21; }
	inline void set_m_Deactivated_21(DeactivateEvent_t587971B94B6589C04E9D72515DF079C1EE91DB3C * value)
	{
		___m_Deactivated_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Deactivated_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinteractorsHoveringU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___U3CinteractorsHoveringU3Ek__BackingField_22)); }
	inline List_1_t8F5833796A8407AFD49E7C1864E6E76653D3F2C4 * get_U3CinteractorsHoveringU3Ek__BackingField_22() const { return ___U3CinteractorsHoveringU3Ek__BackingField_22; }
	inline List_1_t8F5833796A8407AFD49E7C1864E6E76653D3F2C4 ** get_address_of_U3CinteractorsHoveringU3Ek__BackingField_22() { return &___U3CinteractorsHoveringU3Ek__BackingField_22; }
	inline void set_U3CinteractorsHoveringU3Ek__BackingField_22(List_1_t8F5833796A8407AFD49E7C1864E6E76653D3F2C4 * value)
	{
		___U3CinteractorsHoveringU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractorsHoveringU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinteractorsSelectingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___U3CinteractorsSelectingU3Ek__BackingField_23)); }
	inline List_1_tCCED540317FA295CA1BAEBCCD4362DE7EFFB64FD * get_U3CinteractorsSelectingU3Ek__BackingField_23() const { return ___U3CinteractorsSelectingU3Ek__BackingField_23; }
	inline List_1_tCCED540317FA295CA1BAEBCCD4362DE7EFFB64FD ** get_address_of_U3CinteractorsSelectingU3Ek__BackingField_23() { return &___U3CinteractorsSelectingU3Ek__BackingField_23; }
	inline void set_U3CinteractorsSelectingU3Ek__BackingField_23(List_1_tCCED540317FA295CA1BAEBCCD4362DE7EFFB64FD * value)
	{
		___U3CinteractorsSelectingU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractorsSelectingU3Ek__BackingField_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfirstInteractorSelectingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___U3CfirstInteractorSelectingU3Ek__BackingField_24)); }
	inline RuntimeObject* get_U3CfirstInteractorSelectingU3Ek__BackingField_24() const { return ___U3CfirstInteractorSelectingU3Ek__BackingField_24; }
	inline RuntimeObject** get_address_of_U3CfirstInteractorSelectingU3Ek__BackingField_24() { return &___U3CfirstInteractorSelectingU3Ek__BackingField_24; }
	inline void set_U3CfirstInteractorSelectingU3Ek__BackingField_24(RuntimeObject* value)
	{
		___U3CfirstInteractorSelectingU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfirstInteractorSelectingU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_AttachPoseOnSelect_25() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_AttachPoseOnSelect_25)); }
	inline Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * get_m_AttachPoseOnSelect_25() const { return ___m_AttachPoseOnSelect_25; }
	inline Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC ** get_address_of_m_AttachPoseOnSelect_25() { return &___m_AttachPoseOnSelect_25; }
	inline void set_m_AttachPoseOnSelect_25(Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * value)
	{
		___m_AttachPoseOnSelect_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AttachPoseOnSelect_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_LocalAttachPoseOnSelect_26() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_LocalAttachPoseOnSelect_26)); }
	inline Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * get_m_LocalAttachPoseOnSelect_26() const { return ___m_LocalAttachPoseOnSelect_26; }
	inline Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC ** get_address_of_m_LocalAttachPoseOnSelect_26() { return &___m_LocalAttachPoseOnSelect_26; }
	inline void set_m_LocalAttachPoseOnSelect_26(Dictionary_2_tA3135A847BDD5B3E11304035C508DFB9DE47F8CC * value)
	{
		___m_LocalAttachPoseOnSelect_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocalAttachPoseOnSelect_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReticleCache_27() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_ReticleCache_27)); }
	inline Dictionary_2_tA69FF83EA17DA0267E4A9ADE6AFF4334D537FBD8 * get_m_ReticleCache_27() const { return ___m_ReticleCache_27; }
	inline Dictionary_2_tA69FF83EA17DA0267E4A9ADE6AFF4334D537FBD8 ** get_address_of_m_ReticleCache_27() { return &___m_ReticleCache_27; }
	inline void set_m_ReticleCache_27(Dictionary_2_tA69FF83EA17DA0267E4A9ADE6AFF4334D537FBD8 * value)
	{
		___m_ReticleCache_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReticleCache_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_RegisteredInteractionManager_28() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_RegisteredInteractionManager_28)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_m_RegisteredInteractionManager_28() const { return ___m_RegisteredInteractionManager_28; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_m_RegisteredInteractionManager_28() { return &___m_RegisteredInteractionManager_28; }
	inline void set_m_RegisteredInteractionManager_28(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___m_RegisteredInteractionManager_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RegisteredInteractionManager_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnFirstHoverEntered_30() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnFirstHoverEntered_30)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnFirstHoverEntered_30() const { return ___m_OnFirstHoverEntered_30; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnFirstHoverEntered_30() { return &___m_OnFirstHoverEntered_30; }
	inline void set_m_OnFirstHoverEntered_30(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnFirstHoverEntered_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnFirstHoverEntered_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnLastHoverExited_31() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnLastHoverExited_31)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnLastHoverExited_31() const { return ___m_OnLastHoverExited_31; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnLastHoverExited_31() { return &___m_OnLastHoverExited_31; }
	inline void set_m_OnLastHoverExited_31(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnLastHoverExited_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnLastHoverExited_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHoverEntered_32() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnHoverEntered_32)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnHoverEntered_32() const { return ___m_OnHoverEntered_32; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnHoverEntered_32() { return &___m_OnHoverEntered_32; }
	inline void set_m_OnHoverEntered_32(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnHoverEntered_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHoverEntered_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHoverExited_33() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnHoverExited_33)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnHoverExited_33() const { return ___m_OnHoverExited_33; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnHoverExited_33() { return &___m_OnHoverExited_33; }
	inline void set_m_OnHoverExited_33(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnHoverExited_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHoverExited_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectEntered_34() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnSelectEntered_34)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnSelectEntered_34() const { return ___m_OnSelectEntered_34; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnSelectEntered_34() { return &___m_OnSelectEntered_34; }
	inline void set_m_OnSelectEntered_34(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnSelectEntered_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectEntered_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectExited_35() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnSelectExited_35)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnSelectExited_35() const { return ___m_OnSelectExited_35; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnSelectExited_35() { return &___m_OnSelectExited_35; }
	inline void set_m_OnSelectExited_35(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnSelectExited_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectExited_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectCanceled_36() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnSelectCanceled_36)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnSelectCanceled_36() const { return ___m_OnSelectCanceled_36; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnSelectCanceled_36() { return &___m_OnSelectCanceled_36; }
	inline void set_m_OnSelectCanceled_36(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnSelectCanceled_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectCanceled_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnActivate_37() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnActivate_37)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnActivate_37() const { return ___m_OnActivate_37; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnActivate_37() { return &___m_OnActivate_37; }
	inline void set_m_OnActivate_37(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnActivate_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnActivate_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDeactivate_38() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___m_OnDeactivate_38)); }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * get_m_OnDeactivate_38() const { return ___m_OnDeactivate_38; }
	inline XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD ** get_address_of_m_OnDeactivate_38() { return &___m_OnDeactivate_38; }
	inline void set_m_OnDeactivate_38(XRInteractableEvent_tD08962B410D993D428BA0CDF9D1AD5273AEA53DD * value)
	{
		___m_OnDeactivate_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDeactivate_38), (void*)value);
	}

	inline static int32_t get_offset_of_U3ChoveringInteractorsU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF, ___U3ChoveringInteractorsU3Ek__BackingField_39)); }
	inline List_1_tEF7E6AA6FF2A58B22A1943486BE24649DC3CC2AC * get_U3ChoveringInteractorsU3Ek__BackingField_39() const { return ___U3ChoveringInteractorsU3Ek__BackingField_39; }
	inline List_1_tEF7E6AA6FF2A58B22A1943486BE24649DC3CC2AC ** get_address_of_U3ChoveringInteractorsU3Ek__BackingField_39() { return &___U3ChoveringInteractorsU3Ek__BackingField_39; }
	inline void set_U3ChoveringInteractorsU3Ek__BackingField_39(List_1_tEF7E6AA6FF2A58B22A1943486BE24649DC3CC2AC * value)
	{
		___U3ChoveringInteractorsU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChoveringInteractorsU3Ek__BackingField_39), (void*)value);
	}
};

struct XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF_StaticFields
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable::s_InteractionManagerCache
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___s_InteractionManagerCache_29;

public:
	inline static int32_t get_offset_of_s_InteractionManagerCache_29() { return static_cast<int32_t>(offsetof(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF_StaticFields, ___s_InteractionManagerCache_29)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_s_InteractionManagerCache_29() const { return ___s_InteractionManagerCache_29; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_s_InteractionManagerCache_29() { return &___s_InteractionManagerCache_29; }
	inline void set_s_InteractionManagerCache_29(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___s_InteractionManagerCache_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InteractionManagerCache_29), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor
struct XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::registered
	Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * ___registered_4;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::unregistered
	Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * ___unregistered_5;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_InteractionManager
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___m_InteractionManager_6;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_InteractionLayerMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_InteractionLayerMask_7;
	// UnityEngine.XR.Interaction.Toolkit.InteractionLayerMask UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_InteractionLayers
	InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  ___m_InteractionLayers_8;
	// UnityEngine.Transform UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_AttachTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_AttachTransform_9;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_KeepSelectedTargetValid
	bool ___m_KeepSelectedTargetValid_10;
	// UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_StartingSelectedInteractable
	XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF * ___m_StartingSelectedInteractable_11;
	// UnityEngine.XR.Interaction.Toolkit.HoverEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_HoverEntered
	HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * ___m_HoverEntered_12;
	// UnityEngine.XR.Interaction.Toolkit.HoverExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_HoverExited
	HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * ___m_HoverExited_13;
	// UnityEngine.XR.Interaction.Toolkit.SelectEnterEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_SelectEntered
	SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * ___m_SelectEntered_14;
	// UnityEngine.XR.Interaction.Toolkit.SelectExitEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_SelectExited
	SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * ___m_SelectExited_15;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_AllowHover
	bool ___m_AllowHover_16;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_AllowSelect
	bool ___m_AllowSelect_17;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_IsPerformingManualInteraction
	bool ___m_IsPerformingManualInteraction_18;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractable> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::<interactablesHovered>k__BackingField
	List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * ___U3CinteractablesHoveredU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::<interactablesSelected>k__BackingField
	List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * ___U3CinteractablesSelectedU3Ek__BackingField_20;
	// UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::<firstInteractableSelected>k__BackingField
	RuntimeObject* ___U3CfirstInteractableSelectedU3Ek__BackingField_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable,UnityEngine.Pose> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_AttachPoseOnSelect
	Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * ___m_AttachPoseOnSelect_22;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable,UnityEngine.Pose> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_LocalAttachPoseOnSelect
	Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * ___m_LocalAttachPoseOnSelect_23;
	// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractable> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_UnorderedInteractablesHovered
	HashSet_1_tF6BC2B8B0620E4DF0BEEE501FCEC44042347B107 * ___m_UnorderedInteractablesHovered_24;
	// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_UnorderedInteractablesSelected
	HashSet_1_tDFB5CED1A4E6F4596865F0D1796182482075030E * ___m_UnorderedInteractablesSelected_25;
	// UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_ManualInteractionInteractable
	RuntimeObject* ___m_ManualInteractionInteractable_26;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_RegisteredInteractionManager
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___m_RegisteredInteractionManager_27;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_OnHoverEntered
	XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * ___m_OnHoverEntered_29;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_OnHoverExited
	XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * ___m_OnHoverExited_30;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_OnSelectEntered
	XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * ___m_OnSelectEntered_31;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractorEvent UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::m_OnSelectExited
	XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * ___m_OnSelectExited_32;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable> UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::<hoverTargets>k__BackingField
	List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * ___U3ChoverTargetsU3Ek__BackingField_33;

public:
	inline static int32_t get_offset_of_registered_4() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___registered_4)); }
	inline Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * get_registered_4() const { return ___registered_4; }
	inline Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D ** get_address_of_registered_4() { return &___registered_4; }
	inline void set_registered_4(Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * value)
	{
		___registered_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___registered_4), (void*)value);
	}

	inline static int32_t get_offset_of_unregistered_5() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___unregistered_5)); }
	inline Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * get_unregistered_5() const { return ___unregistered_5; }
	inline Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 ** get_address_of_unregistered_5() { return &___unregistered_5; }
	inline void set_unregistered_5(Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * value)
	{
		___unregistered_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unregistered_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractionManager_6() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_InteractionManager_6)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_m_InteractionManager_6() const { return ___m_InteractionManager_6; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_m_InteractionManager_6() { return &___m_InteractionManager_6; }
	inline void set_m_InteractionManager_6(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___m_InteractionManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractionManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractionLayerMask_7() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_InteractionLayerMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_InteractionLayerMask_7() const { return ___m_InteractionLayerMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_InteractionLayerMask_7() { return &___m_InteractionLayerMask_7; }
	inline void set_m_InteractionLayerMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_InteractionLayerMask_7 = value;
	}

	inline static int32_t get_offset_of_m_InteractionLayers_8() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_InteractionLayers_8)); }
	inline InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  get_m_InteractionLayers_8() const { return ___m_InteractionLayers_8; }
	inline InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47 * get_address_of_m_InteractionLayers_8() { return &___m_InteractionLayers_8; }
	inline void set_m_InteractionLayers_8(InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47  value)
	{
		___m_InteractionLayers_8 = value;
	}

	inline static int32_t get_offset_of_m_AttachTransform_9() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_AttachTransform_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_AttachTransform_9() const { return ___m_AttachTransform_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_AttachTransform_9() { return &___m_AttachTransform_9; }
	inline void set_m_AttachTransform_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_AttachTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AttachTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeepSelectedTargetValid_10() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_KeepSelectedTargetValid_10)); }
	inline bool get_m_KeepSelectedTargetValid_10() const { return ___m_KeepSelectedTargetValid_10; }
	inline bool* get_address_of_m_KeepSelectedTargetValid_10() { return &___m_KeepSelectedTargetValid_10; }
	inline void set_m_KeepSelectedTargetValid_10(bool value)
	{
		___m_KeepSelectedTargetValid_10 = value;
	}

	inline static int32_t get_offset_of_m_StartingSelectedInteractable_11() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_StartingSelectedInteractable_11)); }
	inline XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF * get_m_StartingSelectedInteractable_11() const { return ___m_StartingSelectedInteractable_11; }
	inline XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF ** get_address_of_m_StartingSelectedInteractable_11() { return &___m_StartingSelectedInteractable_11; }
	inline void set_m_StartingSelectedInteractable_11(XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF * value)
	{
		___m_StartingSelectedInteractable_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StartingSelectedInteractable_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverEntered_12() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_HoverEntered_12)); }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * get_m_HoverEntered_12() const { return ___m_HoverEntered_12; }
	inline HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 ** get_address_of_m_HoverEntered_12() { return &___m_HoverEntered_12; }
	inline void set_m_HoverEntered_12(HoverEnterEvent_t0CC8E826E7F14B3A2B7A343424C3533BFCE32FC6 * value)
	{
		___m_HoverEntered_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverEntered_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverExited_13() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_HoverExited_13)); }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * get_m_HoverExited_13() const { return ___m_HoverExited_13; }
	inline HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 ** get_address_of_m_HoverExited_13() { return &___m_HoverExited_13; }
	inline void set_m_HoverExited_13(HoverExitEvent_tAAF1D57BA8CEBA5B39F1409EBD6978CE06A5B341 * value)
	{
		___m_HoverExited_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverExited_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectEntered_14() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_SelectEntered_14)); }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * get_m_SelectEntered_14() const { return ___m_SelectEntered_14; }
	inline SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 ** get_address_of_m_SelectEntered_14() { return &___m_SelectEntered_14; }
	inline void set_m_SelectEntered_14(SelectEnterEvent_t147569BD69EAD584499BE2FC174458E3150C5918 * value)
	{
		___m_SelectEntered_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectEntered_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectExited_15() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_SelectExited_15)); }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * get_m_SelectExited_15() const { return ___m_SelectExited_15; }
	inline SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 ** get_address_of_m_SelectExited_15() { return &___m_SelectExited_15; }
	inline void set_m_SelectExited_15(SelectExitEvent_t8A19A5E960E731FDA7BC2509D96527199FDA57F8 * value)
	{
		___m_SelectExited_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectExited_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_AllowHover_16() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_AllowHover_16)); }
	inline bool get_m_AllowHover_16() const { return ___m_AllowHover_16; }
	inline bool* get_address_of_m_AllowHover_16() { return &___m_AllowHover_16; }
	inline void set_m_AllowHover_16(bool value)
	{
		___m_AllowHover_16 = value;
	}

	inline static int32_t get_offset_of_m_AllowSelect_17() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_AllowSelect_17)); }
	inline bool get_m_AllowSelect_17() const { return ___m_AllowSelect_17; }
	inline bool* get_address_of_m_AllowSelect_17() { return &___m_AllowSelect_17; }
	inline void set_m_AllowSelect_17(bool value)
	{
		___m_AllowSelect_17 = value;
	}

	inline static int32_t get_offset_of_m_IsPerformingManualInteraction_18() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_IsPerformingManualInteraction_18)); }
	inline bool get_m_IsPerformingManualInteraction_18() const { return ___m_IsPerformingManualInteraction_18; }
	inline bool* get_address_of_m_IsPerformingManualInteraction_18() { return &___m_IsPerformingManualInteraction_18; }
	inline void set_m_IsPerformingManualInteraction_18(bool value)
	{
		___m_IsPerformingManualInteraction_18 = value;
	}

	inline static int32_t get_offset_of_U3CinteractablesHoveredU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___U3CinteractablesHoveredU3Ek__BackingField_19)); }
	inline List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * get_U3CinteractablesHoveredU3Ek__BackingField_19() const { return ___U3CinteractablesHoveredU3Ek__BackingField_19; }
	inline List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 ** get_address_of_U3CinteractablesHoveredU3Ek__BackingField_19() { return &___U3CinteractablesHoveredU3Ek__BackingField_19; }
	inline void set_U3CinteractablesHoveredU3Ek__BackingField_19(List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * value)
	{
		___U3CinteractablesHoveredU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractablesHoveredU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinteractablesSelectedU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___U3CinteractablesSelectedU3Ek__BackingField_20)); }
	inline List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * get_U3CinteractablesSelectedU3Ek__BackingField_20() const { return ___U3CinteractablesSelectedU3Ek__BackingField_20; }
	inline List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E ** get_address_of_U3CinteractablesSelectedU3Ek__BackingField_20() { return &___U3CinteractablesSelectedU3Ek__BackingField_20; }
	inline void set_U3CinteractablesSelectedU3Ek__BackingField_20(List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * value)
	{
		___U3CinteractablesSelectedU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinteractablesSelectedU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfirstInteractableSelectedU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___U3CfirstInteractableSelectedU3Ek__BackingField_21)); }
	inline RuntimeObject* get_U3CfirstInteractableSelectedU3Ek__BackingField_21() const { return ___U3CfirstInteractableSelectedU3Ek__BackingField_21; }
	inline RuntimeObject** get_address_of_U3CfirstInteractableSelectedU3Ek__BackingField_21() { return &___U3CfirstInteractableSelectedU3Ek__BackingField_21; }
	inline void set_U3CfirstInteractableSelectedU3Ek__BackingField_21(RuntimeObject* value)
	{
		___U3CfirstInteractableSelectedU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfirstInteractableSelectedU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_AttachPoseOnSelect_22() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_AttachPoseOnSelect_22)); }
	inline Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * get_m_AttachPoseOnSelect_22() const { return ___m_AttachPoseOnSelect_22; }
	inline Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 ** get_address_of_m_AttachPoseOnSelect_22() { return &___m_AttachPoseOnSelect_22; }
	inline void set_m_AttachPoseOnSelect_22(Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * value)
	{
		___m_AttachPoseOnSelect_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AttachPoseOnSelect_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_LocalAttachPoseOnSelect_23() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_LocalAttachPoseOnSelect_23)); }
	inline Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * get_m_LocalAttachPoseOnSelect_23() const { return ___m_LocalAttachPoseOnSelect_23; }
	inline Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 ** get_address_of_m_LocalAttachPoseOnSelect_23() { return &___m_LocalAttachPoseOnSelect_23; }
	inline void set_m_LocalAttachPoseOnSelect_23(Dictionary_2_t6A653984EB988FC39CFB8D564C2AC1D6FA223119 * value)
	{
		___m_LocalAttachPoseOnSelect_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LocalAttachPoseOnSelect_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_UnorderedInteractablesHovered_24() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_UnorderedInteractablesHovered_24)); }
	inline HashSet_1_tF6BC2B8B0620E4DF0BEEE501FCEC44042347B107 * get_m_UnorderedInteractablesHovered_24() const { return ___m_UnorderedInteractablesHovered_24; }
	inline HashSet_1_tF6BC2B8B0620E4DF0BEEE501FCEC44042347B107 ** get_address_of_m_UnorderedInteractablesHovered_24() { return &___m_UnorderedInteractablesHovered_24; }
	inline void set_m_UnorderedInteractablesHovered_24(HashSet_1_tF6BC2B8B0620E4DF0BEEE501FCEC44042347B107 * value)
	{
		___m_UnorderedInteractablesHovered_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UnorderedInteractablesHovered_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_UnorderedInteractablesSelected_25() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_UnorderedInteractablesSelected_25)); }
	inline HashSet_1_tDFB5CED1A4E6F4596865F0D1796182482075030E * get_m_UnorderedInteractablesSelected_25() const { return ___m_UnorderedInteractablesSelected_25; }
	inline HashSet_1_tDFB5CED1A4E6F4596865F0D1796182482075030E ** get_address_of_m_UnorderedInteractablesSelected_25() { return &___m_UnorderedInteractablesSelected_25; }
	inline void set_m_UnorderedInteractablesSelected_25(HashSet_1_tDFB5CED1A4E6F4596865F0D1796182482075030E * value)
	{
		___m_UnorderedInteractablesSelected_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UnorderedInteractablesSelected_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ManualInteractionInteractable_26() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_ManualInteractionInteractable_26)); }
	inline RuntimeObject* get_m_ManualInteractionInteractable_26() const { return ___m_ManualInteractionInteractable_26; }
	inline RuntimeObject** get_address_of_m_ManualInteractionInteractable_26() { return &___m_ManualInteractionInteractable_26; }
	inline void set_m_ManualInteractionInteractable_26(RuntimeObject* value)
	{
		___m_ManualInteractionInteractable_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ManualInteractionInteractable_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_RegisteredInteractionManager_27() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_RegisteredInteractionManager_27)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_m_RegisteredInteractionManager_27() const { return ___m_RegisteredInteractionManager_27; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_m_RegisteredInteractionManager_27() { return &___m_RegisteredInteractionManager_27; }
	inline void set_m_RegisteredInteractionManager_27(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___m_RegisteredInteractionManager_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RegisteredInteractionManager_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHoverEntered_29() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_OnHoverEntered_29)); }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * get_m_OnHoverEntered_29() const { return ___m_OnHoverEntered_29; }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 ** get_address_of_m_OnHoverEntered_29() { return &___m_OnHoverEntered_29; }
	inline void set_m_OnHoverEntered_29(XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * value)
	{
		___m_OnHoverEntered_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHoverEntered_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHoverExited_30() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_OnHoverExited_30)); }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * get_m_OnHoverExited_30() const { return ___m_OnHoverExited_30; }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 ** get_address_of_m_OnHoverExited_30() { return &___m_OnHoverExited_30; }
	inline void set_m_OnHoverExited_30(XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * value)
	{
		___m_OnHoverExited_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHoverExited_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectEntered_31() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_OnSelectEntered_31)); }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * get_m_OnSelectEntered_31() const { return ___m_OnSelectEntered_31; }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 ** get_address_of_m_OnSelectEntered_31() { return &___m_OnSelectEntered_31; }
	inline void set_m_OnSelectEntered_31(XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * value)
	{
		___m_OnSelectEntered_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectEntered_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectExited_32() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___m_OnSelectExited_32)); }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * get_m_OnSelectExited_32() const { return ___m_OnSelectExited_32; }
	inline XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 ** get_address_of_m_OnSelectExited_32() { return &___m_OnSelectExited_32; }
	inline void set_m_OnSelectExited_32(XRInteractorEvent_t9DCC223AAFEFEB69B54EE515291C79BEC6189A98 * value)
	{
		___m_OnSelectExited_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectExited_32), (void*)value);
	}

	inline static int32_t get_offset_of_U3ChoverTargetsU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F, ___U3ChoverTargetsU3Ek__BackingField_33)); }
	inline List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * get_U3ChoverTargetsU3Ek__BackingField_33() const { return ___U3ChoverTargetsU3Ek__BackingField_33; }
	inline List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 ** get_address_of_U3ChoverTargetsU3Ek__BackingField_33() { return &___U3ChoverTargetsU3Ek__BackingField_33; }
	inline void set_U3ChoverTargetsU3Ek__BackingField_33(List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * value)
	{
		___U3ChoverTargetsU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChoverTargetsU3Ek__BackingField_33), (void*)value);
	}
};

struct XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F_StaticFields
{
public:
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager UnityEngine.XR.Interaction.Toolkit.XRBaseInteractor::s_InteractionManagerCache
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * ___s_InteractionManagerCache_28;

public:
	inline static int32_t get_offset_of_s_InteractionManagerCache_28() { return static_cast<int32_t>(offsetof(XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F_StaticFields, ___s_InteractionManagerCache_28)); }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * get_s_InteractionManagerCache_28() const { return ___s_InteractionManagerCache_28; }
	inline XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 ** get_address_of_s_InteractionManagerCache_28() { return &___s_InteractionManagerCache_28; }
	inline void set_s_InteractionManagerCache_28(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4 * value)
	{
		___s_InteractionManagerCache_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InteractionManagerCache_28), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator
struct XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardXTranslateAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_KeyboardXTranslateAction_4;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardYTranslateAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_KeyboardYTranslateAction_5;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardZTranslateAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_KeyboardZTranslateAction_6;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateLeftAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ManipulateLeftAction_7;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateRightAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ManipulateRightAction_8;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleManipulateLeftAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleManipulateLeftAction_9;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleManipulateRightAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleManipulateRightAction_10;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateHeadAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ManipulateHeadAction_11;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseDeltaAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_MouseDeltaAction_12;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseScrollAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_MouseScrollAction_13;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RotateModeOverrideAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_RotateModeOverrideAction_14;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleMouseTransformationModeAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleMouseTransformationModeAction_15;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_NegateModeAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_NegateModeAction_16;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_XConstraintAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_XConstraintAction_17;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_YConstraintAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_YConstraintAction_18;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ZConstraintAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ZConstraintAction_19;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ResetAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ResetAction_20;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleCursorLockAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleCursorLockAction_21;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleDevicePositionTargetAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleDevicePositionTargetAction_22;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_TogglePrimary2DAxisTargetAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_TogglePrimary2DAxisTargetAction_23;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ToggleSecondary2DAxisTargetAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_ToggleSecondary2DAxisTargetAction_24;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Axis2DAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Axis2DAction_25;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RestingHandAxis2DAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_RestingHandAxis2DAction_26;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_GripAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_GripAction_27;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_TriggerAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_TriggerAction_28;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_PrimaryButtonAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_PrimaryButtonAction_29;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_SecondaryButtonAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_SecondaryButtonAction_30;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MenuAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_MenuAction_31;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Primary2DAxisClickAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Primary2DAxisClickAction_32;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Secondary2DAxisClickAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Secondary2DAxisClickAction_33;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Primary2DAxisTouchAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Primary2DAxisTouchAction_34;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Secondary2DAxisTouchAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_Secondary2DAxisTouchAction_35;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_PrimaryTouchAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_PrimaryTouchAction_36;
	// UnityEngine.InputSystem.InputActionReference UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_SecondaryTouchAction
	InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * ___m_SecondaryTouchAction_37;
	// UnityEngine.Transform UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_CameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_CameraTransform_38;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Space UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardTranslateSpace
	int32_t ___m_KeyboardTranslateSpace_39;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Space UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseTranslateSpace
	int32_t ___m_MouseTranslateSpace_40;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardXTranslateSpeed
	float ___m_KeyboardXTranslateSpeed_41;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardYTranslateSpeed
	float ___m_KeyboardYTranslateSpeed_42;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardZTranslateSpeed
	float ___m_KeyboardZTranslateSpeed_43;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseXTranslateSensitivity
	float ___m_MouseXTranslateSensitivity_44;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseYTranslateSensitivity
	float ___m_MouseYTranslateSensitivity_45;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseScrollTranslateSensitivity
	float ___m_MouseScrollTranslateSensitivity_46;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseXRotateSensitivity
	float ___m_MouseXRotateSensitivity_47;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseYRotateSensitivity
	float ___m_MouseYRotateSensitivity_48;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseScrollRotateSensitivity
	float ___m_MouseScrollRotateSensitivity_49;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseYRotateInvert
	bool ___m_MouseYRotateInvert_50;
	// UnityEngine.CursorLockMode UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_DesiredCursorLockMode
	int32_t ___m_DesiredCursorLockMode_51;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/TransformationMode UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::<mouseTransformationMode>k__BackingField
	int32_t ___U3CmouseTransformationModeU3Ek__BackingField_52;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator/Axis2DTargets UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::<axis2DTargets>k__BackingField
	int32_t ___U3Caxis2DTargetsU3Ek__BackingField_53;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardXTranslateInput
	float ___m_KeyboardXTranslateInput_54;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardYTranslateInput
	float ___m_KeyboardYTranslateInput_55;
	// System.Single UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_KeyboardZTranslateInput
	float ___m_KeyboardZTranslateInput_56;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateLeftInput
	bool ___m_ManipulateLeftInput_57;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateRightInput
	bool ___m_ManipulateRightInput_58;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulateHeadInput
	bool ___m_ManipulateHeadInput_59;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseDeltaInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MouseDeltaInput_60;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MouseScrollInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MouseScrollInput_61;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RotateModeOverrideInput
	bool ___m_RotateModeOverrideInput_62;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_NegateModeInput
	bool ___m_NegateModeInput_63;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_XConstraintInput
	bool ___m_XConstraintInput_64;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_YConstraintInput
	bool ___m_YConstraintInput_65;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ZConstraintInput
	bool ___m_ZConstraintInput_66;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ResetInput
	bool ___m_ResetInput_67;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Axis2DInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Axis2DInput_68;
	// UnityEngine.Vector2 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RestingHandAxis2DInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RestingHandAxis2DInput_69;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_GripInput
	bool ___m_GripInput_70;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_TriggerInput
	bool ___m_TriggerInput_71;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_PrimaryButtonInput
	bool ___m_PrimaryButtonInput_72;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_SecondaryButtonInput
	bool ___m_SecondaryButtonInput_73;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_MenuInput
	bool ___m_MenuInput_74;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Primary2DAxisClickInput
	bool ___m_Primary2DAxisClickInput_75;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Secondary2DAxisClickInput
	bool ___m_Secondary2DAxisClickInput_76;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Primary2DAxisTouchInput
	bool ___m_Primary2DAxisTouchInput_77;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_Secondary2DAxisTouchInput
	bool ___m_Secondary2DAxisTouchInput_78;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_PrimaryTouchInput
	bool ___m_PrimaryTouchInput_79;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_SecondaryTouchInput
	bool ___m_SecondaryTouchInput_80;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_ManipulatedRestingHandAxis2D
	bool ___m_ManipulatedRestingHandAxis2D_81;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_LeftControllerEuler
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_LeftControllerEuler_82;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RightControllerEuler
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_RightControllerEuler_83;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_CenterEyeEuler
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_CenterEyeEuler_84;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMDState UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_HMDState
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7  ___m_HMDState_85;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_LeftControllerState
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  ___m_LeftControllerState_86;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedControllerState UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RightControllerState
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  ___m_RightControllerState_87;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMD UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_HMDDevice
	XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540 * ___m_HMDDevice_88;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_LeftControllerDevice
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * ___m_LeftControllerDevice_89;
	// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRDeviceSimulator::m_RightControllerDevice
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * ___m_RightControllerDevice_90;

public:
	inline static int32_t get_offset_of_m_KeyboardXTranslateAction_4() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardXTranslateAction_4)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_KeyboardXTranslateAction_4() const { return ___m_KeyboardXTranslateAction_4; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_KeyboardXTranslateAction_4() { return &___m_KeyboardXTranslateAction_4; }
	inline void set_m_KeyboardXTranslateAction_4(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_KeyboardXTranslateAction_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_KeyboardXTranslateAction_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeyboardYTranslateAction_5() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardYTranslateAction_5)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_KeyboardYTranslateAction_5() const { return ___m_KeyboardYTranslateAction_5; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_KeyboardYTranslateAction_5() { return &___m_KeyboardYTranslateAction_5; }
	inline void set_m_KeyboardYTranslateAction_5(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_KeyboardYTranslateAction_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_KeyboardYTranslateAction_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeyboardZTranslateAction_6() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardZTranslateAction_6)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_KeyboardZTranslateAction_6() const { return ___m_KeyboardZTranslateAction_6; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_KeyboardZTranslateAction_6() { return &___m_KeyboardZTranslateAction_6; }
	inline void set_m_KeyboardZTranslateAction_6(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_KeyboardZTranslateAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_KeyboardZTranslateAction_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ManipulateLeftAction_7() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateLeftAction_7)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ManipulateLeftAction_7() const { return ___m_ManipulateLeftAction_7; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ManipulateLeftAction_7() { return &___m_ManipulateLeftAction_7; }
	inline void set_m_ManipulateLeftAction_7(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ManipulateLeftAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ManipulateLeftAction_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_ManipulateRightAction_8() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateRightAction_8)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ManipulateRightAction_8() const { return ___m_ManipulateRightAction_8; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ManipulateRightAction_8() { return &___m_ManipulateRightAction_8; }
	inline void set_m_ManipulateRightAction_8(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ManipulateRightAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ManipulateRightAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleManipulateLeftAction_9() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleManipulateLeftAction_9)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleManipulateLeftAction_9() const { return ___m_ToggleManipulateLeftAction_9; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleManipulateLeftAction_9() { return &___m_ToggleManipulateLeftAction_9; }
	inline void set_m_ToggleManipulateLeftAction_9(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleManipulateLeftAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleManipulateLeftAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleManipulateRightAction_10() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleManipulateRightAction_10)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleManipulateRightAction_10() const { return ___m_ToggleManipulateRightAction_10; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleManipulateRightAction_10() { return &___m_ToggleManipulateRightAction_10; }
	inline void set_m_ToggleManipulateRightAction_10(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleManipulateRightAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleManipulateRightAction_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_ManipulateHeadAction_11() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateHeadAction_11)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ManipulateHeadAction_11() const { return ___m_ManipulateHeadAction_11; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ManipulateHeadAction_11() { return &___m_ManipulateHeadAction_11; }
	inline void set_m_ManipulateHeadAction_11(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ManipulateHeadAction_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ManipulateHeadAction_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_MouseDeltaAction_12() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseDeltaAction_12)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_MouseDeltaAction_12() const { return ___m_MouseDeltaAction_12; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_MouseDeltaAction_12() { return &___m_MouseDeltaAction_12; }
	inline void set_m_MouseDeltaAction_12(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_MouseDeltaAction_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MouseDeltaAction_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_MouseScrollAction_13() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseScrollAction_13)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_MouseScrollAction_13() const { return ___m_MouseScrollAction_13; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_MouseScrollAction_13() { return &___m_MouseScrollAction_13; }
	inline void set_m_MouseScrollAction_13(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_MouseScrollAction_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MouseScrollAction_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_RotateModeOverrideAction_14() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RotateModeOverrideAction_14)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_RotateModeOverrideAction_14() const { return ___m_RotateModeOverrideAction_14; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_RotateModeOverrideAction_14() { return &___m_RotateModeOverrideAction_14; }
	inline void set_m_RotateModeOverrideAction_14(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_RotateModeOverrideAction_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RotateModeOverrideAction_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleMouseTransformationModeAction_15() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleMouseTransformationModeAction_15)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleMouseTransformationModeAction_15() const { return ___m_ToggleMouseTransformationModeAction_15; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleMouseTransformationModeAction_15() { return &___m_ToggleMouseTransformationModeAction_15; }
	inline void set_m_ToggleMouseTransformationModeAction_15(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleMouseTransformationModeAction_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleMouseTransformationModeAction_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_NegateModeAction_16() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_NegateModeAction_16)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_NegateModeAction_16() const { return ___m_NegateModeAction_16; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_NegateModeAction_16() { return &___m_NegateModeAction_16; }
	inline void set_m_NegateModeAction_16(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_NegateModeAction_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_NegateModeAction_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_XConstraintAction_17() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_XConstraintAction_17)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_XConstraintAction_17() const { return ___m_XConstraintAction_17; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_XConstraintAction_17() { return &___m_XConstraintAction_17; }
	inline void set_m_XConstraintAction_17(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_XConstraintAction_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_XConstraintAction_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_YConstraintAction_18() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_YConstraintAction_18)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_YConstraintAction_18() const { return ___m_YConstraintAction_18; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_YConstraintAction_18() { return &___m_YConstraintAction_18; }
	inline void set_m_YConstraintAction_18(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_YConstraintAction_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_YConstraintAction_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_ZConstraintAction_19() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ZConstraintAction_19)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ZConstraintAction_19() const { return ___m_ZConstraintAction_19; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ZConstraintAction_19() { return &___m_ZConstraintAction_19; }
	inline void set_m_ZConstraintAction_19(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ZConstraintAction_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ZConstraintAction_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_ResetAction_20() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ResetAction_20)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ResetAction_20() const { return ___m_ResetAction_20; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ResetAction_20() { return &___m_ResetAction_20; }
	inline void set_m_ResetAction_20(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ResetAction_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResetAction_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleCursorLockAction_21() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleCursorLockAction_21)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleCursorLockAction_21() const { return ___m_ToggleCursorLockAction_21; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleCursorLockAction_21() { return &___m_ToggleCursorLockAction_21; }
	inline void set_m_ToggleCursorLockAction_21(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleCursorLockAction_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleCursorLockAction_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleDevicePositionTargetAction_22() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleDevicePositionTargetAction_22)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleDevicePositionTargetAction_22() const { return ___m_ToggleDevicePositionTargetAction_22; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleDevicePositionTargetAction_22() { return &___m_ToggleDevicePositionTargetAction_22; }
	inline void set_m_ToggleDevicePositionTargetAction_22(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleDevicePositionTargetAction_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleDevicePositionTargetAction_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_TogglePrimary2DAxisTargetAction_23() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_TogglePrimary2DAxisTargetAction_23)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_TogglePrimary2DAxisTargetAction_23() const { return ___m_TogglePrimary2DAxisTargetAction_23; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_TogglePrimary2DAxisTargetAction_23() { return &___m_TogglePrimary2DAxisTargetAction_23; }
	inline void set_m_TogglePrimary2DAxisTargetAction_23(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_TogglePrimary2DAxisTargetAction_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TogglePrimary2DAxisTargetAction_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ToggleSecondary2DAxisTargetAction_24() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ToggleSecondary2DAxisTargetAction_24)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_ToggleSecondary2DAxisTargetAction_24() const { return ___m_ToggleSecondary2DAxisTargetAction_24; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_ToggleSecondary2DAxisTargetAction_24() { return &___m_ToggleSecondary2DAxisTargetAction_24; }
	inline void set_m_ToggleSecondary2DAxisTargetAction_24(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_ToggleSecondary2DAxisTargetAction_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleSecondary2DAxisTargetAction_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_Axis2DAction_25() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Axis2DAction_25)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Axis2DAction_25() const { return ___m_Axis2DAction_25; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Axis2DAction_25() { return &___m_Axis2DAction_25; }
	inline void set_m_Axis2DAction_25(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Axis2DAction_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Axis2DAction_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_RestingHandAxis2DAction_26() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RestingHandAxis2DAction_26)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_RestingHandAxis2DAction_26() const { return ___m_RestingHandAxis2DAction_26; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_RestingHandAxis2DAction_26() { return &___m_RestingHandAxis2DAction_26; }
	inline void set_m_RestingHandAxis2DAction_26(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_RestingHandAxis2DAction_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RestingHandAxis2DAction_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_GripAction_27() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_GripAction_27)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_GripAction_27() const { return ___m_GripAction_27; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_GripAction_27() { return &___m_GripAction_27; }
	inline void set_m_GripAction_27(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_GripAction_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GripAction_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_TriggerAction_28() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_TriggerAction_28)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_TriggerAction_28() const { return ___m_TriggerAction_28; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_TriggerAction_28() { return &___m_TriggerAction_28; }
	inline void set_m_TriggerAction_28(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_TriggerAction_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TriggerAction_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrimaryButtonAction_29() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_PrimaryButtonAction_29)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_PrimaryButtonAction_29() const { return ___m_PrimaryButtonAction_29; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_PrimaryButtonAction_29() { return &___m_PrimaryButtonAction_29; }
	inline void set_m_PrimaryButtonAction_29(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_PrimaryButtonAction_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PrimaryButtonAction_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_SecondaryButtonAction_30() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_SecondaryButtonAction_30)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_SecondaryButtonAction_30() const { return ___m_SecondaryButtonAction_30; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_SecondaryButtonAction_30() { return &___m_SecondaryButtonAction_30; }
	inline void set_m_SecondaryButtonAction_30(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_SecondaryButtonAction_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SecondaryButtonAction_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_MenuAction_31() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MenuAction_31)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_MenuAction_31() const { return ___m_MenuAction_31; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_MenuAction_31() { return &___m_MenuAction_31; }
	inline void set_m_MenuAction_31(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_MenuAction_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MenuAction_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_Primary2DAxisClickAction_32() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Primary2DAxisClickAction_32)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Primary2DAxisClickAction_32() const { return ___m_Primary2DAxisClickAction_32; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Primary2DAxisClickAction_32() { return &___m_Primary2DAxisClickAction_32; }
	inline void set_m_Primary2DAxisClickAction_32(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Primary2DAxisClickAction_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Primary2DAxisClickAction_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Secondary2DAxisClickAction_33() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Secondary2DAxisClickAction_33)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Secondary2DAxisClickAction_33() const { return ___m_Secondary2DAxisClickAction_33; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Secondary2DAxisClickAction_33() { return &___m_Secondary2DAxisClickAction_33; }
	inline void set_m_Secondary2DAxisClickAction_33(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Secondary2DAxisClickAction_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Secondary2DAxisClickAction_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_Primary2DAxisTouchAction_34() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Primary2DAxisTouchAction_34)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Primary2DAxisTouchAction_34() const { return ___m_Primary2DAxisTouchAction_34; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Primary2DAxisTouchAction_34() { return &___m_Primary2DAxisTouchAction_34; }
	inline void set_m_Primary2DAxisTouchAction_34(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Primary2DAxisTouchAction_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Primary2DAxisTouchAction_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Secondary2DAxisTouchAction_35() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Secondary2DAxisTouchAction_35)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_Secondary2DAxisTouchAction_35() const { return ___m_Secondary2DAxisTouchAction_35; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_Secondary2DAxisTouchAction_35() { return &___m_Secondary2DAxisTouchAction_35; }
	inline void set_m_Secondary2DAxisTouchAction_35(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_Secondary2DAxisTouchAction_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Secondary2DAxisTouchAction_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrimaryTouchAction_36() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_PrimaryTouchAction_36)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_PrimaryTouchAction_36() const { return ___m_PrimaryTouchAction_36; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_PrimaryTouchAction_36() { return &___m_PrimaryTouchAction_36; }
	inline void set_m_PrimaryTouchAction_36(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_PrimaryTouchAction_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PrimaryTouchAction_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_SecondaryTouchAction_37() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_SecondaryTouchAction_37)); }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * get_m_SecondaryTouchAction_37() const { return ___m_SecondaryTouchAction_37; }
	inline InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A ** get_address_of_m_SecondaryTouchAction_37() { return &___m_SecondaryTouchAction_37; }
	inline void set_m_SecondaryTouchAction_37(InputActionReference_tAE3699F433D56FD7BA2625B4BD531A299334DB0A * value)
	{
		___m_SecondaryTouchAction_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SecondaryTouchAction_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraTransform_38() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_CameraTransform_38)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_CameraTransform_38() const { return ___m_CameraTransform_38; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_CameraTransform_38() { return &___m_CameraTransform_38; }
	inline void set_m_CameraTransform_38(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_CameraTransform_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraTransform_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeyboardTranslateSpace_39() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardTranslateSpace_39)); }
	inline int32_t get_m_KeyboardTranslateSpace_39() const { return ___m_KeyboardTranslateSpace_39; }
	inline int32_t* get_address_of_m_KeyboardTranslateSpace_39() { return &___m_KeyboardTranslateSpace_39; }
	inline void set_m_KeyboardTranslateSpace_39(int32_t value)
	{
		___m_KeyboardTranslateSpace_39 = value;
	}

	inline static int32_t get_offset_of_m_MouseTranslateSpace_40() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseTranslateSpace_40)); }
	inline int32_t get_m_MouseTranslateSpace_40() const { return ___m_MouseTranslateSpace_40; }
	inline int32_t* get_address_of_m_MouseTranslateSpace_40() { return &___m_MouseTranslateSpace_40; }
	inline void set_m_MouseTranslateSpace_40(int32_t value)
	{
		___m_MouseTranslateSpace_40 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardXTranslateSpeed_41() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardXTranslateSpeed_41)); }
	inline float get_m_KeyboardXTranslateSpeed_41() const { return ___m_KeyboardXTranslateSpeed_41; }
	inline float* get_address_of_m_KeyboardXTranslateSpeed_41() { return &___m_KeyboardXTranslateSpeed_41; }
	inline void set_m_KeyboardXTranslateSpeed_41(float value)
	{
		___m_KeyboardXTranslateSpeed_41 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardYTranslateSpeed_42() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardYTranslateSpeed_42)); }
	inline float get_m_KeyboardYTranslateSpeed_42() const { return ___m_KeyboardYTranslateSpeed_42; }
	inline float* get_address_of_m_KeyboardYTranslateSpeed_42() { return &___m_KeyboardYTranslateSpeed_42; }
	inline void set_m_KeyboardYTranslateSpeed_42(float value)
	{
		___m_KeyboardYTranslateSpeed_42 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardZTranslateSpeed_43() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardZTranslateSpeed_43)); }
	inline float get_m_KeyboardZTranslateSpeed_43() const { return ___m_KeyboardZTranslateSpeed_43; }
	inline float* get_address_of_m_KeyboardZTranslateSpeed_43() { return &___m_KeyboardZTranslateSpeed_43; }
	inline void set_m_KeyboardZTranslateSpeed_43(float value)
	{
		___m_KeyboardZTranslateSpeed_43 = value;
	}

	inline static int32_t get_offset_of_m_MouseXTranslateSensitivity_44() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseXTranslateSensitivity_44)); }
	inline float get_m_MouseXTranslateSensitivity_44() const { return ___m_MouseXTranslateSensitivity_44; }
	inline float* get_address_of_m_MouseXTranslateSensitivity_44() { return &___m_MouseXTranslateSensitivity_44; }
	inline void set_m_MouseXTranslateSensitivity_44(float value)
	{
		___m_MouseXTranslateSensitivity_44 = value;
	}

	inline static int32_t get_offset_of_m_MouseYTranslateSensitivity_45() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseYTranslateSensitivity_45)); }
	inline float get_m_MouseYTranslateSensitivity_45() const { return ___m_MouseYTranslateSensitivity_45; }
	inline float* get_address_of_m_MouseYTranslateSensitivity_45() { return &___m_MouseYTranslateSensitivity_45; }
	inline void set_m_MouseYTranslateSensitivity_45(float value)
	{
		___m_MouseYTranslateSensitivity_45 = value;
	}

	inline static int32_t get_offset_of_m_MouseScrollTranslateSensitivity_46() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseScrollTranslateSensitivity_46)); }
	inline float get_m_MouseScrollTranslateSensitivity_46() const { return ___m_MouseScrollTranslateSensitivity_46; }
	inline float* get_address_of_m_MouseScrollTranslateSensitivity_46() { return &___m_MouseScrollTranslateSensitivity_46; }
	inline void set_m_MouseScrollTranslateSensitivity_46(float value)
	{
		___m_MouseScrollTranslateSensitivity_46 = value;
	}

	inline static int32_t get_offset_of_m_MouseXRotateSensitivity_47() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseXRotateSensitivity_47)); }
	inline float get_m_MouseXRotateSensitivity_47() const { return ___m_MouseXRotateSensitivity_47; }
	inline float* get_address_of_m_MouseXRotateSensitivity_47() { return &___m_MouseXRotateSensitivity_47; }
	inline void set_m_MouseXRotateSensitivity_47(float value)
	{
		___m_MouseXRotateSensitivity_47 = value;
	}

	inline static int32_t get_offset_of_m_MouseYRotateSensitivity_48() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseYRotateSensitivity_48)); }
	inline float get_m_MouseYRotateSensitivity_48() const { return ___m_MouseYRotateSensitivity_48; }
	inline float* get_address_of_m_MouseYRotateSensitivity_48() { return &___m_MouseYRotateSensitivity_48; }
	inline void set_m_MouseYRotateSensitivity_48(float value)
	{
		___m_MouseYRotateSensitivity_48 = value;
	}

	inline static int32_t get_offset_of_m_MouseScrollRotateSensitivity_49() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseScrollRotateSensitivity_49)); }
	inline float get_m_MouseScrollRotateSensitivity_49() const { return ___m_MouseScrollRotateSensitivity_49; }
	inline float* get_address_of_m_MouseScrollRotateSensitivity_49() { return &___m_MouseScrollRotateSensitivity_49; }
	inline void set_m_MouseScrollRotateSensitivity_49(float value)
	{
		___m_MouseScrollRotateSensitivity_49 = value;
	}

	inline static int32_t get_offset_of_m_MouseYRotateInvert_50() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseYRotateInvert_50)); }
	inline bool get_m_MouseYRotateInvert_50() const { return ___m_MouseYRotateInvert_50; }
	inline bool* get_address_of_m_MouseYRotateInvert_50() { return &___m_MouseYRotateInvert_50; }
	inline void set_m_MouseYRotateInvert_50(bool value)
	{
		___m_MouseYRotateInvert_50 = value;
	}

	inline static int32_t get_offset_of_m_DesiredCursorLockMode_51() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_DesiredCursorLockMode_51)); }
	inline int32_t get_m_DesiredCursorLockMode_51() const { return ___m_DesiredCursorLockMode_51; }
	inline int32_t* get_address_of_m_DesiredCursorLockMode_51() { return &___m_DesiredCursorLockMode_51; }
	inline void set_m_DesiredCursorLockMode_51(int32_t value)
	{
		___m_DesiredCursorLockMode_51 = value;
	}

	inline static int32_t get_offset_of_U3CmouseTransformationModeU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___U3CmouseTransformationModeU3Ek__BackingField_52)); }
	inline int32_t get_U3CmouseTransformationModeU3Ek__BackingField_52() const { return ___U3CmouseTransformationModeU3Ek__BackingField_52; }
	inline int32_t* get_address_of_U3CmouseTransformationModeU3Ek__BackingField_52() { return &___U3CmouseTransformationModeU3Ek__BackingField_52; }
	inline void set_U3CmouseTransformationModeU3Ek__BackingField_52(int32_t value)
	{
		___U3CmouseTransformationModeU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_U3Caxis2DTargetsU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___U3Caxis2DTargetsU3Ek__BackingField_53)); }
	inline int32_t get_U3Caxis2DTargetsU3Ek__BackingField_53() const { return ___U3Caxis2DTargetsU3Ek__BackingField_53; }
	inline int32_t* get_address_of_U3Caxis2DTargetsU3Ek__BackingField_53() { return &___U3Caxis2DTargetsU3Ek__BackingField_53; }
	inline void set_U3Caxis2DTargetsU3Ek__BackingField_53(int32_t value)
	{
		___U3Caxis2DTargetsU3Ek__BackingField_53 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardXTranslateInput_54() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardXTranslateInput_54)); }
	inline float get_m_KeyboardXTranslateInput_54() const { return ___m_KeyboardXTranslateInput_54; }
	inline float* get_address_of_m_KeyboardXTranslateInput_54() { return &___m_KeyboardXTranslateInput_54; }
	inline void set_m_KeyboardXTranslateInput_54(float value)
	{
		___m_KeyboardXTranslateInput_54 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardYTranslateInput_55() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardYTranslateInput_55)); }
	inline float get_m_KeyboardYTranslateInput_55() const { return ___m_KeyboardYTranslateInput_55; }
	inline float* get_address_of_m_KeyboardYTranslateInput_55() { return &___m_KeyboardYTranslateInput_55; }
	inline void set_m_KeyboardYTranslateInput_55(float value)
	{
		___m_KeyboardYTranslateInput_55 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardZTranslateInput_56() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_KeyboardZTranslateInput_56)); }
	inline float get_m_KeyboardZTranslateInput_56() const { return ___m_KeyboardZTranslateInput_56; }
	inline float* get_address_of_m_KeyboardZTranslateInput_56() { return &___m_KeyboardZTranslateInput_56; }
	inline void set_m_KeyboardZTranslateInput_56(float value)
	{
		___m_KeyboardZTranslateInput_56 = value;
	}

	inline static int32_t get_offset_of_m_ManipulateLeftInput_57() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateLeftInput_57)); }
	inline bool get_m_ManipulateLeftInput_57() const { return ___m_ManipulateLeftInput_57; }
	inline bool* get_address_of_m_ManipulateLeftInput_57() { return &___m_ManipulateLeftInput_57; }
	inline void set_m_ManipulateLeftInput_57(bool value)
	{
		___m_ManipulateLeftInput_57 = value;
	}

	inline static int32_t get_offset_of_m_ManipulateRightInput_58() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateRightInput_58)); }
	inline bool get_m_ManipulateRightInput_58() const { return ___m_ManipulateRightInput_58; }
	inline bool* get_address_of_m_ManipulateRightInput_58() { return &___m_ManipulateRightInput_58; }
	inline void set_m_ManipulateRightInput_58(bool value)
	{
		___m_ManipulateRightInput_58 = value;
	}

	inline static int32_t get_offset_of_m_ManipulateHeadInput_59() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulateHeadInput_59)); }
	inline bool get_m_ManipulateHeadInput_59() const { return ___m_ManipulateHeadInput_59; }
	inline bool* get_address_of_m_ManipulateHeadInput_59() { return &___m_ManipulateHeadInput_59; }
	inline void set_m_ManipulateHeadInput_59(bool value)
	{
		___m_ManipulateHeadInput_59 = value;
	}

	inline static int32_t get_offset_of_m_MouseDeltaInput_60() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseDeltaInput_60)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MouseDeltaInput_60() const { return ___m_MouseDeltaInput_60; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MouseDeltaInput_60() { return &___m_MouseDeltaInput_60; }
	inline void set_m_MouseDeltaInput_60(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MouseDeltaInput_60 = value;
	}

	inline static int32_t get_offset_of_m_MouseScrollInput_61() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MouseScrollInput_61)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MouseScrollInput_61() const { return ___m_MouseScrollInput_61; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MouseScrollInput_61() { return &___m_MouseScrollInput_61; }
	inline void set_m_MouseScrollInput_61(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MouseScrollInput_61 = value;
	}

	inline static int32_t get_offset_of_m_RotateModeOverrideInput_62() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RotateModeOverrideInput_62)); }
	inline bool get_m_RotateModeOverrideInput_62() const { return ___m_RotateModeOverrideInput_62; }
	inline bool* get_address_of_m_RotateModeOverrideInput_62() { return &___m_RotateModeOverrideInput_62; }
	inline void set_m_RotateModeOverrideInput_62(bool value)
	{
		___m_RotateModeOverrideInput_62 = value;
	}

	inline static int32_t get_offset_of_m_NegateModeInput_63() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_NegateModeInput_63)); }
	inline bool get_m_NegateModeInput_63() const { return ___m_NegateModeInput_63; }
	inline bool* get_address_of_m_NegateModeInput_63() { return &___m_NegateModeInput_63; }
	inline void set_m_NegateModeInput_63(bool value)
	{
		___m_NegateModeInput_63 = value;
	}

	inline static int32_t get_offset_of_m_XConstraintInput_64() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_XConstraintInput_64)); }
	inline bool get_m_XConstraintInput_64() const { return ___m_XConstraintInput_64; }
	inline bool* get_address_of_m_XConstraintInput_64() { return &___m_XConstraintInput_64; }
	inline void set_m_XConstraintInput_64(bool value)
	{
		___m_XConstraintInput_64 = value;
	}

	inline static int32_t get_offset_of_m_YConstraintInput_65() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_YConstraintInput_65)); }
	inline bool get_m_YConstraintInput_65() const { return ___m_YConstraintInput_65; }
	inline bool* get_address_of_m_YConstraintInput_65() { return &___m_YConstraintInput_65; }
	inline void set_m_YConstraintInput_65(bool value)
	{
		___m_YConstraintInput_65 = value;
	}

	inline static int32_t get_offset_of_m_ZConstraintInput_66() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ZConstraintInput_66)); }
	inline bool get_m_ZConstraintInput_66() const { return ___m_ZConstraintInput_66; }
	inline bool* get_address_of_m_ZConstraintInput_66() { return &___m_ZConstraintInput_66; }
	inline void set_m_ZConstraintInput_66(bool value)
	{
		___m_ZConstraintInput_66 = value;
	}

	inline static int32_t get_offset_of_m_ResetInput_67() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ResetInput_67)); }
	inline bool get_m_ResetInput_67() const { return ___m_ResetInput_67; }
	inline bool* get_address_of_m_ResetInput_67() { return &___m_ResetInput_67; }
	inline void set_m_ResetInput_67(bool value)
	{
		___m_ResetInput_67 = value;
	}

	inline static int32_t get_offset_of_m_Axis2DInput_68() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Axis2DInput_68)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Axis2DInput_68() const { return ___m_Axis2DInput_68; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Axis2DInput_68() { return &___m_Axis2DInput_68; }
	inline void set_m_Axis2DInput_68(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Axis2DInput_68 = value;
	}

	inline static int32_t get_offset_of_m_RestingHandAxis2DInput_69() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RestingHandAxis2DInput_69)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_RestingHandAxis2DInput_69() const { return ___m_RestingHandAxis2DInput_69; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_RestingHandAxis2DInput_69() { return &___m_RestingHandAxis2DInput_69; }
	inline void set_m_RestingHandAxis2DInput_69(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_RestingHandAxis2DInput_69 = value;
	}

	inline static int32_t get_offset_of_m_GripInput_70() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_GripInput_70)); }
	inline bool get_m_GripInput_70() const { return ___m_GripInput_70; }
	inline bool* get_address_of_m_GripInput_70() { return &___m_GripInput_70; }
	inline void set_m_GripInput_70(bool value)
	{
		___m_GripInput_70 = value;
	}

	inline static int32_t get_offset_of_m_TriggerInput_71() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_TriggerInput_71)); }
	inline bool get_m_TriggerInput_71() const { return ___m_TriggerInput_71; }
	inline bool* get_address_of_m_TriggerInput_71() { return &___m_TriggerInput_71; }
	inline void set_m_TriggerInput_71(bool value)
	{
		___m_TriggerInput_71 = value;
	}

	inline static int32_t get_offset_of_m_PrimaryButtonInput_72() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_PrimaryButtonInput_72)); }
	inline bool get_m_PrimaryButtonInput_72() const { return ___m_PrimaryButtonInput_72; }
	inline bool* get_address_of_m_PrimaryButtonInput_72() { return &___m_PrimaryButtonInput_72; }
	inline void set_m_PrimaryButtonInput_72(bool value)
	{
		___m_PrimaryButtonInput_72 = value;
	}

	inline static int32_t get_offset_of_m_SecondaryButtonInput_73() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_SecondaryButtonInput_73)); }
	inline bool get_m_SecondaryButtonInput_73() const { return ___m_SecondaryButtonInput_73; }
	inline bool* get_address_of_m_SecondaryButtonInput_73() { return &___m_SecondaryButtonInput_73; }
	inline void set_m_SecondaryButtonInput_73(bool value)
	{
		___m_SecondaryButtonInput_73 = value;
	}

	inline static int32_t get_offset_of_m_MenuInput_74() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_MenuInput_74)); }
	inline bool get_m_MenuInput_74() const { return ___m_MenuInput_74; }
	inline bool* get_address_of_m_MenuInput_74() { return &___m_MenuInput_74; }
	inline void set_m_MenuInput_74(bool value)
	{
		___m_MenuInput_74 = value;
	}

	inline static int32_t get_offset_of_m_Primary2DAxisClickInput_75() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Primary2DAxisClickInput_75)); }
	inline bool get_m_Primary2DAxisClickInput_75() const { return ___m_Primary2DAxisClickInput_75; }
	inline bool* get_address_of_m_Primary2DAxisClickInput_75() { return &___m_Primary2DAxisClickInput_75; }
	inline void set_m_Primary2DAxisClickInput_75(bool value)
	{
		___m_Primary2DAxisClickInput_75 = value;
	}

	inline static int32_t get_offset_of_m_Secondary2DAxisClickInput_76() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Secondary2DAxisClickInput_76)); }
	inline bool get_m_Secondary2DAxisClickInput_76() const { return ___m_Secondary2DAxisClickInput_76; }
	inline bool* get_address_of_m_Secondary2DAxisClickInput_76() { return &___m_Secondary2DAxisClickInput_76; }
	inline void set_m_Secondary2DAxisClickInput_76(bool value)
	{
		___m_Secondary2DAxisClickInput_76 = value;
	}

	inline static int32_t get_offset_of_m_Primary2DAxisTouchInput_77() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Primary2DAxisTouchInput_77)); }
	inline bool get_m_Primary2DAxisTouchInput_77() const { return ___m_Primary2DAxisTouchInput_77; }
	inline bool* get_address_of_m_Primary2DAxisTouchInput_77() { return &___m_Primary2DAxisTouchInput_77; }
	inline void set_m_Primary2DAxisTouchInput_77(bool value)
	{
		___m_Primary2DAxisTouchInput_77 = value;
	}

	inline static int32_t get_offset_of_m_Secondary2DAxisTouchInput_78() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_Secondary2DAxisTouchInput_78)); }
	inline bool get_m_Secondary2DAxisTouchInput_78() const { return ___m_Secondary2DAxisTouchInput_78; }
	inline bool* get_address_of_m_Secondary2DAxisTouchInput_78() { return &___m_Secondary2DAxisTouchInput_78; }
	inline void set_m_Secondary2DAxisTouchInput_78(bool value)
	{
		___m_Secondary2DAxisTouchInput_78 = value;
	}

	inline static int32_t get_offset_of_m_PrimaryTouchInput_79() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_PrimaryTouchInput_79)); }
	inline bool get_m_PrimaryTouchInput_79() const { return ___m_PrimaryTouchInput_79; }
	inline bool* get_address_of_m_PrimaryTouchInput_79() { return &___m_PrimaryTouchInput_79; }
	inline void set_m_PrimaryTouchInput_79(bool value)
	{
		___m_PrimaryTouchInput_79 = value;
	}

	inline static int32_t get_offset_of_m_SecondaryTouchInput_80() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_SecondaryTouchInput_80)); }
	inline bool get_m_SecondaryTouchInput_80() const { return ___m_SecondaryTouchInput_80; }
	inline bool* get_address_of_m_SecondaryTouchInput_80() { return &___m_SecondaryTouchInput_80; }
	inline void set_m_SecondaryTouchInput_80(bool value)
	{
		___m_SecondaryTouchInput_80 = value;
	}

	inline static int32_t get_offset_of_m_ManipulatedRestingHandAxis2D_81() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_ManipulatedRestingHandAxis2D_81)); }
	inline bool get_m_ManipulatedRestingHandAxis2D_81() const { return ___m_ManipulatedRestingHandAxis2D_81; }
	inline bool* get_address_of_m_ManipulatedRestingHandAxis2D_81() { return &___m_ManipulatedRestingHandAxis2D_81; }
	inline void set_m_ManipulatedRestingHandAxis2D_81(bool value)
	{
		___m_ManipulatedRestingHandAxis2D_81 = value;
	}

	inline static int32_t get_offset_of_m_LeftControllerEuler_82() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_LeftControllerEuler_82)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_LeftControllerEuler_82() const { return ___m_LeftControllerEuler_82; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_LeftControllerEuler_82() { return &___m_LeftControllerEuler_82; }
	inline void set_m_LeftControllerEuler_82(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_LeftControllerEuler_82 = value;
	}

	inline static int32_t get_offset_of_m_RightControllerEuler_83() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RightControllerEuler_83)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_RightControllerEuler_83() const { return ___m_RightControllerEuler_83; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_RightControllerEuler_83() { return &___m_RightControllerEuler_83; }
	inline void set_m_RightControllerEuler_83(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_RightControllerEuler_83 = value;
	}

	inline static int32_t get_offset_of_m_CenterEyeEuler_84() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_CenterEyeEuler_84)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_CenterEyeEuler_84() const { return ___m_CenterEyeEuler_84; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_CenterEyeEuler_84() { return &___m_CenterEyeEuler_84; }
	inline void set_m_CenterEyeEuler_84(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_CenterEyeEuler_84 = value;
	}

	inline static int32_t get_offset_of_m_HMDState_85() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_HMDState_85)); }
	inline XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7  get_m_HMDState_85() const { return ___m_HMDState_85; }
	inline XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7 * get_address_of_m_HMDState_85() { return &___m_HMDState_85; }
	inline void set_m_HMDState_85(XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7  value)
	{
		___m_HMDState_85 = value;
	}

	inline static int32_t get_offset_of_m_LeftControllerState_86() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_LeftControllerState_86)); }
	inline XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  get_m_LeftControllerState_86() const { return ___m_LeftControllerState_86; }
	inline XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175 * get_address_of_m_LeftControllerState_86() { return &___m_LeftControllerState_86; }
	inline void set_m_LeftControllerState_86(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  value)
	{
		___m_LeftControllerState_86 = value;
	}

	inline static int32_t get_offset_of_m_RightControllerState_87() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RightControllerState_87)); }
	inline XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  get_m_RightControllerState_87() const { return ___m_RightControllerState_87; }
	inline XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175 * get_address_of_m_RightControllerState_87() { return &___m_RightControllerState_87; }
	inline void set_m_RightControllerState_87(XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175  value)
	{
		___m_RightControllerState_87 = value;
	}

	inline static int32_t get_offset_of_m_HMDDevice_88() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_HMDDevice_88)); }
	inline XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540 * get_m_HMDDevice_88() const { return ___m_HMDDevice_88; }
	inline XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540 ** get_address_of_m_HMDDevice_88() { return &___m_HMDDevice_88; }
	inline void set_m_HMDDevice_88(XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540 * value)
	{
		___m_HMDDevice_88 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HMDDevice_88), (void*)value);
	}

	inline static int32_t get_offset_of_m_LeftControllerDevice_89() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_LeftControllerDevice_89)); }
	inline XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * get_m_LeftControllerDevice_89() const { return ___m_LeftControllerDevice_89; }
	inline XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 ** get_address_of_m_LeftControllerDevice_89() { return &___m_LeftControllerDevice_89; }
	inline void set_m_LeftControllerDevice_89(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * value)
	{
		___m_LeftControllerDevice_89 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LeftControllerDevice_89), (void*)value);
	}

	inline static int32_t get_offset_of_m_RightControllerDevice_90() { return static_cast<int32_t>(offsetof(XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825, ___m_RightControllerDevice_90)); }
	inline XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * get_m_RightControllerDevice_90() const { return ___m_RightControllerDevice_90; }
	inline XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 ** get_address_of_m_RightControllerDevice_90() { return &___m_RightControllerDevice_90; }
	inline void set_m_RightControllerDevice_90(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49 * value)
	{
		___m_RightControllerDevice_90 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RightControllerDevice_90), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager
struct XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::interactorRegistered
	Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * ___interactorRegistered_4;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::interactorUnregistered
	Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * ___interactorUnregistered_5;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::interactableRegistered
	Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * ___interactableRegistered_6;
	// System.Action`1<UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::interactableUnregistered
	Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * ___interactableUnregistered_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Collider,UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_ColliderToInteractableMap
	Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * ___m_ColliderToInteractableMap_9;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager/RegistrationList`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractor> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_Interactors
	RegistrationList_1_t4D52D9B0E80F106AA327DDFA7DCDBCA8F4051098 * ___m_Interactors_10;
	// UnityEngine.XR.Interaction.Toolkit.XRInteractionManager/RegistrationList`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_Interactables
	RegistrationList_1_t6F5496053891E363EC0159ED3ADE4170E39DEA22 * ___m_Interactables_11;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRHoverInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_CurrentHovered
	List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * ___m_CurrentHovered_12;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRSelectInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_CurrentSelected
	List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * ___m_CurrentSelected_13;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_ValidTargets
	List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * ___m_ValidTargets_14;
	// System.Collections.Generic.HashSet`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_UnorderedValidTargets
	HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * ___m_UnorderedValidTargets_15;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_DeprecatedValidTargets
	List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * ___m_DeprecatedValidTargets_16;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractor> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_ScratchInteractors
	List_1_tCAFBFD76706B54DB653FF3DD6594D969F208A84E * ___m_ScratchInteractors_17;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_ScratchInteractables
	List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * ___m_ScratchInteractables_18;
	// UnityEngine.XR.Interaction.Toolkit.SelectEnterEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_SelectEnterEventArgs
	SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE * ___m_SelectEnterEventArgs_19;
	// UnityEngine.XR.Interaction.Toolkit.SelectExitEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_SelectExitEventArgs
	SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E * ___m_SelectExitEventArgs_20;
	// UnityEngine.XR.Interaction.Toolkit.HoverEnterEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_HoverEnterEventArgs
	HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9 * ___m_HoverEnterEventArgs_21;
	// UnityEngine.XR.Interaction.Toolkit.HoverExitEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_HoverExitEventArgs
	HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293 * ___m_HoverExitEventArgs_22;
	// UnityEngine.XR.Interaction.Toolkit.InteractorRegisteredEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_InteractorRegisteredEventArgs
	InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE * ___m_InteractorRegisteredEventArgs_23;
	// UnityEngine.XR.Interaction.Toolkit.InteractorUnregisteredEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_InteractorUnregisteredEventArgs
	InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206 * ___m_InteractorUnregisteredEventArgs_24;
	// UnityEngine.XR.Interaction.Toolkit.InteractableRegisteredEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_InteractableRegisteredEventArgs
	InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F * ___m_InteractableRegisteredEventArgs_25;
	// UnityEngine.XR.Interaction.Toolkit.InteractableUnregisteredEventArgs UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::m_InteractableUnregisteredEventArgs
	InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D * ___m_InteractableUnregisteredEventArgs_26;

public:
	inline static int32_t get_offset_of_interactorRegistered_4() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___interactorRegistered_4)); }
	inline Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * get_interactorRegistered_4() const { return ___interactorRegistered_4; }
	inline Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D ** get_address_of_interactorRegistered_4() { return &___interactorRegistered_4; }
	inline void set_interactorRegistered_4(Action_1_t6051DEDF984988738563FB98BD7D78AD81A0C77D * value)
	{
		___interactorRegistered_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactorRegistered_4), (void*)value);
	}

	inline static int32_t get_offset_of_interactorUnregistered_5() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___interactorUnregistered_5)); }
	inline Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * get_interactorUnregistered_5() const { return ___interactorUnregistered_5; }
	inline Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 ** get_address_of_interactorUnregistered_5() { return &___interactorUnregistered_5; }
	inline void set_interactorUnregistered_5(Action_1_tE6C3262B5BB25E9A20BA4358C5687205008A1261 * value)
	{
		___interactorUnregistered_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactorUnregistered_5), (void*)value);
	}

	inline static int32_t get_offset_of_interactableRegistered_6() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___interactableRegistered_6)); }
	inline Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * get_interactableRegistered_6() const { return ___interactableRegistered_6; }
	inline Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD ** get_address_of_interactableRegistered_6() { return &___interactableRegistered_6; }
	inline void set_interactableRegistered_6(Action_1_t9F32937D0483D7D5F5EE6D501C90EC8AD96A73BD * value)
	{
		___interactableRegistered_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactableRegistered_6), (void*)value);
	}

	inline static int32_t get_offset_of_interactableUnregistered_7() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___interactableUnregistered_7)); }
	inline Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * get_interactableUnregistered_7() const { return ___interactableUnregistered_7; }
	inline Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA ** get_address_of_interactableUnregistered_7() { return &___interactableUnregistered_7; }
	inline void set_interactableUnregistered_7(Action_1_t093BAA55388BF4C1A03FA3625640C13B56C601AA * value)
	{
		___interactableUnregistered_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactableUnregistered_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColliderToInteractableMap_9() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_ColliderToInteractableMap_9)); }
	inline Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * get_m_ColliderToInteractableMap_9() const { return ___m_ColliderToInteractableMap_9; }
	inline Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 ** get_address_of_m_ColliderToInteractableMap_9() { return &___m_ColliderToInteractableMap_9; }
	inline void set_m_ColliderToInteractableMap_9(Dictionary_2_tD06DEC3B9E7AC19B31379B218D65E9C58BFF9894 * value)
	{
		___m_ColliderToInteractableMap_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColliderToInteractableMap_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactors_10() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_Interactors_10)); }
	inline RegistrationList_1_t4D52D9B0E80F106AA327DDFA7DCDBCA8F4051098 * get_m_Interactors_10() const { return ___m_Interactors_10; }
	inline RegistrationList_1_t4D52D9B0E80F106AA327DDFA7DCDBCA8F4051098 ** get_address_of_m_Interactors_10() { return &___m_Interactors_10; }
	inline void set_m_Interactors_10(RegistrationList_1_t4D52D9B0E80F106AA327DDFA7DCDBCA8F4051098 * value)
	{
		___m_Interactors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactors_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactables_11() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_Interactables_11)); }
	inline RegistrationList_1_t6F5496053891E363EC0159ED3ADE4170E39DEA22 * get_m_Interactables_11() const { return ___m_Interactables_11; }
	inline RegistrationList_1_t6F5496053891E363EC0159ED3ADE4170E39DEA22 ** get_address_of_m_Interactables_11() { return &___m_Interactables_11; }
	inline void set_m_Interactables_11(RegistrationList_1_t6F5496053891E363EC0159ED3ADE4170E39DEA22 * value)
	{
		___m_Interactables_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactables_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentHovered_12() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_CurrentHovered_12)); }
	inline List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * get_m_CurrentHovered_12() const { return ___m_CurrentHovered_12; }
	inline List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 ** get_address_of_m_CurrentHovered_12() { return &___m_CurrentHovered_12; }
	inline void set_m_CurrentHovered_12(List_1_t0EDE7B646794983AD89A3E567022A7A1C17FB365 * value)
	{
		___m_CurrentHovered_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentHovered_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentSelected_13() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_CurrentSelected_13)); }
	inline List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * get_m_CurrentSelected_13() const { return ___m_CurrentSelected_13; }
	inline List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E ** get_address_of_m_CurrentSelected_13() { return &___m_CurrentSelected_13; }
	inline void set_m_CurrentSelected_13(List_1_t471EBB9BADE4A0B8711AC39DEF94BC759D83FC6E * value)
	{
		___m_CurrentSelected_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_ValidTargets_14() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_ValidTargets_14)); }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * get_m_ValidTargets_14() const { return ___m_ValidTargets_14; }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 ** get_address_of_m_ValidTargets_14() { return &___m_ValidTargets_14; }
	inline void set_m_ValidTargets_14(List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * value)
	{
		___m_ValidTargets_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ValidTargets_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_UnorderedValidTargets_15() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_UnorderedValidTargets_15)); }
	inline HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * get_m_UnorderedValidTargets_15() const { return ___m_UnorderedValidTargets_15; }
	inline HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F ** get_address_of_m_UnorderedValidTargets_15() { return &___m_UnorderedValidTargets_15; }
	inline void set_m_UnorderedValidTargets_15(HashSet_1_tDFE64DF247F047B9475BD969377F52A61B0FB92F * value)
	{
		___m_UnorderedValidTargets_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UnorderedValidTargets_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeprecatedValidTargets_16() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_DeprecatedValidTargets_16)); }
	inline List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * get_m_DeprecatedValidTargets_16() const { return ___m_DeprecatedValidTargets_16; }
	inline List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 ** get_address_of_m_DeprecatedValidTargets_16() { return &___m_DeprecatedValidTargets_16; }
	inline void set_m_DeprecatedValidTargets_16(List_1_t8F9C188959ECAB5554BCCA621026A2CC5EE3DE91 * value)
	{
		___m_DeprecatedValidTargets_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeprecatedValidTargets_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScratchInteractors_17() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_ScratchInteractors_17)); }
	inline List_1_tCAFBFD76706B54DB653FF3DD6594D969F208A84E * get_m_ScratchInteractors_17() const { return ___m_ScratchInteractors_17; }
	inline List_1_tCAFBFD76706B54DB653FF3DD6594D969F208A84E ** get_address_of_m_ScratchInteractors_17() { return &___m_ScratchInteractors_17; }
	inline void set_m_ScratchInteractors_17(List_1_tCAFBFD76706B54DB653FF3DD6594D969F208A84E * value)
	{
		___m_ScratchInteractors_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScratchInteractors_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScratchInteractables_18() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_ScratchInteractables_18)); }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * get_m_ScratchInteractables_18() const { return ___m_ScratchInteractables_18; }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 ** get_address_of_m_ScratchInteractables_18() { return &___m_ScratchInteractables_18; }
	inline void set_m_ScratchInteractables_18(List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * value)
	{
		___m_ScratchInteractables_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScratchInteractables_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectEnterEventArgs_19() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_SelectEnterEventArgs_19)); }
	inline SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE * get_m_SelectEnterEventArgs_19() const { return ___m_SelectEnterEventArgs_19; }
	inline SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE ** get_address_of_m_SelectEnterEventArgs_19() { return &___m_SelectEnterEventArgs_19; }
	inline void set_m_SelectEnterEventArgs_19(SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE * value)
	{
		___m_SelectEnterEventArgs_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectEnterEventArgs_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectExitEventArgs_20() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_SelectExitEventArgs_20)); }
	inline SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E * get_m_SelectExitEventArgs_20() const { return ___m_SelectExitEventArgs_20; }
	inline SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E ** get_address_of_m_SelectExitEventArgs_20() { return &___m_SelectExitEventArgs_20; }
	inline void set_m_SelectExitEventArgs_20(SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E * value)
	{
		___m_SelectExitEventArgs_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectExitEventArgs_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverEnterEventArgs_21() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_HoverEnterEventArgs_21)); }
	inline HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9 * get_m_HoverEnterEventArgs_21() const { return ___m_HoverEnterEventArgs_21; }
	inline HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9 ** get_address_of_m_HoverEnterEventArgs_21() { return &___m_HoverEnterEventArgs_21; }
	inline void set_m_HoverEnterEventArgs_21(HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9 * value)
	{
		___m_HoverEnterEventArgs_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverEnterEventArgs_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_HoverExitEventArgs_22() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_HoverExitEventArgs_22)); }
	inline HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293 * get_m_HoverExitEventArgs_22() const { return ___m_HoverExitEventArgs_22; }
	inline HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293 ** get_address_of_m_HoverExitEventArgs_22() { return &___m_HoverExitEventArgs_22; }
	inline void set_m_HoverExitEventArgs_22(HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293 * value)
	{
		___m_HoverExitEventArgs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HoverExitEventArgs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractorRegisteredEventArgs_23() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_InteractorRegisteredEventArgs_23)); }
	inline InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE * get_m_InteractorRegisteredEventArgs_23() const { return ___m_InteractorRegisteredEventArgs_23; }
	inline InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE ** get_address_of_m_InteractorRegisteredEventArgs_23() { return &___m_InteractorRegisteredEventArgs_23; }
	inline void set_m_InteractorRegisteredEventArgs_23(InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE * value)
	{
		___m_InteractorRegisteredEventArgs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractorRegisteredEventArgs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractorUnregisteredEventArgs_24() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_InteractorUnregisteredEventArgs_24)); }
	inline InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206 * get_m_InteractorUnregisteredEventArgs_24() const { return ___m_InteractorUnregisteredEventArgs_24; }
	inline InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206 ** get_address_of_m_InteractorUnregisteredEventArgs_24() { return &___m_InteractorUnregisteredEventArgs_24; }
	inline void set_m_InteractorUnregisteredEventArgs_24(InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206 * value)
	{
		___m_InteractorUnregisteredEventArgs_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractorUnregisteredEventArgs_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractableRegisteredEventArgs_25() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_InteractableRegisteredEventArgs_25)); }
	inline InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F * get_m_InteractableRegisteredEventArgs_25() const { return ___m_InteractableRegisteredEventArgs_25; }
	inline InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F ** get_address_of_m_InteractableRegisteredEventArgs_25() { return &___m_InteractableRegisteredEventArgs_25; }
	inline void set_m_InteractableRegisteredEventArgs_25(InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F * value)
	{
		___m_InteractableRegisteredEventArgs_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractableRegisteredEventArgs_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractableUnregisteredEventArgs_26() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4, ___m_InteractableUnregisteredEventArgs_26)); }
	inline InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D * get_m_InteractableUnregisteredEventArgs_26() const { return ___m_InteractableUnregisteredEventArgs_26; }
	inline InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D ** get_address_of_m_InteractableUnregisteredEventArgs_26() { return &___m_InteractableUnregisteredEventArgs_26; }
	inline void set_m_InteractableUnregisteredEventArgs_26(InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D * value)
	{
		___m_InteractableUnregisteredEventArgs_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractableUnregisteredEventArgs_26), (void*)value);
	}
};

struct XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRInteractionManager> UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::<activeInteractionManagers>k__BackingField
	List_1_t19B2C24A261798D687174CD32FE67952C7E70DD7 * ___U3CactiveInteractionManagersU3Ek__BackingField_8;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_PreprocessInteractorsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_PreprocessInteractorsMarker_27;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_ProcessInteractorsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_ProcessInteractorsMarker_28;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_ProcessInteractablesMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_ProcessInteractablesMarker_29;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_GetValidTargetsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_GetValidTargetsMarker_30;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_FilterRegisteredValidTargetsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_FilterRegisteredValidTargetsMarker_31;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_EvaluateInvalidSelectionsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_EvaluateInvalidSelectionsMarker_32;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_EvaluateInvalidHoversMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_EvaluateInvalidHoversMarker_33;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_EvaluateValidSelectionsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_EvaluateValidSelectionsMarker_34;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_EvaluateValidHoversMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_EvaluateValidHoversMarker_35;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_SelectEnterMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_SelectEnterMarker_36;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_SelectExitMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_SelectExitMarker_37;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_HoverEnterMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_HoverEnterMarker_38;
	// Unity.Profiling.ProfilerMarker UnityEngine.XR.Interaction.Toolkit.XRInteractionManager::s_HoverExitMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___s_HoverExitMarker_39;

public:
	inline static int32_t get_offset_of_U3CactiveInteractionManagersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___U3CactiveInteractionManagersU3Ek__BackingField_8)); }
	inline List_1_t19B2C24A261798D687174CD32FE67952C7E70DD7 * get_U3CactiveInteractionManagersU3Ek__BackingField_8() const { return ___U3CactiveInteractionManagersU3Ek__BackingField_8; }
	inline List_1_t19B2C24A261798D687174CD32FE67952C7E70DD7 ** get_address_of_U3CactiveInteractionManagersU3Ek__BackingField_8() { return &___U3CactiveInteractionManagersU3Ek__BackingField_8; }
	inline void set_U3CactiveInteractionManagersU3Ek__BackingField_8(List_1_t19B2C24A261798D687174CD32FE67952C7E70DD7 * value)
	{
		___U3CactiveInteractionManagersU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CactiveInteractionManagersU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_PreprocessInteractorsMarker_27() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_PreprocessInteractorsMarker_27)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_PreprocessInteractorsMarker_27() const { return ___s_PreprocessInteractorsMarker_27; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_PreprocessInteractorsMarker_27() { return &___s_PreprocessInteractorsMarker_27; }
	inline void set_s_PreprocessInteractorsMarker_27(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_PreprocessInteractorsMarker_27 = value;
	}

	inline static int32_t get_offset_of_s_ProcessInteractorsMarker_28() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_ProcessInteractorsMarker_28)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_ProcessInteractorsMarker_28() const { return ___s_ProcessInteractorsMarker_28; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_ProcessInteractorsMarker_28() { return &___s_ProcessInteractorsMarker_28; }
	inline void set_s_ProcessInteractorsMarker_28(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_ProcessInteractorsMarker_28 = value;
	}

	inline static int32_t get_offset_of_s_ProcessInteractablesMarker_29() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_ProcessInteractablesMarker_29)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_ProcessInteractablesMarker_29() const { return ___s_ProcessInteractablesMarker_29; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_ProcessInteractablesMarker_29() { return &___s_ProcessInteractablesMarker_29; }
	inline void set_s_ProcessInteractablesMarker_29(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_ProcessInteractablesMarker_29 = value;
	}

	inline static int32_t get_offset_of_s_GetValidTargetsMarker_30() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_GetValidTargetsMarker_30)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_GetValidTargetsMarker_30() const { return ___s_GetValidTargetsMarker_30; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_GetValidTargetsMarker_30() { return &___s_GetValidTargetsMarker_30; }
	inline void set_s_GetValidTargetsMarker_30(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_GetValidTargetsMarker_30 = value;
	}

	inline static int32_t get_offset_of_s_FilterRegisteredValidTargetsMarker_31() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_FilterRegisteredValidTargetsMarker_31)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_FilterRegisteredValidTargetsMarker_31() const { return ___s_FilterRegisteredValidTargetsMarker_31; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_FilterRegisteredValidTargetsMarker_31() { return &___s_FilterRegisteredValidTargetsMarker_31; }
	inline void set_s_FilterRegisteredValidTargetsMarker_31(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_FilterRegisteredValidTargetsMarker_31 = value;
	}

	inline static int32_t get_offset_of_s_EvaluateInvalidSelectionsMarker_32() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_EvaluateInvalidSelectionsMarker_32)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_EvaluateInvalidSelectionsMarker_32() const { return ___s_EvaluateInvalidSelectionsMarker_32; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_EvaluateInvalidSelectionsMarker_32() { return &___s_EvaluateInvalidSelectionsMarker_32; }
	inline void set_s_EvaluateInvalidSelectionsMarker_32(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_EvaluateInvalidSelectionsMarker_32 = value;
	}

	inline static int32_t get_offset_of_s_EvaluateInvalidHoversMarker_33() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_EvaluateInvalidHoversMarker_33)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_EvaluateInvalidHoversMarker_33() const { return ___s_EvaluateInvalidHoversMarker_33; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_EvaluateInvalidHoversMarker_33() { return &___s_EvaluateInvalidHoversMarker_33; }
	inline void set_s_EvaluateInvalidHoversMarker_33(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_EvaluateInvalidHoversMarker_33 = value;
	}

	inline static int32_t get_offset_of_s_EvaluateValidSelectionsMarker_34() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_EvaluateValidSelectionsMarker_34)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_EvaluateValidSelectionsMarker_34() const { return ___s_EvaluateValidSelectionsMarker_34; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_EvaluateValidSelectionsMarker_34() { return &___s_EvaluateValidSelectionsMarker_34; }
	inline void set_s_EvaluateValidSelectionsMarker_34(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_EvaluateValidSelectionsMarker_34 = value;
	}

	inline static int32_t get_offset_of_s_EvaluateValidHoversMarker_35() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_EvaluateValidHoversMarker_35)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_EvaluateValidHoversMarker_35() const { return ___s_EvaluateValidHoversMarker_35; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_EvaluateValidHoversMarker_35() { return &___s_EvaluateValidHoversMarker_35; }
	inline void set_s_EvaluateValidHoversMarker_35(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_EvaluateValidHoversMarker_35 = value;
	}

	inline static int32_t get_offset_of_s_SelectEnterMarker_36() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_SelectEnterMarker_36)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_SelectEnterMarker_36() const { return ___s_SelectEnterMarker_36; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_SelectEnterMarker_36() { return &___s_SelectEnterMarker_36; }
	inline void set_s_SelectEnterMarker_36(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_SelectEnterMarker_36 = value;
	}

	inline static int32_t get_offset_of_s_SelectExitMarker_37() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_SelectExitMarker_37)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_SelectExitMarker_37() const { return ___s_SelectExitMarker_37; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_SelectExitMarker_37() { return &___s_SelectExitMarker_37; }
	inline void set_s_SelectExitMarker_37(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_SelectExitMarker_37 = value;
	}

	inline static int32_t get_offset_of_s_HoverEnterMarker_38() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_HoverEnterMarker_38)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_HoverEnterMarker_38() const { return ___s_HoverEnterMarker_38; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_HoverEnterMarker_38() { return &___s_HoverEnterMarker_38; }
	inline void set_s_HoverEnterMarker_38(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_HoverEnterMarker_38 = value;
	}

	inline static int32_t get_offset_of_s_HoverExitMarker_39() { return static_cast<int32_t>(offsetof(XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields, ___s_HoverExitMarker_39)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_s_HoverExitMarker_39() const { return ___s_HoverExitMarker_39; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_s_HoverExitMarker_39() { return &___s_HoverExitMarker_39; }
	inline void set_s_HoverExitMarker_39(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___s_HoverExitMarker_39 = value;
	}
};


// Unity.XR.CoreUtils.XROrigin
struct XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera Unity.XR.CoreUtils.XROrigin::m_Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_Camera_4;
	// UnityEngine.Transform Unity.XR.CoreUtils.XROrigin::<TrackablesParent>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CTrackablesParentU3Ek__BackingField_5;
	// System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs> Unity.XR.CoreUtils.XROrigin::TrackablesParentTransformChanged
	Action_1_t61B82ED6C34AFA284483A0FB8F1E0D07BA0C266E * ___TrackablesParentTransformChanged_6;
	// UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::m_OriginBaseGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_OriginBaseGameObject_8;
	// UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::m_CameraFloorOffsetObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CameraFloorOffsetObject_9;
	// Unity.XR.CoreUtils.XROrigin/TrackingOriginMode Unity.XR.CoreUtils.XROrigin::m_RequestedTrackingOriginMode
	int32_t ___m_RequestedTrackingOriginMode_10;
	// System.Single Unity.XR.CoreUtils.XROrigin::m_CameraYOffset
	float ___m_CameraYOffset_11;
	// UnityEngine.XR.TrackingOriginModeFlags Unity.XR.CoreUtils.XROrigin::<CurrentTrackingOriginMode>k__BackingField
	int32_t ___U3CCurrentTrackingOriginModeU3Ek__BackingField_12;
	// System.Boolean Unity.XR.CoreUtils.XROrigin::m_CameraInitialized
	bool ___m_CameraInitialized_14;
	// System.Boolean Unity.XR.CoreUtils.XROrigin::m_CameraInitializing
	bool ___m_CameraInitializing_15;

public:
	inline static int32_t get_offset_of_m_Camera_4() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_Camera_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_Camera_4() const { return ___m_Camera_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_Camera_4() { return &___m_Camera_4; }
	inline void set_m_Camera_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_Camera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Camera_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTrackablesParentU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___U3CTrackablesParentU3Ek__BackingField_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CTrackablesParentU3Ek__BackingField_5() const { return ___U3CTrackablesParentU3Ek__BackingField_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CTrackablesParentU3Ek__BackingField_5() { return &___U3CTrackablesParentU3Ek__BackingField_5; }
	inline void set_U3CTrackablesParentU3Ek__BackingField_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CTrackablesParentU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTrackablesParentU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_TrackablesParentTransformChanged_6() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___TrackablesParentTransformChanged_6)); }
	inline Action_1_t61B82ED6C34AFA284483A0FB8F1E0D07BA0C266E * get_TrackablesParentTransformChanged_6() const { return ___TrackablesParentTransformChanged_6; }
	inline Action_1_t61B82ED6C34AFA284483A0FB8F1E0D07BA0C266E ** get_address_of_TrackablesParentTransformChanged_6() { return &___TrackablesParentTransformChanged_6; }
	inline void set_TrackablesParentTransformChanged_6(Action_1_t61B82ED6C34AFA284483A0FB8F1E0D07BA0C266E * value)
	{
		___TrackablesParentTransformChanged_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackablesParentTransformChanged_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_OriginBaseGameObject_8() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_OriginBaseGameObject_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_OriginBaseGameObject_8() const { return ___m_OriginBaseGameObject_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_OriginBaseGameObject_8() { return &___m_OriginBaseGameObject_8; }
	inline void set_m_OriginBaseGameObject_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_OriginBaseGameObject_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OriginBaseGameObject_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraFloorOffsetObject_9() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_CameraFloorOffsetObject_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CameraFloorOffsetObject_9() const { return ___m_CameraFloorOffsetObject_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CameraFloorOffsetObject_9() { return &___m_CameraFloorOffsetObject_9; }
	inline void set_m_CameraFloorOffsetObject_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CameraFloorOffsetObject_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraFloorOffsetObject_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_RequestedTrackingOriginMode_10() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_RequestedTrackingOriginMode_10)); }
	inline int32_t get_m_RequestedTrackingOriginMode_10() const { return ___m_RequestedTrackingOriginMode_10; }
	inline int32_t* get_address_of_m_RequestedTrackingOriginMode_10() { return &___m_RequestedTrackingOriginMode_10; }
	inline void set_m_RequestedTrackingOriginMode_10(int32_t value)
	{
		___m_RequestedTrackingOriginMode_10 = value;
	}

	inline static int32_t get_offset_of_m_CameraYOffset_11() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_CameraYOffset_11)); }
	inline float get_m_CameraYOffset_11() const { return ___m_CameraYOffset_11; }
	inline float* get_address_of_m_CameraYOffset_11() { return &___m_CameraYOffset_11; }
	inline void set_m_CameraYOffset_11(float value)
	{
		___m_CameraYOffset_11 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentTrackingOriginModeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___U3CCurrentTrackingOriginModeU3Ek__BackingField_12)); }
	inline int32_t get_U3CCurrentTrackingOriginModeU3Ek__BackingField_12() const { return ___U3CCurrentTrackingOriginModeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CCurrentTrackingOriginModeU3Ek__BackingField_12() { return &___U3CCurrentTrackingOriginModeU3Ek__BackingField_12; }
	inline void set_U3CCurrentTrackingOriginModeU3Ek__BackingField_12(int32_t value)
	{
		___U3CCurrentTrackingOriginModeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_m_CameraInitialized_14() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_CameraInitialized_14)); }
	inline bool get_m_CameraInitialized_14() const { return ___m_CameraInitialized_14; }
	inline bool* get_address_of_m_CameraInitialized_14() { return &___m_CameraInitialized_14; }
	inline void set_m_CameraInitialized_14(bool value)
	{
		___m_CameraInitialized_14 = value;
	}

	inline static int32_t get_offset_of_m_CameraInitializing_15() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D, ___m_CameraInitializing_15)); }
	inline bool get_m_CameraInitializing_15() const { return ___m_CameraInitializing_15; }
	inline bool* get_address_of_m_CameraInitializing_15() { return &___m_CameraInitializing_15; }
	inline void set_m_CameraInitializing_15(bool value)
	{
		___m_CameraInitializing_15 = value;
	}
};

struct XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem> Unity.XR.CoreUtils.XROrigin::s_InputSubsystems
	List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * ___s_InputSubsystems_13;

public:
	inline static int32_t get_offset_of_s_InputSubsystems_13() { return static_cast<int32_t>(offsetof(XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D_StaticFields, ___s_InputSubsystems_13)); }
	inline List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * get_s_InputSubsystems_13() const { return ___s_InputSubsystems_13; }
	inline List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 ** get_address_of_s_InputSubsystems_13() { return &___s_InputSubsystems_13; }
	inline void set_s_InputSubsystems_13(List_1_t39579540B4BF5D674E4CAA282D3CEA957BCB90D4 * value)
	{
		___s_InputSubsystems_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InputSubsystems_13), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_RaycastResultCache_4)); }
	inline List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t367B604D3EA3D6A9EC95A32A521EF83F5DA9B447 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResultCache_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_AxisEventData_5)); }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AxisEventData_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_EventSystem_6)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_BaseEventData_7)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BaseEventData_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_InputOverride_8)); }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputOverride_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924, ___m_DefaultInput_9)); }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_tEF29D9AD913DF0552A9C51AF200B4FEB08AF737D * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultInput_9), (void*)value);
	}
};


// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.BaseRaycaster::m_RootRaycaster
	BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * ___m_RootRaycaster_4;

public:
	inline static int32_t get_offset_of_m_RootRaycaster_4() { return static_cast<int32_t>(offsetof(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876, ___m_RootRaycaster_4)); }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * get_m_RootRaycaster_4() const { return ___m_RootRaycaster_4; }
	inline BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 ** get_address_of_m_RootRaycaster_4() { return &___m_RootRaycaster_4; }
	inline void set_m_RootRaycaster_4(BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876 * value)
	{
		___m_RootRaycaster_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RootRaycaster_4), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable
struct BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127  : public XRBaseInteractable_t6D8F476542495E625DE6A3454D4BC9323B6EDCBF
{
public:
	// UnityEngine.XR.Interaction.Toolkit.TeleportationProvider UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable::m_TeleportationProvider
	TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B * ___m_TeleportationProvider_40;
	// UnityEngine.XR.Interaction.Toolkit.MatchOrientation UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable::m_MatchOrientation
	int32_t ___m_MatchOrientation_41;
	// UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable/TeleportTrigger UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable::m_TeleportTrigger
	int32_t ___m_TeleportTrigger_42;
	// UnityEngine.XR.Interaction.Toolkit.TeleportingEvent UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable::m_Teleporting
	TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC * ___m_Teleporting_43;
	// UnityEngine.XR.Interaction.Toolkit.TeleportingEventArgs UnityEngine.XR.Interaction.Toolkit.BaseTeleportationInteractable::m_TeleportingEventArgs
	TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D * ___m_TeleportingEventArgs_44;

public:
	inline static int32_t get_offset_of_m_TeleportationProvider_40() { return static_cast<int32_t>(offsetof(BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127, ___m_TeleportationProvider_40)); }
	inline TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B * get_m_TeleportationProvider_40() const { return ___m_TeleportationProvider_40; }
	inline TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B ** get_address_of_m_TeleportationProvider_40() { return &___m_TeleportationProvider_40; }
	inline void set_m_TeleportationProvider_40(TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B * value)
	{
		___m_TeleportationProvider_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TeleportationProvider_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_MatchOrientation_41() { return static_cast<int32_t>(offsetof(BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127, ___m_MatchOrientation_41)); }
	inline int32_t get_m_MatchOrientation_41() const { return ___m_MatchOrientation_41; }
	inline int32_t* get_address_of_m_MatchOrientation_41() { return &___m_MatchOrientation_41; }
	inline void set_m_MatchOrientation_41(int32_t value)
	{
		___m_MatchOrientation_41 = value;
	}

	inline static int32_t get_offset_of_m_TeleportTrigger_42() { return static_cast<int32_t>(offsetof(BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127, ___m_TeleportTrigger_42)); }
	inline int32_t get_m_TeleportTrigger_42() const { return ___m_TeleportTrigger_42; }
	inline int32_t* get_address_of_m_TeleportTrigger_42() { return &___m_TeleportTrigger_42; }
	inline void set_m_TeleportTrigger_42(int32_t value)
	{
		___m_TeleportTrigger_42 = value;
	}

	inline static int32_t get_offset_of_m_Teleporting_43() { return static_cast<int32_t>(offsetof(BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127, ___m_Teleporting_43)); }
	inline TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC * get_m_Teleporting_43() const { return ___m_Teleporting_43; }
	inline TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC ** get_address_of_m_Teleporting_43() { return &___m_Teleporting_43; }
	inline void set_m_Teleporting_43(TeleportingEvent_tEDEA9FCF356C7991654FE65C2A83A96619E4A7DC * value)
	{
		___m_Teleporting_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Teleporting_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_TeleportingEventArgs_44() { return static_cast<int32_t>(offsetof(BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127, ___m_TeleportingEventArgs_44)); }
	inline TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D * get_m_TeleportingEventArgs_44() const { return ___m_TeleportingEventArgs_44; }
	inline TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D ** get_address_of_m_TeleportingEventArgs_44() { return &___m_TeleportingEventArgs_44; }
	inline void set_m_TeleportingEventArgs_44(TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D * value)
	{
		___m_TeleportingEventArgs_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TeleportingEventArgs_44), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase
struct ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044  : public LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448
{
public:
	// System.Single UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_MoveSpeed
	float ___m_MoveSpeed_8;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_EnableStrafe
	bool ___m_EnableStrafe_9;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_UseGravity
	bool ___m_UseGravity_10;
	// UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase/GravityApplicationMode UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_GravityApplicationMode
	int32_t ___m_GravityApplicationMode_11;
	// UnityEngine.Transform UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_ForwardSource
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_ForwardSource_12;
	// UnityEngine.CharacterController UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_CharacterController
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ___m_CharacterController_13;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_AttemptedGetCharacterController
	bool ___m_AttemptedGetCharacterController_14;
	// UnityEngine.Vector3 UnityEngine.XR.Interaction.Toolkit.ContinuousMoveProviderBase::m_VerticalVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_VerticalVelocity_15;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_8() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_MoveSpeed_8)); }
	inline float get_m_MoveSpeed_8() const { return ___m_MoveSpeed_8; }
	inline float* get_address_of_m_MoveSpeed_8() { return &___m_MoveSpeed_8; }
	inline void set_m_MoveSpeed_8(float value)
	{
		___m_MoveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_EnableStrafe_9() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_EnableStrafe_9)); }
	inline bool get_m_EnableStrafe_9() const { return ___m_EnableStrafe_9; }
	inline bool* get_address_of_m_EnableStrafe_9() { return &___m_EnableStrafe_9; }
	inline void set_m_EnableStrafe_9(bool value)
	{
		___m_EnableStrafe_9 = value;
	}

	inline static int32_t get_offset_of_m_UseGravity_10() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_UseGravity_10)); }
	inline bool get_m_UseGravity_10() const { return ___m_UseGravity_10; }
	inline bool* get_address_of_m_UseGravity_10() { return &___m_UseGravity_10; }
	inline void set_m_UseGravity_10(bool value)
	{
		___m_UseGravity_10 = value;
	}

	inline static int32_t get_offset_of_m_GravityApplicationMode_11() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_GravityApplicationMode_11)); }
	inline int32_t get_m_GravityApplicationMode_11() const { return ___m_GravityApplicationMode_11; }
	inline int32_t* get_address_of_m_GravityApplicationMode_11() { return &___m_GravityApplicationMode_11; }
	inline void set_m_GravityApplicationMode_11(int32_t value)
	{
		___m_GravityApplicationMode_11 = value;
	}

	inline static int32_t get_offset_of_m_ForwardSource_12() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_ForwardSource_12)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_ForwardSource_12() const { return ___m_ForwardSource_12; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_ForwardSource_12() { return &___m_ForwardSource_12; }
	inline void set_m_ForwardSource_12(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_ForwardSource_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ForwardSource_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CharacterController_13() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_CharacterController_13)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get_m_CharacterController_13() const { return ___m_CharacterController_13; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of_m_CharacterController_13() { return &___m_CharacterController_13; }
	inline void set_m_CharacterController_13(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		___m_CharacterController_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CharacterController_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_AttemptedGetCharacterController_14() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_AttemptedGetCharacterController_14)); }
	inline bool get_m_AttemptedGetCharacterController_14() const { return ___m_AttemptedGetCharacterController_14; }
	inline bool* get_address_of_m_AttemptedGetCharacterController_14() { return &___m_AttemptedGetCharacterController_14; }
	inline void set_m_AttemptedGetCharacterController_14(bool value)
	{
		___m_AttemptedGetCharacterController_14 = value;
	}

	inline static int32_t get_offset_of_m_VerticalVelocity_15() { return static_cast<int32_t>(offsetof(ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044, ___m_VerticalVelocity_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_VerticalVelocity_15() const { return ___m_VerticalVelocity_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_VerticalVelocity_15() { return &___m_VerticalVelocity_15; }
	inline void set_m_VerticalVelocity_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_VerticalVelocity_15 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.ContinuousTurnProviderBase
struct ContinuousTurnProviderBase_tF9080699926533D4568B35784A9B226C46FB823C  : public LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448
{
public:
	// System.Single UnityEngine.XR.Interaction.Toolkit.ContinuousTurnProviderBase::m_TurnSpeed
	float ___m_TurnSpeed_8;

public:
	inline static int32_t get_offset_of_m_TurnSpeed_8() { return static_cast<int32_t>(offsetof(ContinuousTurnProviderBase_tF9080699926533D4568B35784A9B226C46FB823C, ___m_TurnSpeed_8)); }
	inline float get_m_TurnSpeed_8() const { return ___m_TurnSpeed_8; }
	inline float* get_address_of_m_TurnSpeed_8() { return &___m_TurnSpeed_8; }
	inline void set_m_TurnSpeed_8(float value)
	{
		___m_TurnSpeed_8 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.InteractionLayerSettings
struct InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600  : public ScriptableSettings_1_tDF3258339DCB126011FADD9A3DAC28C73FA20AE9
{
public:
	// System.String[] UnityEngine.XR.Interaction.Toolkit.InteractionLayerSettings::m_LayerNames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_LayerNames_20;

public:
	inline static int32_t get_offset_of_m_LayerNames_20() { return static_cast<int32_t>(offsetof(InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600, ___m_LayerNames_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_LayerNames_20() const { return ___m_LayerNames_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_LayerNames_20() { return &___m_LayerNames_20; }
	inline void set_m_LayerNames_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_LayerNames_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LayerNames_20), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase
struct SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A  : public LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448
{
public:
	// System.Single UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_TurnAmount
	float ___m_TurnAmount_8;
	// System.Single UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_DebounceTime
	float ___m_DebounceTime_9;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_EnableTurnLeftRight
	bool ___m_EnableTurnLeftRight_10;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_EnableTurnAround
	bool ___m_EnableTurnAround_11;
	// System.Single UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_CurrentTurnAmount
	float ___m_CurrentTurnAmount_12;
	// System.Single UnityEngine.XR.Interaction.Toolkit.SnapTurnProviderBase::m_TimeStarted
	float ___m_TimeStarted_13;

public:
	inline static int32_t get_offset_of_m_TurnAmount_8() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_TurnAmount_8)); }
	inline float get_m_TurnAmount_8() const { return ___m_TurnAmount_8; }
	inline float* get_address_of_m_TurnAmount_8() { return &___m_TurnAmount_8; }
	inline void set_m_TurnAmount_8(float value)
	{
		___m_TurnAmount_8 = value;
	}

	inline static int32_t get_offset_of_m_DebounceTime_9() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_DebounceTime_9)); }
	inline float get_m_DebounceTime_9() const { return ___m_DebounceTime_9; }
	inline float* get_address_of_m_DebounceTime_9() { return &___m_DebounceTime_9; }
	inline void set_m_DebounceTime_9(float value)
	{
		___m_DebounceTime_9 = value;
	}

	inline static int32_t get_offset_of_m_EnableTurnLeftRight_10() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_EnableTurnLeftRight_10)); }
	inline bool get_m_EnableTurnLeftRight_10() const { return ___m_EnableTurnLeftRight_10; }
	inline bool* get_address_of_m_EnableTurnLeftRight_10() { return &___m_EnableTurnLeftRight_10; }
	inline void set_m_EnableTurnLeftRight_10(bool value)
	{
		___m_EnableTurnLeftRight_10 = value;
	}

	inline static int32_t get_offset_of_m_EnableTurnAround_11() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_EnableTurnAround_11)); }
	inline bool get_m_EnableTurnAround_11() const { return ___m_EnableTurnAround_11; }
	inline bool* get_address_of_m_EnableTurnAround_11() { return &___m_EnableTurnAround_11; }
	inline void set_m_EnableTurnAround_11(bool value)
	{
		___m_EnableTurnAround_11 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTurnAmount_12() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_CurrentTurnAmount_12)); }
	inline float get_m_CurrentTurnAmount_12() const { return ___m_CurrentTurnAmount_12; }
	inline float* get_address_of_m_CurrentTurnAmount_12() { return &___m_CurrentTurnAmount_12; }
	inline void set_m_CurrentTurnAmount_12(float value)
	{
		___m_CurrentTurnAmount_12 = value;
	}

	inline static int32_t get_offset_of_m_TimeStarted_13() { return static_cast<int32_t>(offsetof(SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A, ___m_TimeStarted_13)); }
	inline float get_m_TimeStarted_13() const { return ___m_TimeStarted_13; }
	inline float* get_address_of_m_TimeStarted_13() { return &___m_TimeStarted_13; }
	inline void set_m_TimeStarted_13(float value)
	{
		___m_TimeStarted_13 = value;
	}
};


// UnityEngine.XR.Interaction.Toolkit.TeleportationProvider
struct TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B  : public LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448
{
public:
	// UnityEngine.XR.Interaction.Toolkit.TeleportRequest UnityEngine.XR.Interaction.Toolkit.TeleportationProvider::<currentRequest>k__BackingField
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  ___U3CcurrentRequestU3Ek__BackingField_8;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.TeleportationProvider::<validRequest>k__BackingField
	bool ___U3CvalidRequestU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CcurrentRequestU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B, ___U3CcurrentRequestU3Ek__BackingField_8)); }
	inline TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  get_U3CcurrentRequestU3Ek__BackingField_8() const { return ___U3CcurrentRequestU3Ek__BackingField_8; }
	inline TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2 * get_address_of_U3CcurrentRequestU3Ek__BackingField_8() { return &___U3CcurrentRequestU3Ek__BackingField_8; }
	inline void set_U3CcurrentRequestU3Ek__BackingField_8(TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2  value)
	{
		___U3CcurrentRequestU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CvalidRequestU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B, ___U3CvalidRequestU3Ek__BackingField_9)); }
	inline bool get_U3CvalidRequestU3Ek__BackingField_9() const { return ___U3CvalidRequestU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CvalidRequestU3Ek__BackingField_9() { return &___U3CvalidRequestU3Ek__BackingField_9; }
	inline void set_U3CvalidRequestU3Ek__BackingField_9(bool value)
	{
		___U3CvalidRequestU3Ek__BackingField_9 = value;
	}
};


// UnityEngine.InputSystem.XR.XRController
struct XRController_t54F264B6E8ECCD1875DA99199413F61E236D8326  : public TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5
{
public:

public:
};


// UnityEngine.InputSystem.XR.XRHMD
struct XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A  : public TrackedDevice_tC22257C9CF0AE3D4CC45B4DDD5F16E6B99A917C5
{
public:
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<leftEyePosition>k__BackingField
	Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * ___U3CleftEyePositionU3Ek__BackingField_43;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<leftEyeRotation>k__BackingField
	QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * ___U3CleftEyeRotationU3Ek__BackingField_44;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<rightEyePosition>k__BackingField
	Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * ___U3CrightEyePositionU3Ek__BackingField_45;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<rightEyeRotation>k__BackingField
	QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * ___U3CrightEyeRotationU3Ek__BackingField_46;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<centerEyePosition>k__BackingField
	Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * ___U3CcenterEyePositionU3Ek__BackingField_47;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<centerEyeRotation>k__BackingField
	QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * ___U3CcenterEyeRotationU3Ek__BackingField_48;

public:
	inline static int32_t get_offset_of_U3CleftEyePositionU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CleftEyePositionU3Ek__BackingField_43)); }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * get_U3CleftEyePositionU3Ek__BackingField_43() const { return ___U3CleftEyePositionU3Ek__BackingField_43; }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 ** get_address_of_U3CleftEyePositionU3Ek__BackingField_43() { return &___U3CleftEyePositionU3Ek__BackingField_43; }
	inline void set_U3CleftEyePositionU3Ek__BackingField_43(Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * value)
	{
		___U3CleftEyePositionU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyePositionU3Ek__BackingField_43), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftEyeRotationU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CleftEyeRotationU3Ek__BackingField_44)); }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * get_U3CleftEyeRotationU3Ek__BackingField_44() const { return ___U3CleftEyeRotationU3Ek__BackingField_44; }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF ** get_address_of_U3CleftEyeRotationU3Ek__BackingField_44() { return &___U3CleftEyeRotationU3Ek__BackingField_44; }
	inline void set_U3CleftEyeRotationU3Ek__BackingField_44(QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * value)
	{
		___U3CleftEyeRotationU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyeRotationU3Ek__BackingField_44), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyePositionU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CrightEyePositionU3Ek__BackingField_45)); }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * get_U3CrightEyePositionU3Ek__BackingField_45() const { return ___U3CrightEyePositionU3Ek__BackingField_45; }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 ** get_address_of_U3CrightEyePositionU3Ek__BackingField_45() { return &___U3CrightEyePositionU3Ek__BackingField_45; }
	inline void set_U3CrightEyePositionU3Ek__BackingField_45(Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * value)
	{
		___U3CrightEyePositionU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyePositionU3Ek__BackingField_45), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyeRotationU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CrightEyeRotationU3Ek__BackingField_46)); }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * get_U3CrightEyeRotationU3Ek__BackingField_46() const { return ___U3CrightEyeRotationU3Ek__BackingField_46; }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF ** get_address_of_U3CrightEyeRotationU3Ek__BackingField_46() { return &___U3CrightEyeRotationU3Ek__BackingField_46; }
	inline void set_U3CrightEyeRotationU3Ek__BackingField_46(QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * value)
	{
		___U3CrightEyeRotationU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyeRotationU3Ek__BackingField_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyePositionU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CcenterEyePositionU3Ek__BackingField_47)); }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * get_U3CcenterEyePositionU3Ek__BackingField_47() const { return ___U3CcenterEyePositionU3Ek__BackingField_47; }
	inline Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 ** get_address_of_U3CcenterEyePositionU3Ek__BackingField_47() { return &___U3CcenterEyePositionU3Ek__BackingField_47; }
	inline void set_U3CcenterEyePositionU3Ek__BackingField_47(Vector3Control_t9AB8CA69116112896589CEB0B9BB7A0E42739E15 * value)
	{
		___U3CcenterEyePositionU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyePositionU3Ek__BackingField_47), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyeRotationU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A, ___U3CcenterEyeRotationU3Ek__BackingField_48)); }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * get_U3CcenterEyeRotationU3Ek__BackingField_48() const { return ___U3CcenterEyeRotationU3Ek__BackingField_48; }
	inline QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF ** get_address_of_U3CcenterEyeRotationU3Ek__BackingField_48() { return &___U3CcenterEyeRotationU3Ek__BackingField_48; }
	inline void set_U3CcenterEyeRotationU3Ek__BackingField_48(QuaternionControl_tDB0B05D0B1B73296BA48A0F8BE9E21C86B1811AF * value)
	{
		___U3CcenterEyeRotationU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyeRotationU3Ek__BackingField_48), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRRig
struct XRRig_t5979BC5624B3BB053EB0043B7CD2256665BE8AF5  : public XROrigin_t9B8AF4E46E31903F17DF7A13F317DB1CF366777D
{
public:
	// UnityEngine.GameObject UnityEngine.XR.Interaction.Toolkit.XRRig::m_CameraGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CameraGameObject_16;

public:
	inline static int32_t get_offset_of_m_CameraGameObject_16() { return static_cast<int32_t>(offsetof(XRRig_t5979BC5624B3BB053EB0043B7CD2256665BE8AF5, ___m_CameraGameObject_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CameraGameObject_16() const { return ___m_CameraGameObject_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CameraGameObject_16() { return &___m_CameraGameObject_16; }
	inline void set_m_CameraGameObject_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CameraGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraGameObject_16), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor
struct XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279  : public XRBaseInteractor_t78FB23CA3FFA1C934E25A72B172879C3AE6D887F
{
public:
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_ShowInteractableHoverMeshes
	bool ___m_ShowInteractableHoverMeshes_34;
	// UnityEngine.Material UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_InteractableHoverMeshMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_InteractableHoverMeshMaterial_35;
	// UnityEngine.Material UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_InteractableCantHoverMeshMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_InteractableCantHoverMeshMaterial_36;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_SocketActive
	bool ___m_SocketActive_37;
	// System.Single UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_InteractableHoverScale
	float ___m_InteractableHoverScale_38;
	// System.Single UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_RecycleDelayTime
	float ___m_RecycleDelayTime_39;
	// System.Single UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_LastRemoveTime
	float ___m_LastRemoveTime_40;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.IXRInteractable> UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::<unsortedValidTargets>k__BackingField
	List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * ___U3CunsortedValidTargetsU3Ek__BackingField_41;
	// UnityEngine.XR.Interaction.Toolkit.Utilities.TriggerContactMonitor UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_TriggerContactMonitor
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5 * ___m_TriggerContactMonitor_42;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.Interaction.Toolkit.IXRInteractable,System.ValueTuple`2<UnityEngine.MeshFilter,UnityEngine.Renderer>[]> UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::m_MeshFilterCache
	Dictionary_2_t879F533E50F5919795E89088ADF39BA7A56A4256 * ___m_MeshFilterCache_43;

public:
	inline static int32_t get_offset_of_m_ShowInteractableHoverMeshes_34() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_ShowInteractableHoverMeshes_34)); }
	inline bool get_m_ShowInteractableHoverMeshes_34() const { return ___m_ShowInteractableHoverMeshes_34; }
	inline bool* get_address_of_m_ShowInteractableHoverMeshes_34() { return &___m_ShowInteractableHoverMeshes_34; }
	inline void set_m_ShowInteractableHoverMeshes_34(bool value)
	{
		___m_ShowInteractableHoverMeshes_34 = value;
	}

	inline static int32_t get_offset_of_m_InteractableHoverMeshMaterial_35() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_InteractableHoverMeshMaterial_35)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_InteractableHoverMeshMaterial_35() const { return ___m_InteractableHoverMeshMaterial_35; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_InteractableHoverMeshMaterial_35() { return &___m_InteractableHoverMeshMaterial_35; }
	inline void set_m_InteractableHoverMeshMaterial_35(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_InteractableHoverMeshMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractableHoverMeshMaterial_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractableCantHoverMeshMaterial_36() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_InteractableCantHoverMeshMaterial_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_InteractableCantHoverMeshMaterial_36() const { return ___m_InteractableCantHoverMeshMaterial_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_InteractableCantHoverMeshMaterial_36() { return &___m_InteractableCantHoverMeshMaterial_36; }
	inline void set_m_InteractableCantHoverMeshMaterial_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_InteractableCantHoverMeshMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractableCantHoverMeshMaterial_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_SocketActive_37() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_SocketActive_37)); }
	inline bool get_m_SocketActive_37() const { return ___m_SocketActive_37; }
	inline bool* get_address_of_m_SocketActive_37() { return &___m_SocketActive_37; }
	inline void set_m_SocketActive_37(bool value)
	{
		___m_SocketActive_37 = value;
	}

	inline static int32_t get_offset_of_m_InteractableHoverScale_38() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_InteractableHoverScale_38)); }
	inline float get_m_InteractableHoverScale_38() const { return ___m_InteractableHoverScale_38; }
	inline float* get_address_of_m_InteractableHoverScale_38() { return &___m_InteractableHoverScale_38; }
	inline void set_m_InteractableHoverScale_38(float value)
	{
		___m_InteractableHoverScale_38 = value;
	}

	inline static int32_t get_offset_of_m_RecycleDelayTime_39() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_RecycleDelayTime_39)); }
	inline float get_m_RecycleDelayTime_39() const { return ___m_RecycleDelayTime_39; }
	inline float* get_address_of_m_RecycleDelayTime_39() { return &___m_RecycleDelayTime_39; }
	inline void set_m_RecycleDelayTime_39(float value)
	{
		___m_RecycleDelayTime_39 = value;
	}

	inline static int32_t get_offset_of_m_LastRemoveTime_40() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_LastRemoveTime_40)); }
	inline float get_m_LastRemoveTime_40() const { return ___m_LastRemoveTime_40; }
	inline float* get_address_of_m_LastRemoveTime_40() { return &___m_LastRemoveTime_40; }
	inline void set_m_LastRemoveTime_40(float value)
	{
		___m_LastRemoveTime_40 = value;
	}

	inline static int32_t get_offset_of_U3CunsortedValidTargetsU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___U3CunsortedValidTargetsU3Ek__BackingField_41)); }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * get_U3CunsortedValidTargetsU3Ek__BackingField_41() const { return ___U3CunsortedValidTargetsU3Ek__BackingField_41; }
	inline List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 ** get_address_of_U3CunsortedValidTargetsU3Ek__BackingField_41() { return &___U3CunsortedValidTargetsU3Ek__BackingField_41; }
	inline void set_U3CunsortedValidTargetsU3Ek__BackingField_41(List_1_tA0AA68EBEDA30BFC60B674328ADD3173B39F3B23 * value)
	{
		___U3CunsortedValidTargetsU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CunsortedValidTargetsU3Ek__BackingField_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_TriggerContactMonitor_42() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_TriggerContactMonitor_42)); }
	inline TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5 * get_m_TriggerContactMonitor_42() const { return ___m_TriggerContactMonitor_42; }
	inline TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5 ** get_address_of_m_TriggerContactMonitor_42() { return &___m_TriggerContactMonitor_42; }
	inline void set_m_TriggerContactMonitor_42(TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5 * value)
	{
		___m_TriggerContactMonitor_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TriggerContactMonitor_42), (void*)value);
	}

	inline static int32_t get_offset_of_m_MeshFilterCache_43() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279, ___m_MeshFilterCache_43)); }
	inline Dictionary_2_t879F533E50F5919795E89088ADF39BA7A56A4256 * get_m_MeshFilterCache_43() const { return ___m_MeshFilterCache_43; }
	inline Dictionary_2_t879F533E50F5919795E89088ADF39BA7A56A4256 ** get_address_of_m_MeshFilterCache_43() { return &___m_MeshFilterCache_43; }
	inline void set_m_MeshFilterCache_43(Dictionary_2_t879F533E50F5919795E89088ADF39BA7A56A4256 * value)
	{
		___m_MeshFilterCache_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MeshFilterCache_43), (void*)value);
	}
};

struct XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.MeshFilter> UnityEngine.XR.Interaction.Toolkit.XRSocketInteractor::s_MeshFilters
	List_1_tF4FF55D8DD6EFED1BBCBF60B3D5905B0C1CA6C8E * ___s_MeshFilters_44;

public:
	inline static int32_t get_offset_of_s_MeshFilters_44() { return static_cast<int32_t>(offsetof(XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279_StaticFields, ___s_MeshFilters_44)); }
	inline List_1_tF4FF55D8DD6EFED1BBCBF60B3D5905B0C1CA6C8E * get_s_MeshFilters_44() const { return ___s_MeshFilters_44; }
	inline List_1_tF4FF55D8DD6EFED1BBCBF60B3D5905B0C1CA6C8E ** get_address_of_s_MeshFilters_44() { return &___s_MeshFilters_44; }
	inline void set_s_MeshFilters_44(List_1_tF4FF55D8DD6EFED1BBCBF60B3D5905B0C1CA6C8E * value)
	{
		___s_MeshFilters_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MeshFilters_44), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousMoveProvider
struct ActionBasedContinuousMoveProvider_tA1E91E01A6936255752856265BCBFBDBA093293A  : public ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044
{
public:
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousMoveProvider::m_LeftHandMoveAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_LeftHandMoveAction_16;
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousMoveProvider::m_RightHandMoveAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_RightHandMoveAction_17;

public:
	inline static int32_t get_offset_of_m_LeftHandMoveAction_16() { return static_cast<int32_t>(offsetof(ActionBasedContinuousMoveProvider_tA1E91E01A6936255752856265BCBFBDBA093293A, ___m_LeftHandMoveAction_16)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_LeftHandMoveAction_16() const { return ___m_LeftHandMoveAction_16; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_LeftHandMoveAction_16() { return &___m_LeftHandMoveAction_16; }
	inline void set_m_LeftHandMoveAction_16(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_LeftHandMoveAction_16 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandMoveAction_16))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandMoveAction_16))->___m_Reference_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RightHandMoveAction_17() { return static_cast<int32_t>(offsetof(ActionBasedContinuousMoveProvider_tA1E91E01A6936255752856265BCBFBDBA093293A, ___m_RightHandMoveAction_17)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_RightHandMoveAction_17() const { return ___m_RightHandMoveAction_17; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_RightHandMoveAction_17() { return &___m_RightHandMoveAction_17; }
	inline void set_m_RightHandMoveAction_17(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_RightHandMoveAction_17 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandMoveAction_17))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandMoveAction_17))->___m_Reference_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousTurnProvider
struct ActionBasedContinuousTurnProvider_t9D9A463B9FEA2F13D91375EE51B4A7044D642ED2  : public ContinuousTurnProviderBase_tF9080699926533D4568B35784A9B226C46FB823C
{
public:
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousTurnProvider::m_LeftHandTurnAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_LeftHandTurnAction_9;
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedContinuousTurnProvider::m_RightHandTurnAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_RightHandTurnAction_10;

public:
	inline static int32_t get_offset_of_m_LeftHandTurnAction_9() { return static_cast<int32_t>(offsetof(ActionBasedContinuousTurnProvider_t9D9A463B9FEA2F13D91375EE51B4A7044D642ED2, ___m_LeftHandTurnAction_9)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_LeftHandTurnAction_9() const { return ___m_LeftHandTurnAction_9; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_LeftHandTurnAction_9() { return &___m_LeftHandTurnAction_9; }
	inline void set_m_LeftHandTurnAction_9(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_LeftHandTurnAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandTurnAction_9))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandTurnAction_9))->___m_Reference_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RightHandTurnAction_10() { return static_cast<int32_t>(offsetof(ActionBasedContinuousTurnProvider_t9D9A463B9FEA2F13D91375EE51B4A7044D642ED2, ___m_RightHandTurnAction_10)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_RightHandTurnAction_10() const { return ___m_RightHandTurnAction_10; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_RightHandTurnAction_10() { return &___m_RightHandTurnAction_10; }
	inline void set_m_RightHandTurnAction_10(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_RightHandTurnAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandTurnAction_10))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandTurnAction_10))->___m_Reference_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.XR.Interaction.Toolkit.ActionBasedSnapTurnProvider
struct ActionBasedSnapTurnProvider_t53E79F486EEE1ADF3C8B63E117564275FE38F03F  : public SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A
{
public:
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedSnapTurnProvider::m_LeftHandSnapTurnAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_LeftHandSnapTurnAction_14;
	// UnityEngine.InputSystem.InputActionProperty UnityEngine.XR.Interaction.Toolkit.ActionBasedSnapTurnProvider::m_RightHandSnapTurnAction
	InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  ___m_RightHandSnapTurnAction_15;

public:
	inline static int32_t get_offset_of_m_LeftHandSnapTurnAction_14() { return static_cast<int32_t>(offsetof(ActionBasedSnapTurnProvider_t53E79F486EEE1ADF3C8B63E117564275FE38F03F, ___m_LeftHandSnapTurnAction_14)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_LeftHandSnapTurnAction_14() const { return ___m_LeftHandSnapTurnAction_14; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_LeftHandSnapTurnAction_14() { return &___m_LeftHandSnapTurnAction_14; }
	inline void set_m_LeftHandSnapTurnAction_14(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_LeftHandSnapTurnAction_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandSnapTurnAction_14))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LeftHandSnapTurnAction_14))->___m_Reference_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RightHandSnapTurnAction_15() { return static_cast<int32_t>(offsetof(ActionBasedSnapTurnProvider_t53E79F486EEE1ADF3C8B63E117564275FE38F03F, ___m_RightHandSnapTurnAction_15)); }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  get_m_RightHandSnapTurnAction_15() const { return ___m_RightHandSnapTurnAction_15; }
	inline InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117 * get_address_of_m_RightHandSnapTurnAction_15() { return &___m_RightHandSnapTurnAction_15; }
	inline void set_m_RightHandSnapTurnAction_15(InputActionProperty_t06B73D292EAF16B8F59887441F6E9BA21A993117  value)
	{
		___m_RightHandSnapTurnAction_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandSnapTurnAction_15))->___m_Action_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RightHandSnapTurnAction_15))->___m_Reference_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider
struct DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A  : public ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044
{
public:
	// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider/InputAxes UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider::m_InputBinding
	int32_t ___m_InputBinding_16;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseController> UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider::m_Controllers
	List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * ___m_Controllers_17;
	// System.Single UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider::m_DeadzoneMin
	float ___m_DeadzoneMin_18;
	// System.Single UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider::m_DeadzoneMax
	float ___m_DeadzoneMax_19;

public:
	inline static int32_t get_offset_of_m_InputBinding_16() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A, ___m_InputBinding_16)); }
	inline int32_t get_m_InputBinding_16() const { return ___m_InputBinding_16; }
	inline int32_t* get_address_of_m_InputBinding_16() { return &___m_InputBinding_16; }
	inline void set_m_InputBinding_16(int32_t value)
	{
		___m_InputBinding_16 = value;
	}

	inline static int32_t get_offset_of_m_Controllers_17() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A, ___m_Controllers_17)); }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * get_m_Controllers_17() const { return ___m_Controllers_17; }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 ** get_address_of_m_Controllers_17() { return &___m_Controllers_17; }
	inline void set_m_Controllers_17(List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * value)
	{
		___m_Controllers_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controllers_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeadzoneMin_18() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A, ___m_DeadzoneMin_18)); }
	inline float get_m_DeadzoneMin_18() const { return ___m_DeadzoneMin_18; }
	inline float* get_address_of_m_DeadzoneMin_18() { return &___m_DeadzoneMin_18; }
	inline void set_m_DeadzoneMin_18(float value)
	{
		___m_DeadzoneMin_18 = value;
	}

	inline static int32_t get_offset_of_m_DeadzoneMax_19() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A, ___m_DeadzoneMax_19)); }
	inline float get_m_DeadzoneMax_19() const { return ___m_DeadzoneMax_19; }
	inline float* get_address_of_m_DeadzoneMax_19() { return &___m_DeadzoneMax_19; }
	inline void set_m_DeadzoneMax_19(float value)
	{
		___m_DeadzoneMax_19 = value;
	}
};

struct DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A_StaticFields
{
public:
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>[] UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousMoveProvider::k_Vec2UsageList
	InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* ___k_Vec2UsageList_20;

public:
	inline static int32_t get_offset_of_k_Vec2UsageList_20() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A_StaticFields, ___k_Vec2UsageList_20)); }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* get_k_Vec2UsageList_20() const { return ___k_Vec2UsageList_20; }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D** get_address_of_k_Vec2UsageList_20() { return &___k_Vec2UsageList_20; }
	inline void set_k_Vec2UsageList_20(InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* value)
	{
		___k_Vec2UsageList_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_Vec2UsageList_20), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider
struct DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5  : public ContinuousTurnProviderBase_tF9080699926533D4568B35784A9B226C46FB823C
{
public:
	// UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider/InputAxes UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider::m_InputBinding
	int32_t ___m_InputBinding_9;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseController> UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider::m_Controllers
	List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * ___m_Controllers_10;
	// System.Single UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider::m_DeadzoneMin
	float ___m_DeadzoneMin_11;
	// System.Single UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider::m_DeadzoneMax
	float ___m_DeadzoneMax_12;

public:
	inline static int32_t get_offset_of_m_InputBinding_9() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5, ___m_InputBinding_9)); }
	inline int32_t get_m_InputBinding_9() const { return ___m_InputBinding_9; }
	inline int32_t* get_address_of_m_InputBinding_9() { return &___m_InputBinding_9; }
	inline void set_m_InputBinding_9(int32_t value)
	{
		___m_InputBinding_9 = value;
	}

	inline static int32_t get_offset_of_m_Controllers_10() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5, ___m_Controllers_10)); }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * get_m_Controllers_10() const { return ___m_Controllers_10; }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 ** get_address_of_m_Controllers_10() { return &___m_Controllers_10; }
	inline void set_m_Controllers_10(List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * value)
	{
		___m_Controllers_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controllers_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeadzoneMin_11() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5, ___m_DeadzoneMin_11)); }
	inline float get_m_DeadzoneMin_11() const { return ___m_DeadzoneMin_11; }
	inline float* get_address_of_m_DeadzoneMin_11() { return &___m_DeadzoneMin_11; }
	inline void set_m_DeadzoneMin_11(float value)
	{
		___m_DeadzoneMin_11 = value;
	}

	inline static int32_t get_offset_of_m_DeadzoneMax_12() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5, ___m_DeadzoneMax_12)); }
	inline float get_m_DeadzoneMax_12() const { return ___m_DeadzoneMax_12; }
	inline float* get_address_of_m_DeadzoneMax_12() { return &___m_DeadzoneMax_12; }
	inline void set_m_DeadzoneMax_12(float value)
	{
		___m_DeadzoneMax_12 = value;
	}
};

struct DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5_StaticFields
{
public:
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>[] UnityEngine.XR.Interaction.Toolkit.DeviceBasedContinuousTurnProvider::k_Vec2UsageList
	InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* ___k_Vec2UsageList_13;

public:
	inline static int32_t get_offset_of_k_Vec2UsageList_13() { return static_cast<int32_t>(offsetof(DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5_StaticFields, ___k_Vec2UsageList_13)); }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* get_k_Vec2UsageList_13() const { return ___k_Vec2UsageList_13; }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D** get_address_of_k_Vec2UsageList_13() { return &___k_Vec2UsageList_13; }
	inline void set_k_Vec2UsageList_13(InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* value)
	{
		___k_Vec2UsageList_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_Vec2UsageList_13), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider
struct DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064  : public SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A
{
public:
	// UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider/InputAxes UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider::m_TurnUsage
	int32_t ___m_TurnUsage_14;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.XRBaseController> UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider::m_Controllers
	List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * ___m_Controllers_15;
	// System.Single UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider::m_DeadZone
	float ___m_DeadZone_16;

public:
	inline static int32_t get_offset_of_m_TurnUsage_14() { return static_cast<int32_t>(offsetof(DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064, ___m_TurnUsage_14)); }
	inline int32_t get_m_TurnUsage_14() const { return ___m_TurnUsage_14; }
	inline int32_t* get_address_of_m_TurnUsage_14() { return &___m_TurnUsage_14; }
	inline void set_m_TurnUsage_14(int32_t value)
	{
		___m_TurnUsage_14 = value;
	}

	inline static int32_t get_offset_of_m_Controllers_15() { return static_cast<int32_t>(offsetof(DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064, ___m_Controllers_15)); }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * get_m_Controllers_15() const { return ___m_Controllers_15; }
	inline List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 ** get_address_of_m_Controllers_15() { return &___m_Controllers_15; }
	inline void set_m_Controllers_15(List_1_tDDA7F7DE3BB48A8953CF38781606396F70A2B157 * value)
	{
		___m_Controllers_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controllers_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeadZone_16() { return static_cast<int32_t>(offsetof(DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064, ___m_DeadZone_16)); }
	inline float get_m_DeadZone_16() const { return ___m_DeadZone_16; }
	inline float* get_address_of_m_DeadZone_16() { return &___m_DeadZone_16; }
	inline void set_m_DeadZone_16(float value)
	{
		___m_DeadZone_16 = value;
	}
};

struct DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064_StaticFields
{
public:
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>[] UnityEngine.XR.Interaction.Toolkit.DeviceBasedSnapTurnProvider::k_Vec2UsageList
	InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* ___k_Vec2UsageList_17;

public:
	inline static int32_t get_offset_of_k_Vec2UsageList_17() { return static_cast<int32_t>(offsetof(DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064_StaticFields, ___k_Vec2UsageList_17)); }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* get_k_Vec2UsageList_17() const { return ___k_Vec2UsageList_17; }
	inline InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D** get_address_of_k_Vec2UsageList_17() { return &___k_Vec2UsageList_17; }
	inline void set_k_Vec2UsageList_17(InputFeatureUsage_1U5BU5D_t27060F54EAD3290B38467818428947B85ED30C2D* value)
	{
		___k_Vec2UsageList_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_Vec2UsageList_17), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.TeleportationAnchor
struct TeleportationAnchor_t1DE5130A523D2A19AA321D3E01BAFB6DA7E7639F  : public BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127
{
public:
	// UnityEngine.Transform UnityEngine.XR.Interaction.Toolkit.TeleportationAnchor::m_TeleportAnchorTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_TeleportAnchorTransform_45;

public:
	inline static int32_t get_offset_of_m_TeleportAnchorTransform_45() { return static_cast<int32_t>(offsetof(TeleportationAnchor_t1DE5130A523D2A19AA321D3E01BAFB6DA7E7639F, ___m_TeleportAnchorTransform_45)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_TeleportAnchorTransform_45() const { return ___m_TeleportAnchorTransform_45; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_TeleportAnchorTransform_45() { return &___m_TeleportAnchorTransform_45; }
	inline void set_m_TeleportAnchorTransform_45(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_TeleportAnchorTransform_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TeleportAnchorTransform_45), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.TeleportationArea
struct TeleportationArea_tC1FB8AD1C58C7FE3606E669683D36C4EC8F44CD3  : public BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster
struct TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0  : public BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876
{
public:
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_IgnoreReversedGraphics
	bool ___m_IgnoreReversedGraphics_6;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_CheckFor2DOcclusion
	bool ___m_CheckFor2DOcclusion_7;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_CheckFor3DOcclusion
	bool ___m_CheckFor3DOcclusion_8;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_BlockingMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_BlockingMask_9;
	// UnityEngine.QueryTriggerInteraction UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_RaycastTriggerInteraction
	int32_t ___m_RaycastTriggerInteraction_10;
	// UnityEngine.Canvas UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_11;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_HasWarnedEventCameraNull
	bool ___m_HasWarnedEventCameraNull_12;
	// UnityEngine.RaycastHit[] UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_OcclusionHits3D
	RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* ___m_OcclusionHits3D_13;
	// UnityEngine.RaycastHit2D[] UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_OcclusionHits2D
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ___m_OcclusionHits2D_14;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::m_RaycastResultsCache
	List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * ___m_RaycastResultsCache_17;

public:
	inline static int32_t get_offset_of_m_IgnoreReversedGraphics_6() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_IgnoreReversedGraphics_6)); }
	inline bool get_m_IgnoreReversedGraphics_6() const { return ___m_IgnoreReversedGraphics_6; }
	inline bool* get_address_of_m_IgnoreReversedGraphics_6() { return &___m_IgnoreReversedGraphics_6; }
	inline void set_m_IgnoreReversedGraphics_6(bool value)
	{
		___m_IgnoreReversedGraphics_6 = value;
	}

	inline static int32_t get_offset_of_m_CheckFor2DOcclusion_7() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_CheckFor2DOcclusion_7)); }
	inline bool get_m_CheckFor2DOcclusion_7() const { return ___m_CheckFor2DOcclusion_7; }
	inline bool* get_address_of_m_CheckFor2DOcclusion_7() { return &___m_CheckFor2DOcclusion_7; }
	inline void set_m_CheckFor2DOcclusion_7(bool value)
	{
		___m_CheckFor2DOcclusion_7 = value;
	}

	inline static int32_t get_offset_of_m_CheckFor3DOcclusion_8() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_CheckFor3DOcclusion_8)); }
	inline bool get_m_CheckFor3DOcclusion_8() const { return ___m_CheckFor3DOcclusion_8; }
	inline bool* get_address_of_m_CheckFor3DOcclusion_8() { return &___m_CheckFor3DOcclusion_8; }
	inline void set_m_CheckFor3DOcclusion_8(bool value)
	{
		___m_CheckFor3DOcclusion_8 = value;
	}

	inline static int32_t get_offset_of_m_BlockingMask_9() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_BlockingMask_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_BlockingMask_9() const { return ___m_BlockingMask_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_BlockingMask_9() { return &___m_BlockingMask_9; }
	inline void set_m_BlockingMask_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_BlockingMask_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTriggerInteraction_10() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_RaycastTriggerInteraction_10)); }
	inline int32_t get_m_RaycastTriggerInteraction_10() const { return ___m_RaycastTriggerInteraction_10; }
	inline int32_t* get_address_of_m_RaycastTriggerInteraction_10() { return &___m_RaycastTriggerInteraction_10; }
	inline void set_m_RaycastTriggerInteraction_10(int32_t value)
	{
		___m_RaycastTriggerInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_Canvas_11)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasWarnedEventCameraNull_12() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_HasWarnedEventCameraNull_12)); }
	inline bool get_m_HasWarnedEventCameraNull_12() const { return ___m_HasWarnedEventCameraNull_12; }
	inline bool* get_address_of_m_HasWarnedEventCameraNull_12() { return &___m_HasWarnedEventCameraNull_12; }
	inline void set_m_HasWarnedEventCameraNull_12(bool value)
	{
		___m_HasWarnedEventCameraNull_12 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionHits3D_13() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_OcclusionHits3D_13)); }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* get_m_OcclusionHits3D_13() const { return ___m_OcclusionHits3D_13; }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09** get_address_of_m_OcclusionHits3D_13() { return &___m_OcclusionHits3D_13; }
	inline void set_m_OcclusionHits3D_13(RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* value)
	{
		___m_OcclusionHits3D_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OcclusionHits3D_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_OcclusionHits2D_14() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_OcclusionHits2D_14)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get_m_OcclusionHits2D_14() const { return ___m_OcclusionHits2D_14; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of_m_OcclusionHits2D_14() { return &___m_OcclusionHits2D_14; }
	inline void set_m_OcclusionHits2D_14(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		___m_OcclusionHits2D_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OcclusionHits2D_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastResultsCache_17() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0, ___m_RaycastResultsCache_17)); }
	inline List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * get_m_RaycastResultsCache_17() const { return ___m_RaycastResultsCache_17; }
	inline List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 ** get_address_of_m_RaycastResultsCache_17() { return &___m_RaycastResultsCache_17; }
	inline void set_m_RaycastResultsCache_17(List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * value)
	{
		___m_RaycastResultsCache_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResultsCache_17), (void*)value);
	}
};

struct TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields
{
public:
	// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitComparer UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::s_RaycastHitComparer
	RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16 * ___s_RaycastHitComparer_15;
	// UnityEngine.Vector3[] UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::s_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Corners_16;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster/RaycastHitData> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceGraphicRaycaster::s_SortedGraphics
	List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * ___s_SortedGraphics_18;

public:
	inline static int32_t get_offset_of_s_RaycastHitComparer_15() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields, ___s_RaycastHitComparer_15)); }
	inline RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16 * get_s_RaycastHitComparer_15() const { return ___s_RaycastHitComparer_15; }
	inline RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16 ** get_address_of_s_RaycastHitComparer_15() { return &___s_RaycastHitComparer_15; }
	inline void set_s_RaycastHitComparer_15(RaycastHitComparer_t9E2608076093424476D4CA9AAD5DDF464D710F16 * value)
	{
		___s_RaycastHitComparer_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastHitComparer_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_Corners_16() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields, ___s_Corners_16)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Corners_16() const { return ___s_Corners_16; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Corners_16() { return &___s_Corners_16; }
	inline void set_s_Corners_16(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Corners_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Corners_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_SortedGraphics_18() { return static_cast<int32_t>(offsetof(TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields, ___s_SortedGraphics_18)); }
	inline List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * get_s_SortedGraphics_18() const { return ___s_SortedGraphics_18; }
	inline List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 ** get_address_of_s_SortedGraphics_18() { return &___s_SortedGraphics_18; }
	inline void set_s_SortedGraphics_18(List_1_t55EE191B4306C5D2B5D41684C2A6BC7149C2FB87 * value)
	{
		___s_SortedGraphics_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SortedGraphics_18), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster
struct TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26  : public BaseRaycaster_tBC0FB2CBE6D3D40991EC20F689C43F76AD82A876
{
public:
	// UnityEngine.QueryTriggerInteraction UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_RaycastTriggerInteraction
	int32_t ___m_RaycastTriggerInteraction_6;
	// UnityEngine.LayerMask UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_EventMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_EventMask_7;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_MaxRayIntersections
	int32_t ___m_MaxRayIntersections_8;
	// UnityEngine.Camera UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_EventCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_EventCamera_9;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_HasWarnedEventCameraNull
	bool ___m_HasWarnedEventCameraNull_10;
	// UnityEngine.RaycastHit[] UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_RaycastHits
	RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* ___m_RaycastHits_11;
	// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitComparer UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_RaycastHitComparer
	RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B * ___m_RaycastHitComparer_12;
	// UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster/RaycastHitArraySegment UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_RaycastArrayWrapper
	RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D * ___m_RaycastArrayWrapper_13;
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit> UnityEngine.XR.Interaction.Toolkit.UI.TrackedDevicePhysicsRaycaster::m_RaycastResultsCache
	List_1_t16A48BE6E71AEE33E12B53A47FDF4F51B5D9AE8D * ___m_RaycastResultsCache_14;

public:
	inline static int32_t get_offset_of_m_RaycastTriggerInteraction_6() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_RaycastTriggerInteraction_6)); }
	inline int32_t get_m_RaycastTriggerInteraction_6() const { return ___m_RaycastTriggerInteraction_6; }
	inline int32_t* get_address_of_m_RaycastTriggerInteraction_6() { return &___m_RaycastTriggerInteraction_6; }
	inline void set_m_RaycastTriggerInteraction_6(int32_t value)
	{
		___m_RaycastTriggerInteraction_6 = value;
	}

	inline static int32_t get_offset_of_m_EventMask_7() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_EventMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_EventMask_7() const { return ___m_EventMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_EventMask_7() { return &___m_EventMask_7; }
	inline void set_m_EventMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_EventMask_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxRayIntersections_8() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_MaxRayIntersections_8)); }
	inline int32_t get_m_MaxRayIntersections_8() const { return ___m_MaxRayIntersections_8; }
	inline int32_t* get_address_of_m_MaxRayIntersections_8() { return &___m_MaxRayIntersections_8; }
	inline void set_m_MaxRayIntersections_8(int32_t value)
	{
		___m_MaxRayIntersections_8 = value;
	}

	inline static int32_t get_offset_of_m_EventCamera_9() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_EventCamera_9)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_EventCamera_9() const { return ___m_EventCamera_9; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_EventCamera_9() { return &___m_EventCamera_9; }
	inline void set_m_EventCamera_9(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_EventCamera_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventCamera_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasWarnedEventCameraNull_10() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_HasWarnedEventCameraNull_10)); }
	inline bool get_m_HasWarnedEventCameraNull_10() const { return ___m_HasWarnedEventCameraNull_10; }
	inline bool* get_address_of_m_HasWarnedEventCameraNull_10() { return &___m_HasWarnedEventCameraNull_10; }
	inline void set_m_HasWarnedEventCameraNull_10(bool value)
	{
		___m_HasWarnedEventCameraNull_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastHits_11() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_RaycastHits_11)); }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* get_m_RaycastHits_11() const { return ___m_RaycastHits_11; }
	inline RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09** get_address_of_m_RaycastHits_11() { return &___m_RaycastHits_11; }
	inline void set_m_RaycastHits_11(RaycastHitU5BU5D_t6778DB95346906446AAD3A1A36904F1846435A09* value)
	{
		___m_RaycastHits_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastHits_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastHitComparer_12() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_RaycastHitComparer_12)); }
	inline RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B * get_m_RaycastHitComparer_12() const { return ___m_RaycastHitComparer_12; }
	inline RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B ** get_address_of_m_RaycastHitComparer_12() { return &___m_RaycastHitComparer_12; }
	inline void set_m_RaycastHitComparer_12(RaycastHitComparer_tCA5FEE5D0CE3423F0E29534AC45FAB5A6308DA3B * value)
	{
		___m_RaycastHitComparer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastHitComparer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastArrayWrapper_13() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_RaycastArrayWrapper_13)); }
	inline RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D * get_m_RaycastArrayWrapper_13() const { return ___m_RaycastArrayWrapper_13; }
	inline RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D ** get_address_of_m_RaycastArrayWrapper_13() { return &___m_RaycastArrayWrapper_13; }
	inline void set_m_RaycastArrayWrapper_13(RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D * value)
	{
		___m_RaycastArrayWrapper_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastArrayWrapper_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_RaycastResultsCache_14() { return static_cast<int32_t>(offsetof(TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26, ___m_RaycastResultsCache_14)); }
	inline List_1_t16A48BE6E71AEE33E12B53A47FDF4F51B5D9AE8D * get_m_RaycastResultsCache_14() const { return ___m_RaycastResultsCache_14; }
	inline List_1_t16A48BE6E71AEE33E12B53A47FDF4F51B5D9AE8D ** get_address_of_m_RaycastResultsCache_14() { return &___m_RaycastResultsCache_14; }
	inline void set_m_RaycastResultsCache_14(List_1_t16A48BE6E71AEE33E12B53A47FDF4F51B5D9AE8D * value)
	{
		___m_RaycastResultsCache_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RaycastResultsCache_14), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule
struct UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B  : public BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924
{
public:
	// System.Action`2<UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::finalizeRaycastResults
	Action_2_tF7F5D29D757EA874F339B7A237256E126BFEECB0 * ___finalizeRaycastResults_10;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::pointerEnter
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___pointerEnter_11;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::pointerExit
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___pointerExit_12;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::pointerDown
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___pointerDown_13;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::pointerUp
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___pointerUp_14;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::pointerClick
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___pointerClick_15;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::initializePotentialDrag
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___initializePotentialDrag_16;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::beginDrag
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___beginDrag_17;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::drag
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___drag_18;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::endDrag
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___endDrag_19;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::drop
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___drop_20;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::scroll
	Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * ___scroll_21;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::updateSelected
	Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * ___updateSelected_22;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.AxisEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::move
	Action_2_t2E1567A92AA3BBF15C7916601A0CA9FF174E10A8 * ___move_23;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::submit
	Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * ___submit_24;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::cancel
	Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * ___cancel_25;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_ClickSpeed
	float ___m_ClickSpeed_26;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_MoveDeadzone
	float ___m_MoveDeadzone_27;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_RepeatDelay
	float ___m_RepeatDelay_28;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_RepeatRate
	float ___m_RepeatRate_29;
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_TrackedDeviceDragThresholdMultiplier
	float ___m_TrackedDeviceDragThresholdMultiplier_30;
	// UnityEngine.Camera UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::<uiCamera>k__BackingField
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___U3CuiCameraU3Ek__BackingField_31;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_CachedAxisEvent
	AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * ___m_CachedAxisEvent_32;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_PointerEventByPointerId
	Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * ___m_PointerEventByPointerId_33;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.XR.Interaction.Toolkit.UI.TrackedDeviceEventData> UnityEngine.XR.Interaction.Toolkit.UI.UIInputModule::m_TrackedDeviceEventByPointerId
	Dictionary_2_tB97027AE691647A2073D7680E86BE6AB876F1CBF * ___m_TrackedDeviceEventByPointerId_34;

public:
	inline static int32_t get_offset_of_finalizeRaycastResults_10() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___finalizeRaycastResults_10)); }
	inline Action_2_tF7F5D29D757EA874F339B7A237256E126BFEECB0 * get_finalizeRaycastResults_10() const { return ___finalizeRaycastResults_10; }
	inline Action_2_tF7F5D29D757EA874F339B7A237256E126BFEECB0 ** get_address_of_finalizeRaycastResults_10() { return &___finalizeRaycastResults_10; }
	inline void set_finalizeRaycastResults_10(Action_2_tF7F5D29D757EA874F339B7A237256E126BFEECB0 * value)
	{
		___finalizeRaycastResults_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___finalizeRaycastResults_10), (void*)value);
	}

	inline static int32_t get_offset_of_pointerEnter_11() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___pointerEnter_11)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_pointerEnter_11() const { return ___pointerEnter_11; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_pointerEnter_11() { return &___pointerEnter_11; }
	inline void set_pointerEnter_11(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___pointerEnter_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerEnter_11), (void*)value);
	}

	inline static int32_t get_offset_of_pointerExit_12() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___pointerExit_12)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_pointerExit_12() const { return ___pointerExit_12; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_pointerExit_12() { return &___pointerExit_12; }
	inline void set_pointerExit_12(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___pointerExit_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerExit_12), (void*)value);
	}

	inline static int32_t get_offset_of_pointerDown_13() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___pointerDown_13)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_pointerDown_13() const { return ___pointerDown_13; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_pointerDown_13() { return &___pointerDown_13; }
	inline void set_pointerDown_13(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___pointerDown_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerDown_13), (void*)value);
	}

	inline static int32_t get_offset_of_pointerUp_14() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___pointerUp_14)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_pointerUp_14() const { return ___pointerUp_14; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_pointerUp_14() { return &___pointerUp_14; }
	inline void set_pointerUp_14(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___pointerUp_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerUp_14), (void*)value);
	}

	inline static int32_t get_offset_of_pointerClick_15() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___pointerClick_15)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_pointerClick_15() const { return ___pointerClick_15; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_pointerClick_15() { return &___pointerClick_15; }
	inline void set_pointerClick_15(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___pointerClick_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerClick_15), (void*)value);
	}

	inline static int32_t get_offset_of_initializePotentialDrag_16() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___initializePotentialDrag_16)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_initializePotentialDrag_16() const { return ___initializePotentialDrag_16; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_initializePotentialDrag_16() { return &___initializePotentialDrag_16; }
	inline void set_initializePotentialDrag_16(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___initializePotentialDrag_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initializePotentialDrag_16), (void*)value);
	}

	inline static int32_t get_offset_of_beginDrag_17() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___beginDrag_17)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_beginDrag_17() const { return ___beginDrag_17; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_beginDrag_17() { return &___beginDrag_17; }
	inline void set_beginDrag_17(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___beginDrag_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___beginDrag_17), (void*)value);
	}

	inline static int32_t get_offset_of_drag_18() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___drag_18)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_drag_18() const { return ___drag_18; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_drag_18() { return &___drag_18; }
	inline void set_drag_18(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___drag_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___drag_18), (void*)value);
	}

	inline static int32_t get_offset_of_endDrag_19() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___endDrag_19)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_endDrag_19() const { return ___endDrag_19; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_endDrag_19() { return &___endDrag_19; }
	inline void set_endDrag_19(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___endDrag_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endDrag_19), (void*)value);
	}

	inline static int32_t get_offset_of_drop_20() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___drop_20)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_drop_20() const { return ___drop_20; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_drop_20() { return &___drop_20; }
	inline void set_drop_20(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___drop_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___drop_20), (void*)value);
	}

	inline static int32_t get_offset_of_scroll_21() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___scroll_21)); }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * get_scroll_21() const { return ___scroll_21; }
	inline Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB ** get_address_of_scroll_21() { return &___scroll_21; }
	inline void set_scroll_21(Action_2_tB3C7143368112207D1B4065D3DEB725FB19641AB * value)
	{
		___scroll_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scroll_21), (void*)value);
	}

	inline static int32_t get_offset_of_updateSelected_22() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___updateSelected_22)); }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * get_updateSelected_22() const { return ___updateSelected_22; }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 ** get_address_of_updateSelected_22() { return &___updateSelected_22; }
	inline void set_updateSelected_22(Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * value)
	{
		___updateSelected_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___updateSelected_22), (void*)value);
	}

	inline static int32_t get_offset_of_move_23() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___move_23)); }
	inline Action_2_t2E1567A92AA3BBF15C7916601A0CA9FF174E10A8 * get_move_23() const { return ___move_23; }
	inline Action_2_t2E1567A92AA3BBF15C7916601A0CA9FF174E10A8 ** get_address_of_move_23() { return &___move_23; }
	inline void set_move_23(Action_2_t2E1567A92AA3BBF15C7916601A0CA9FF174E10A8 * value)
	{
		___move_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___move_23), (void*)value);
	}

	inline static int32_t get_offset_of_submit_24() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___submit_24)); }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * get_submit_24() const { return ___submit_24; }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 ** get_address_of_submit_24() { return &___submit_24; }
	inline void set_submit_24(Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * value)
	{
		___submit_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submit_24), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_25() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___cancel_25)); }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * get_cancel_25() const { return ___cancel_25; }
	inline Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 ** get_address_of_cancel_25() { return &___cancel_25; }
	inline void set_cancel_25(Action_2_tCF61021940278C0CF31CBDFE18DCB01E053826A3 * value)
	{
		___cancel_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ClickSpeed_26() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_ClickSpeed_26)); }
	inline float get_m_ClickSpeed_26() const { return ___m_ClickSpeed_26; }
	inline float* get_address_of_m_ClickSpeed_26() { return &___m_ClickSpeed_26; }
	inline void set_m_ClickSpeed_26(float value)
	{
		___m_ClickSpeed_26 = value;
	}

	inline static int32_t get_offset_of_m_MoveDeadzone_27() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_MoveDeadzone_27)); }
	inline float get_m_MoveDeadzone_27() const { return ___m_MoveDeadzone_27; }
	inline float* get_address_of_m_MoveDeadzone_27() { return &___m_MoveDeadzone_27; }
	inline void set_m_MoveDeadzone_27(float value)
	{
		___m_MoveDeadzone_27 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_28() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_RepeatDelay_28)); }
	inline float get_m_RepeatDelay_28() const { return ___m_RepeatDelay_28; }
	inline float* get_address_of_m_RepeatDelay_28() { return &___m_RepeatDelay_28; }
	inline void set_m_RepeatDelay_28(float value)
	{
		___m_RepeatDelay_28 = value;
	}

	inline static int32_t get_offset_of_m_RepeatRate_29() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_RepeatRate_29)); }
	inline float get_m_RepeatRate_29() const { return ___m_RepeatRate_29; }
	inline float* get_address_of_m_RepeatRate_29() { return &___m_RepeatRate_29; }
	inline void set_m_RepeatRate_29(float value)
	{
		___m_RepeatRate_29 = value;
	}

	inline static int32_t get_offset_of_m_TrackedDeviceDragThresholdMultiplier_30() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_TrackedDeviceDragThresholdMultiplier_30)); }
	inline float get_m_TrackedDeviceDragThresholdMultiplier_30() const { return ___m_TrackedDeviceDragThresholdMultiplier_30; }
	inline float* get_address_of_m_TrackedDeviceDragThresholdMultiplier_30() { return &___m_TrackedDeviceDragThresholdMultiplier_30; }
	inline void set_m_TrackedDeviceDragThresholdMultiplier_30(float value)
	{
		___m_TrackedDeviceDragThresholdMultiplier_30 = value;
	}

	inline static int32_t get_offset_of_U3CuiCameraU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___U3CuiCameraU3Ek__BackingField_31)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_U3CuiCameraU3Ek__BackingField_31() const { return ___U3CuiCameraU3Ek__BackingField_31; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_U3CuiCameraU3Ek__BackingField_31() { return &___U3CuiCameraU3Ek__BackingField_31; }
	inline void set_U3CuiCameraU3Ek__BackingField_31(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___U3CuiCameraU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuiCameraU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedAxisEvent_32() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_CachedAxisEvent_32)); }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * get_m_CachedAxisEvent_32() const { return ___m_CachedAxisEvent_32; }
	inline AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E ** get_address_of_m_CachedAxisEvent_32() { return &___m_CachedAxisEvent_32; }
	inline void set_m_CachedAxisEvent_32(AxisEventData_t5F2EE83206BFD1BC59087D1C9CE31A4389A17E1E * value)
	{
		___m_CachedAxisEvent_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedAxisEvent_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerEventByPointerId_33() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_PointerEventByPointerId_33)); }
	inline Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * get_m_PointerEventByPointerId_33() const { return ___m_PointerEventByPointerId_33; }
	inline Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 ** get_address_of_m_PointerEventByPointerId_33() { return &___m_PointerEventByPointerId_33; }
	inline void set_m_PointerEventByPointerId_33(Dictionary_2_t52ECB6047A9EDAD198D0CC53F331CDEAAA83BED8 * value)
	{
		___m_PointerEventByPointerId_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerEventByPointerId_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedDeviceEventByPointerId_34() { return static_cast<int32_t>(offsetof(UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B, ___m_TrackedDeviceEventByPointerId_34)); }
	inline Dictionary_2_tB97027AE691647A2073D7680E86BE6AB876F1CBF * get_m_TrackedDeviceEventByPointerId_34() const { return ___m_TrackedDeviceEventByPointerId_34; }
	inline Dictionary_2_tB97027AE691647A2073D7680E86BE6AB876F1CBF ** get_address_of_m_TrackedDeviceEventByPointerId_34() { return &___m_TrackedDeviceEventByPointerId_34; }
	inline void set_m_TrackedDeviceEventByPointerId_34(Dictionary_2_tB97027AE691647A2073D7680E86BE6AB876F1CBF * value)
	{
		___m_TrackedDeviceEventByPointerId_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedDeviceEventByPointerId_34), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController
struct XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49  : public XRController_t54F264B6E8ECCD1875DA99199413F61E236D8326
{
public:
	// UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<primary2DAxis>k__BackingField
	Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * ___U3Cprimary2DAxisU3Ek__BackingField_43;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<trigger>k__BackingField
	AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * ___U3CtriggerU3Ek__BackingField_44;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<grip>k__BackingField
	AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * ___U3CgripU3Ek__BackingField_45;
	// UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<secondary2DAxis>k__BackingField
	Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * ___U3Csecondary2DAxisU3Ek__BackingField_46;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<primaryButton>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CprimaryButtonU3Ek__BackingField_47;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<primaryTouch>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CprimaryTouchU3Ek__BackingField_48;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<secondaryButton>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CsecondaryButtonU3Ek__BackingField_49;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<secondaryTouch>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CsecondaryTouchU3Ek__BackingField_50;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<gripButton>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CgripButtonU3Ek__BackingField_51;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<triggerButton>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CtriggerButtonU3Ek__BackingField_52;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<menuButton>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CmenuButtonU3Ek__BackingField_53;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<primary2DAxisClick>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3Cprimary2DAxisClickU3Ek__BackingField_54;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<primary2DAxisTouch>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3Cprimary2DAxisTouchU3Ek__BackingField_55;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<secondary2DAxisClick>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3Csecondary2DAxisClickU3Ek__BackingField_56;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<secondary2DAxisTouch>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3Csecondary2DAxisTouchU3Ek__BackingField_57;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<batteryLevel>k__BackingField
	AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * ___U3CbatteryLevelU3Ek__BackingField_58;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedController::<userPresence>k__BackingField
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * ___U3CuserPresenceU3Ek__BackingField_59;

public:
	inline static int32_t get_offset_of_U3Cprimary2DAxisU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Cprimary2DAxisU3Ek__BackingField_43)); }
	inline Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * get_U3Cprimary2DAxisU3Ek__BackingField_43() const { return ___U3Cprimary2DAxisU3Ek__BackingField_43; }
	inline Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 ** get_address_of_U3Cprimary2DAxisU3Ek__BackingField_43() { return &___U3Cprimary2DAxisU3Ek__BackingField_43; }
	inline void set_U3Cprimary2DAxisU3Ek__BackingField_43(Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * value)
	{
		___U3Cprimary2DAxisU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cprimary2DAxisU3Ek__BackingField_43), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtriggerU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CtriggerU3Ek__BackingField_44)); }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * get_U3CtriggerU3Ek__BackingField_44() const { return ___U3CtriggerU3Ek__BackingField_44; }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 ** get_address_of_U3CtriggerU3Ek__BackingField_44() { return &___U3CtriggerU3Ek__BackingField_44; }
	inline void set_U3CtriggerU3Ek__BackingField_44(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * value)
	{
		___U3CtriggerU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtriggerU3Ek__BackingField_44), (void*)value);
	}

	inline static int32_t get_offset_of_U3CgripU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CgripU3Ek__BackingField_45)); }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * get_U3CgripU3Ek__BackingField_45() const { return ___U3CgripU3Ek__BackingField_45; }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 ** get_address_of_U3CgripU3Ek__BackingField_45() { return &___U3CgripU3Ek__BackingField_45; }
	inline void set_U3CgripU3Ek__BackingField_45(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * value)
	{
		___U3CgripU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CgripU3Ek__BackingField_45), (void*)value);
	}

	inline static int32_t get_offset_of_U3Csecondary2DAxisU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Csecondary2DAxisU3Ek__BackingField_46)); }
	inline Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * get_U3Csecondary2DAxisU3Ek__BackingField_46() const { return ___U3Csecondary2DAxisU3Ek__BackingField_46; }
	inline Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 ** get_address_of_U3Csecondary2DAxisU3Ek__BackingField_46() { return &___U3Csecondary2DAxisU3Ek__BackingField_46; }
	inline void set_U3Csecondary2DAxisU3Ek__BackingField_46(Vector2Control_t342BA848221108F8818F05BF3CB484934F582935 * value)
	{
		___U3Csecondary2DAxisU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Csecondary2DAxisU3Ek__BackingField_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CprimaryButtonU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CprimaryButtonU3Ek__BackingField_47)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CprimaryButtonU3Ek__BackingField_47() const { return ___U3CprimaryButtonU3Ek__BackingField_47; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CprimaryButtonU3Ek__BackingField_47() { return &___U3CprimaryButtonU3Ek__BackingField_47; }
	inline void set_U3CprimaryButtonU3Ek__BackingField_47(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CprimaryButtonU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CprimaryButtonU3Ek__BackingField_47), (void*)value);
	}

	inline static int32_t get_offset_of_U3CprimaryTouchU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CprimaryTouchU3Ek__BackingField_48)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CprimaryTouchU3Ek__BackingField_48() const { return ___U3CprimaryTouchU3Ek__BackingField_48; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CprimaryTouchU3Ek__BackingField_48() { return &___U3CprimaryTouchU3Ek__BackingField_48; }
	inline void set_U3CprimaryTouchU3Ek__BackingField_48(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CprimaryTouchU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CprimaryTouchU3Ek__BackingField_48), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsecondaryButtonU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CsecondaryButtonU3Ek__BackingField_49)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CsecondaryButtonU3Ek__BackingField_49() const { return ___U3CsecondaryButtonU3Ek__BackingField_49; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CsecondaryButtonU3Ek__BackingField_49() { return &___U3CsecondaryButtonU3Ek__BackingField_49; }
	inline void set_U3CsecondaryButtonU3Ek__BackingField_49(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CsecondaryButtonU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsecondaryButtonU3Ek__BackingField_49), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsecondaryTouchU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CsecondaryTouchU3Ek__BackingField_50)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CsecondaryTouchU3Ek__BackingField_50() const { return ___U3CsecondaryTouchU3Ek__BackingField_50; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CsecondaryTouchU3Ek__BackingField_50() { return &___U3CsecondaryTouchU3Ek__BackingField_50; }
	inline void set_U3CsecondaryTouchU3Ek__BackingField_50(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CsecondaryTouchU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsecondaryTouchU3Ek__BackingField_50), (void*)value);
	}

	inline static int32_t get_offset_of_U3CgripButtonU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CgripButtonU3Ek__BackingField_51)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CgripButtonU3Ek__BackingField_51() const { return ___U3CgripButtonU3Ek__BackingField_51; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CgripButtonU3Ek__BackingField_51() { return &___U3CgripButtonU3Ek__BackingField_51; }
	inline void set_U3CgripButtonU3Ek__BackingField_51(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CgripButtonU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CgripButtonU3Ek__BackingField_51), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtriggerButtonU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CtriggerButtonU3Ek__BackingField_52)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CtriggerButtonU3Ek__BackingField_52() const { return ___U3CtriggerButtonU3Ek__BackingField_52; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CtriggerButtonU3Ek__BackingField_52() { return &___U3CtriggerButtonU3Ek__BackingField_52; }
	inline void set_U3CtriggerButtonU3Ek__BackingField_52(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CtriggerButtonU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtriggerButtonU3Ek__BackingField_52), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmenuButtonU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CmenuButtonU3Ek__BackingField_53)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CmenuButtonU3Ek__BackingField_53() const { return ___U3CmenuButtonU3Ek__BackingField_53; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CmenuButtonU3Ek__BackingField_53() { return &___U3CmenuButtonU3Ek__BackingField_53; }
	inline void set_U3CmenuButtonU3Ek__BackingField_53(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CmenuButtonU3Ek__BackingField_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuButtonU3Ek__BackingField_53), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cprimary2DAxisClickU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Cprimary2DAxisClickU3Ek__BackingField_54)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3Cprimary2DAxisClickU3Ek__BackingField_54() const { return ___U3Cprimary2DAxisClickU3Ek__BackingField_54; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3Cprimary2DAxisClickU3Ek__BackingField_54() { return &___U3Cprimary2DAxisClickU3Ek__BackingField_54; }
	inline void set_U3Cprimary2DAxisClickU3Ek__BackingField_54(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3Cprimary2DAxisClickU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cprimary2DAxisClickU3Ek__BackingField_54), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cprimary2DAxisTouchU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Cprimary2DAxisTouchU3Ek__BackingField_55)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3Cprimary2DAxisTouchU3Ek__BackingField_55() const { return ___U3Cprimary2DAxisTouchU3Ek__BackingField_55; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3Cprimary2DAxisTouchU3Ek__BackingField_55() { return &___U3Cprimary2DAxisTouchU3Ek__BackingField_55; }
	inline void set_U3Cprimary2DAxisTouchU3Ek__BackingField_55(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3Cprimary2DAxisTouchU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cprimary2DAxisTouchU3Ek__BackingField_55), (void*)value);
	}

	inline static int32_t get_offset_of_U3Csecondary2DAxisClickU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Csecondary2DAxisClickU3Ek__BackingField_56)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3Csecondary2DAxisClickU3Ek__BackingField_56() const { return ___U3Csecondary2DAxisClickU3Ek__BackingField_56; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3Csecondary2DAxisClickU3Ek__BackingField_56() { return &___U3Csecondary2DAxisClickU3Ek__BackingField_56; }
	inline void set_U3Csecondary2DAxisClickU3Ek__BackingField_56(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3Csecondary2DAxisClickU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Csecondary2DAxisClickU3Ek__BackingField_56), (void*)value);
	}

	inline static int32_t get_offset_of_U3Csecondary2DAxisTouchU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3Csecondary2DAxisTouchU3Ek__BackingField_57)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3Csecondary2DAxisTouchU3Ek__BackingField_57() const { return ___U3Csecondary2DAxisTouchU3Ek__BackingField_57; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3Csecondary2DAxisTouchU3Ek__BackingField_57() { return &___U3Csecondary2DAxisTouchU3Ek__BackingField_57; }
	inline void set_U3Csecondary2DAxisTouchU3Ek__BackingField_57(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3Csecondary2DAxisTouchU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Csecondary2DAxisTouchU3Ek__BackingField_57), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbatteryLevelU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CbatteryLevelU3Ek__BackingField_58)); }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * get_U3CbatteryLevelU3Ek__BackingField_58() const { return ___U3CbatteryLevelU3Ek__BackingField_58; }
	inline AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 ** get_address_of_U3CbatteryLevelU3Ek__BackingField_58() { return &___U3CbatteryLevelU3Ek__BackingField_58; }
	inline void set_U3CbatteryLevelU3Ek__BackingField_58(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22 * value)
	{
		___U3CbatteryLevelU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatteryLevelU3Ek__BackingField_58), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuserPresenceU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49, ___U3CuserPresenceU3Ek__BackingField_59)); }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * get_U3CuserPresenceU3Ek__BackingField_59() const { return ___U3CuserPresenceU3Ek__BackingField_59; }
	inline ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D ** get_address_of_U3CuserPresenceU3Ek__BackingField_59() { return &___U3CuserPresenceU3Ek__BackingField_59; }
	inline void set_U3CuserPresenceU3Ek__BackingField_59(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * value)
	{
		___U3CuserPresenceU3Ek__BackingField_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuserPresenceU3Ek__BackingField_59), (void*)value);
	}
};


// UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation.XRSimulatedHMD
struct XRSimulatedHMD_tE5A264AD475895067EBDC8293CAE486DD24CA540  : public XRHMD_t92534015B658392D6A49AFAA924E0B2394583C1A
{
public:

public:
};


// UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule
struct XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A  : public UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B
{
public:
	// System.Single UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_MaxTrackedDeviceRaycastDistance
	float ___m_MaxTrackedDeviceRaycastDistance_35;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_EnableXRInput
	bool ___m_EnableXRInput_36;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_EnableMouseInput
	bool ___m_EnableMouseInput_37;
	// System.Boolean UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_EnableTouchInput
	bool ___m_EnableTouchInput_38;
	// System.Int32 UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_RollingPointerId
	int32_t ___m_RollingPointerId_39;
	// UnityEngine.XR.Interaction.Toolkit.UI.MouseModel UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_Mouse
	MouseModel_t5636263593FF0F39C4269B519076318964801C67  ___m_Mouse_40;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredTouch> UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_RegisteredTouches
	List_1_tF908382B3F9A3360582C830544139A8160622A6C * ___m_RegisteredTouches_41;
	// System.Collections.Generic.List`1<UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule/RegisteredInteractor> UnityEngine.XR.Interaction.Toolkit.UI.XRUIInputModule::m_RegisteredInteractors
	List_1_tBA68AA99B3AE6EFB57B37FF744D7E22CFE9F81BE * ___m_RegisteredInteractors_42;

public:
	inline static int32_t get_offset_of_m_MaxTrackedDeviceRaycastDistance_35() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_MaxTrackedDeviceRaycastDistance_35)); }
	inline float get_m_MaxTrackedDeviceRaycastDistance_35() const { return ___m_MaxTrackedDeviceRaycastDistance_35; }
	inline float* get_address_of_m_MaxTrackedDeviceRaycastDistance_35() { return &___m_MaxTrackedDeviceRaycastDistance_35; }
	inline void set_m_MaxTrackedDeviceRaycastDistance_35(float value)
	{
		___m_MaxTrackedDeviceRaycastDistance_35 = value;
	}

	inline static int32_t get_offset_of_m_EnableXRInput_36() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_EnableXRInput_36)); }
	inline bool get_m_EnableXRInput_36() const { return ___m_EnableXRInput_36; }
	inline bool* get_address_of_m_EnableXRInput_36() { return &___m_EnableXRInput_36; }
	inline void set_m_EnableXRInput_36(bool value)
	{
		___m_EnableXRInput_36 = value;
	}

	inline static int32_t get_offset_of_m_EnableMouseInput_37() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_EnableMouseInput_37)); }
	inline bool get_m_EnableMouseInput_37() const { return ___m_EnableMouseInput_37; }
	inline bool* get_address_of_m_EnableMouseInput_37() { return &___m_EnableMouseInput_37; }
	inline void set_m_EnableMouseInput_37(bool value)
	{
		___m_EnableMouseInput_37 = value;
	}

	inline static int32_t get_offset_of_m_EnableTouchInput_38() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_EnableTouchInput_38)); }
	inline bool get_m_EnableTouchInput_38() const { return ___m_EnableTouchInput_38; }
	inline bool* get_address_of_m_EnableTouchInput_38() { return &___m_EnableTouchInput_38; }
	inline void set_m_EnableTouchInput_38(bool value)
	{
		___m_EnableTouchInput_38 = value;
	}

	inline static int32_t get_offset_of_m_RollingPointerId_39() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_RollingPointerId_39)); }
	inline int32_t get_m_RollingPointerId_39() const { return ___m_RollingPointerId_39; }
	inline int32_t* get_address_of_m_RollingPointerId_39() { return &___m_RollingPointerId_39; }
	inline void set_m_RollingPointerId_39(int32_t value)
	{
		___m_RollingPointerId_39 = value;
	}

	inline static int32_t get_offset_of_m_Mouse_40() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_Mouse_40)); }
	inline MouseModel_t5636263593FF0F39C4269B519076318964801C67  get_m_Mouse_40() const { return ___m_Mouse_40; }
	inline MouseModel_t5636263593FF0F39C4269B519076318964801C67 * get_address_of_m_Mouse_40() { return &___m_Mouse_40; }
	inline void set_m_Mouse_40(MouseModel_t5636263593FF0F39C4269B519076318964801C67  value)
	{
		___m_Mouse_40 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_LeftButton_5))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_LeftButton_5))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_RightButton_6))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_RightButton_6))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___m_GameObject_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_Mouse_40))->___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedRaycastU3Ek__BackingField_3))->___module_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedGameObjectU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CpressedGameObjectRawU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_Mouse_40))->___m_MiddleButton_7))->___m_ImplementationData_2))->___U3CdraggedGameObjectU3Ek__BackingField_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Mouse_40))->___m_InternalData_8))->___U3ChoverTargetsU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Mouse_40))->___m_InternalData_8))->___U3CpointerTargetU3Ek__BackingField_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RegisteredTouches_41() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_RegisteredTouches_41)); }
	inline List_1_tF908382B3F9A3360582C830544139A8160622A6C * get_m_RegisteredTouches_41() const { return ___m_RegisteredTouches_41; }
	inline List_1_tF908382B3F9A3360582C830544139A8160622A6C ** get_address_of_m_RegisteredTouches_41() { return &___m_RegisteredTouches_41; }
	inline void set_m_RegisteredTouches_41(List_1_tF908382B3F9A3360582C830544139A8160622A6C * value)
	{
		___m_RegisteredTouches_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RegisteredTouches_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_RegisteredInteractors_42() { return static_cast<int32_t>(offsetof(XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A, ___m_RegisteredInteractors_42)); }
	inline List_1_tBA68AA99B3AE6EFB57B37FF744D7E22CFE9F81BE * get_m_RegisteredInteractors_42() const { return ___m_RegisteredInteractors_42; }
	inline List_1_tBA68AA99B3AE6EFB57B37FF744D7E22CFE9F81BE ** get_address_of_m_RegisteredInteractors_42() { return &___m_RegisteredInteractors_42; }
	inline void set_m_RegisteredInteractors_42(List_1_tBA68AA99B3AE6EFB57B37FF744D7E22CFE9F81BE * value)
	{
		___m_RegisteredInteractors_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RegisteredInteractors_42), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3274[6] = 
{
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_mode_0(),
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_srcBlend_1(),
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_dstBlend_2(),
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_zWrite_3(),
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_baseColor_4(),
	ShaderPropertyLookup_t5A62EB0A1E69E714E8760DA8ABB2545B6725A09E_StaticFields::get_offset_of_color_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3275[11] = 
{
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_ShowInteractableHoverMeshes_34(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_InteractableHoverMeshMaterial_35(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_InteractableCantHoverMeshMaterial_36(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_SocketActive_37(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_InteractableHoverScale_38(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_RecycleDelayTime_39(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_LastRemoveTime_40(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_U3CunsortedValidTargetsU3Ek__BackingField_41(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_TriggerContactMonitor_42(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279::get_offset_of_m_MeshFilterCache_43(),
	XRSocketInteractor_t8DE39EB2BC2B0492DB06C424789CBAD66CFAD279_StaticFields::get_offset_of_s_MeshFilters_44(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3276[2] = 
{
	InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47::get_offset_of_m_Bits_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InteractionLayerMask_t9AFE7E0AC5956A410A71C6C26CB6793B882C3D47::get_offset_of_m_Mask_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3277[4] = 
{
	0,
	0,
	0,
	InteractionLayerSettings_t7AB306A5D9F3D2799805FFF8A3BAFF24EE383600::get_offset_of_m_LayerNames_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3278[2] = 
{
	BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE::get_offset_of_U3CinteractorObjectU3Ek__BackingField_0(),
	BaseInteractionEventArgs_t0C66D1C53D051664FAE1FF62C7CDCC6E68219CFE::get_offset_of_U3CinteractableObjectU3Ek__BackingField_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3280[1] = 
{
	TeleportingEventArgs_t491BDF6773E9B1A7098DB80231DFD3BE76B7A72D::get_offset_of_U3CteleportRequestU3Ek__BackingField_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3282[1] = 
{
	HoverEnterEventArgs_tF1B183301350DF582F55A16F203031B1CFC3A7C9::get_offset_of_U3CmanagerU3Ek__BackingField_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3284[2] = 
{
	HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293::get_offset_of_U3CmanagerU3Ek__BackingField_2(),
	HoverExitEventArgs_t2BA31B52E0F98F7F2BB264E853633E0FCCA7D293::get_offset_of_U3CisCanceledU3Ek__BackingField_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3286[1] = 
{
	SelectEnterEventArgs_tEB89AEF4AAB84886C278347B17FCA2C4C3C956DE::get_offset_of_U3CmanagerU3Ek__BackingField_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3288[2] = 
{
	SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E::get_offset_of_U3CmanagerU3Ek__BackingField_2(),
	SelectExitEventArgs_t5BD68DE25AAF50453DF01C76A1A637D3187F264E::get_offset_of_U3CisCanceledU3Ek__BackingField_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3293[1] = 
{
	BaseRegistrationEventArgs_t7279FAE947C14E1ED72D8FC9829251EA4AB78461::get_offset_of_U3CmanagerU3Ek__BackingField_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3294[1] = 
{
	InteractorRegisteredEventArgs_t094E4A6231184E16492D604FB8C293B9930B06AE::get_offset_of_U3CinteractorObjectU3Ek__BackingField_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3295[1] = 
{
	InteractableRegisteredEventArgs_tA8FAFBC8EED946D75CF642327918A231A8DB394F::get_offset_of_U3CinteractableObjectU3Ek__BackingField_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3296[1] = 
{
	InteractorUnregisteredEventArgs_t8CEBB9B66A23C041A3EF5F89621650A46B832206::get_offset_of_U3CinteractorObjectU3Ek__BackingField_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3297[1] = 
{
	InteractableUnregisteredEventArgs_tAC5CAF1AAEC37101C1248BF12E39788AA0BB6A4D::get_offset_of_U3CinteractableObjectU3Ek__BackingField_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3300[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3301[36] = 
{
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_interactorRegistered_4(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_interactorUnregistered_5(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_interactableRegistered_6(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_interactableUnregistered_7(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_U3CactiveInteractionManagersU3Ek__BackingField_8(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_ColliderToInteractableMap_9(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_Interactors_10(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_Interactables_11(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_CurrentHovered_12(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_CurrentSelected_13(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_ValidTargets_14(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_UnorderedValidTargets_15(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_DeprecatedValidTargets_16(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_ScratchInteractors_17(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_ScratchInteractables_18(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_SelectEnterEventArgs_19(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_SelectExitEventArgs_20(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_HoverEnterEventArgs_21(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_HoverExitEventArgs_22(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_InteractorRegisteredEventArgs_23(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_InteractorUnregisteredEventArgs_24(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_InteractableRegisteredEventArgs_25(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4::get_offset_of_m_InteractableUnregisteredEventArgs_26(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_PreprocessInteractorsMarker_27(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_ProcessInteractorsMarker_28(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_ProcessInteractablesMarker_29(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_GetValidTargetsMarker_30(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_FilterRegisteredValidTargetsMarker_31(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_EvaluateInvalidSelectionsMarker_32(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_EvaluateInvalidHoversMarker_33(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_EvaluateValidSelectionsMarker_34(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_EvaluateValidHoversMarker_35(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_SelectEnterMarker_36(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_SelectExitMarker_37(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_HoverEnterMarker_38(),
	XRInteractionManager_tDAFFE50E3E31F73912C35D646ED70337C27F53F4_StaticFields::get_offset_of_s_HoverExitMarker_39(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3302[5] = 
{
	UpdatePhase_tDDD5125832C7AE3CDF170690377B689E62D6DD07::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3303[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3304[5] = 
{
	CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2::get_offset_of_m_LocomotionProvider_4(),
	CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2::get_offset_of_m_MinHeight_5(),
	CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2::get_offset_of_m_MaxHeight_6(),
	CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2::get_offset_of_m_XROrigin_7(),
	CharacterControllerDriver_tC98D47A70033C6BF6C3A50DC4BB762798D1DF4B2::get_offset_of_m_CharacterController_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3305[2] = 
{
	ActionBasedContinuousMoveProvider_tA1E91E01A6936255752856265BCBFBDBA093293A::get_offset_of_m_LeftHandMoveAction_16(),
	ActionBasedContinuousMoveProvider_tA1E91E01A6936255752856265BCBFBDBA093293A::get_offset_of_m_RightHandMoveAction_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3306[2] = 
{
	ActionBasedContinuousTurnProvider_t9D9A463B9FEA2F13D91375EE51B4A7044D642ED2::get_offset_of_m_LeftHandTurnAction_9(),
	ActionBasedContinuousTurnProvider_t9D9A463B9FEA2F13D91375EE51B4A7044D642ED2::get_offset_of_m_RightHandTurnAction_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3307[3] = 
{
	GravityApplicationMode_tD5AF61AA4BF84CE90354D159B0AA2A1FE368B3F4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3308[8] = 
{
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_MoveSpeed_8(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_EnableStrafe_9(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_UseGravity_10(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_GravityApplicationMode_11(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_ForwardSource_12(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_CharacterController_13(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_AttemptedGetCharacterController_14(),
	ContinuousMoveProviderBase_tEEA003EAE25A3AAEEF9FF9B39FA30EE046059044::get_offset_of_m_VerticalVelocity_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3309[1] = 
{
	ContinuousTurnProviderBase_tF9080699926533D4568B35784A9B226C46FB823C::get_offset_of_m_TurnSpeed_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3310[3] = 
{
	InputAxes_tFE5F92BD5F5CC2643A056F4DB0B100BBE6FD9088::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3311[5] = 
{
	DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A::get_offset_of_m_InputBinding_16(),
	DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A::get_offset_of_m_Controllers_17(),
	DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A::get_offset_of_m_DeadzoneMin_18(),
	DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A::get_offset_of_m_DeadzoneMax_19(),
	DeviceBasedContinuousMoveProvider_tF69DA7A55E9977C3D34323FA7AFD8546B700EC9A_StaticFields::get_offset_of_k_Vec2UsageList_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3312[3] = 
{
	InputAxes_t7BCA8B7350420526D7BDD69EFDDFF4F1AAC49679::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3313[5] = 
{
	DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5::get_offset_of_m_InputBinding_9(),
	DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5::get_offset_of_m_Controllers_10(),
	DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5::get_offset_of_m_DeadzoneMin_11(),
	DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5::get_offset_of_m_DeadzoneMax_12(),
	DeviceBasedContinuousTurnProvider_t45AB88FBDFBD2C5443D3A2FCDB9729AA99D7B6D5_StaticFields::get_offset_of_k_Vec2UsageList_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3314[4] = 
{
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448::get_offset_of_beginLocomotion_4(),
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448::get_offset_of_endLocomotion_5(),
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448::get_offset_of_m_System_6(),
	LocomotionProvider_tF07C5527C6B8CDB38B435E8EDC50DF8CA86C6448::get_offset_of_startLocomotion_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3315[4] = 
{
	RequestResult_tAAD8D78E7F477222E3AE79D3CE637A8D243B8D31::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3316[4] = 
{
	LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D::get_offset_of_m_CurrentExclusiveProvider_4(),
	LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D::get_offset_of_m_TimeMadeExclusive_5(),
	LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D::get_offset_of_m_Timeout_6(),
	LocomotionSystem_tEF4F8D19A639BFEBBD4C52AE52AB74D337BB4C5D::get_offset_of_m_XROrigin_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3317[2] = 
{
	ActionBasedSnapTurnProvider_t53E79F486EEE1ADF3C8B63E117564275FE38F03F::get_offset_of_m_LeftHandSnapTurnAction_14(),
	ActionBasedSnapTurnProvider_t53E79F486EEE1ADF3C8B63E117564275FE38F03F::get_offset_of_m_RightHandSnapTurnAction_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3318[3] = 
{
	InputAxes_tCC63ED974CDE78E4FEB19C8098822FD391B4ED53::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3319[4] = 
{
	DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064::get_offset_of_m_TurnUsage_14(),
	DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064::get_offset_of_m_Controllers_15(),
	DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064::get_offset_of_m_DeadZone_16(),
	DeviceBasedSnapTurnProvider_t982487731127A98BCAA326D086F69A540B072064_StaticFields::get_offset_of_k_Vec2UsageList_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3320[6] = 
{
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_TurnAmount_8(),
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_DebounceTime_9(),
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_EnableTurnLeftRight_10(),
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_EnableTurnAround_11(),
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_CurrentTurnAmount_12(),
	SnapTurnProviderBase_tA78E924429435219628652177951828C14DB6D2A::get_offset_of_m_TimeStarted_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3321[5] = 
{
	MatchOrientation_t6A47BD089FA54BAE235D4DAE2414F83F54577D0E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3322[4] = 
{
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2::get_offset_of_destinationPosition_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2::get_offset_of_destinationRotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2::get_offset_of_requestTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TeleportRequest_t6A637ADD208F2B911F1A3D87FA7436C4B487E5C2::get_offset_of_matchOrientation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3323[9] = 
{
	TeleportTrigger_t5BA91931CF4212743F56E048DDAF656BA4F6D4E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3324[5] = 
{
	BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127::get_offset_of_m_TeleportationProvider_40(),
	BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127::get_offset_of_m_MatchOrientation_41(),
	BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127::get_offset_of_m_TeleportTrigger_42(),
	BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127::get_offset_of_m_Teleporting_43(),
	BaseTeleportationInteractable_t7544BF7BEFB6962A0A9B81F38A20EA2EED30F127::get_offset_of_m_TeleportingEventArgs_44(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3325[1] = 
{
	TeleportationAnchor_t1DE5130A523D2A19AA321D3E01BAFB6DA7E7639F::get_offset_of_m_TeleportAnchorTransform_45(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3327[2] = 
{
	TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B::get_offset_of_U3CcurrentRequestU3Ek__BackingField_8(),
	TeleportationProvider_tE71462746C5C887126DB26D020358B1E85A3306B::get_offset_of_U3CvalidRequestU3Ek__BackingField_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3329[25] = 
{
	Button_tADDD1BFDCE9AADC94CC4B6D4678484CB0FB5833C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3330[8] = 
{
	ButtonReadType_t681DDCB14F5D5D7849EBFF6D3A3D64315D698BAE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3331[2] = 
{
	ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonInfo_tDA2196FFF77CB4BECFEC114BFD4715A3CDE247ED::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3332[2] = 
{
	InputHelpers_t14ED0E9CACB9EAE012078A8FCDE3162F9701B4DD_StaticFields::get_offset_of_s_ButtonData_0(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3333[2] = 
{
	SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5_StaticFields::get_offset_of_s_InteractableDistanceSqrMap_0(),
	SortingHelpers_tD44C24B5650B3B5E461BDF0C1B6D888EE33E5EB5_StaticFields::get_offset_of_s_InteractableDistanceComparison_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3334[1] = 
{
	XRRig_t5979BC5624B3BB053EB0043B7CD2256665BE8AF5::get_offset_of_m_CameraGameObject_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3335[39] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3336[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3337[2] = 
{
	ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1_StaticFields::get_offset_of_s_Assemblies_0(),
	ReflectionUtils_tAFAB9940191FBEA52D36C7ADF6EBE79EE255A9A1_StaticFields::get_offset_of_s_TypesPerAssembly_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3338[3] = 
{
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3339[1] = 
{
	ScriptableSettingsPathAttribute_t9D69DCC2E340AB6B3209F95783BD118EC27DEB50::get_offset_of_U3CpathU3Ek__BackingField_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3341[7] = 
{
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_contactAdded_0(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_contactRemoved_1(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_U3CinteractionManagerU3Ek__BackingField_2(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_m_EnteredColliders_3(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_m_UnorderedInteractables_4(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5::get_offset_of_m_EnteredUnassociatedColliders_5(),
	TriggerContactMonitor_t041FDA658746E866D9AD6C84F039343A1AD51CB5_StaticFields::get_offset_of_s_ScratchColliders_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3342[8] = 
{
	0,
	0,
	0,
	0,
	0,
	ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields::get_offset_of_k_PathTrimChars_9(),
	ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields::get_offset_of_k_InvalidCharacters_10(),
	ScriptableSettingsBase_t3BB0B2844F4C66A113A74AACD19B1837F7F6CEF2_StaticFields::get_offset_of_k_InvalidStrings_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3343[2] = 
{
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3344[3] = 
{
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B::get_offset_of_U3CconsecutiveMoveCountU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B::get_offset_of_U3ClastMoveDirectionU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t3D025B8045197490685278B67A5C2D333E81172B::get_offset_of_U3ClastMoveTimeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3345[6] = 
{
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_U3CmoveU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_U3CsubmitButtonDeltaU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_U3CcancelButtonDeltaU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_U3CimplementationDataU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_m_SubmitButtonDown_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JoystickModel_t8971BA24BD178AB5240BB211CBB69D1659985B93::get_offset_of_m_CancelButtonDown_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3346[4] = 
{
	ButtonDeltaState_t49C4EFA8C1D3EAFA4A6E1C88169D98A0C57E80A5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3347[7] = 
{
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CisDraggingU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CpressedTimeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CpressedPositionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CpressedRaycastU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CpressedGameObjectU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_tC17849C5416013C776FA2ADA92F2B2A9A968EB98::get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3348[3] = 
{
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818::get_offset_of_U3ClastFrameDeltaU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818::get_offset_of_m_IsDown_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseButtonModel_t6FB84DD7DDB186412B0B9BCAA494E49EF90C2818::get_offset_of_m_ImplementationData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3349[2] = 
{
	InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63::get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalData_t74E9BF9D07C132377EE7DFEA82682EA9869F2B63::get_offset_of_U3CpointerTargetU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3350[9] = 
{
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_U3CpointerIdU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_U3CchangedThisFrameU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_U3CdeltaPositionU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_ScrollDelta_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_LeftButton_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_RightButton_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_MiddleButton_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseModel_t5636263593FF0F39C4269B519076318964801C67::get_offset_of_m_InternalData_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3351[9] = 
{
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpointerTargetU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CisDraggingU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpressedTimeU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpressedPositionU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpressedRaycastU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpressedGameObjectU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t324E2D0B2A39E6DEDCA979D1D411A31A03ED0886::get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3352[7] = 
{
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_U3CpointerIdU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_U3CselectDeltaU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_U3CchangedThisFrameU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_U3CdeltaPositionU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_m_SelectPhase_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_m_Position_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchModel_t54376335176DE8822CA134D6B3FE0C85DABFB984::get_offset_of_m_ImplementationData_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3353[3] = 
{
	TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63::get_offset_of_U3CrayPointsU3Ek__BackingField_24(),
	TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63::get_offset_of_U3CrayHitIndexU3Ek__BackingField_25(),
	TrackedDeviceEventData_tC99E271E6E72059A3D9F95C1CC26ED897952CA63::get_offset_of_U3ClayerMaskU3Ek__BackingField_26(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3354[5] = 
{
	RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41::get_offset_of_U3CgraphicU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41::get_offset_of_U3CworldHitPositionU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41::get_offset_of_U3CscreenPositionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41::get_offset_of_U3CdistanceU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHitData_t76D245303A305EF3E4272A6D385DA265013BEE41::get_offset_of_U3CdisplayIndexU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3356[14] = 
{
	0,
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_IgnoreReversedGraphics_6(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_CheckFor2DOcclusion_7(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_CheckFor3DOcclusion_8(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_BlockingMask_9(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_RaycastTriggerInteraction_10(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_Canvas_11(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_HasWarnedEventCameraNull_12(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_OcclusionHits3D_13(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_OcclusionHits2D_14(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields::get_offset_of_s_RaycastHitComparer_15(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields::get_offset_of_s_Corners_16(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0::get_offset_of_m_RaycastResultsCache_17(),
	TrackedDeviceGraphicRaycaster_tDD437ED1D930A17A5F48617483631651B69D70B0_StaticFields::get_offset_of_s_SortedGraphics_18(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3357[10] = 
{
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3ChoverTargetsU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpointerTargetU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CisDraggingU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpressedTimeU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpositionU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpressedPositionU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpressedRaycastU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpressedGameObjectU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CpressedGameObjectRawU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImplementationData_t8A1ECD5E0CDA1CFEBBC264A4A91FD52B672B6BE6::get_offset_of_U3CdraggedGameObjectU3Ek__BackingField_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3358[14] = 
{
	0,
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_ImplementationData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_U3CpointerIdU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_SelectDown_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_U3CselectDeltaU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_U3CchangedThisFrameU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_Position_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_Orientation_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_RaycastPoints_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_U3CcurrentRaycastU3Ek__BackingField_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_U3CcurrentRaycastEndpointIndexU3Ek__BackingField_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_RaycastLayerMask_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_ScrollDelta_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackedDeviceModel_t2E578FB0FF9BCBDDD3C122E38365852B20DCC494::get_offset_of_m_MaxRaycastDistance_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3359[3] = 
{
	RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D::get_offset_of_m_Count_0(),
	RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D::get_offset_of_m_Hits_1(),
	RaycastHitArraySegment_t04A248B0BED68FB207CC9A643D86E0DBBEEB0F5D::get_offset_of_m_CurrentIndex_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3361[10] = 
{
	0,
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_RaycastTriggerInteraction_6(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_EventMask_7(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_MaxRayIntersections_8(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_EventCamera_9(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_HasWarnedEventCameraNull_10(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_RaycastHits_11(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_RaycastHitComparer_12(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_RaycastArrayWrapper_13(),
	TrackedDevicePhysicsRaycaster_t98C08151B1906E0EF072865E8D92B4A05F178D26::get_offset_of_m_RaycastResultsCache_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3362[25] = 
{
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_finalizeRaycastResults_10(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_pointerEnter_11(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_pointerExit_12(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_pointerDown_13(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_pointerUp_14(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_pointerClick_15(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_initializePotentialDrag_16(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_beginDrag_17(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_drag_18(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_endDrag_19(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_drop_20(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_scroll_21(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_updateSelected_22(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_move_23(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_submit_24(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_cancel_25(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_ClickSpeed_26(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_MoveDeadzone_27(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_RepeatDelay_28(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_RepeatRate_29(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_TrackedDeviceDragThresholdMultiplier_30(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_U3CuiCameraU3Ek__BackingField_31(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_CachedAxisEvent_32(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_PointerEventByPointerId_33(),
	UIInputModule_tC876FE1C3323186A7E554C919836CB76162F3D3B::get_offset_of_m_TrackedDeviceEventByPointerId_34(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3364[2] = 
{
	RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555::get_offset_of_interactor_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RegisteredInteractor_t8230FD08A7596CAEC3311F9D5AF6907485EB3555::get_offset_of_model_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3365[3] = 
{
	RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8::get_offset_of_isValid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8::get_offset_of_touchId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RegisteredTouch_t51D3EF617C675C506B44B0B8CDCC3410F4B3A3D8::get_offset_of_model_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3366[8] = 
{
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_MaxTrackedDeviceRaycastDistance_35(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_EnableXRInput_36(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_EnableMouseInput_37(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_EnableTouchInput_38(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_RollingPointerId_39(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_Mouse_40(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_RegisteredTouches_41(),
	XRUIInputModule_t725AE7A89596ED0E41EDF2A508D0A9954DBBDA3A::get_offset_of_m_RegisteredInteractors_42(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3367[5] = 
{
	Cardinal_tCC65359DA694D1FE249B8A590598161786FD3BC1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3369[1] = 
{
	InputActionManager_t200B2EBBC1D2A94D34E66818449CC1A3BA12963B::get_offset_of_m_ActionAssets_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3372[4] = 
{
	Space_t145148EFA4ABDCF429FA18A63E4C01D03E2CB75C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3373[3] = 
{
	TransformationMode_tFAD711C19386573876EBDE687EA3E407CB1C191A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3374[5] = 
{
	Axis2DTargets_tDA1ABED2BAB31548E373BA5BA1FA1CE335A16174::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3375[87] = 
{
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardXTranslateAction_4(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardYTranslateAction_5(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardZTranslateAction_6(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateLeftAction_7(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateRightAction_8(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleManipulateLeftAction_9(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleManipulateRightAction_10(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateHeadAction_11(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseDeltaAction_12(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseScrollAction_13(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RotateModeOverrideAction_14(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleMouseTransformationModeAction_15(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_NegateModeAction_16(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_XConstraintAction_17(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_YConstraintAction_18(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ZConstraintAction_19(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ResetAction_20(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleCursorLockAction_21(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleDevicePositionTargetAction_22(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_TogglePrimary2DAxisTargetAction_23(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ToggleSecondary2DAxisTargetAction_24(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Axis2DAction_25(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RestingHandAxis2DAction_26(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_GripAction_27(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_TriggerAction_28(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_PrimaryButtonAction_29(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_SecondaryButtonAction_30(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MenuAction_31(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Primary2DAxisClickAction_32(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Secondary2DAxisClickAction_33(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Primary2DAxisTouchAction_34(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Secondary2DAxisTouchAction_35(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_PrimaryTouchAction_36(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_SecondaryTouchAction_37(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_CameraTransform_38(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardTranslateSpace_39(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseTranslateSpace_40(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardXTranslateSpeed_41(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardYTranslateSpeed_42(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardZTranslateSpeed_43(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseXTranslateSensitivity_44(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseYTranslateSensitivity_45(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseScrollTranslateSensitivity_46(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseXRotateSensitivity_47(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseYRotateSensitivity_48(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseScrollRotateSensitivity_49(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseYRotateInvert_50(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_DesiredCursorLockMode_51(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_U3CmouseTransformationModeU3Ek__BackingField_52(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_U3Caxis2DTargetsU3Ek__BackingField_53(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardXTranslateInput_54(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardYTranslateInput_55(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_KeyboardZTranslateInput_56(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateLeftInput_57(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateRightInput_58(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulateHeadInput_59(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseDeltaInput_60(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MouseScrollInput_61(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RotateModeOverrideInput_62(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_NegateModeInput_63(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_XConstraintInput_64(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_YConstraintInput_65(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ZConstraintInput_66(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ResetInput_67(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Axis2DInput_68(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RestingHandAxis2DInput_69(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_GripInput_70(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_TriggerInput_71(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_PrimaryButtonInput_72(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_SecondaryButtonInput_73(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_MenuInput_74(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Primary2DAxisClickInput_75(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Secondary2DAxisClickInput_76(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Primary2DAxisTouchInput_77(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_Secondary2DAxisTouchInput_78(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_PrimaryTouchInput_79(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_SecondaryTouchInput_80(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_ManipulatedRestingHandAxis2D_81(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_LeftControllerEuler_82(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RightControllerEuler_83(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_CenterEyeEuler_84(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_HMDState_85(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_LeftControllerState_86(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RightControllerState_87(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_HMDDevice_88(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_LeftControllerDevice_89(),
	XRDeviceSimulator_t0EDF8E0310ED3B76390C014060AB6C29E1A53825::get_offset_of_m_RightControllerDevice_90(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3376[17] = 
{
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Cprimary2DAxisU3Ek__BackingField_43(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CtriggerU3Ek__BackingField_44(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CgripU3Ek__BackingField_45(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Csecondary2DAxisU3Ek__BackingField_46(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CprimaryButtonU3Ek__BackingField_47(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CprimaryTouchU3Ek__BackingField_48(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CsecondaryButtonU3Ek__BackingField_49(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CsecondaryTouchU3Ek__BackingField_50(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CgripButtonU3Ek__BackingField_51(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CtriggerButtonU3Ek__BackingField_52(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CmenuButtonU3Ek__BackingField_53(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Cprimary2DAxisClickU3Ek__BackingField_54(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Cprimary2DAxisTouchU3Ek__BackingField_55(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Csecondary2DAxisClickU3Ek__BackingField_56(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3Csecondary2DAxisTouchU3Ek__BackingField_57(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CbatteryLevelU3Ek__BackingField_58(),
	XRSimulatedController_t7771B4DF96F9B4259DFFFD68E30136C1FEAD7E49::get_offset_of_U3CuserPresenceU3Ek__BackingField_59(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3377[13] = 
{
	ControllerButton_t7C56CD3F9B93A26CC907FBD592F9767BA9F19379::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3378[10] = 
{
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_primary2DAxis_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_trigger_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_grip_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_secondary2DAxis_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_buttons_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_batteryLevel_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_trackingState_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_isTracked_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_devicePosition_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedControllerState_tF8F4FD13AD3CC46E85B73D5F2BCEB18FF0531175::get_offset_of_deviceRotation_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3380[10] = 
{
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_leftEyePosition_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_leftEyeRotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_rightEyePosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_rightEyeRotation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_centerEyePosition_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_centerEyeRotation_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_trackingState_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_isTracked_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_devicePosition_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRSimulatedHMDState_tDAE563E2C0FBDB44CF1D0272A9668F5B0A13D5D7::get_offset_of_deviceRotation_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3381[6] = 
{
	Directions_t706FFDE7EB6AC4B89416A24CEA9BBD645934B1F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3382[5] = 
{
	SweepBehavior_tDAD4BA105BD12352FE6CD1A76FAC0240D2AF970D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3383[4] = 
{
	State_t19A336E7F61A10B1B18A391FD305B0BB89FF7506::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3384[6] = 
{
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864::get_offset_of_directions_0(),
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864::get_offset_of_sweepBehavior_1(),
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864::get_offset_of_pressPoint_2(),
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864_StaticFields::get_offset_of_U3CdefaultPressPointU3Ek__BackingField_3(),
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864::get_offset_of_m_State_4(),
	SectorInteraction_tACC3C76E7B4442EF35450F945BE85947A3D87864::get_offset_of_m_WasValidDirection_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3385[3] = 
{
	Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB::get_offset_of_first_1(),
	Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB::get_offset_of_second_2(),
	Vector3FallbackComposite_t64A6C5351568FD8413F35365FC6950D867C517AB::get_offset_of_third_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3386[3] = 
{
	QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF::get_offset_of_first_1(),
	QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF::get_offset_of_second_2(),
	QuaternionFallbackComposite_tFE2B8F7A070F888763C0A4101B4F26E6DA4AC4CF::get_offset_of_third_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3398[1] = 
{
	U3CPrivateImplementationDetailsU3E_tAE2FF88FB3600B4B96FF4BDC31623A4700964BC1_StaticFields::get_offset_of_EF009A504B02E9FA1780A6FC59A5C67B0A4D7047D162261557CBB081B12EC34D_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3400[4] = 
{
	0,
	ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9_StaticFields::get_offset_of_xrInstance_16(),
	ConformanceAutomationFeature_t478B4CAC5B595A158989531913A8BDC16D7837A9_StaticFields::get_offset_of_xrSession_17(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3401[2] = 
{
	XrVector2f_t7B19E7F74801E61C8C4A8F0949E26FFFE01D4715::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrVector2f_t7B19E7F74801E61C8C4A8F0949E26FFFE01D4715::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3402[3] = 
{
	XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrVector3f_tAEE4DA7A16C267C1ACF04D56A0F067370CB9A6A4::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3403[4] = 
{
	XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrQuaternionf_tC5A255D4B24FA709BFAB9CA4D4FE0BC54E3A432F::get_offset_of_w_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3404[2] = 
{
	XrPosef_tB03D03353E15F17DAE1AD0D9977F69E2A9B3CED9::get_offset_of_orientation_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XrPosef_tB03D03353E15F17DAE1AD0D9977F69E2A9B3CED9::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3406[5] = 
{
	ScriptEvent_tA81311951D5C2E7C1929F4B9D4BA3FF14C3C824F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3408[5] = 
{
	XrViewConfigurationType_t04F6747D133D0D0CCF9BC3B648E0C18FA75532A7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3409[6] = 
{
	XrSpaceLocationFlags_t38EFB6B836CCDA335449EDE431C293C3EA7594C4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3410[6] = 
{
	XrViewStateFlags_t33EB51930377C244468A9D7E75BEB092CC226F72::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3411[59] = 
{
	XrResult_tF77146A73E185AD8F8E819BD2594ABB23963D377::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3412[10] = 
{
	XrSessionState_tC70FDAC8A29DBA216D5F0AD647D517BFC65DAB07::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3413[6] = 
{
	XrReferenceSpaceType_t6B2E244095E7092E0951F8017620159C39FE76E8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3414[3] = 
{
	0,
	MockDriver_t2FED9AE481D4DFF95EB440EAA8BF0AF218DED00A_StaticFields::get_offset_of_onScriptEvent_16(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3416[2] = 
{
	0,
	MockRuntime_tECF1827BDFD6C6AD4099A7015BC07B8B639304F3::get_offset_of_ignoreValidationErrors_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3418[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3420[5] = 
{
	RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32_StaticFields::get_offset_of_kEditorToPlayerRequestDebuggerOutput_15(),
	RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32_StaticFields::get_offset_of_kPlayerToEditorSendDebuggerOutput_16(),
	RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32::get_offset_of_cacheSize_17(),
	RuntimeDebuggerOpenXRFeature_tE0D0BEE876CB547A518EF73B7596C17F6A507B32::get_offset_of_perThreadCacheSize_18(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3423[2] = 
{
	PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D::get_offset_of_texture_4(),
	PausePlane_t0D36089F6018AE368E2CEE94C4AF95A67FDA317D::get_offset_of_textureSize_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3424[11] = 
{
	ButtonOption_tCF6CB86364E608A8E6F00C72916A695CA5442E27::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3425[18] = 
{
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_texture_4(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_textureSize_5(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_lSource_6(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_targetColor_7(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of__play_8(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of__pauPlane_9(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_videoPlayer_10(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_videoFrame_11(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_objs_12(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28_StaticFields::get_offset_of_availableButtons_13(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_deviceCharacteristic_14(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_button_15(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_OnPress_16(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_OnRelease_17(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_U3CIsPressedU3Ek__BackingField_18(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_inputDevices_19(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_inputValue_20(),
	Whiteboard_t2C1B71E46CE62D9FE62F5DB0750FCA9427816C28::get_offset_of_inputFeature_21(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3426[11] = 
{
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__tip_4(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__penSize_5(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__renderer_6(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__colors_7(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__tipHeight_8(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__touch_9(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__whiteboard_10(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__touchPos_11(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__lastTouchPos_12(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__touchedLastFrame_13(),
	WhiteboardMarker_tB17CBC13491A6BBDE7C3725BD87ABFFD031BC864::get_offset_of__lastTouchRot_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
