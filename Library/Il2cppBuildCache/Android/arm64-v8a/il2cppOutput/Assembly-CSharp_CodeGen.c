﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void PausePlane::Start()
extern void PausePlane_Start_m4750B93765515BB3BDBCA7E97B23E2C962D60420 (void);
// 0x00000002 System.Void PausePlane::.ctor()
extern void PausePlane__ctor_m8FD27CFA806B057E69C20089DEBE82017D122E37 (void);
// 0x00000003 System.Boolean Whiteboard::get_IsPressed()
extern void Whiteboard_get_IsPressed_mF1B4935099B51A3498FC0BFA9CC9764482FF3F3E (void);
// 0x00000004 System.Void Whiteboard::set_IsPressed(System.Boolean)
extern void Whiteboard_set_IsPressed_mEBADFBBF53A08D5734844400791A47E89057D9AA (void);
// 0x00000005 System.Void Whiteboard::Awake()
extern void Whiteboard_Awake_m0A77B51FB40937EE76EEB0A80617F9291717B604 (void);
// 0x00000006 System.Void Whiteboard::PlayPause()
extern void Whiteboard_PlayPause_mE1206758948B384ED7AC9E4CB268BFEBCCE95E9B (void);
// 0x00000007 System.Void Whiteboard::OnNewFrame(UnityEngine.Video.VideoPlayer,System.Int64)
extern void Whiteboard_OnNewFrame_m6579C262F534F5E65E2DB0A9FFC064E177A4E4E5 (void);
// 0x00000008 UnityEngine.Color32 Whiteboard::CalculateAverageColorFromTexture(UnityEngine.Texture2D)
extern void Whiteboard_CalculateAverageColorFromTexture_m846C45BC4A286A6A7BFDC01A8892FCE9EFFF0CEF (void);
// 0x00000009 System.Void Whiteboard::Start()
extern void Whiteboard_Start_m7C9478B33F6AC746ADD2578B340D804D7AB8F945 (void);
// 0x0000000A System.Void Whiteboard::Update()
extern void Whiteboard_Update_m0650CEA939508617841C2DE495E7CE3D0BCDD267 (void);
// 0x0000000B System.Void Whiteboard::.ctor()
extern void Whiteboard__ctor_mCC19A00EDC2F7222C164E411940B7712D8570D4E (void);
// 0x0000000C System.Void Whiteboard::.cctor()
extern void Whiteboard__cctor_m26019FFFD8098BB84B98D211C991868E545282FB (void);
// 0x0000000D System.Void WhiteboardMarker::Start()
extern void WhiteboardMarker_Start_m1E6B5C733610DFB17D24937427B156A79C1FA4FC (void);
// 0x0000000E System.Void WhiteboardMarker::Update()
extern void WhiteboardMarker_Update_mC8886F52A0594BB3DA031210102E8D481A1FFC86 (void);
// 0x0000000F System.Void WhiteboardMarker::Draw()
extern void WhiteboardMarker_Draw_m0514C502A9E17A46FC593294E24A811E970E7739 (void);
// 0x00000010 System.Void WhiteboardMarker::.ctor()
extern void WhiteboardMarker__ctor_m74EB8B305273DE59D673C8B6FAE50BB44ADB03E5 (void);
static Il2CppMethodPointer s_methodPointers[16] = 
{
	PausePlane_Start_m4750B93765515BB3BDBCA7E97B23E2C962D60420,
	PausePlane__ctor_m8FD27CFA806B057E69C20089DEBE82017D122E37,
	Whiteboard_get_IsPressed_mF1B4935099B51A3498FC0BFA9CC9764482FF3F3E,
	Whiteboard_set_IsPressed_mEBADFBBF53A08D5734844400791A47E89057D9AA,
	Whiteboard_Awake_m0A77B51FB40937EE76EEB0A80617F9291717B604,
	Whiteboard_PlayPause_mE1206758948B384ED7AC9E4CB268BFEBCCE95E9B,
	Whiteboard_OnNewFrame_m6579C262F534F5E65E2DB0A9FFC064E177A4E4E5,
	Whiteboard_CalculateAverageColorFromTexture_m846C45BC4A286A6A7BFDC01A8892FCE9EFFF0CEF,
	Whiteboard_Start_m7C9478B33F6AC746ADD2578B340D804D7AB8F945,
	Whiteboard_Update_m0650CEA939508617841C2DE495E7CE3D0BCDD267,
	Whiteboard__ctor_mCC19A00EDC2F7222C164E411940B7712D8570D4E,
	Whiteboard__cctor_m26019FFFD8098BB84B98D211C991868E545282FB,
	WhiteboardMarker_Start_m1E6B5C733610DFB17D24937427B156A79C1FA4FC,
	WhiteboardMarker_Update_mC8886F52A0594BB3DA031210102E8D481A1FFC86,
	WhiteboardMarker_Draw_m0514C502A9E17A46FC593294E24A811E970E7739,
	WhiteboardMarker__ctor_m74EB8B305273DE59D673C8B6FAE50BB44ADB03E5,
};
static const int32_t s_InvokerIndices[16] = 
{
	2410,
	2410,
	2390,
	1949,
	2410,
	2410,
	1127,
	1253,
	2410,
	2410,
	2410,
	3957,
	2410,
	2410,
	2410,
	2410,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	16,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
