using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.XR;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.Video;
using Unity.XR.Oculus;


public class Whiteboard : MonoBehaviour
{
    public Texture2D texture;
    public Vector2 textureSize = new Vector2(2048, 2048);
    public Light lSource;
    private Color targetColor;
    [SerializeField] private Transform _play;
    [SerializeField] private Transform _pauPlane;
    private VideoPlayer videoPlayer;
    Texture2D videoFrame;
    private PausePlane _rtTex;
    Texture2D convertedTex2D;
    Material newMat;
    public GameObject objs;

    static readonly Dictionary<string, InputFeatureUsage<bool>> availableButtons = new Dictionary<string, InputFeatureUsage<bool>>
    {
        {"triggerButton", CommonUsages.triggerButton },
        {"thumbrest", OculusUsages.thumbrest },
        {"primary2DAxisClick", CommonUsages.primary2DAxisClick },
        {"primary2DAxisTouch", CommonUsages.primary2DAxisTouch },
        {"menuButton", CommonUsages.menuButton },
        {"gripButton", CommonUsages.gripButton },
        {"secondaryButton", CommonUsages.secondaryButton },
        {"secondaryTouch", CommonUsages.secondaryTouch },
        {"primaryButton", CommonUsages.primaryButton },
        {"primaryTouch", CommonUsages.primaryTouch },
    };

    public enum ButtonOption
    {
        triggerButton,
        thumbrest,
        primary2DAxisClick,
        primary2DAxisTouch,
        menuButton,
        gripButton,
        secondaryButton,
        secondaryTouch,
        primaryButton,
        primaryTouch
    };

    [Tooltip("Input device role (left or right controller)")]

    public InputDeviceCharacteristics deviceCharacteristic;

    [Tooltip("Select the button")]
    public ButtonOption button;

    [Tooltip("Event when the button starts being pressed")]
    public UnityEvent OnPress;

    [Tooltip("Event when the button is released")]
    public UnityEvent OnRelease;


    public bool IsPressed { get; private set; }

    List<InputDevice> inputDevices;
    bool inputValue;

    InputFeatureUsage<bool> inputFeature;

    void Awake()
    {
        newMat = Resources.Load("Assets/WhiteBoard/Materials/Transparent", typeof(Material)) as Material;
        string featureLabel = Enum.GetName(typeof(ButtonOption), button);
        videoPlayer = _play.GetComponent<VideoPlayer>();
        availableButtons.TryGetValue(featureLabel, out inputFeature);
        inputDevices = new List<InputDevice>();

    }

    public void PlayPause()
    {
        if(videoPlayer.isPlaying)
        {
            videoPlayer.sendFrameReadyEvents = true;
            videoPlayer.frameReady += OnNewFrame;
            videoPlayer.Pause();
            _play.GetComponent<VideoPlayer>().enabled = false;
            var r = GetComponent<Renderer>();
            //_pauPlane.GetComponent<Renderer>().material.mainTexture = Texture2D.blackTexture;
            //_pauPlane.GetComponent<Renderer>().material.mainTexture = videoFrame;
            _rtTex = GameObject.FindGameObjectWithTag("PausePlane").GetComponent<PausePlane>();
            //convertedTex2D = new Texture2D(videoPlayer.texture.width, videoPlayer.texture.height, TextureFormat.ARGB32, false);
            //convertedTex2D.ReadPixels(new Rect(0, 0, videoPlayer.texture.width, videoPlayer.texture.height), 0, 0);
            //convertedTex2D.Apply();
            //objs.GetComponent<Renderer>().sharedMaterial.mainTexture = Texture2D.whiteTexture;
            _rtTex.texture = Resize(videoFrame,(int)_rtTex.textureSize.x,(int)_rtTex.textureSize.y);
            _rtTex.edittex();
            
            r.enabled = false;
        } else
        {
            _play.GetComponent<VideoPlayer>().enabled = true;
            var r = GetComponent<Renderer>();
            r.enabled = true;
            videoPlayer.Play();
            targetColor = CalculateAverageColorFromTexture(videoFrame);
            lSource.color = targetColor;
            _rtTex.Save();
        }
    }

    private void OnNewFrame(VideoPlayer source, long frameIdx)
    {
        RenderTexture rend = source.texture as RenderTexture;

        if (videoFrame.width != rend.width || videoFrame.height != rend.height)
        {
            videoFrame.Resize(rend.width, rend.height);
        }
        RenderTexture.active = rend;
        videoFrame.ReadPixels(new Rect(0, 0, rend.width, rend.height), 0, 0);
        videoFrame.Apply();
        RenderTexture.active = null;
        
    }

    Texture2D Resize(Texture2D texs, int targetX, int targetY)
    {
        RenderTexture rt = new RenderTexture(targetX, targetY, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texs, rt);
        Texture2D result = new Texture2D(targetX, targetY);
        result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
        result.Apply();
        return result;
    }
    Color32 CalculateAverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;
        float r = 0;
        float g = 0;
        float b = 0;

        for (int i = 0;i<total;i++)
        {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
        }

        return new Color32((byte)(r / total), (byte)(g / total), (byte)(b / total), 0);

    }


    void Start()
    {
        var r = GetComponent<Renderer>();
        texture = new Texture2D((int)textureSize.x, (int)textureSize.y);
        videoFrame = new Texture2D(2, 2);
        r.material.mainTexture = Texture2D.whiteTexture;
    }

    void Update()
    {
        InputDevices.GetDevicesWithCharacteristics(deviceCharacteristic, inputDevices);

        for (int i = 0; i < inputDevices.Count; i++)
        {
            if (inputDevices[i].TryGetFeatureValue(inputFeature,
                out inputValue) && inputValue)
            {
                // if start pressing, trigger event
                if (!IsPressed)
                {
                    IsPressed = true;
                    OnPress.Invoke();
                    Debug.Log($"Pressed {deviceCharacteristic} Controller {button}");
                    PlayPause();
                }
            }


            else if (IsPressed)
            {
                IsPressed = false;
                OnRelease.Invoke();
                Debug.Log($"Released {deviceCharacteristic} Controller {button}");
            }

        }

    }


}
